# Recyklomat VPN initialization

## Requirements

*   openvpn installed

        sudo apt-get install openvpn

*   master certificate authority file ca.crt
*   your client certificate, e.g. client.crt
*   your client key, e.g. client.key
*   client configuration, e.g. client.conf
*   client certificate authority file, e.g. client.csr

## Instruction

Just copy certification files ( ca.crt, client.crt, client.key, client.conf and client.csr ) to `/etc/openvpn` folder
and restart platform. 
    
    sudo reboot

