#Local backend initialization

##Basic requirements

The manual below assumes that the software sources/binary files of local backend have already been copied to the target platform.

Recyklomat local backend is using Python 3.6. 
Whole installation and configuration of backend is made by initialization script:

    ./backend_control.sh --install

If you encounter some problems with init script or just want to install and configure python 3.6 yourself you can follow instructions below.

Let's start wih updating packages lists:

    sudo apt-get update

You can download python3.6 by:

    sudo apt-get install python3.6

If there is no python in packages list you must download and compile it yourself(this can take some time):

    sudo apt-get install python3-dev libffi-dev libssl-dev wget gcc make zlibc -y
    wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
    tar xJf Python-3.6.3.tar.xz
    cd Python-3.6.3
    ./configure
    make
    sudo make install
    sudo pip3 install --upgrade pip
    cd ..

Some python packages are also needed:

    sudo python3.6 -m pip install setuptools
    sudo python3.6 -m pip install tornado>=6.0.2
    sudo python3.6 -m pip install python-dateutil>=2.8.0

##Configuration

```config.py``` file is used for backend configuration.
Comments inside will help you to configure backend correctly.

##Usage of the local backend

Every operation with backend should be executed with ```backend_control.sh```.
Script needs to have execute permissions

    chmod +x backend_control.sh

To see manual for ```backend_control.sh``` use help command

    ./backend_control.sh --help

After that manual will be displayed
```
Witaj w skrypcie do kontroli recyklomatowego backendu.
    Użycie: ./backend_control.sh <opcja>
Uwaga! skrypt powinien być uruchamiany z poziomu docelowego uzytkownika!
Dostepne opcje:
    --help             wyświetl ten komunikat
    --start            uruchom backend
    --kill             zabij aktualnie uruchomiony backend
    --status           sprawdz czy backend jest uruchomiony
    --install          zainstaluj wszystkie potrzebne zaleznosci (tylko ubuntu/debian)
    --test             odpal skrócone testy integracyjne aby sprawdzić czy środowisko jest dobrze zestawione
    --debug            uruchom backend bez podlaczania sie do sterownika z uruchomiona konsola debugowa dostepna pod adresem http://<url>:<port>/de
```

To run backend just use 

    ./backend_control.sh --start

If automation driver is running, backnd will automaticly connect to it. If automation driver isn't running backend will
try to connect every few seconds.

To stop backend use
    
    ./backend_control.sh --kill

