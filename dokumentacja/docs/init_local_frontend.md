#Local frontend initialization

##Basic requirements

The manual below assumes that the software sources/binary files of local backend have already been copied to the target platform.

Local frontend files need to be place inside web directory in backend sources.

Chromium-browser is used for displaying local frontend. You can get it by:

    sudo apt-get install chromium-browser

##Configuration

```config.json``` file is used for frontend configuration and it always need to be places near index.html file.

**Every parameters with _path suffix needs to be set. Otherwise frontend won't work.**

Every text and its size can be modified separately.

Important parameters:

```
"accepted_garbage_list": ["ALU", "GLA", "PET", "DEO", "SYR", "BAT"]
```
Defining which garbage will be displayed as possible to recycle by Recyklomat.

```
"request_interval": 500
```
Defining how ofted frontend asking backend about status.

```
"transform": "matrix(0, -1, 1, 0, 120, -136)"
```
Defining shift and rotation of the frontend on the screen. Change second and third parameters from -1, 1 to 1, -1 to rotate
the screen 180 degrees.

##Usage of the local frontend

If you have frontend configured and copied to backend web directory you can run local backend and visit ```http://127.0.0.1:8888/``` site
in your browser (we used chromium-browser). Frontend should start and be ready to use.

Below you can find script that was used to launch browser with frontend:

    DISPLAY=:0 chromium-browser \
            --disable-session-crashed-bubble \
            --disable-notifications \
            --disable-translate \
            --disable-infobars \
            --disable-suggestions-service \
            --disable-touch-drag-drop \
            --disable-save-password-bubble \
            --overscroll-history-navigation=0 \
            --kiosk \
            --incognito 127.0.0.1:8888 &

