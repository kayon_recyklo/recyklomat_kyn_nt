# Initialization of QR code reader

## Basic Requirements

The manual below assumes that the software sources/binary files of qr scanner support have already been copied to the target platform.

*   Zebra DS457 QR code reader connected via USB to RPi
    ![zebra_code_reader](img/Zebra_DS457.jpg)
*   config barcode printed or saved on mobile device with display
    ![code_reader_config_barcode](img/qr_core_reader_config.jpg)
    
## Instruction

1.  Scan config barcode by clicking button in the upper part of Zebra reader.
2.  Scanner should make characteristic sound of successful scan.
3.  Restart Raspberry Pi connected to Zebra reader.
4.  Now under /dev/ directory you should see ttyACM0 device.
5.  (optional) If you can't see ttyACM0 go back to 1. - maybe config barcode wasn't scanned properly.
6.  Make sure scanner support sources was compiled (by make) and there was no errors.
7.  Config scanning parameters by using `./config /dev/ttyACMX`, X-number of ACM device. Example:
        
        ./config /dev/ttyACM0
        
8.  Once again scanner should make few characteristic beeps. If so config was successful, if not check if device number is
    correct.
9.  Now, if backend is configured properly, reader should start everytime Recyklomat finishes transaction and should
    successfully scan QR codes from applications.
