#Automation driver initialization

##Basic requirements

The manual below assumes that the software sources/binary files of automation driver have already been copied to the target platform.

You can use install_script.sh to get needed packages and tools. You can also get it yourself - information below.

Packages to download:

    sudo apt-get install libreadline-dev -y

Useful tools to download:

    sudo apt-get install tmux -y
    sudo apt-get install screen -y

##Configuration

```iomap.ini``` is used to configure automation driver.
Number of devices are connected to real devices with driver program parameters during start-up. More information below.

##Usage of the driver

```qps``` program is main driver for Recyklomat automaton, other programs are additional tools.

```qps``` needs some optons to be defined:

- ```devices=n``` - informs how many iodriver/redrivers are connected to QPS Core
- ```uarts=m``` - informs how many uarts are used for devices connection
- ```uartK-dev``` - informs that K uart transfer data over dev file (example: /dev/canbus0)
- ```deviceX=hexaddress@uartY``` - informs about device X (where 0 < X < n) address (32 bits) and that device X comunicates with uart with Y
  index (where 0 < Y < m)

  Example of qps starting command:

    ./qps uarts=1 devices=5 uart0=/dev/can0 device0=030000aa@uart0 device1=0b0000aa@uart0 device2=0c0000aa@uart0 device3=020000aa@uart0 device4=057ca4b0@uart0

