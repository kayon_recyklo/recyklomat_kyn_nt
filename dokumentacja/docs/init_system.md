# Recyklomat OS initialization

## Requirements

To start initializing OS you need properly flashed QPS Core Platform - check [QPS Core Flashing](../init_qps_core) 

The manual below assumes that the initialization config files and scripts have already been copied to the target platform.

## Instruction

1.  Go to directory with configuration files and scripts (eg. useful_scripts_recyklomat)
2.  Copy `run_qps` script to `/usr/bin/` directory.
    
        cp run_qps /usr/bin/

3.  Copy `congif.txt` to `/boot/` directory.

        cp config.txt /boot/

4.  Copy `50-qps.conf` file to `/etc/lightdm/lightdm.conf.d/`

        cp 50-qps.conf /etc/lightdm/lightdm.conf.d/

5.  Check if you have `/home/pi/config/i3/config` file in your system. If not - reboot system.

        sudo reboot

6.  Now `/home/pi/.config/i3/config` should be avaliable. Add executing of run_qps script to i3 config file

        echo "exec /usr/bin/run_qps" >> /home/pi/.config/i3/config

7.  Copy `start_qps` script to `/home/pi/` directory.

        cp start_qps /home/pi/

8.  Copy `start_chromium` script to `/home/pi` directory.

        cp start_chromium /home/pi/

9.  Copy `start_backend` script to backend directory eg. `/home/pi/backend_recyklomat/`

        cp start_backend /home/pi/backend_recyklomat/

10. In configuration scripts you can find other usefull tools for temperature controle, kill and restart
    chromium-browser or clean screen sessions. Check and use them if you need to.
