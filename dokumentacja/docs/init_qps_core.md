# QPS Core Flashing

## Requirements

*   Raspberry Pi Compute Modle 3/3+ Development Kit
    ![RPi CM3/3+ Dev Kit](img/rpi_dev_kit.jpg)
*   .img file to write to RPi
*   PC with linux with usb interface (can be another RPi)
*   git

        sudo apt-get install git

*   libusb
        
        sudo apt-get install libusb-1.0.0.-dev

*   usb - microusb cable 
*   microusb power supply (tested at 2A, recommended 2.5A)
    
## Instruction

1.  Place J3 jumper at 3.3V option.
2.  Place J4 jumper at EN option.
3.  Insert RPi CM3/3+ module to dev-kit slot.
4.  Plug power supply to POWER IN connector.
5.  Don't plug usb-micro usb signal cable yet!
6.  Optionall you can check block devices avaliable in system, later you will see what devices showed up.
   
        lsblk

7.  Clone usbboot program repository and build it.
        
        git clone --depth=1 https:/github.com/raspberrypi/usbboot
        cd usbboot
        make

8.  Run usbboot with sudo.

        sudo ./rpiboot

9.  Connect PC with USB Slave connector in RPi Dev Kit by usb-microusb cable.
10. (Linux Virtual Machine) Pass control over RPi Device (Broadcom BCM2710 Boot) to virtual machine by selecting Devices
    -> USB -> Broadcom BCM2710 Boot. Repeat this step if needed in next steps.
11. Rpiboot program will automaticly find device and will provice eMMC flash memory access.
12. Check block devices again to find newly added RPi flash device.

        lsblk

13. (Problems) There are RPi CM3+ without flash memory mounted.
14. If new block device showed up you can write your image to it by dd program. Examples below:

        sudo dd if=path_to_raw_os_image.img of=/dev/sdX bs=4MiB status=progress

15. After dd operation perhaps you want to check system block devices again to verify correct completion.

        lsblk

16. Disconnect RPi from power supply and set J4 jumper to DIS position.
17. DONE! :)
