#!bin/bash
# /etc/init.d/recyc_init.sh
### BEGIN INIT INFO
# Provides:          recyc_init.sh
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start recyc deamon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO
/home/pi/start_qps
sleep 15
/home/pi/recyklomat_backend/start_backend
