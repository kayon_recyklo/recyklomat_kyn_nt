#!/bin/bash
repeat=y
can_speed=0
can_dev=/dev/
dev_address=000000aa
echo "_____GENERATION OF NOATALK SCRIPTS_____"
while [[ "$repeat" = y ]]
do
    	echo "Enter CAN transmition speed"
    	read -e -i "$can_speed" can_speed
    	echo "Enter device you want to talk to (usually /dev/can0 or /dev/canbus0)"
	read -e -i "$can_dev" can_dev
	echo "Enter device address"
	read -e -i "$dev_address" dev_address
	echo "../bin/noatalk $can_speed $can_dev $dev_address" > "./noatalk_$dev_address"
	chmod +x "./noatalk_$dev_address"
	echo "Generating $dev_address at $can_dev with $can_speed speed"
	repeat=n
	echo "Would you like to create noatalk script for another device? [y/n]"
	read -e -i "$repeat" repeat
done
