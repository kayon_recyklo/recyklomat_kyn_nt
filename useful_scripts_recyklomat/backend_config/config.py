
class Config(object):
    # sciezka do socketa wystawianego przez driver. W momencie uruchomienia backendu driver musi juz dzialac, a socket
    # musi istniec
    DRIVER_SOCKET_PATH = '/tmp/be-socket'

    # port na ktorym ma byc dostepne GUI. Nie chcesz tego zmieniać
    GUI_PORT = 8888

    # adres na ktorym GUI ma oczekiwac na polaczenia. Do testow mozna wykorzystac adres '0.0.0.0' (nasluchuj na
    # dowolnym adresie), docelowo powinno to byc '127.0.0.1'
    GUI_HOST = "127.0.0.1"

    # pelna sciezka do pliku z logami recyklomatu wraz z nazwa pliku, Folder nadrzedny musi
    # istniec i aplikacja musi miec prawa zapisu do tego folderu. Prawdopodobnie chcesz to ustawic na
    # /var/recuklomat_backend/log/recyklomat.log
    LOG_FILE_PATH = "/home/pi/recyklomat_backend/log/recyklomat_backend"

    # maksymalny rozmiar pojedynczego loga w Bajtach. Domyślnie 100 MB
    LOG_SIZE_BYTES = 1024*1024*100

    # maksymalna liczba logow. W momencie przekroczenia tej liczby najstarszy log zostanie usuniety. Domyślnie 5
    LOG_COUNT = 5

    # pelen adres glownego serwera, do ktorego nalezy przesylac raporty z recyklomatu
    URI = "https://ets-api.aws.ambsoft.pl/v1/intake"

    # identyfikator recyklomatu
    RVMID = '5d25b1df0083ec00013ef547'
    CODE = '92640'

    # sciezka do folderu, w ktorym beda przechowywane raporty, ktorych nie udalo sie wyslac (np. ze wzgledu na chwilowa
    # niedostepnosc sieci). Prawdoodobnie chcesz to ustawić na /var/recyklomat_backend/reports_to_send
    CACHE_FOLDER = '/recyklomat_reports_to_send'

    # Czas, w sekundach, przez jaki na ekranie jest wyswietlane podsumowanie zawierajace mi. sume zdobytych punktow.
    # Domyślnie 10 s
    SUMMARY_TIMEOUT = 10

    # minimalny czas, przez który niektóre wiadomości, np. info o wysyłaniu raportu, będą wyświtlane. Domylnie 3 s.
    MINIMAL_TIME_OF_DISPLAYING_MESSAGE_S = 3


    # Czas, po ktorym backend zalozy ze doszlo do zwiechy. Domyślnie 300 s.
    ERROR_TIMEOUT = 180

    # czas po ktorym zakladamy ze uzykownik zrezygnowal z transakcji. Domyślnie 180 s.
    USER_INTERACTION_TIMEOUT = 180

    # plik, do ktorego zapisywany jest aktualny stan sterownika na wypadek zwiechy. Prawdopodobnie chcesz ustawić to na
    # /var/recyklomat_backend/state.json
    STATE_CACHING_FILE = '/home/pi/recyklomat_backend/recyklomat_backend_state.json'

    # pelna sciezka do pliku wykonywalnego odpowiedzialnego za kontrole czytnika kodow QR
    QR_CODE_DRIVER_CMD = '/home/pi/recyklo_skaner/scan /dev/ttyACM0 500'

    # polecenie drukujace paragon. Dostepne formaty {ALU}, {GLA}, {DEO}, {SYR}, {PET}, {BAT}, {POINTS}, {TRANSACTION_TAG}
    PRINTER_COMMAND = "/home/pi/drukarka_recyklo/./print_receipt.py {POINTS} {ALU} {GLA} {DEO} {SYR} {PET} {BAT} {TRANSACTION_TAG}"

    # wartosc, ktora bedzie wysylana w polu customerId, jesli uzytkownik nie zeskanuje swojego kodu qr
    DEFAULT_USER_ID = "-1"

