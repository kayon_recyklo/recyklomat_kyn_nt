# Sterownik recyklomatu firmy Kayon

Niniejsze repozytorium zawiera implementację sterownika
recyklomatu firmy Kayon. Aktualnie dostępne są następujące
programy:

- `noatalk` - program służący do komunikacji z urządzeniami
NoaTech IO Driver z poziomu linii komend

- `io-driver` - symulator sterownika NoaTech IO Driver
wraz z interpreterem skryptow testowych

- `virtualcan` - wirtualny interfejs CAN

- `qps` - główny sterownik recyklomatu

Szczegółową dokumentację architektury sterowika można znaleźć
w katalogu `doc`.

## Budowanie

Do zbudowania projektu potrzebne jest środowisko uniksowe
obsługujące POSIX threads oraz kompilator GCC. Jeśli te wymagania
są spełnione, wystarczy z poziomu powłoki systemu wywołać komendę

    $ make

Po poprawnym zbudowaniu, programy wykonywalne będą dostępne
w katalogu `bin`.

### Konfiguracja środowiska

Projekt można uruchamiać zarówno na lokalnych systemach
(komputery PC z systemami Windows, Linux albo OS X), jak
i na systemie docelowym (ARTIK 710s), jednak w tym ostatnim
przypadku niezbędny jest kompilator produkujacy kod wynikowy
dla procesorów ARM.

W przypadku kompilacji dla systemu Windows 10, warto skonfigurować
[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
i zainstalować jedną dystrybucji dostępną w Windows Store.

Dla innych wersji Windowsa można próbować korzystać ze środowiska
[Cygwin](https://www.cygwin.com/).
Można też wypróbować środowisko [MSYS](http://www.mingw.org/wiki/msys)
lub [MSYS2](https://www.msys2.org/), z zastrzeżeniem, że korzystanie
z wątków POSIX może być na tych systemach problematyczne.

## Konfiguracja systemu

### Mapowanie wejść/wyjść urządzenia IODriver na rzeczywiste urządzenia

Istnieje wiele możliwych sposobów połączenia wejść i wyjść cyfrowych
do rzeczywistych sensorów i aktuatorów. Oczywiście, jeżeli kable zostaną
źle podłączone, urządzenie nie będzie działało prawidłowo.

Plik `include/iomap.h` zawiera specyfikację struktury połączeń wejść
i wyjść cyfrowych z poszczególnymi połączeniami. Na obecnym etapie
zmiana tej struktury wymaga rekompilacji programu.

## Korzystanie z symulatora IO Drivera oraz wirtualnego CAN

Oprócz używania sterownika QPS w środowisku produkcyjnym, możliwe jest
(np. na potrzeby testowania) uruchomienie wirtualnego systemu, bez
konieczności podłączania rzeczywistego sprzętu.

*UWAGA! Wszystkie programy i skrypty uruchamiamy z poziomu katalogu
głównego (zawierającego ten plik `README.md` oraz katalogi `bin`, `test`,
`src`, `include`!*

Aby podłączyć wirtualny sterownik, należy (przy pomocy polecenia `mkfifo`)
stworzyć tzn. "nazwane potoki" ("named pipes"). Można do tego celu użyć
skryptu `tests/make-noa-fifos.sh`, który (uruchomiony z odpowiednimi
uprawnieniami) utworzy żądaną ilość par potoków w katalogu `/dev/`, np.

    $ sudo tests/make-noa-fifos.sh 2

spowoduje utworzenie potoków `/dev/noa-pc-can`, `/dev/noa-can-pc`,
`/dev/noa-io0-can`, `/dev/noa-can-io0`, `/dev/noa-io1-can` oraz
`/dev/noa-can-io1`.

Upewwniwszy się, że użytkownik posiada odpowiednie uprawnienia do tych węzłów,
można uruchomić wirtualny interfejs CAN następująco:

    $ bin/virtualcan /dev/noa-io0-can /dev/noa-can-io0 /dev/noa-pc-can /dev/noa-can-pc

Program `virtualcan` przyjmuje jako argumenty pary argumentów:

    virtualcan wejście1 wyjście1 wejście2 wyjście2 ...

Można też użyć do tego celu pomocniczego skryptu

Gdy program zostanie uruchomiony, można  (najlepiej w innym terminalu) uruchomić
symulator IO drivera:

    $ bin/io-driver /dev/noa-can-io0 /dev/noa-io0-can aabbccdd

W trzecim terminalu z kolei można uruchomić narzędzie `noatalk` albo sterownika `qps`.
Uruchamianie sterownika qps opisane jest w późniejszej sekcji. Program `noatalk`
można uruchomić następująco:

    $  bin/noatalk /dev/noa-can-pc /dev/noa-pc-can aabbccdd

Wówczas możliwa powinna być komunikacja programu `noatalk` ze sterownikiem `io-driver`:

    aabbccdd@pipe/noa-can-pc> version
    NoaTech IO Driver aabbccdd version 0.10
    aabbccdd@pipe/noa-can-pc> status
    inA = 1
    inB = 1
    inC = 1
    inD = 1
    out0 = 0 (0)
    out1 = 0 (0)
    out2 = 0 (0)
    out3 = 0 (0)
    out4 = 0 (0)
    out5 = 0 (0)
    out6 = 0 (0)
    out7 = 0 (0)
    system voltage = 0
    aabbccdd@pipe/noa-can-pc>

## Korzystanie ze sterownika QPS:

### Uruchamianie skryptu testowego:

Dowolny terminal:

    $ sudo tests/make-noa-fifos.sh

Terminal 1:

    $ tests/run-virtual-can.sh

Terminal 2:

    $ tests/run-io-driver.sh < tests/test.io

Terminal 3 (w ciągu maksymalnie 10 sekund od uruchomienia procesu `io-driver`):

    $ tests/run-qps-on-pipe.sh recognizer=tests/items.list

Po uruchomieniu testu na terminalu 2 wyświetlać się będą po kolei symulowane
akcje użytkownika oraz urządzeń, natomiast terminal 3 wypisywać będzie informacje
diagnostyczne sterownika `qps`.

Proces w terminalu 2 powinien zakończyć się poleceniem `exit` - oznacza ono, że komunikacja
przebiegła poprawnie i że nie doszło do żadnych zakleszczeń). Test powinien się wykonać
w czasie ok. 30 sekund.

### Uruchamianie testów przy pomocy multipleksera terminala (`tmux`)

Jeżeli w danym systemie zainstalowany jest program `tmux`, testy można uruchomić,
wpisując:

    $ make check

albo

    $ tests/run-test.sh

Jeżeli potoki `/dev/noa-*` nie istnieją, może być konieczne wpisanie

    $ sudo make check

(skrypt próbuje stworzyć niezbędne kolejki automatycznie, jednak może nie mieć
odpowidnich uprawnień do zapisu w katalogu `/dev`)

### Opcje uruchamiania sterownika `qps`

Uruchomienie sterownika `qps` wymaga podania następujacych opcji (por. skrypt
`tests/run-qps-on-pipe.sh`):

- `devices=n` - informuje sterownik, że jest do niego podłączonych `n` > 0 urządzeń

- `uarts=m` - informuje sterownik, że urządzenia są w sumie połączone za pośrednictwem
`m` > 0 UART-ów, względnie par kolejek FIFO (gdzie `m`<=`n`)

- `inK=dev_in outK=dev_out` - informuje sterownik, że `K`-ty UART (gdzie 0 <= `K` < `m`)
czyta z pliku `dev_in` i pisze do pliku `dev_out`. Jeżeli `dev_in` jest taki sam, jak
`dev_out`, to zamiast używać opcji `inK` i `outK` można napisać:

- `uartK=dev` - informuje sterownik, że `K`-ty UART wymienia dane za pośrednictwem
pliku `dev` (np. `/dev/ttyS1`). Alternatywnie do tej opcji można użyć opcji `inK`
oraz `outK` (zob. powyżej)

- `deviceX=hexaddr@uartY` - informuje sterownik, że urządzenie o indeksie `X` (gdzie
0 <= `X` < `n`) ma adres `hexaddr` (32-bitowa liczba heksadecymalna) i komunikuje się
za pośrednictwem UARTa o indeksie `Y` (gdzie 0 <= `Y` < `m`)

- `recognizer=device` - informuje sterownik, że informacje o obiekcie widzianym
przez kamerę będą czytane z urządzenia `device`. Informacje te są przekazywane
w postaci linii tekstu (zob. plik `tests/items.list`)
