# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list
# nadajemy nazwy dla poszczegolnych wejsc i wyjsc

define aluopen 4
define aluclose 3
define petopen 6
define petclose 5
define glaopen 2
define glaclose 1

define glasensor a

define doorlock 0
define led 7
# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000
#ledy
expect led 1
expect led 0

expect doorlock 1

expect aluopen 1
expect aluclose 1

expect led 1
exit
