# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list
# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define petdetector a
define aludetector b
define othdetector c
define conv 5

# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000
expect conv 1

wait 4500

set aludetector 1
wait 300
set aludetector 0

wait 400

set petdetector 1
wait 300
set petdetector 0

wait 6000
exit
