# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list
# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define rools 7
define door_sensor a
define electrolock 6
define dropbox 5
#na poczatek wyzerujmy wszystkie wejscia
set a 1
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000
# rolki
expect rools 1
expect rools 0
#elektrozaczep
expect electrolock 1
expect dropbox 1

expect dropbox 0
expect electrolock 0
expect 0 1
exit
