# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list
# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define glassdetector a
define petdetector b
define aludetector c
define deodetector d
define glassarm 0
define glassarmclose 1
define petarm 2
define petarmclose 3
define aluarm 4
define aluarmclose 5
define petcrasher 6
define petcrasherclose 7

# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000

#ALU
wait 3500
set petdetector 1
wait 300
set petdetector 0
wait 500
set aludetector 1
wait 300
set aludetector 0
wait 100
expect aluarm 1
expect aluarm 0
expect aluarmclose 1
expect aluarmclose 0

expect aluarm 1
