# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list

# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define doordetector a
define doorhold 0
define dropboxhold 1
define dropbox 2
define dropboxconveynor 3
define dropboxleds 4
define dropboxarm 5
define wait 6
# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000

#PET
set doordetector 0
wait 5000
set doordetector 1
#expect doorhold 1
#expect dropbox 1
#expect dropbox 0
#expect doorhold 0

#ALU
#wait 2000
#set doordetector 0
#wait 2500
#set doordetector 1
#expect doorhold 1
#expect dropbox 1
#expect dropbox 0
#expect doorhold 0

#GLA
#wait 1000
#set doordetector 0
#wait 2500
#set doordetector 1
#expect doorhold 1
#expect dropbox 1
#expect dropbox 0
#expect doorhold 0

#DEO
#wait 2500
##set doordetector 0
#wait 500
#set doordetector 1
#expect doorhold 1
#expect dropbox 1
#expect dropbox 0
#expect doorhold 0

#BAT
#wait 1000
#set doordetector 0
#wait 2500
#set doordetector 1
#expect doorhold 1
#expect dropboxarm 0
#expect dropboxconveynor 1
#expect dropboxconveynor 0
#expect doorhold 0

#SYR
#wait 1000
#set doordetector 0
#wait 2500
#set doordetector 1
#expect doorhold 1
#expect dropboxarm 1
#expect dropboxconveynor 1
#expect dropboxconveynor 0
#expect doorhold 0

#expect doorhold 1
expect 7 1
exit
