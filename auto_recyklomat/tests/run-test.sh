#!/bin/bash

test_file="tests/opto/no_reaction1/pet_pet"

file0="${test_file}0.io"
file1="${test_file}1.io"
file2="${test_file}2.io"

rm /tmp/errors
rm /tmp/be-socket
rm /tmp/talk-socket
rm /dev/noa-pc-can
rm /dev/noa-can-pc
for((i=0; i<3;i++)); do
    rm /dev/noa-can-io$i;
    rm /dev/noa-io$i-can;
done;
   
if [ ! -e /dev/noa-can-pc ]; then
    mkfifo -m 666 /dev/noa-can-pc
fi

if [ ! -e /dev/noa-pc-can ]; then
    mkfifo -m 666 /dev/noa-pc-can
fi

if [ ! -e /dev/noa-can-io0 ]; then
    mkfifo -m 666 /dev/noa-can-io0
fi

if [ ! -e /dev/noa-io0-can ]; then
    mkfifo -m 666 /dev/noa-io0-can
fi

if [ ! -e /dev/noa-can-io1 ]; then
    mkfifo -m 666 /dev/noa-can-io1
fi

if [ ! -e /dev/noa-io1-can ]; then
    mkfifo -m 666 /dev/noa-io1-can
fi

if [ ! -e /dev/noa-can-io2 ]; then
    mkfifo -m 666 /dev/noa-can-io2
fi

if [ ! -e /dev/noa-io2-can ]; then
    mkfifo -m 666 /dev/noa-io2-can
fi

set -x
tmux new "tests/run-virtual-can.sh 3 ; echo PRESS RETURN ; read " ';' \
     split -v -p 80 "tests/run-io-driver.sh 0 ddccbba0 < $file0 ; echo PRESS RETURN ; read " ';' \
     split -h -p 75 "tests/run-io-driver.sh 1 ddccbba1 < $file1 ; echo PRESS RETURN ; read " ';' \
     split -h -p 66 "tests/run-io-driver.sh 2 ddccbba2 < $file2 ; echo PRESS RETURN ; read " ';' \
     split -h -p 50 "sleep 2;tests/run-qps-on-pipe.sh recognizer=; echo PRESS RETURN ; read " \



#set -x
#tmux new "tests/run-virtual-can.sh 3 ; echo PRESS RETURN ; read " ';' \
#     split -v -p 80 "tests/run-io-driver.sh 0 ddccbba0 < tests/test.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 75 "tests/run-io-driver.sh 1 ddccbba1 < tests/test.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 66 "tests/run-io-driver.sh 2 ddccbba2 < tests/test.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 50 "sleep 2;tests/run-qps-on-pipe.sh recognizer=; echo PRESS RETURN ; read " \


#tmux new "tests/run-virtual-can.sh 3 ; echo PRESS RETURN ; read " ';' \
#     split -v -p 80 "tests/run-io-driver.sh 0 ddccbba0 < tests/test0alu.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 75 "tests/run-io-driver.sh 1 ddccbba1 < tests/test1alu.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 66 "tests/run-io-driver.sh 2 ddccbba2 < tests/test2alu.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 50 "sleep 2;tests/run-qps-on-pipe.sh recognizer=; echo PRESS RETURN ; read " \


#     split -v -p 80 "tests/run-io-driver.sh 0 ddccbba0 < tests/test0.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 75 "tests/run-io-driver.sh 1 ddccbba1 < tests/test1.io ; echo PRESS RETURN ; read " ';' \
#     split -h -p 66 "tests/run-io-driver.sh 2 ddccbba2 < tests/test2.io ; echo PRESS RETURN ; read " ';' \
 
