/*
 * false_recognizer.c
 *
 *  Created on: Apr 9, 2019
 *      Author: Fixed
 */


/* a client in the unix domain */
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>

#define RECOG_SOCKET_PATH "/tmp/recog-socket"


#define OUT(msg, ...) fprintf(stderr, msg "\n", ##__VA_ARGS__); fflush(stderr)
#define ERROR(msg, ...) fprintf(stderr, "%s(%d): ERROR: " msg "\n", __FILE__, __LINE__, ## __VA_ARGS__)
#define WARN(msg, ...) OUT("Warning: "msg, ##__VA_ARGS__)

#define INFO OUT

#define LOG OUT

void error(const char *);

int main()//int argc, char *argv[])
{
   /*if( argc == 2 ) {
	  printf("The argument supplied is %s\n", argv[1]);
   }
   else if( argc > 2 ) {
	  printf("Too many arguments supplied.\n");
   }
   else {
	  printf("One argument expected.\n");
   }*/
   FILE *recognizer;
   recognizer = fopen("/home/osboxes/recyc/tests/items.list","r");
   int sockfd, servlen,n;
   struct sockaddr_un  serv_addr;
   char req;
   char *buf;
   size_t bufsize = 4;

   bzero((char *)&serv_addr,sizeof(serv_addr));
   serv_addr.sun_family = AF_UNIX;
   strcpy(serv_addr.sun_path, RECOG_SOCKET_PATH);
   servlen = strlen(serv_addr.sun_path) +
                 sizeof(serv_addr.sun_family);
   if ((sockfd = socket(AF_UNIX, SOCK_STREAM,0)) < 0)
       error("Creating socket");
   if (connect(sockfd, (struct sockaddr *)
                         &serv_addr, servlen) < 0)
       error("Connecting");
   while(1) {
	   n=read(sockfd,&req,1);
	   if(strcmp(&req, "\n"))
	   {
		   getline(&buf, &bufsize, recognizer);
		   printf("Got request - answering with %s", buf);
		   write(sockfd, buf, strlen(buf));
		   free(buf);
	   }
   }
   close(sockfd);
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

