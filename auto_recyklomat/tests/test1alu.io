# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list

# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define conveyorinfo a
define conveyorinfo b
define conveyorstart 0
define conveyordir 1
define conveyorspeeda 2
define conveyorspeedb 3
define conveyorspeedc 4
define alucrasher 6
define alucrasherclose 7

# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000

#ALU
expect conveyorstart 1
wait 200
expect alucrasher 1
expect alucrasher 0
expect alucrasherclose 1
expect alucrasherclose 0
expect alucrasher 1
expect alucrasher 0
expect alucrasherclose 1
expect alucrasherclose 0
expect conveyorstart 0
wait 200
expect conveyorstart 0
expect conveyorstart 1

exit
