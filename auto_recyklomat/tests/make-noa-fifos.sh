#!/bin/bash

mkfifo -m 666 /dev/noa-pc-can /dev/noa-can-pc

if [ "$#" -lt 1 ]; then
    ARGS=1
else
    ARGS=$1
fi
    
for ((n=0;n<$ARGS;n++));
    do mkfifo -m 666 /dev/noa-io$n-can /dev/noa-can-io$n;
done;

