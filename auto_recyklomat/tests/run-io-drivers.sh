#!/bin/bash

if [ "$#" -lt 1 ]; then
    ADDR0=0
else
    ADDR0=$1
fi

COMMAND="tmux new \"./bin/io-driver /dev/noa-can-io0 /dev/noa-io0-can $ADDR0\""

for((i=1;i<$#;i++)); do
    j=$[ $i + 1 ]
    k=$[ $i - 1 ]
    COMMAND+=" ';' split \"./bin/io-driver /dev/noa-io$i-can /dev/noa-can-io$i ${!j}\" ';' selectp -t $k ";
done;

set -x

eval $COMMAND

