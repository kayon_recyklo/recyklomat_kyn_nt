#!/bin/bash

if [ "$#" -lt 1 ]; then
    ARG=0
else
    ARG=$1
fi

if [ "$#" -lt 2 ]; then
    ADDR=0
else
    ADDR=$2
fi

set -x
./bin/io-driver /dev/noa-can-io$ARG /dev/noa-io$ARG-can $ADDR 
