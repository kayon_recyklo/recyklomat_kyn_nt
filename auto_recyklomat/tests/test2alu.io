# Skrypt symulatora IO Drivera.
#
# Udajemy tutaj, ze jest obiekt, ktory przesuwa sie
# po pasie transmisyjnym.
#
# Kolejnosc obslugiwanych elementow musi odpowiadac
# wpisom zawartym w pliku items.list

# nadajemy nazwy dla poszczegolnych wejsc i wyjsc
define doordetector a
define petbutton b
define alubutton c
define glabutton d
define doorhold 0
define dropboxhold 1
define dropbox 2
define dropboxconveynor 3
define dropboxleds 4
define dropboxarm 5
define dropboxrolls 6

# na poczatek wyzerujmy wszystkie wejscia
set a 0
set b 0
set c 0
set d 0

# i upewnijmy sie, ze wyjscia sa wyzerowane:
assert 0 0
assert 1 0
assert 2 0
assert 3 0
assert 4 0
assert 5 0
assert 6 0
assert 7 0

# odczekujemy, zeby miec pewnosc, ze QPS sie uruchomil
wait 5000

#PET
set doordetector 0
wait 2000
set doordetector 1
expect dropboxrolls 1
expect dropboxrolls 0
expect dropboxrolls 1
expect dropboxrolls 0
set petbutton 1
wait 300
set petbutton 0
expect dropbox 1
expect dropbox 0
expect doorhold 0
wait 500
set alubutton 1
wait 300
set alubutton 0
wait 100
set doordetector 0
wait 1000
set doordetector 1
wait 1000
expect doorhold 1
expect dropbox 1
expect dropbox 0
expect doorhold 0
expect doorhold 1
exit
