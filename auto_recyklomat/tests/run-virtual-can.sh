#!/bin/bash

if [ "$#" -lt 1 ]; then
    ARGS=1
else
    ARGS=$1
fi

#COMMAND="./bin/virtualcan -v"
COMMAND="./bin/virtualcan "

for((i=0;i<$ARGS;i++)); do
    COMMAND+=" /dev/noa-io$i-can /dev/noa-can-io$i";
done;

COMMAND+=" /dev/noa-pc-can /dev/noa-can-pc"

set -x
eval $COMMAND

