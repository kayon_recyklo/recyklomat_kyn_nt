#include "task.h"

extern volatile uint16_t militimer;
extern task_t tasks[8];

void task_add(uint8_t channel, uint8_t type, uint16_t period)
{
    uint16_t militimer_shadow;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        militimer_shadow = militimer;
    }
    
    if(period > 32768)
    period = 32768;
    
    tasks[channel].timepoint = militimer_shadow + period;
    tasks[channel].type = type;
}

inline void task_cancel(uint8_t channel)
{
    tasks[channel].type = 0;
}
