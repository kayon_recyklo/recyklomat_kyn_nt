#include "i2c.h"

void I2C_Init()
{
	TWBR = 72;
	TWCR = (1<<TWEN);	
}

void I2C_Start()
{
	TWCR = ((1<<TWINT) | (1<<TWSTA) | (1<<TWEN));
	while (!(TWCR & (1<<TWINT)));
}

void I2C_Stop(void)
{
	TWCR = ((1<< TWINT) | (1<<TWEN) | (1<<TWSTO));
}

void I2C_Write(uint8_t data)
{
	TWDR = data;
	TWCR = ((1<<TWINT) | (1<<TWEN));
	while(!(TWCR & (1 <<TWINT)));
}

uint8_t I2C_Read(uint8_t ack)
{
	TWCR = ((1<<TWINT) | (1<<TWEN) | (ack<<TWEA));
	while(!(TWCR & (1 <<TWINT)));
	return TWDR;
}