#ifndef TASK_H_
#define TASK_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

typedef struct
{
    uint8_t type;
    uint16_t timepoint;
}task_t;

void task_add(uint8_t channel, uint8_t type, uint16_t period);
void task_cancel(uint8_t channel);

#endif /* TASK_H_ */