#ifndef USART_H_
#define USART_H_

#include <avr/io.h>

void USART_Init(uint16_t ubrr);
void USART_Transmit(uint8_t data);

#endif /* USART_H_ */