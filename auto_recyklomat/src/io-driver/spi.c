#include "spi.h"

void SPI_Init()
{
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1)|(1<<SPR0);
}

void SPI_Transmit(uint8_t data)
{
	CS_LO;
	SPDR = data;
	while(!(SPSR & (1<<SPIF)));
	CS_HI;
}