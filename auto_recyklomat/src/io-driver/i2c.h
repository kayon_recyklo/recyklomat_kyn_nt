#ifndef I2C_H_
#define I2C_H_

#include <avr/io.h>

void I2C_Init();
void I2C_Start();
void I2C_Stop(void);
void I2C_Write(uint8_t data);
uint8_t I2C_Read(uint8_t data);

#endif /* I2C_H_ */