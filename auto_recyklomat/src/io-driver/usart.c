#include "usart.h"

void USART_Init(uint16_t ubrr)
{
	UBRRH = (uint8_t)(ubrr>>8);
	UBRRL = (uint8_t)ubrr;
	UCSRB = (1<<RXCIE)|(1<<RXEN)|(1<<TXEN);
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
}

void USART_Transmit(uint8_t data)
{
	while(!(UCSRA & (1<<UDRE)))
	;
	UDR = data;
}