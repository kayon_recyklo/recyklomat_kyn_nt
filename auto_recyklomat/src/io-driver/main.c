/*
 *	QPS IODriver firmware
 *  Compliant with Rev B
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <time.h>
#include <stdio.h>
#include "i2c.h"
#include "usart.h"
#include "spi.h"
#include "defs.h"
#include "noaproto.h"
#include "task.h"

#define VERSION_MAJOR	0
#define VERSION_MINOR	12

uint32_t counter;

// Communication protocol variables
extern volatile uint8_t head;
extern volatile uint8_t tail;
extern uint8_t rx[128];
extern uint8_t tx[128];

uint32_t id = 0x00000000;
uint16_t current[8];
uint16_t peak_current[8];
uint16_t minimum_ex_current[8] = {10,10,10,10,10,10,10,10};
uint16_t maximum_ex_current[8] = {4000,4000,4000,4000,4000,4000,4000,4000};
uint16_t voltage;
uint16_t minimum_ex_voltage = 10000;
uint16_t maximum_ex_voltage = 15000;

uint32_t logicClock;
volatile uint8_t output_latch;
volatile uint8_t default_output_latch = 0x00;
volatile uint8_t input_state;
volatile uint8_t output_state;
volatile uint8_t channel_mux = 0;

volatile uint16_t militimer;
task_t tasks[8];

void print_in_size( void );

void load_settings()
{
	default_output_latch = eeprom_read_byte(EE_DEFAULT_OUTPUT_LATCH_OFFSET);
	minimum_ex_voltage = eeprom_read_word(EE_MINIMUM_EX_VOLTAGE_OFFSET);
	maximum_ex_voltage = eeprom_read_word(EE_MAXIMUM_EX_VOLTAGE_OFFSET);
	eeprom_read_block(minimum_ex_current,EE_MINIMUM_EX_CURRENT_OFFSET,16);
	eeprom_read_block(maximum_ex_current,EE_MAXIMUM_EX_CURRENT_OFFSET,16);
}

void save_settings()
{
	eeprom_write_byte(EE_DEFAULT_OUTPUT_LATCH_OFFSET,default_output_latch);
	eeprom_write_word(EE_MINIMUM_EX_VOLTAGE_OFFSET,minimum_ex_voltage);
	eeprom_write_word(EE_MAXIMUM_EX_VOLTAGE_OFFSET,maximum_ex_voltage);
	eeprom_write_block(minimum_ex_current,EE_MINIMUM_EX_CURRENT_OFFSET,16);
	eeprom_write_block(maximum_ex_current,EE_MAXIMUM_EX_CURRENT_OFFSET,16);
	eeprom_write_byte(EE_EEINITIALIZED_OFFSET,0x00);
}

uint16_t MCP3021_Sample()
{
	I2C_Start();
	I2C_Write(0x91);
	uint8_t high = I2C_Read(1);
	uint8_t low = I2C_Read(0);
	I2C_Stop();
	
	return ((uint16_t)high << 6) | (low >> 2);
}

int main(void)
{
	DDRA = 0x00;
	DDRB = 0xBF;
	DDRC = 0x0F;
	DDRD = 0xFE;
	
    // Initialize SPI
    EN_HI;
    SPI_Init();
    SPI_Transmit(0x00);
    EN_LO;
    
	id = eeprom_read_dword(EE_ID_OFFSET);
	
	// Load or initialize settings in EEPROM
	if(eeprom_read_byte(EE_EEINITIALIZED_OFFSET) == 0x00)
		load_settings();
	else
		save_settings();
	
	// Initialize UART
	USART_Init(103);
	
	// Initialize I2C for voltage measurement
	I2C_Init();
	
    // Set default output values
	for(uint8_t t=0;t<8;t++)
	{
		if(default_output_latch & (1 << t))
		{
			output_latch |= (1 << t);
			SPI_Transmit(output_latch);
		}
		_delay_ms(200);
	}
	
	// Initialize ADC for current measurement
	ADMUX = (1<<REFS1)|(1<<REFS0);
	ADCSRA = (1<<ADEN)|(1<<ADIE)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	
	// Initialize timer 0
	TCCR0 = (1<<CS02)|(1<<CS00);
	
	// Initialize timer 1 in CTC mode
	TCCR1A = 0;
	TCCR1B = (1<<WGM12)|(0<<CS12)|(1<<CS10);
	OCR1A = 16000;
	
	// Enable timer interrupts
	TIMSK = (1<<TOIE0) | (1<<TOIE2) | (1<<OCIE1A);
	
	sei();


        uint32_t loop=0;

    while (1) 
    {
        uint16_t militimer_shadow;
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
        {
            militimer_shadow = militimer;
        }
       
        if( loop++ == 50000 )
        {
            loop=0;
    //        printf("counter: %d\n", counter);
        }

		// System voltage measurement, fixed point conversion
		uint16_t mcpSample = MCP3021_Sample();
		//voltage = (uint16_t)((((uint32_t)mcpSample)*19564) >> 7); // Rev A voltage divider
		voltage = (uint16_t)((((uint32_t)mcpSample)*6875) >> 7); // Rev B voltage divider
		
		// Monitor input channels
		if(PINC & 0x80) input_state &= ~INPUTA_BM; else input_state |= INPUTA_BM;
		if(PINC & 0x40) input_state &= ~INPUTB_BM; else input_state |= INPUTB_BM;
		if(PINC & 0x20) input_state &= ~INPUTC_BM; else input_state |= INPUTC_BM;
		if(PINC & 0x10) input_state &= ~INPUTD_BM; else input_state |= INPUTD_BM;
		
		// Monitor output channels
		for(int i=0;i<8;i++)
		{
            uint8_t mask = (1<<i);
            
			if(current[i] > minimum_ex_current[i] && current[i] < maximum_ex_current[i])
				output_state |= mask;
			else
				output_state &= ~mask;
				
			if(current[i] > maximum_ex_current[i])
            {
				output_latch &= ~mask;
                SPI_Transmit(output_latch);
            }
            
            int16_t distance = militimer_shadow - tasks[i].timepoint;
            if(tasks[i].type > 0 && distance > 0)
            {
                if(tasks[i].type == ENABLE_FOR_PERIOD) {
                	//OUT("Disabling channel %d after period", i);
                	output_latch &= ~mask;
                }


                if(tasks[i].type == DISABLE_FOR_PERIOD) {
                	//OUT("Enabling channel %d after period", i);
                	output_latch |= mask;
                }
                SPI_Transmit(output_latch);
                task_cancel(i);
            }               
		}
		
		// Find awaiting frame in buffer
		uint8_t frame_length = match();
		if(frame_length > 0)
		{
			read(head,frame_length);
			uint8_t opcode = NP_RX_OPCODE;
			uint32_t mac = NP_RX_TARGET;
	
                        OUT("opcode: %02X\nmac: %08X\nid: %08X\n", opcode, mac, id);
			
                        if((mac == id) && (opcode & NP_RESPONSE_BITMASK) == 0)
			{
				switch(opcode)
				{
					case VERSION:
					{
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 2;
						NP_TXDATA_UINT8(0) = VERSION_MAJOR;
						NP_TXDATA_UINT8(1) = VERSION_MINOR;
						break;
					}
					case GET_STATE:
					{
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 5;
						NP_TXDATA_UINT8(0) = input_state;
						NP_TXDATA_UINT8(1) = output_state;
						NP_TXDATA_UINT8(2) = output_latch;
						NP_TXDATA_UINT16(3) = voltage;
						break;
					}
					case SET_CURRENT_LIMITS:
					{
						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
						minimum_ex_current[channel] = NP_RXDATA_UINT16(1);
						maximum_ex_current[channel] = NP_RXDATA_UINT16(3);
						eeprom_write_word(EE_MINIMUM_EX_CURRENT_OFFSET+(channel*2),minimum_ex_current[channel]);
						eeprom_write_word(EE_MAXIMUM_EX_CURRENT_OFFSET+(channel*2),maximum_ex_current[channel]);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 0;
						break;
					}
					case SET_VOLTAGE_LIMITS:
					{
						minimum_ex_voltage = NP_RXDATA_UINT16(0);
						maximum_ex_voltage = NP_RXDATA_UINT16(2);
						eeprom_write_word(EE_MINIMUM_EX_VOLTAGE_OFFSET,minimum_ex_voltage);
						eeprom_write_word(EE_MAXIMUM_EX_VOLTAGE_OFFSET,maximum_ex_voltage);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 0;
						break;
					}
					case GET_VOLTAGE_LIMITS:
					{
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 4;
						NP_TXDATA_UINT16(0) = minimum_ex_voltage;
						NP_TXDATA_UINT16(2) = maximum_ex_voltage;
						break;
					}
					case GET_OUTPUT_INFO:
					{
						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 9;
						NP_TXDATA_UINT8(0) = channel;
						NP_TXDATA_UINT16(1) = current[channel];
						NP_TXDATA_UINT16(3) = peak_current[channel];
						NP_TXDATA_UINT16(5) = minimum_ex_current[channel];
						NP_TXDATA_UINT16(7) = maximum_ex_current[channel];
						break;
					}
					case ENABLE_OUTPUT:
					{
						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
                        task_cancel(channel);
						output_latch |= (1<<channel);
						SPI_Transmit(output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 1;
						NP_TXDATA_UINT8(0) = channel;
						break;
					}
					case DISABLE_OUTPUT:
					{
						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
                        task_cancel(channel);
						output_latch &= ~(1<<channel);
						SPI_Transmit(output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 1;
						NP_TXDATA_UINT8(0) = channel;
						break;
					}
					case SET_OUTPUT:
					{
						output_latch = NP_RXDATA_UINT8(0);
						SPI_Transmit(output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 1;
						NP_TXDATA_UINT8(0) = output_latch;
						break;
					}
					case SET_DEFAULT_OUTPUT:
					{
						default_output_latch = NP_RXDATA_UINT8(0);
						eeprom_write_byte(EE_DEFAULT_OUTPUT_LATCH_OFFSET,default_output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 1;
						NP_TXDATA_UINT8(0) = default_output_latch;
						break;
					}
					case GET_DEFAULT_OUTPUT:
					{
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 1;
						NP_TXDATA_UINT8(0) = default_output_latch;
						break;
					}
					case ENABLE_FOR_PERIOD:
					{

						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
						uint16_t period = NP_RXDATA_UINT16(1);
						//OUT("Enabling chan %d for %d", channel, period);
                        task_add(channel, ENABLE_FOR_PERIOD, period);
                        output_latch |= (1<<channel);
                        SPI_Transmit(output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 3;
						NP_TXDATA_UINT8(0) = channel;
						NP_TXDATA_UINT16(1) = period;
						break;
					}
					case DISABLE_FOR_PERIOD:
					{
						uint8_t channel = NP_RXDATA_UINT8(0) & 0x07;
						uint16_t period = NP_RXDATA_UINT16(1);
						//OUT("Disabling chan %d for %d", channel, period);
                        task_add(channel, DISABLE_FOR_PERIOD, period);
                        output_latch &= ~(1<<channel);
                        SPI_Transmit(output_latch);
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 3;
						NP_TXDATA_UINT8(0) = channel;
						NP_TXDATA_UINT16(1) = period;
						break;
					}
					case GET_EVENTS:
					{
						NP_TX_OPCODE = opcode | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 0;
						break;
					}
					default:
						NP_TX_OPCODE = opcode | NP_ERROR_BITMASK | NP_RESPONSE_BITMASK;
						NP_TX_LENGTH = NP_BASE_LENGTH + 0;
				}
				
				NP_TX_TARGET = id;
				send();
			}
			discard(frame_length);
		}
    }
}

ISR(ADC_vect){
	// Convert sample to physical value and store
	uint16_t adcSample = ADCW;
	uint16_t curSample = (adcSample*25) >> 2;
	current[channel_mux] = (3*current[channel_mux] + curSample) >> 2;
	
	if(current[channel_mux] > peak_current[channel_mux])
		peak_current[channel_mux] = current[channel_mux];
	
	channel_mux = (channel_mux + 1) & 0x07;
}

ISR(TIMER0_OVF_vect)
{
	// Increment channel multiplexer and start ADC conversion
	ADMUX = (1<<REFS1)|(1<<REFS0)|(7-channel_mux);
	ADCSRA |= (1<<ADSC);
}

ISR(TIMER1_COMPA_vect)
{
	militimer++;
}
