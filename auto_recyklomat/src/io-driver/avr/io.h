#ifndef AVR_IO_H
#define AVR_IO_H

#include <stdint.h>
#include <stddef.h>

int gettid( void );

#define OUT(msg, ...) fprintf(stderr, msg "\n", ## __VA_ARGS__); fflush(stderr)

#define TID_OUT(name) fprintf(stderr, name "_tid: %u\n", gettid()); fflush(stderr)

extern uint8_t DDRA;
extern uint8_t DDRB;
extern uint8_t DDRC;
extern uint8_t DDRD;
extern uint8_t ADMUX;
extern uint8_t ADCSRA;
extern uint8_t TCCR0;
extern uint8_t TIMSK;
extern uint8_t PINC;
//extern uint8_t UDR;

extern uint8_t PINB;
extern uint8_t PINB3;
extern uint8_t TOIE2;
extern uint8_t CS10;
extern uint32_t OCR1A;
extern uint32_t OCIE1A;
extern uint8_t CS12;
extern uint8_t TCCR1B;
extern uint8_t WGM12;
extern uint8_t PORTB;
extern uint8_t TCCR1A;

#define ADCW 0
#define REFS1 1
#define REFS0 0
#define ADSC 0
#define CS00 0
#define CS02 2
#define ADPS0 0
#define ADPS1 1
#define ADPS2 2
#define ADIE 0
#define ADEN 0
#define TOIE0 0
#define UDRE 0

static inline void sei(void) { }

extern void io_write(uint8_t *data, uint8_t length);
extern uint8_t io_read_byte(void);

#define UDR io_read_byte()


#include <stdio.h>
static inline void fprinthex(FILE *out, char *array, size_t size) {
    for (int i = 0; i < size; ++i) {
        fprintf(out, "%02X ", array[i]);
    }
}

#define printhex(array, size) fprinthex(stdout, array, size)

#define DUMPHEX(array, size) {                                  \
        printf(# array "[" # size "=%d]: ", (int) (size));      \
        printhex((char *) array, (size_t) size);                \
        printf("\n");                                           \
    }


#endif // AVR_IO_H
