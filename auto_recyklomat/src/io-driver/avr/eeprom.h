#ifndef AVR_EEPROM_H
#define AVR_EEPROM_H

#include <stdint.h>
#include <stddef.h>

extern uint8_t eeprom_read_byte(const uint8_t *p);

extern uint16_t eeprom_read_word(const uint16_t *p);

extern uint32_t eeprom_read_dword(const uint32_t *p);

extern void eeprom_read_block(void *dst, const void *src, size_t n);

extern void eeprom_write_byte(uint8_t *p, uint8_t value);

extern void eeprom_write_word(uint16_t *p, uint16_t value);

extern void eeprom_write_dword(uint32_t *p, uint16_t value);

extern void eeprom_write_block(const void *src, void *dst, size_t n);


#endif // AVR_EEPROM_H
