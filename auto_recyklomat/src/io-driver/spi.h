#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>

#define CS_HI	PORTB |= (1<<PINB4)
#define CS_LO	PORTB &= ~(1<<PINB4)

#define EN_HI   PORTB |= (1<<PINB3)
#define EN_LO   PORTB &= ~(1<<PINB3);

void SPI_Init();
void SPI_Transmit(uint8_t data);

#endif /* SPI_H_ */