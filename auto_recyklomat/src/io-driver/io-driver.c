
#include <stdint.h>
#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <ctype.h>
#include <time.h>
#include <sys/syscall.h>

#include "avr/io.h"
#include "defs.h"

extern uint32_t counter;

#define STX 0x02
#define ETX 0x03


uint8_t DDRA;
uint8_t DDRB;
uint8_t DDRC;
uint8_t DDRD;
uint8_t ADMUX;
uint8_t ADCSRA;
uint8_t TCCR0;
uint8_t TIMSK;
uint8_t UDR;
uint8_t PINC;

uint8_t PINB;
uint8_t PINB3;
uint8_t TOIE2;
uint8_t CS10;
uint32_t OCR1A;
uint32_t OCIE1A;
uint8_t CS12;
uint8_t TCCR1B;
uint8_t WGM12;
uint8_t TCCR1A;
uint8_t PORTB;

void I2C_Init() {}

void I2C_Start() {}

void I2C_Stop(void) {
	//opoznienie poza kodem NT
    //struct timespec sleeptime, dummytime;
    //sleeptime.tv_sec = 0;
    //sleeptime.tv_nsec = 10L;
    //nanosleep(&sleeptime, &dummytime);
}

void I2C_Write(uint8_t data) {}

uint8_t I2C_Read(uint8_t ack) { return 0; }

void SPI_Init() {}

void SPI_Transmit(uint8_t data) {}

void USART_Init(uint16_t ubrr) {}

void USART_Transmit(uint8_t data) {}

uint8_t eeprom_read_byte(const uint8_t *p) {
    return 0;
}

uint16_t eeprom_read_word(const uint16_t *p) {
    return 0;
}

static uint32_t address = 0;

uint32_t eeprom_read_dword(const uint32_t *p) {
    if (p == EE_ID_OFFSET) {
        return address;
    }
    return 0;
}

void eeprom_read_block(void *dst, const void *src, size_t n) { }

void eeprom_write_byte(uint8_t *p, uint8_t value) { }

void eeprom_write_word(uint16_t *p, uint16_t value) { }

void eeprom_write_dword(uint32_t *p, uint16_t value) { }

void eeprom_write_block(const void *src, void *dst, size_t n) { }

extern uint8_t checksum(volatile uint8_t *buffer, uint8_t offset, uint8_t length);

void _delay_ms(uint32_t ms) {
    (void) usleep(1000 * ((useconds_t) ms));
}

static int in, out;

void
io_write(uint8_t *data, uint8_t length) {
    write(out, data, length);
    OUT(stderr, "wrote %d bytes: ", length);

    for( uint8_t i=0; i<length; i++ ) 
    {   
        OUT(stderr, "%02X ", data[i]);
    }

    OUT(stderr, "\n");
}

static inline int
read_with_timeout(int fd, volatile uint8_t *buffer, size_t size, clock_t timeout_ms) {
    assert(fd < FD_SETSIZE);
    assert(timeout_ms >= 0);
    struct timeval tv = {
        tv.tv_sec  = timeout_ms / 1000,
        tv.tv_usec = (timeout_ms % 1000)*1000
    };

    fd_set in;
    FD_ZERO(&in);
    FD_SET(fd, &in);
    int result = select(fd+1, &in, NULL, NULL, &tv);
    if (result < 0) {
        perror(__FUNCTION__);
        return result;
    }

    if (result == 0) {
        return result;
    }

    return read(fd, (void*)buffer, size);
}

uint8_t
io_read_byte(void) {
    int n;
    volatile uint8_t bytes[1];
    n = read_with_timeout(in, bytes, sizeof(bytes), 1000); 

    if( n != 1 )
    {   
        OUT("read timeout!\n");
    }    

//    n = read(in, bytes, sizeof(bytes));
//    assert(n == 1);

//    for( volatile uint32_t i=0; i<50000; i++);
//    usleep(125L);
//    fprintf(stderr, "r: %02X\n", bytes[0]);
//    fflush(stderr);

    return bytes[0];
}

extern void avmain(void);
extern void USART_RXC_vect(void);
extern void TIMER1_COMPA_vect(void);

static void *
read_bytes(void *unused) {

    TID_OUT("read");
while(1) {
        USART_RXC_vect();
    }
    return NULL;
}

static void *
millisecond_timer(void *unused) {
    struct timespec sleeptime, dummytime;
    sleeptime.tv_sec = 0;
    sleeptime.tv_nsec = 1000L;

    while(1) {
       TIMER1_COMPA_vect();
       nanosleep(&sleeptime, &dummytime);
    }
    return NULL;
}

static inline const char *
skip_chars(int (*ischar)(int), const char *input) {
    while (ischar(*input)) {
        ++input;
    }
    return input;
}


#define NELEMS(a) (sizeof(a)/(sizeof((a)[0])))

static inline const char *is_prefix_token(const char *prefix, const char *string) {
    while(*prefix) {
        if(!*string || (*prefix++ != *string++)) {
            return NULL;
        }
    }
    if (isalnum(*string) || *string == '_') {
        return NULL;
    }
    return string;
}

static inline bool
is_output_channel(int x) {
    return 0 <= x && x <= 7;
}

static inline bool
is_input_channel(int x) {
    return 0xa <= x && x <= 0xd;
}

static inline bool
is_channel(int x) {
    return is_input_channel(x) || is_output_channel(x);
}

typedef struct {
    char *name;
    int channel;
} definition;

typedef struct {
    definition definitions[16];
    int num_definitions;
    bool running;
    int line;
} Program;

typedef Program *ProgramState;

static inline int channel(const char *name, ProgramState state) {
    if (strlen(name) == 1) {
        if ('0' <= *name  && *name <= '7') {
            return *name - '0';
        }
        if ('a' <= *name && *name <= 'd') {
            return *name - 'a' + 0xa;
        }
    }
 
    for (int i = 0; i < state->num_definitions; ++i) {
        definition *def = &state->definitions[i];
        if (!strcmp(def->name, name)) {
            return def->channel;
        }
    }
    return -1;
}

typedef struct {
    const char *prefix;
    ProgramState (*action)(const char *arg, ProgramState state);
    const char *help;
} command_t;

#define COMMAND

static COMMAND ProgramState define(const char *args, ProgramState state) {
    char *name = NULL;
    int value;
    if (sscanf(args, "%m[a-z] %x", &name, &value) < 2) {
        OUT("Invalid argument. Usage: define <name> <value>");
        if (name) {
            free(name);
        }
        return state;
    }
    assert(state->num_definitions < NELEMS(state->definitions));
    assert(strlen(name) > 1 && "identifier must consist of more than one character");
    assert(is_channel(value));
    state->definitions[state->num_definitions++] = (definition) {
        .name = name,
        .channel = value
    };
    return state;
}


static COMMAND ProgramState print(const char *args, ProgramState state) {
    OUT("%s", args);
    return state;
}

static inline void set_input_channel(int chan, int value) {
    if (!is_input_channel(chan)) {
        OUT("Invalid <input-channel>. Use [a-d] or a defined name of an input channel");
        return;
    }

    if (value != 0 && value != 1) {
        OUT("<value> can either be 0 or 1");
        return;
    }

    int n = 1 + chan - 0xa;
    if (value == 1) {
        PINC &= ~(1 << (8-n));
    }
    else {
        PINC |= (1 << (8-n));
    }
}                                                

static COMMAND ProgramState set(const char *args, ProgramState state) {
    char *chan = NULL;
    int value;
    if (sscanf(args, "%ms %d", &chan, &value) != 2) {
        OUT("Invalid argument. Usage: set <input-channel> <value>");
        if (chan) {
            free(chan);
        }
        return state;
    }
    set_input_channel(channel(chan, state), value);

    free(chan);
    return state;
}

static COMMAND ProgramState check(const char *args, ProgramState state) {
    char *chan = NULL;
    int value;
    if (sscanf(args, "%ms %d", &chan, &value) != 2) {
        OUT("Invalid argument. Usage: assert <channel> <value>");
        goto end;
    }
    if (value != 0 && value != 1) {
        OUT("<value> can either be 0 or 1");
        goto end;
    }
    int c = channel(chan, state);

    if (is_input_channel(c)) {
        extern uint8_t input_state;
        if (!!(input_state & (1 << (c - 0xa))) != value) {
            OUT("assertion in line %d failed: %s", state->line, args);
        }        
    }
    else if (is_output_channel(c)) {
        extern uint8_t output_state;
        if (!!(output_state & (1 << c)) != value) {
            OUT("assertion in line %d failed: %s", state->line, args);
        }
    }
    else {
        assert(!is_channel(c));
        OUT("Invalid <channel>. Use [0-7a-d] or a defined name of a channel");
    }

 end:
    if (chan) {
        free(chan);
    }
    return state;
}

#define wait_for_output() usleep(10000)

static COMMAND ProgramState expect(const char *args, ProgramState state) {
    char *chan = NULL;
    int value;
    if (sscanf(args, "%ms %d", &chan, &value) != 2) {
        OUT("Invalid argument. Usage: expect <output-channel> <value>");
        goto end;
    }
    if (value != 0 && value != 1) {
        OUT("<value> can either be 0 or 1");
        goto end;
    }
    int c = channel(chan, state);

    if (is_output_channel(c)) {
        extern uint8_t output_latch;
        while (!!(output_latch & (1 << c)) != value) {
            wait_for_output();
        }
    }
    else {
        OUT("Invalid <output-channel>. Use [0-7] or a defined name of a channel");
    }

 end:
    if (chan) {
        free(chan);
    }
    return state;
}

static COMMAND ProgramState finish(const char *args, ProgramState state) {
    state->running = false;
    return state;
}

static COMMAND ProgramState help(const char *args, ProgramState state);

static COMMAND ProgramState wait(const char *args, ProgramState state) {
    unsigned int ms;
    if (sscanf(args, "%d", &ms)) {
        usleep(1000 * ms);
    }
    else {
        OUT("Invalid argument to wait in line %d: %s", state->line, args);
    }
    return state;
}

static command_t commands[] = {
    { .prefix = "exit", .action = finish, "exit program" },
    { .prefix = "define", .action = define, "give a name to a channel" },
    { .prefix = "assert", .action = check, "check whether a given condition is true" },
    { .prefix = "print", .action = print, "print its arguments" },
    { .prefix = "set", .action = set, "set the value of an input channel" },
    { .prefix = "expect", .action = expect, "wait for a condition to be satisfied" },
    { .prefix = "wait",  .action = wait, "wait for a number of milliseconds" },
    { .prefix = "help", .action = help, "list available commands" }
};

static ProgramState help(const char *args, ProgramState state) {
    OUT("Available commands:");
    for (int i = 0; i < NELEMS(commands); ++i) {
        OUT("  %s -- %s", commands[i].prefix, commands[i].help);
    }
    return state;
}

static ProgramState command_dispatch(const char *line, ProgramState state) {
    for (int i = 0; i < NELEMS(commands); ++i) {
        const char *args;
        if ((args = is_prefix_token(commands[i].prefix, line))) {
            return commands[i].action(skip_chars(isspace, args), state);
        }
    }
    OUT("Unrecognized command: `%s'. Type `help' to list available commands.",
        line);
    return state;
}


int gettid( void )
{
    return syscall(SYS_gettid);
}
    
int main(int argc, const char *argv[])  {

    if (argc < 2 || argc > 4) {
        OUT("Usage: %s <in> [<out>] [address]", argv[0]);
        return -1;
    }

    const char *pipein = argv[1];
    const char *pipeout;
    
    if (argc > 2) {
        if (sscanf(argv[argc-1], "%x", &address) == 1) {
            pipeout = argv[argc-2];
        }
        else {
            address = 0;
            pipeout = argv[argc-1];
        }
    }

    if (pipein != pipeout) {
        OUT("Opening pair of files: %s, %s", pipein, pipeout);
        in = open(pipein, O_RDONLY);
        OUT("file %s opened for reading (%d)", pipein, in);
        out = open(pipeout, O_WRONLY);
        OUT("file %s opened for writing (%d)", pipeout, out);
    }
    else {
        puts("Opening a single file");
        in = out = open(pipein, O_RDWR);
    }

    assert(in > 0);
    assert(out > 0);
    
    pthread_t noatalk, readbytes, millisecondtimer;
    pthread_create(&noatalk, NULL, (void *(*)(void *)) avmain, NULL);
    pthread_create(&readbytes, NULL, read_bytes, NULL);
    pthread_create(&millisecondtimer, NULL, millisecond_timer, NULL);

    Program programState = {
        .num_definitions = 0,
        .running = true,
        .line = 1
    };

    ProgramState state = &programState;
    
    while (state && state->running) {
        char prompt[16];
        snprintf(prompt, sizeof(prompt), "%d> ", state->line);
        char *line = readline(prompt);
        if (!line) {
            break;
        }
        const char *command = skip_chars(isspace, line);
        if (*command && *command != '#') {
            state = command_dispatch(command, state);
        }
        state->line++;
        free(line);
    }
    
    return 0;
}
