#ifndef DEFS_H_
#define DEFS_H_

#define VERSION					0x00
#define GET_STATE				0x01
#define SET_CURRENT_LIMITS		0x02
#define GET_OUTPUT_INFO			0x03
#define SET_VOLTAGE_LIMITS		0x04
#define GET_VOLTAGE_LIMITS		0x05
#define ENABLE_OUTPUT			0x06
#define DISABLE_OUTPUT			0x07
#define SET_OUTPUT				0x08
#define SET_DEFAULT_OUTPUT		0x09
#define GET_DEFAULT_OUTPUT		0x0A
#define ENABLE_FOR_PERIOD		0x0B
#define DISABLE_FOR_PERIOD		0x0C
#define GET_EVENTS			0x0D

#define CHX_OVERLOAD			0x00
#define CH0_OVERLOAD			0x00
#define CH1_OVERLOAD			0x01
#define CH2_OVERLOAD			0x02
#define CH3_OVERLOAD			0x03
#define CH4_OVERLOAD			0x04
#define CH5_OVERLOAD			0x05
#define CH6_OVERLOAD			0x06
#define CH7_OVERLOAD			0x07
#define CHX_UNDERLOAD			0x10
#define CH0_UNDERLOAD			0x10
#define CH1_UNDERLOAD			0x11
#define CH2_UNDERLOAD			0x12
#define CH3_UNDERLOAD			0x13
#define CH4_UNDERLOAD			0x14
#define CH5_UNDERLOAD			0x15
#define CH6_UNDERLOAD			0x16
#define CH7_UNDERLOAD			0x17

#define INPUTA_RISING_EDGE		0x0A
#define INPUTB_RISING_EDGE		0x0B
#define INPUTC_RISING_EDGE		0x0C
#define INPUTD_RISING_EDGE		0x0D
#define INPUTA_FALLING_EDGE		0x1A
#define INPUTB_FALLING_EDGE		0x1B
#define INPUTC_FALLING_EDGE		0x1C
#define INPUTD_FALLING_EDGE		0x1D
#define UNDERVOLTAGE			0x20
#define OVERVOLTAGE				0x21
#define UNEXPECTED_PWR_LOSS		0x22

#define INPUTA_BM				0x01
#define INPUTB_BM				0x02
#define INPUTC_BM				0x04
#define INPUTD_BM				0x08

#define EE_ID_OFFSET					(void*)0
#define EE_EEINITIALIZED_OFFSET			(void*)4
#define EE_DEFAULT_OUTPUT_LATCH_OFFSET	(void*)5
#define EE_MINIMUM_EX_VOLTAGE_OFFSET	(void*)6
#define EE_MAXIMUM_EX_VOLTAGE_OFFSET	(void*)8
#define EE_MINIMUM_EX_CURRENT_OFFSET	(void*)10
#define EE_MAXIMUM_EX_CURRENT_OFFSET	(void*)26
#define EE_PEAK_CURRENT_OFFSET			(void*)42

#define DEBUG_INFO 0

#undef OUT

#if DEBUG_INFO
#define OUT(msg, ...) fprintf(stderr, msg, __VA_ARG__); fflush(stderr)
#else
#define OUT(msg, ...)
#endif


#endif /* DEFS_H_ */
