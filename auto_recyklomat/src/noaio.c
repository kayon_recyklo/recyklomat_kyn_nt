#include <termios.h>
#include <unistd.h>

#include "util.h"
#include "noaio.h"

#if NOAIO_DEBUG == 1
#define NOA_OUT(msg, ...) OUT(msg, __VA_ARG__)
#else
#define NOA_OUT(msg, ...)
#undef DUMPHEX
#define DUMPHEX(a, b)
#endif

#define NOAIO_FLUSH 1

const char *NOAIO_request_name(NOAIO_REQUEST request) {
    switch (request) {
    case NOAIO_REQUEST_GET_VERSION:
        return "GET_VERSION";
    case NOAIO_REQUEST_GET_STATE:
        return "GET_STATE";
    case NOAIO_REQUEST_SET_CURRENT_LIMITS:
        return "SET_CURRENT_LIMITS";
    case NOAIO_REQUEST_GET_OUTPUT_INFO:
        return "GET_OUTPUT_INFO";
    case NOAIO_REQUEST_SET_VOLTAGE_LIMITS:
        return "SET_VOLTAGE_LIMITS";
    case NOAIO_REQUEST_GET_VOLTAGE_LIMITS:
        return "GET_VOLTAGE_LIMITS";
    case NOAIO_REQUEST_ENABLE_OUTPUT:
        return "ENABLE_OUTPUT";
    case NOAIO_REQUEST_DISABLE_OUTPUT:
        return "DISABLE_OUTPUT";
    case NOAIO_REQUEST_SET_OUTPUT:
        return "SET_OUTPUT";
    case NOAIO_REQUEST_SET_DEFAULT_OUTPUT:
        return "SET_DEFAULT_OUTPUT";
    case NOAIO_REQUEST_GET_DEFAULT_OUTPUT:
        return "GET_DEFAULT_OUTPUT";
    case NOAIO_REQUEST_ENABLE_FOR_PERIOD:
        return "ENABLE_FOR_PERIOD";
    case NOAIO_REQUEST_DISABLE_FOR_PERIOD:
        return "DISABLE_FOR_PERIOD";
    case NOAIO_REQUEST_GET_EVENTS:
        return "GET_EVENTS";
        
    case NOAIO_REQUEST_ENTER_PROGRAM:
        return "ENTER_PROGRAM";
    case NOAIO_REQUEST_ENTER_BOOTLOADER:
        return "ENTER_BOOTLOADER";
    case NOAIO_REQUEST_WRITE_FLASH_PAGE:
        return "WRITE_FLASH_PAGE";
        
    default:
        return "UNKNOWN_REQUEST";
    }
}
        
uint8_t NOAIO_num_events(NOAIO_RESPONSE_FRAME_GET_EVENTS *frame) {
    if (frame->header.request != NOAIO_REQUEST_GET_EVENTS) {
        return 0;
    }
    const size_t event_bytes = (frame->header.length
                                - sizeof(NOAIO_HEADER)
                                - sizeof(uint8_t)
                                - 2*sizeof(NOAIO_TX));
    if (event_bytes % sizeof(NOAIO_EVENT_TIME) != 0) {
        return 0;
    }
    return event_bytes / sizeof(NOAIO_EVENT_TIME);
}

static inline int memindex(void *base, char c, size_t size) {
    void *at = memchr(base, c, size);
    if (!at) {
        return size;
    }
    return at-base;
}

static int NOAIO_shift_frame_to_start(byte *buffer, size_t size, int skip) {
    // wywala się już tutaj
    if (skip >= size) {
        NOA_OUT("skip >= size - %d >= %d", skip, size);
        return 0;
    }
    int i = skip + memindex(buffer+skip, NOAIO_TX_START, size-skip);
    if (i >= size) {
        NOA_OUT("i >= size");
        return 0;
    }
    memmove(buffer, buffer+i, size);
    return size - i;
}

static clock_t
NOAIO_read_frame_portion(int fd, NOAIO_INCOMMING_FRAME *f, clock_t timeout_ms) {
    if (timeout_ms < 0) {
        return 0;
    }
    
    long long start = time_us();
    int count = read_with_timeout(fd, &f->buffer[f->next],
                                  sizeof(f->buffer)-f->next, timeout_ms);
  
    if( count == 0 )
    {
        NOA_OUT("count == 0");
       
#if NOAIO_DEBUG == 1
         while(2);
#endif
    }
 
    assert(count >= 0);

    NOA_OUT("count: %d f->next: %d", count, f->next);
    
    DUMPHEX(f->buffer, count);

    f->next += (f->next
                ? count
                : NOAIO_shift_frame_to_start(f->buffer, count, 0));

    NOA_OUT("f->next: %d", f->next);

    long long elapsed = time_us() - start;
    return (clock_t) (elapsed / 1000);
}

NOAIO_FRAME *
NOAIO_get_frame(int fd, NOAIO_INCOMMING_FRAME *f, clock_t timeout_ms) {
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX* const frame
        = (NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *) f->buffer;

    if (f->shift > 0) {
//        NOA_OUT("f->shift > 0");
        f->next = NOAIO_shift_frame_to_start(f->buffer, f->next, f->shift);
        f->shift = 0;

//        NOA_OUT("f->next: %d", f->next);
    }

    for (clock_t elapsed = 0;
         elapsed < timeout_ms;
         elapsed += NOAIO_read_frame_portion(fd, f, timeout_ms - elapsed)) {

        // to powoduje wywalenie
        if (f->next < 1) {
            NOA_OUT("f->next(%d) < 1", f->next);
            continue;
        }
        
        if (frame->stx != NOAIO_TX_START) {
            NOA_OUT("STX byte not present (%02X)", frame->stx);
            f->next = NOAIO_shift_frame_to_start(f->buffer, f->next, 1);
            continue;
        }
        
        if (f->next < (sizeof(frame->stx) + sizeof(frame->noaio.header.length))) {
            NOA_OUT("not enough data to read the frame");
            continue;
        }

        if (f->next < frame->noaio.header.length) { 
            NOA_OUT("f->next < frame...");
            continue;
        }

        if (NOAIO_checksum(frame) != NOAIO_retrieve_checksum(frame)) {
            NOA_OUT("invalid checksum: transmitted: %02x, calculated: %02x",
                NOAIO_retrieve_checksum(frame), NOAIO_checksum(frame));
            DUMPHEX((byte*)frame, frame->noaio.header.length);
            f->next = NOAIO_shift_frame_to_start(f->buffer, f->next, 1);
            continue;
        }

        //DUMPHEX(f->buffer, frame->noaio.header.length);
        NOA_OUT("FRAME %d OK", frame_no++);
        f->shift = frame->noaio.header.length;
        return &frame->noaio;
    }

    NOA_OUT("FRAME %d ERR", frame_no++);
//    while(1);
    return NULL;
}

NOAIO_FRAME *
NOAIO_request(int out, int in, NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *outgoing,
              NOAIO_INCOMMING_FRAME *incomming, clock_t timeout_ms) {
    int n = write(out, outgoing, outgoing->noaio.header.length);
    if (n < 0) {
        perror("NOAIO_request");
        return NULL;
    }
    
    if (n != outgoing->noaio.header.length) {
        WARN(LOGS_TYPE_CAN, "failed to write %d bytes", outgoing->noaio.header.length);
    }
    
    NOAIO_FRAME *response = NOAIO_get_frame(in, incomming, timeout_ms);
    if (!response) {
        WARN(LOGS_TYPE_CAN, "no request echo received within %ld ms", timeout_ms);
        return NULL;
    }

    // interfejs CAN odsyla nam kazda wyslana ramke, dlatego
    // spodziewamy sie otrzymac kopie ramki, ktora przed chwila
    // wyslalismy
    
    if (memcmp(&outgoing->noaio, response,
               outgoing->noaio.header.length - sizeof(NOAIO_TX_START))) {
        DUMPHEX(&outgoing->noaio, outgoing->noaio.header.length
                - sizeof(NOAIO_TX_START));
        DUMPHEX(response, response->header.length - sizeof(NOAIO_TX_START));
        
        WARN(LOGS_TYPE_CAN, "request echo was not received (skipped)");
        if (response->header.request
            != NOAIO_RESPONSE_TO(outgoing->noaio.header.request)) {
        	WARN(LOGS_TYPE_CAN, "the response does not address the request");
            NOAIO_flush_input(in);
        }
        return response;
    }
    response = NOAIO_get_frame(in, incomming, timeout_ms);
    if (!response) {
        WARN(LOGS_TYPE_CAN, "no response received within %ld ms", timeout_ms);
        return NULL;
    }

    if (response->header.request
        != (NOAIO_RESPONSE_TO(outgoing->noaio.header.request)) ) 
    {

    	DUMPHEX(outgoing, outgoing->noaio.header.length);
        WARN(LOGS_TYPE_CAN, "the response does not address the request - different headers: got-0x%02X, exp-0x%02X", response->header.request, outgoing->noaio.header.request );
        DUMPHEX(response, response->header.length);
        NOAIO_flush_input(in);
    }

    return response;
}

void
NOAIO_flush_input(int infd) {
#if NOAIO_FLUSH == 1
    byte buffer[1024];
    int n, iterations = 0;
    do {
        n = read_with_timeout(infd, buffer, NELEMS(buffer), NOAIO_FLUSH_TIMEOUT);
        WARN(LOGS_TYPE_CAN, "Flushed %d bytes", n);
        if(++iterations > NOAIO_FLUSH_MAX_ITERATIONS) {
            ERROR(LOGS_TYPE_CAN, "Can flush");
            break;
        }
    } while(n > 0);
#endif
}

NOAIO_FRAME *
NOAIO_copy_frame(NOAIO_FRAME *frame) {
    size_t size = frame->header.length - 2*sizeof(NOAIO_TX) - sizeof(uint8_t);
    assert(size > 0);
    void *copy = malloc(size);
    memcpy(copy, frame, size);
    return copy;
}
