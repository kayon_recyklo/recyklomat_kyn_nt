#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <termios.h>
#include "iomap.h"
#include "util.h"

#define __USE_GNU
#include <stdlib.h>
#include <signal.h>
#include <execinfo.h>

bool is_lib_call( char * fun )
{
	return ( strstr(fun, "/lib/") || strstr(fun, "__print_trace") || strstr(fun, "__signal_handler") );
}

void __print_trace( void )
{
	void* trace[128];
	char ** functions;

	size_t size = backtrace(trace, NELEMS(trace));
	functions = backtrace_symbols(trace, size);

	fprintf(stderr, "\nStack trace:\n\n");

	for( size_t i=0; i<size; i++ )
	{
		if( is_lib_call(functions[i]) == 0 )
		{
			fprintf(stderr, "%s\n", functions[i]);
		}
	}

	fprintf(stderr, "\nStack trace with lib calls:\n\n");

	for( size_t i=0; i<size; i++ )
	{
		fprintf(stderr, "%s\n", functions[i]);
	}
}

void print_line( void * p )
{
	char cmd[200];
	sprintf(cmd, "addr2line -a %p -e %s", p, qps_exe_path);

	fprintf(stderr, "\nError located in:\n");
	system(cmd);
	fprintf(stderr, "\n");
}

void __signal_handler(int signo, siginfo_t *info, void *arg)
{
    ucontext_t *p=(ucontext_t *)arg;
    void * val;
    fprintf(stderr, "\nERROR: Signal %s received\n", strsignal(signo));
//    printf("siginfo address=%p\n",info->si_addr);

#ifdef REG_EIP
    val = (void*)p->uc_mcontext.gregs[REG_EIP];
#else
    val = (void*)p->uc_mcontext.arm_pc;
#endif

    __print_trace();
    print_line(val);

    abort();  
}

void register_signal( int signal, const struct sigaction * act )
{
	if( sigaction(signal, act, NULL) == -1 )
	{
		ERROR(LOGS_TYPE_INIT, "Unable to register handler to %s", strsignal(signal));
	}
	else
	{
		INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Handler %s", strsignal(signal)); 
	}
}

void exceptions_handler_init( void )
{
    struct sigaction sa;

    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = __signal_handler;
    sa.sa_flags   = SA_SIGINFO;

    register_signal(SIGFPE, &sa);
    register_signal(SIGSEGV, &sa);
    register_signal(SIGILL, &sa);
    register_signal(SIGBUS, &sa);
}
