#include "recyc.h"
#include "util.h"
#include "qps-ext-device-errors.h"
#include "qps-backend.h"
#include "qps-recognizer.h"
#include "qps-talk.h"

#include <stdio.h>

const char * qps_exe_path;

int main(int argc, const char *argv[]) {

	qps_exe_path = argv[0];
	exceptions_handler_init();

	MESSAGE("main start");

	char * arg = load_iomap_config( argv[1] );

    typedef void *(*thread)(void (*));

    BackendData backend_data;
    konfident->data = &backend_data;
    pthread_queue_init(&backend_data.queue);
    pthread_create(&backend_data.thread, NULL, (thread) QPS_backend, &backend_data);

	MESSAGE("backend start");

	errors_data_t err_data;
	pthread_queue_init(&err_data.queue);
	pthread_create(&err_data.thread, NULL, (thread)error_serwer, &err_data);

	MESSAGE("errors start");

    Recyc *recyc = Recyc_init(arg, konfident);

	MESSAGE("recyc start");

    QPS_talk_data qps_talk_data = {
    		.recyc = recyc,
            .client_socket = 0,
            .server_socket = 0,
    };

    pthread_queue_init(&qps_talk_data.queue);
    pthread_create(&qps_talk_data.thread, NULL, (thread) QPS_talk, &qps_talk_data);

	MESSAGE("talk start");

    // ==============================  inicjalizacja watku zapisujacego do pliku
    QPS_write_to_file log_data;

    pthread_queue_init(&log_data.to_write_queue);
    pthread_create(&log_data.thread, NULL, (thread) QPS_save_to_file, &log_data);


    MESSAGE("log start");
    // ==============================

    recyc->takeOver(recyc);
    return 0;
}
