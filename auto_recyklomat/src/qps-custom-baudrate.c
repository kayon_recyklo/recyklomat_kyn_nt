#include <stdint.h>

#include <stropts.h>
#include <asm/termios.h>

int set_custom_baudrate( int uart, uint32_t baud )
{
	struct termios2 tio;

	if( ioctl(uart, TCGETS2, &tio) == -1 ) return -1;
	
	tio.c_cflag &= ~CBAUD;
	tio.c_cflag |= BOTHER;
	tio.c_ispeed = baud;
	tio.c_ospeed = baud;

	if( ioctl(uart, TCSETS2, &tio) == -1 ) return -1;

	return 0;
}
