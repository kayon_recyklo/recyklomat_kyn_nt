/*
 * qps_logs.c
 *	+ watek pobierajacy wskaznik na stringa z logiem i zapisujacy go w pliku
 *	+ tworzenie logow i wkladanie ich do kolejki
 */

#include "qps.h"
#include "util.h"
#include "qps-logs.h"

#include <stdarg.h>		// va_list, va_start

QPS_write_to_file logs_str;

enum log_type{
	_OUT,
	_ERROR,
	_WARN
};

#define MAX_MSG_LEN 40
#define REM_SPACE(buf, ptr) (sizeof(buf)-(ptr-buf))

void send_log(const char* str){

#if SEND_TO_THE_FILE || SHOW_IN_COMMAND_LINE
	char* str_ptr = (char*)malloc(strlen(str)+1);
	strcpy(str_ptr, str);
	pthread_queue_push_back(&logs_str.to_write_queue, str_ptr);
#endif

}

void prepare_log(enum log_type type, const char* msg, const char* category, const char * status, va_list args){
	char str[512] = {0};
	char msg_buf[512] = {0};
	char * write_ptr = str;
	SET_NEW_TIME;

	if(type == _OUT){
		write_ptr += snprintf(write_ptr, REM_SPACE(str, write_ptr), "%04d-%02d-%02d %02d:%02d:%02d %-6s%-6s", tm.tm_year+1900,
													tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, "INFO", category);
	}
	else if(type == _ERROR){
		write_ptr += snprintf(write_ptr, REM_SPACE(str, write_ptr), "%04d-%02d-%02d %02d:%02d:%02d %-6s%-6s",
								tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, "ERROR", category);
	}
	else if(type == _WARN) {
		write_ptr += snprintf(write_ptr, REM_SPACE(str, write_ptr), "%04d-%02d-%02d %02d:%02d:%02d %-6s%-6s",
								tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, "WARN", category);
	}
	else {
		printf("Never goes here\n");
	}

	int size_to_be = vsnprintf(msg_buf, MAX_MSG_LEN+1, msg, args);
	// zapodaj gwiazdke
	if( size_to_be > MAX_MSG_LEN )
	{
		msg_buf[MAX_MSG_LEN-1] = '*';
	}


	write_ptr += snprintf(write_ptr, REM_SPACE(str, write_ptr), "%-40s", msg_buf);
	if( status )
	{
		write_ptr += snprintf(write_ptr, REM_SPACE(str, write_ptr), "%10s", status);
	}
	*write_ptr = '\n';

	send_log(str);	// wyzruca otrzymanego stringa na commandline i do kolejki zapisu do pliku
}

void INFO(const char* category, const char * status, const char* msg, ...){

	va_list args;
	va_start(args, msg);
	prepare_log(_OUT, msg, category, status, args);	// rowniez wysyla gotowego stringa co command line i do kolejki zapisu do piku
}

void ERROR(const char* category, const char* msg, ...){

	va_list args;
	va_start(args, msg);
	prepare_log(_ERROR, msg, category, LOGS_STATUS_FAIL, args);	// rowniez wysyla gotowego stringa co command line i do kolejki zapisu do piku
}

void WARN(const char* category, const char* msg, ...){

	va_list args;
	va_start(args, msg);
	prepare_log(_WARN, msg, category, LOGS_STATUS_FAIL, args);	// rowniez wysyla gotowego stringa co command line i do kolejki zapisu do piku
}


/*	watek: przyjmuje wskaznik na string z kolejki robiac pop_front i zapisuje string do pliku
 */
THREAD void *QPS_save_to_file( void* arg ){
	while(1){
		char* str = (char *)pthread_queue_pop_front(&logs_str.to_write_queue);

#if SHOW_IN_COMMAND_LINE
	fprintf(stderr, "%s", str);					// wysylanie loga na command line
	fflush(stderr);
#endif
#if SEND_TO_THE_FILE
		memset(today, 0, sizeof(today));	// zeruje calego today[]
		memset(recyc_new, 0, sizeof(recyc_new));	// zeruje calego recyc_new[]
		memset(recyc_old, 0, sizeof(recyc_old));	// zeruje calego recyc_old[]

		char* ptr = today;

		SET_NEW_TIME;	// potrzebny do nazw plikow .log

		if(ENABLE_SET_PATH)
			ptr += sprintf(ptr, LOG_PATH);
		ptr += sprintf(ptr, "%04d-%02d-%02d.log", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday);
		if(file == NULL){
			file = fopen(today, "a");
		}
		else{
			GET_FILE_SIZE(file);
			if(size>LOG_B_LIMIT){

				sprintf(recyc_new, NAZWA, tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday);
				sprintf(recyc_old, NAZWA_OLD, tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday);
				rename(recyc_new, recyc_old);

				file = fopen(today, "a");
			}
		}
		if(file == NULL){
			printf("the PATH is probably wrong\n");
		}
		else{
			fprintf(file, "%s", str);
			fflush(file);
		}
#endif
		free(str);
	}
	return NULL;
}
