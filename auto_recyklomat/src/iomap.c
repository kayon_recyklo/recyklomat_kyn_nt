/*
 * iomap.c
 *
 *  Created on: Aug 27, 2019
 *      Author: Marek Mikulski
 *
 *  Kod jest wlasnoscia firmy Kayon
 */
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
#include <string.h>

#include "qps.h"
#include "pthread-fifo.h"

#define IOMAP_C
#define QPS_DEFINE_REFLEX_TABLE
#include "iomap.h"
#include "qps-conveyor-devices.h"

/*
 * 	.device to numer iodrivera -> patrz start_qps.sh
 * 	.out to wyjscie iodrivera 0 - 7
 * 	.min i .max_limit_mA to limity pradowe podawane w mA
 */

void conveyor_out_ovc_callback( iodriver_config * config, Recyc * recyc );
void conveyor_current_ok_callback( iodriver_config * config, Recyc * recyc );

extern conveyor_device_t pet_crusher;
extern conveyor_device_t alu_crusher;
							// ------------------------ DEVICE 0
struct iodriver_config pet_crusher_open = {			// PET CRUSHER
		.device = 0,
		.out = NOAIO_OUTPUT_1,	// inicjalizuje sie w iomap.h
		.init_state = OFF,
		.min_limit_mA = 0,
		.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 250,
		.current_measure = 1
};

struct iodriver_config pet_crusher_close = {
		.device = 0,
		.out = NOAIO_OUTPUT_2,	// inicjalizuje sie w iomap.h
		.init_state = OFF,
		.min_limit_mA = 0,
		.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 250,
		.current_measure = 1
};

iodriver_input_info_t pet_crusher_current_sense = {
		.device = 4,
		.in = NOAIO_ANALOG_INPUT_A,

		.max_voltage_mv = 700,
		.ovc_cnt = 0,
		.max_ovc_cnt = 3,

		.arg = &pet_crusher
};

iodriver_input_info_t alu_crusher_current_sense = {
		.device = 4,
		.in = NOAIO_ANALOG_INPUT_B,

		.max_voltage_mv = 1000,
		.ovc_cnt = 0,
		.max_ovc_cnt = 3,

		.arg = &alu_crusher
};

struct iodriver_config alu_crusher_open = {			// ALU CRUSHER
	.device = 0,
	.out = NOAIO_OUTPUT_3,	// inicjalizuje sie w iomap.h
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 250,
		     .current_measure = 1	
};

struct iodriver_config alu_crusher_close = {
	.device = 0,
	.out = NOAIO_OUTPUT_4,	// inicjalizuje sie w iomap.h
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 250,
	.current_measure = 1
};
/*
struct iodriver_config dropbox_door_lock = {
	.device = 0,
	.out = NOAIO_UNINITIALIZED,
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 50,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 50,
	.current_measure = 0
};
*/
struct iodriver_config dropbox_conveyor = {			// DROPBOX
	.device = 0,
	.out = NOAIO_OUTPUT_6,
	.init_state = OFF,
	.min_limit_mA = 506,
	.max_limit_mA = 4006,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1500,
	.current_measure = 0
};

struct iodriver_config dropbox_director = {
	.device = 0,
	.out = NOAIO_OUTPUT_7,
	.init_state = OFF,
	.min_limit_mA = 507,
	.max_limit_mA = 4007,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 2000,
	.current_measure = 0
};
						// ------------------------- DEVICE 1
struct iodriver_config printer = {				// PRINTER
		.device = 1,
		.out = NOAIO_OUTPUT_0,
		.init_state = ON,
		.min_limit_mA = 0,
		.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1000,
		.current_measure = 1 
};

struct iodriver_config dropbox_leds = {
	.device = 1,
	.out = NOAIO_OUTPUT_7,
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 2100,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 2100,
		.current_measure = 1 
};
								// ----------------------- DEVICE 2
struct iodriver_config dropbox_rolls = {
		.device = 2,
		.out = NOAIO_OUTPUT_0,
		.init_state = OFF,
		.min_limit_mA = 0,
		.max_limit_mA = 700,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 700,
		.current_measure = 1 
};

struct iodriver_config conveyor_speed_a = {		// CONVEYOR SPEED
		.device = 2,
		.out = NOAIO_OUTPUT_1,
		.init_state = OFF,
		.min_limit_mA = 0,
		.max_limit_mA = 50,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 30,
		.current_measure = 0 
};

struct iodriver_config conveyor_speed_b = {
		.device = 2,
		.out = NOAIO_OUTPUT_2,
		.init_state = ON,
		.min_limit_mA = 0,
		.max_limit_mA = 50,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 30,
		.current_measure = 0 
};

struct iodriver_config conveyor_speed_c = {
		.device = 2,
		.out = NOAIO_OUTPUT_3,
		.init_state = ON,
		.min_limit_mA = 0,
		.max_limit_mA = 50,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 30,
		.current_measure = 0 
};

struct iodriver_config conveyor_dir = {			// CONVEYOR DIRECTION
		.device = 2,
		.out = NOAIO_OUTPUT_4,
		.init_state = OFF,
		.min_limit_mA = 0,
		.max_limit_mA = 50,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 30,
		.current_measure = 0 
};

struct iodriver_config dropbox = {
	.device = 2,
	.out = NOAIO_OUTPUT_5,
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 200,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 200,
		.current_measure = 1 
};

struct iodriver_config dropbox_lock = {
	.device = 2,
	.out = NOAIO_OUTPUT_6,
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 200,
		.current_measure = 0 
};

struct iodriver_config conveyor_start = {		// CONVEYOR START
	.device = 2,
	.out = NOAIO_OUTPUT_7,
	.init_state = OFF,
	.min_limit_mA = 0,
	.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1500,
		.current_measure = 1,
	.ovc_callback = conveyor_out_ovc_callback,
	.current_ok_callback = conveyor_current_ok_callback
};

struct iodriver_config qps_display = {				// QPS
		.device = 3,
		.out = NOAIO_OUTPUT_1,
		.init_state = ON,
		.min_limit_mA = 0,
		.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1300,
		.current_measure = 1 
};

struct iodriver_config qps_pwr = {
		.device = 3,
		.out = NOAIO_OUTPUT_2,
		.init_state = ON,
		.min_limit_mA = 0,
		.max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1700,
		.current_measure = 1 
};

struct iodriver_config gla_director_close = {
    .device = 1,
    .out = NOAIO_OUTPUT_5,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};

struct iodriver_config gla_director_open = {
    .device = 1,
    .out = NOAIO_OUTPUT_6,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};
struct iodriver_config alu_director_close = {
    .device = 1,
    .out = NOAIO_OUTPUT_1,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};
struct iodriver_config alu_director_open = {
    .device = 1,
    .out = NOAIO_OUTPUT_2,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};
struct iodriver_config pet_director_close = {
    .device = 1,
    .out = NOAIO_OUTPUT_3,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};

struct iodriver_config pet_director_open = {
    .device = 1,
    .out = NOAIO_OUTPUT_4,
    .init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 300,
    .soft_min_limit_mA = 0,
    .soft_max_limit_mA = 50
};
struct iodriver_config default_config = {			// DEFAULT
	.init_state = OFF,
    .min_limit_mA = 0,
    .max_limit_mA = 5000,
                .soft_min_limit_mA = 0,
                .soft_max_limit_mA = 1000,
		.current_measure = 0 
};

struct iodriver_voltage_limits_t iodriver_voltage_limits[64] = {
	{.device = 0, .min_voltage_mv = 22000, .max_voltage_mv = 26000},
	{.device = 1, .min_voltage_mv = 22000, .max_voltage_mv = 26000},
	{.device = 2, .min_voltage_mv = 22000, .max_voltage_mv = 26000},
	{.device = 3, .min_voltage_mv = 10000, .max_voltage_mv = 14000},
	{.device = 4, .min_voltage_mv = 10000, .max_voltage_mv = 14000}
};

dropbox_time_info_t dropbox_times = {
	.delay_ms = 400,
	.open_time_ms = 1500,
	.min_rolls_time_ms = 500,
	.max_rolls_time_ms = 1500,
	.lock_time_ms = 2000,
	.rolls_time_ms = 2500,
	.conveyor_passage_time_ms = 4000,
	.led_time_ms = 1000,
	.door_safety_time_s = 7,
	.recognition_delay_ms = 1500,
	.next_recognition_delay_ms = 500
};


conveyor_time_info_t conveyor_times = {
	.conveyor_passage_time_ms = 25000,
	.reflex_timeout_s = 4,
	.first_reflex_timeout_s = 7,
	.reflex_high_state_timeout_s = 4,

	.arm_time_ms = 1000,
	.first_arm_closing_delay_ms = 6000,
	.arm_closing_delay_ms = 3500,

	.crashing_time_ms = 3000,
	.releasing_time_ms = 3500,

	.crashing_delay_ms = 1000,
	.releasing_delay_ms = 2000,

	.crasher_test_offset_ms = 600,
	.director_test_offset_ms = 300,
};

recyc_config_params_t recyc_config = {
	.can_pooling_perioid_ms = 200,
	.crusher_max_voltage_mv = 1500,
	.crusher_max_ovc_cnt = 3,
	.opto_max_error_cnt = 2,
        .can_baudrate = B57600,
};

// czasy stanu wysokiego na wyjsciu wyzwalajacym ruch kierownicy
#define QPS_ARM_TIME_MS		 					conveyor_times.arm_time_ms
#define QPS_FIRST_ARM_CLOSING_DELAY_MS					conveyor_times.first_arm_closing_delay_ms
#define QPS_ARM_CLOSING_DELAY_MS					conveyor_times.arm_closing_delay_ms
// czasy zgniatania i cofania wgniatacza
#define QPS_CRASHING_TIME_MS						conveyor_times.crashing_time_ms
#define QPS_RELEASING_TIME_MS						conveyor_times.releasing_time_ms

#define QPS_CRASHING_DELAY_TIME_MS					conveyor_times.crashing_delay_ms
#define QPS_RELEASING_DELAY_MS						conveyor_times.releasing_delay_ms

// opoznienie testow wzgledem ruchu urzadzen tasmociagu
#define QPS_CRUSHER_TEST_OFFSET_MS					conveyor_times.crasher_test_offset_ms
#define QPS_DIRECTOR_TEST_OFFSET_MS					conveyor_times.director_test_offset_ms

conveyor_device_t gla_director = {
	.name = "GLA director",
	.device_id = GLA_DIRECTOR,
	.status = CONVEYOR_DEVICE_UNINITIALIZED,

//       	.before_open_delay = 0, 
//	.opening_time = QPS_ARM_TIME_MS,	
//	.before_close_delay = QPS_FIRST_ARM_CLOSING_DELAY_MS,
//	.closing_time = QPS_ARM_TIME_MS,
//	.test_offset = QPS_DIRECTOR_TEST_OFFSET_MS,

	.open_out = {
		.device_idx = 1, 
		.channel = NOAIO_CHANNEL_OUT6,
	},

	.open_sensor = {
		.device_idx = 1,
		.channel = NOAIO_CHANNEL_INA, 
	},

	.close_out = {
		.device_idx = 1,
		.channel = NOAIO_CHANNEL_OUT5,
	},

	.close_sensor = {
		.device_idx = 1,
		.channel = NOAIO_CHANNEL_INB,
	},
};

conveyor_device_t pet_director = {
	.name = "PET director",
	.device_id = PET_DIRECTOR,
	.status = CONVEYOR_DEVICE_UNINITIALIZED,

//       	.before_open_delay = 0, 
//	.opening_time = QPS_ARM_TIME_MS,	
//	.before_close_delay = QPS_ARM_CLOSING_DELAY_MS,
//	.closing_time = QPS_ARM_TIME_MS,
//	.test_offset = QPS_DIRECTOR_TEST_OFFSET_MS,

	.open_out = {
		.device_idx = 1, 
		.channel = NOAIO_CHANNEL_OUT4,
	},

	.open_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED, 
	},

	.close_out = {
		.device_idx = 1,
		.channel = NOAIO_CHANNEL_OUT3,
	},

	.close_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED,
	},
};

conveyor_device_t alu_director = {
	.name = "ALU director",
	.device_id = ALU_DIRECTOR,
	.status = CONVEYOR_DEVICE_UNINITIALIZED,

 //      	.before_open_delay = 0, 
//	.opening_time = QPS_ARM_TIME_MS,	
//	.before_close_delay = QPS_ARM_CLOSING_DELAY_MS,
//	.closing_time = QPS_ARM_TIME_MS,
//	.test_offset = QPS_DIRECTOR_TEST_OFFSET_MS,

	.open_out = {
		.device_idx = 1, 
		.channel = NOAIO_CHANNEL_OUT2,
	},

	.open_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED, 
	},

	.close_out = {
		.device_idx = 1,
		.channel = NOAIO_CHANNEL_OUT1,
	},

	.close_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED,
	},
};

conveyor_device_t pet_crusher = {
	.name = "PET crusher",
	.device_id = PET_CRUSHER,
	.status = CONVEYOR_DEVICE_UNINITIALIZED,

 //      	.before_open_delay = QPS_CRASHING_DELAY_TIME_MS, 
//	.opening_time = QPS_CRASHING_TIME_MS,	
//	.before_close_delay = QPS_RELEASING_DELAY_MS,
//	.closing_time = QPS_RELEASING_TIME_MS,
//	.test_offset = QPS_CRUSHER_TEST_OFFSET_MS,

	.open_out = {
		.device_idx = 0, 
		.channel = NOAIO_CHANNEL_OUT1,
	},

	.open_sensor = {
		.device_idx = 4,
		.channel = NOAIO_CHANNEL_IND, 
	},

	.close_out = {
		.device_idx = 0,
		.channel = NOAIO_CHANNEL_OUT2,
	},

	.close_sensor = {
		.device_idx = 4,
		.channel = NOAIO_CHANNEL_IND,
	},
};

conveyor_device_t alu_crusher = {
	.name = "ALU crusher",
	.device_id = ALU_CRUSHER,
	.status = CONVEYOR_DEVICE_UNINITIALIZED,

 //      	.before_open_delay = QPS_CRASHING_DELAY_TIME_MS, 
//	.opening_time = QPS_CRASHING_TIME_MS,	
//	.before_close_delay = QPS_RELEASING_DELAY_MS,
//	.closing_time = QPS_RELEASING_TIME_MS,
//	.test_offset = QPS_CRUSHER_TEST_OFFSET_MS,

	.open_out = {
		.device_idx = 0, 
		.channel = NOAIO_CHANNEL_OUT3,
	},

	.open_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED,
	},

	.close_out = {
		.device_idx = 0,
		.channel = NOAIO_CHANNEL_OUT4,
	},

	.close_sensor = {
		.device_idx = NOAIO_UNINITIALIZED,
		.channel = NOAIO_UNINITIALIZED,
	},
};

iodriver_input_info_t pet_opto_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t pet_director_open_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t pet_director_close_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t alu_opto_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t alu_director_open_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t alu_director_close_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t oth_opto_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t gla_director_open_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t gla_director_close_sensor_input = {
    .device = 0,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t pet_crusher_open_sensor_input = {
    .device = 4,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t pet_crusher_close_sensor_input = {
    .device = 4,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t alu_crusher_open_sensor_input = {
    .device = 4,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t alu_crusher_close_sensor_input = {
    .device = 4,
    .in = NOAIO_UNINITIALIZED
};

iodriver_input_info_t door_input = {
    .device = 1,
    .in = NOAIO_INPUT_C
};

QPSReflex QPS_reflex_table[] = {
	{ 
		.itemType = "GLA\n", 
		.input = { .device_index = NOAIO_UNINITIALIZED, .bit = NOAIO_UNINITIALIZED },
          
		.director = &gla_director,
		.crusher = NULL,
		
		.prev = NULL
	},
	
	{ 
		.itemType = "PET\n", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_A },
		
		.director = &pet_director,
		.crusher = &pet_crusher,

#if QPS_OPTOS != QPS_NO_FIRST_OPTO
		.prev = &QPS_reflex_table[0],
#else
		.prev = NULL,
#endif
	},

    	{ 
		.itemType = "ALU\n", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_B },
    	
		.director = &alu_director,
		.crusher = &alu_crusher,

		.prev = &QPS_reflex_table[1],
    	},
        
	{ 
		.itemType = "DEO\n", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_C },
          
		.director = NULL,
		.crusher = NULL,

		.prev = &QPS_reflex_table[2],
	}
};



QPSInitialAction QPS_small_items[] = {
	    { .itemType = "SYR\n", .action = NULL},
	        { .itemType = "BAT\n", .action = NULL},
};

static void update_conveyor_device( conveyor_device_t * device, iodriver_input_info_t * open_sensor, 
                                    iodriver_input_info_t * close_sensor, iodriver_config * open_out, iodriver_config * close_out,
	       			    uint16_t before_open_delay, uint16_t opening_time, uint16_t before_close_delay, 
				    uint16_t closing_time, uint16_t test_offset)
{
    device->open_out.device_idx = open_out->device;
    device->open_out.channel = NOAIO_output_channel(open_out->out);
    
    device->close_out.device_idx = close_out->device;
    device->close_out.channel = NOAIO_output_channel(close_out->out);
   
    device->open_sensor.device_idx = open_sensor->device;
    device->open_sensor.channel = open_sensor->in;

    device->close_sensor.device_idx = close_sensor->device;
    device->close_sensor.channel = close_sensor->in; 

    device->before_open_delay = before_open_delay;
    device->opening_time = opening_time;
    device->before_close_delay = before_close_delay;
    device->closing_time = closing_time;
    device->test_offset = test_offset;
}

void update_reflex_input( QPSReflex * reflex, iodriver_input_info_t * in )
{
    reflex->input.bit = NOAIO_input(in->in);
    reflex->input.device_index = in->device;
}

void update_conveyor_devices_config( void )
{
    update_conveyor_device(&gla_director, &gla_director_open_sensor_input, 
        &gla_director_close_sensor_input, &gla_director_open, &gla_director_close,
	0, QPS_ARM_TIME_MS, QPS_FIRST_ARM_CLOSING_DELAY_MS, QPS_ARM_TIME_MS, QPS_DIRECTOR_TEST_OFFSET_MS);

    update_conveyor_device(&pet_director, &pet_director_open_sensor_input, 
        &pet_director_close_sensor_input, &pet_director_open, &pet_director_close,
	0, QPS_ARM_TIME_MS, QPS_ARM_CLOSING_DELAY_MS, QPS_ARM_TIME_MS, QPS_DIRECTOR_TEST_OFFSET_MS);

    update_conveyor_device(&alu_director, &alu_director_open_sensor_input, 
        &alu_director_close_sensor_input, &alu_director_open, &alu_director_close,
	0, QPS_ARM_TIME_MS, QPS_ARM_CLOSING_DELAY_MS, QPS_ARM_TIME_MS, QPS_DIRECTOR_TEST_OFFSET_MS);

    update_conveyor_device(&pet_crusher, &pet_crusher_open_sensor_input, 
        &pet_crusher_close_sensor_input, &pet_crusher_open, &pet_crusher_close,
	QPS_CRASHING_DELAY_TIME_MS, QPS_CRASHING_TIME_MS, QPS_RELEASING_DELAY_MS, QPS_RELEASING_TIME_MS, QPS_CRUSHER_TEST_OFFSET_MS);
    
    update_conveyor_device(&alu_crusher, &alu_crusher_open_sensor_input, 
        &alu_crusher_close_sensor_input, &alu_crusher_open, &alu_crusher_close,
	QPS_CRASHING_DELAY_TIME_MS, QPS_CRASHING_TIME_MS, QPS_RELEASING_DELAY_MS, QPS_RELEASING_TIME_MS, QPS_CRUSHER_TEST_OFFSET_MS);

    for( uint8_t i=0; i<NELEMS(QPS_reflex_table); i++ )
    {
        QPSReflex * reflex = &QPS_reflex_table[i];

        SWITCH(reflex->itemType, {
            CASE(ItemTypePET)
            {
                update_reflex_input(reflex, &pet_opto_input);
            }
            CASE(ItemTypeALU)
            {
                update_reflex_input(reflex, &alu_opto_input);
            }
            CASE(ItemTypeDEO)
            {
                update_reflex_input(reflex, &oth_opto_input);
            }
        });        
    }
}

