/*
 * sockets.c
 *
 *  Created on: May 24, 2019
 *      Author: Fixed
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/un.h>
#include <stdio.h>


#include "sockets.h"
#include "util.h"

int init_socket(char* name) {
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Initializing %s socket", name);
	int sockfd;
	struct sockaddr_un;
	if ((sockfd = socket(AF_UNIX,SOCK_STREAM,0)) < 0) {
		ERROR(LOGS_TYPE_INIT, "Failed to initialize %s socket", name);
	}
	else {
		INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "%s socket initialized", name);
	}
	return sockfd;
}

bool socket_client_conn(int socket, char* path, char* name) {
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Initializing %s socket connection at %s", name, path);
	int servlen;
	struct sockaddr_un  serv_addr;
	bzero((char *)&serv_addr,sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, path);
	servlen = strlen(serv_addr.sun_path) +
				 sizeof(serv_addr.sun_family);
	if (connect(socket, (struct sockaddr *)
						 &serv_addr, servlen) < 0) {
		ERROR(LOGS_TYPE_INIT, "Failed to connect to %s socket", name);
		return 0;
	}
	else {
		INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "%s socket connection initialized", name);
		return 1;
	}
}

void socket_server_bind(int socket, char* path, char* name) {
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Binding %s", name);
	int servlen;
	struct sockaddr_un  serv_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, path);
	servlen=strlen(serv_addr.sun_path) +
					 sizeof(serv_addr.sun_family);
	if(bind(socket,(struct sockaddr *)&serv_addr,servlen)<0) {
		ERROR(LOGS_TYPE_SOCKET, "Failed to bind %s socket", path);
	}
	else {
		INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_OK, "%s socket binded", name);
	}
}

int socket_server_wait_for_connection(int socket, int possible_connections,
		char* name) {
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Waiting connection to %s", name);
	struct sockaddr_un  cli_addr;
	socklen_t clilen;
	listen(socket,possible_connections);
	clilen = sizeof(cli_addr);
	int new_socket = accept(
		socket,(struct sockaddr *)&cli_addr,&clilen);
	if (new_socket < 0) {
		ERROR(LOGS_TYPE_INIT, "%d socket connection", socket);
	}
	else {
		INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "%s socket connection", name);
	}
	return new_socket;
}

int blocking_write_to_socket(int socket, char* msg, int msg_size) {
	int n = write(socket, msg, msg_size);
	return n;
}

int blocking_read_from_socket(int socket, char* buf, int buf_size) {
	int n = read(socket, buf, buf_size);
	return n;
}

void close_socket_conn(int socket, char* name){
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Closing %s socket connection", name);
	close(socket);
}
void close_server_socket(int socket, char* name) {
	INFO(LOGS_TYPE_SOCKET, LOGS_STATUS_NONE, "Closing %s socket", name);
	close(socket);
}
