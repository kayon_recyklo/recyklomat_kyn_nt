/*
 * qps-backend.c
 *
 *  Created on: Mar 15, 2019
 *      Author: Fixed
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/un.h>
#include <stdio.h>
#include "qps-backend.h"
#include <stdarg.h>
#include <unistd.h>
#include "sockets.h"

Supervisor *konfident = &(Supervisor) {
	.report = process_report,
	.data = NULL
};

typedef enum {
	recognized_item = 0,
	scanned_ID = 1,
	hatch_closed = 2,
	hatch_opened = 3,
} BE_event_type;

#define MAX_EVENT_STR_SIZE 30

char backend_events[][MAX_EVENT_STR_SIZE] = {
		"recognized",
		"qr_code",
		"hatch_close",
		"hatch_open",
};

typedef struct {
	BE_event_type type;
	char arg[40];
}BE_event;

void process_report(Supervisor *self, Report condition, ...) {
	va_list arg_list;
	va_start(arg_list, condition);

	BackendData *backend_data = (BackendData*) self->data;
	//INFO(LOGS_TYPE_SV, "Processing raport");
	switch(condition) {
	case ReportRecognizedItem:
	{
		ItemType item_type = va_arg(arg_list, ItemType);
		BE_event *event = MAKE(BE_event,
								.type = recognized_item);
		char be_item_type[5] = "";
		strcpy(be_item_type, item_type);
		memset(be_item_type+3, '\n', 1);
		strcpy(event->arg, be_item_type);
		pthread_queue_push_back(&backend_data->queue, event);
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting %s recognition", item_type);
	}

	break;
	case ReportDoorsClosed:
	{
		BE_event *event = MAKE(BE_event,
								.type = hatch_closed);
		pthread_queue_push_back(&backend_data->queue, event);
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting closed doors");
	}
	break;
	case ReportDoorsOpened:
	{
		BE_event *event = MAKE(BE_event,
								.type = hatch_opened);
		pthread_queue_push_back(&backend_data->queue, event);
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting opened doors");
	}
	break;
	case ReportNewUserID:
	{
		char* scanned_id = va_arg(arg_list, char*);
		BE_event *event = MAKE(BE_event,
								.type = scanned_ID);
		strcpy(event->arg, scanned_id);
		pthread_queue_push_back(&backend_data->queue, event);
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting new user");
	}
	break;
	case ReportStatusIdle:
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting status: IDLE");
		break;
	case ReportStatusProcessing:
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting status: PROCESSING");
		break;
	case ReportStatusError:
		INFO(LOGS_TYPE_SV, LOGS_STATUS_NONE, "Reporting status: ERROR");
		break;
	default:
		ERROR(LOGS_TYPE_SV, "WRONG REPORT");
		break;
	}
	va_end(arg_list);
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

#define POSSIBLE_CONNECTIONS 1
#define MAX_MSG_SIZE 3

THREAD void *QPS_backend(BackendData *backend_data) {


	char be_socket_name[] = "backend";
	int BE_socket = init_socket(be_socket_name);
	socket_server_bind(BE_socket, BE_SOCKET_PATH, be_socket_name);
	
    while(1) {
        int BE_conn_socket =
                socket_server_wait_for_connection(BE_socket, 
                                                  POSSIBLE_CONNECTIONS,
                                                  be_socket_name);
        if(BE_conn_socket == -1) {
            WARN(LOGS_TYPE_BE, "Unexpected fail during waiting for backend connection");
        }
        else {
            while(1) {
                usleep(1000L*1000);
                BE_event *event = 
                    (BE_event*)pthread_queue_pop_front(&backend_data->queue);
                char BE_str_to_send[100];
                memset(BE_str_to_send, 0, 100);
                sprintf(BE_str_to_send, 
                        "%s %s\n", 
                        backend_events[event->type], 
                        event->arg);
                int size_of_BE_message = strlen(BE_str_to_send);
                /*
		INFO(LOGS_TYPE_BE, "message(len: %d): %s", 
                     size_of_BE_message, 
                     BE_str_to_send);
                */
		free(event);
                
                int n = send(BE_conn_socket, 
                             BE_str_to_send, 
                             size_of_BE_message,
                             MSG_NOSIGNAL);
                if(n == -1) {
                    ERROR(LOGS_TYPE_BE, "Unable to write to backend socket");
                    break;
                }
            }
        }
    }

}

