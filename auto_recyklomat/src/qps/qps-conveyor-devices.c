#include "qps-command.h"
#include "util.h"
#include "pthread-queue.h"
#include "pthread-timer.h"
#include "pthread-fifo.h"
#include "iomap.h"

#include "qps-conveyor-devices.h"

static void device_open( conveyor_device_t * self );
static void device_close( conveyor_device_t * self );
static void device_set_status( conveyor_device_t * self, conveyor_device_status_value_t new_status );
static conveyor_device_status_value_t device_get_status( conveyor_device_t * self );
static void device_set_next_status( conveyor_device_t * self, conveyor_device_status_value_t new_status );
static conveyor_device_status_value_t device_get_next_status( conveyor_device_t * self );
static void device_next_cycle_step( conveyor_device_t * self );
static bool device_is_normal_cycle( conveyor_device_t * device );
static void device_set_normal_cycle( conveyor_device_t * device, bool value );

#define THREAD_SAFE_BLOCK(mutex, block) {pthread_mutex_lock(mutex); \
	block; \
	pthread_mutex_unlock(mutex); }

// czesc maszyny stanow
// odpowiada za przejscie do kolejnego stanu, jesli wszystko jest ok
static void device_check_for_error( conveyor_device_t * device )
{
	if( device->hw_status_ok(device) )		// jesli status urzadzenia jest prawidlowy to przechodzi do kolejnego stanu
	{
		conveyor_device_status_value_t next_status = device_get_next_status(device);
		device_set_status(device, next_status);
		device_next_cycle_step(device);
	}
	else						// jesli stan urzadzenia nie jest prawidlowy to przechodzi do procedury obslugi bledu
	{
		device->on_error(device);
	}
}

static void check_state_after( conveyor_device_t * device, uint16_t time_ms )
{
	pthread_after_ms(time_ms, 
			(delayed_procedure_t) device_check_for_error,
			device);
}

// obudowa funkcjonalnosci enable_for_perioid
static void QPS_enable_pin( IODriver_out_t * pout, QPSRecycData * qps, uint16_t after_ms, uint16_t for_ms )
{
	IODriver * device = (IODriver*)&qps->devices[pout->device_idx];

	QPSRemote *remote = NEW(QPSRemote);

	NOAIO_DECLARE_REQUEST_FRAME(command,
					ENABLE_FOR_PERIOD,
					device->address,
					.channel = pout->channel,
					.period_ms = for_ms);
	remote->io = device;
	
	memcpy(&remote->command, &command, sizeof(command));

	pthread_after_ms(after_ms,
			(delayed_procedure_t) QPS_send_then_release,
			remote);
}

#define TEST_STATUS_DISABLED(self) if( self->get_status(self) == CONVEYOR_DEVICE_DISABLED ){ \
										INFO(LOGS_TYPE_CONV, LOGS_STATUS_NONE, "device %s is disabled", self->name); \
										return; \
									}

// ogolna funkcja wywolujaca ruch urzadzenia
// punkt wyjscia obslugi bledow
static void device_move( conveyor_device_t * self, uint16_t before_delay, conveyor_device_status_value_t target_state )
{
	IODriver_out_t * pout;
	uint16_t duration;

	conveyor_device_status_value_t status = device_get_status(self);

	switch( target_state )
	{
		case CONVEYOR_DEVICE_OPENED:
			if( status == CONVEYOR_DEVICE_CONFIRMED_OPENED ) return;

			pout = &self->open_out;
			duration = self->opening_time;

			device_set_status(self, CONVEYOR_DEVICE_OPENING);
			device_set_next_status(self, CONVEYOR_DEVICE_CONFIRMED_OPENING);
		break;
		case CONVEYOR_DEVICE_CLOSED:
			if( status == CONVEYOR_DEVICE_READY ) return;

			pout = &self->close_out;
			duration = self->closing_time;

			device_set_status(self, CONVEYOR_DEVICE_CLOSING);
			device_set_next_status(self, CONVEYOR_DEVICE_CONFIRMED_CLOSING);
		break;

		default:
		pout = NULL;
		duration = 0;
	}

	QPS_enable_pin(pout, self->qps, before_delay, duration);

	// zaplanowanie testu pozycji
	// test_offset jest uzyty tylko tutaj - przesuwa w czasie cala sekwencje testowa
	check_state_after(self, before_delay+self->test_offset);
}

static void device_open( conveyor_device_t * self )
{
	if( device_get_status(self) == CONVEYOR_DEVICE_DISABLED ) return;
	INFO(LOGS_TYPE_CONV, LOGS_STATUS_NONE, "Opening device: %s", self->name);	

	device_move(self, self->before_open_delay, CONVEYOR_DEVICE_OPENED);
}

static void device_close( conveyor_device_t * self )
{
	if( device_get_status(self) == CONVEYOR_DEVICE_DISABLED ) return;
	INFO(LOGS_TYPE_CONV, LOGS_STATUS_NONE, "Closing device: %s", self->name);	

	device_move(self, self->before_close_delay, CONVEYOR_DEVICE_CLOSED);
}

static void device_open_now( conveyor_device_t * self )
{
	if( device_get_status(self) == CONVEYOR_DEVICE_DISABLED ) return;
	INFO(LOGS_TYPE_CONV, LOGS_STATUS_NONE, "opening now %s", self->name);

        device_set_normal_cycle(self, 0);
	device_move(self, 0, CONVEYOR_DEVICE_OPENED);
}

void device_close_now( conveyor_device_t * self )
{
	if( device_get_status(self) == CONVEYOR_DEVICE_DISABLED ) return;
	INFO(LOGS_TYPE_CONV, LOGS_STATUS_NONE, "closing now %s", self->name);

        device_set_normal_cycle(self, 0);
	device_move(self, 0, CONVEYOR_DEVICE_CLOSED);
}

static void device_set_status( conveyor_device_t * self, conveyor_device_status_value_t new_status )
{
	THREAD_SAFE_BLOCK( &self->status.mutex, {
		self->status.value = new_status;
	});
}

static conveyor_device_status_value_t device_get_status( conveyor_device_t * self )
{
	conveyor_device_status_value_t status;

	THREAD_SAFE_BLOCK( &self->status.mutex, {
		status = self->status.value;
	});

	return status;
}

static bool device_is_normal_cycle( conveyor_device_t * device )
{
    bool retval;

    pthread_mutex_lock(&device->status.mutex);
    retval = device->status.is_normal_cycle;
    pthread_mutex_unlock(&device->status.mutex);

    return retval;
}

static void device_set_normal_cycle( conveyor_device_t * device, bool value )
{
    pthread_mutex_lock(&device->status.mutex);
    device->status.is_normal_cycle = value;
    pthread_mutex_unlock(&device->status.mutex);
}       

static void device_set_next_status( conveyor_device_t * self, conveyor_device_status_value_t new_status )
{
	THREAD_SAFE_BLOCK( &self->status.mutex, {
		self->status.next_status = new_status;
	});
}

static conveyor_device_status_value_t device_get_next_status( conveyor_device_t * self )
{
	conveyor_device_status_value_t status;

	THREAD_SAFE_BLOCK( &self->status.mutex, {
		status = self->status.next_status;
	});

	return status;
}

static void device_set_error_mode(conveyor_device_t* self, conveyor_device_error_mode_t error_mode){
	
	pthread_mutex_lock(&self->error_info.mutex);
	self->error_info.error_mode = error_mode;
	pthread_mutex_unlock(&self->error_info.mutex);

}	
static conveyor_device_error_mode_t device_get_error_mode(conveyor_device_t* self){
	conveyor_device_error_mode_t error_mode;

	pthread_mutex_lock(&self->error_info.mutex);
	error_mode = self->error_info.error_mode;
	pthread_mutex_unlock(&self->error_info.mutex);

	return error_mode;
	
}	

static conveyor_device_phase_t device_current_phase( conveyor_device_t * device )
{
	conveyor_device_status_value_t status = device->get_status(device);
	// aktualna faza to otwieranie
	if( status == CONVEYOR_DEVICE_OPENING ||
		status == CONVEYOR_DEVICE_OPENED ||
		status == CONVEYOR_DEVICE_CONFIRMED_CLOSING ||
		status == CONVEYOR_DEVICE_CONFIRMED_OPENED )
	{
		return CONVEYOR_DEVICE_PHASE_OPENING;
	}

	// aktualna faza to otwieranie
	if( status == CONVEYOR_DEVICE_CLOSING ||
		status == CONVEYOR_DEVICE_CLOSED ||
		status == CONVEYOR_DEVICE_CONFIRMED_OPENING ||
		status == CONVEYOR_DEVICE_CONFIRMED_CLOSED  ||
		status == CONVEYOR_DEVICE_READY )
	{
		return CONVEYOR_DEVICE_PHASE_CLOSING;
	}

	return CONVEYOR_DEVICE_PHASE_NONE;
}

static void device_repeat_phase( conveyor_device_t * device )
{
	conveyor_device_phase_t phase = device_current_phase(device);

	if( phase == CONVEYOR_DEVICE_PHASE_OPENING )
	{
		device->open_now(device);
		return;
	}

	if( phase == CONVEYOR_DEVICE_PHASE_CLOSING )
	{
		device->close_now(device);
		return;
	}
}

static void device_start_other_phase( conveyor_device_t * device )
{
	conveyor_device_phase_t phase = device_current_phase(device);

	if( phase == CONVEYOR_DEVICE_PHASE_OPENING )
	{
		device->close_now(device);
		return;
	}

	if( phase == CONVEYOR_DEVICE_PHASE_CLOSING )
	{
		device->open_now(device);
		return;
	}
}

static void device_start_cycle( conveyor_device_t * self )
{
	if( device_get_status(self) != CONVEYOR_DEVICE_READY ) return;

	self->error_info.local_error = CONVEYOR_DEVICE_NO_ERROR;
	self->error_info.error_mode = CONVEYOR_DEVICE_NEM;
	self->error_info.try_number = 0;

        device_set_normal_cycle(self, 1);
	device_open(self);
}

static void device_start_next( conveyor_device_t * self )
{
	conveyor_device_t * next = self->next_device;

	if( next ) device_start_cycle(next);
}

// czesc maszyny stanow
// odpowiada za ewentualne wlaczenie urzadzen, przejscie ze stanu potwierdzonego do teoretycznego i
// ustawienie kolejnego stanu do potwierdzenia
static void device_next_cycle_step( conveyor_device_t * self )
{
	conveyor_device_status_value_t status = device_get_status(self);

	switch( status )
	{
		case CONVEYOR_DEVICE_CONFIRMED_OPENING:
			device_set_status(self, CONVEYOR_DEVICE_OPENED);
			device_set_next_status(self, CONVEYOR_DEVICE_CONFIRMED_OPENED);
			check_state_after(self, self->opening_time);
		break;

		case CONVEYOR_DEVICE_CONFIRMED_OPENED:
			if( device_is_normal_cycle(self) )
			{
				device_close(self); // start sprawdzania stanu juz tu jest
			}
			self->on_phase_complete(self->arg);
		break;

		case CONVEYOR_DEVICE_CONFIRMED_CLOSING:
			device_set_status(self, CONVEYOR_DEVICE_CLOSED);
			device_set_next_status(self, CONVEYOR_DEVICE_CONFIRMED_CLOSED);
			check_state_after(self, self->closing_time);
		break;

		case CONVEYOR_DEVICE_CONFIRMED_CLOSED:
			device_set_status(self, CONVEYOR_DEVICE_READY);
			self->on_phase_complete(self->arg);
			
                        if( device_is_normal_cycle(self) )
                        {
                            self->on_cycle_complete(self->arg);
                            device_start_next(self);	
                        }
		break;

		default:
			self->on_error(self);
	}
}

static bool device_default_error_check( conveyor_device_t * self )
{
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "device %s status checked", self->name);
	return 1;
}

static void device_default_on_error( conveyor_device_t * self )
{
    ERROR(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "device %s failed", self->name);
}

static void device_default_on_cycle_complete( conveyor_device_t * self )
{
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "device %s cycle complete", self->name);
}

static void device_default_on_phase_complete( conveyor_device_t * self )
{
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "device %s phase complete", self->name);
}

void conveyor_device_set_pin_state( IODriver_out_t * pout, QPSRecycData * qps, bool state )
{
	IODriver * device = (IODriver*)&qps->devices[pout->device_idx];

	QPSRemote *remote = NEW(QPSRemote);
	remote->io = device;
	
	if( state )
	{	
		NOAIO_DECLARE_REQUEST_FRAME(command,
						ENABLE_OUTPUT,
						device->address,
						.channel = pout->channel);
		memcpy(&remote->command, &command, sizeof(command));
	}
	else
	{
		NOAIO_DECLARE_REQUEST_FRAME(command,
						DISABLE_OUTPUT,
						device->address,
						.channel = pout->channel);
		memcpy(&remote->command, &command, sizeof(command));
	}
	QPS_send_then_release(remote);
}

void conveyor_device_init( conveyor_device_t * device, QPSRecycData * qps )
{
	if( device == NULL ) return;

	device->qps = qps;

	// ustawienie wskaxnika na recyc
	device->recyc = qps->recyc;

	// ruch
	device->start_working_cycle = device_start_cycle;
	device->open_now = device_open_now;
	device->close_now = device_close_now;
	device->open = device_open;
	device->close = device_close;
	device->repeat_phase = device_repeat_phase;
	device->start_other_phase = device_start_other_phase;

	// status
	device->get_status = device_get_status;	
	device->get_next_status = device_get_next_status;
	device->set_status = device_set_status;
	device->get_current_phase = device_current_phase;

	// error_mode
	device->get_error_mode = device_get_error_mode;
	device->set_error_mode = device_set_error_mode;

	// bledy
	device->hw_status_ok = device_default_error_check;
	device->on_error = device_default_on_error;

	// error info domyslnie ustawione na opcje nie bylo errorow
	device->error_info.local_error = CONVEYOR_DEVICE_NO_ERROR;
	device->error_info.error_mode = CONVEYOR_DEVICE_NEM;
	device->error_info.try_number = 0;
	device->error_info.max_try_number = 1;
	pthread_mutex_init(&device->error_info.mutex, NULL);

	// domyslnie trzyma wskaznik na siebie
	device->arg = device;

	// callbacki od zakonczenia faz ruchu
	device->on_cycle_complete = device_default_on_cycle_complete;
	device->on_phase_complete = device_default_on_phase_complete;

        // wlaczenie kolejnego urzadzenie w lancuchu
        device->start_next = device_start_next;

	device_set_status(device, CONVEYOR_DEVICE_READY);
}
