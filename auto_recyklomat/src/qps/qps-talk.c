/*
 * qps-talk.c
 *
 *  Created on: May 30, 2019
 *      Author: osboxes
 */
#include "qps.h"
#include "qps-talk.h"
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "iomap.h"
#include "sockets.h"

typedef struct {
    IODriver *source_device;
    int input_index;
    bool new_value;
} QPSInputEvent;

#define QUIT_CONSOLE "QC"
#define TEST_CMD "TC"
#define PET_RECOG "PR"
#define ALU_RECOG "AR"
#define GLA_RECOG "GR"
#define DEO_RECOG "DR"
#define BAT_RECOG "BR"
#define SYR_RECOG "SR"
#define OTH_RECOG "OR"
#define NO_RECOG "NR"
#define CLOSE_DOORS "CD"
#define OPEN_DOORS "OD"
#define RELOAD_CONFIG "RC"
#define EMPTY_MESSAGE ""
#define RELOAD_IOMAP "RI"

#define QPS_TALK_SOCKET_NAME "qps_talk"
#define MAX_CONNECTIONS 5
#define QPS_TALK_SOCKET_PATH "/tmp/talk-socket"

int QPS_execute_cmd(char* cmd, QPS_talk_data *qps_talk_data) {
    if(!strcmp(cmd, QUIT_CONSOLE)) {
        close_server_socket(qps_talk_data->client_socket, //client_socket,
                            QPS_TALK_SOCKET_NAME);
        return -1;
    }
    else if(!strcmp(cmd, TEST_CMD)) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Testing console communication");
    }
    else if(!strcmp(cmd, PET_RECOG)) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng PET recognition"); 
	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
	qps->recognition.mocking = true;
	strcpy(qps->recognition.mocked_recognition, ItemTypePET);
    }
    else if(!strcmp(cmd, ALU_RECOG)) {
    	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng ALU recognition"); 
	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeALU);
    }
    else if(!strcmp(cmd, GLA_RECOG)) {
	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng GLA recognition"); 
	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeGLA);
    }
    else if(!strcmp(cmd, DEO_RECOG)) {
    	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng DEO recognition");
	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeDEO);
    }
    else if(!strcmp(cmd, BAT_RECOG)) {
    	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng BAT recognition");
	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeBAT);
    }
    else if(!strcmp(cmd, SYR_RECOG)) {
	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng SYR recognition");
    	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeSYR);
    }
    else if(!strcmp(cmd, OTH_RECOG)) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulatng OTH recognition");
        QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        qps->recognition.mocking = true;
        strcpy(qps->recognition.mocked_recognition, ItemTypeOTH); 
    }
    else if(!strcmp(cmd, NO_RECOG)) {
        QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
	qps->recognition.mocking = false;
    }
    else if(!strcmp(cmd, CLOSE_DOORS)) {
        QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        IODriver *device = 
            &qps->devices[QPS_DROPBOX_DOOR_DETECTOR_DEVICE];
        int i = NOAIO_input_channel_index(NOAIO_input_channel(QPS_DROPBOX_DOOR_DETECTOR_IN));
        bool new_value = true;
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating closing doors");
        QPSInputEvent *event = MAKE(QPSInputEvent,
                                    .source_device = device,
                                    .input_index = i,
                                    .new_value = new_value);
        pthread_queue_push_back(device->UART->event_queue, event);
    }
    else if(!strcmp(cmd, OPEN_DOORS)) {
    	QPSRecycData *qps = (QPSRecycData *) qps_talk_data->recyc->data;
        IODriver *device = 
            &qps->devices[QPS_DROPBOX_DOOR_DETECTOR_DEVICE];
        int i = NOAIO_input_channel_index(NOAIO_input_channel(QPS_DROPBOX_DOOR_DETECTOR_IN));
        bool new_value = false;
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating opening doors");
        QPSInputEvent *event = MAKE(QPSInputEvent,
                                    .source_device = device,
                                    .input_index = i,
                                    .new_value = new_value);
        pthread_queue_push_back(device->UART->event_queue, event);
    }
    else if(!strcmp(cmd, RELOAD_CONFIG)) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Reloading configuration");
    }
    else if(!strcmp(cmd, EMPTY_MESSAGE)) {
        WARN(LOGS_TYPE_CONSOLE, "Unexpected console dissconect");
        close_server_socket(qps_talk_data->client_socket, 
                            QPS_TALK_SOCKET_NAME);
        return -1;
    }
    else if(!strcmp(cmd, RELOAD_IOMAP)) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Reloading IOmap");
    }
    else {
        WARN(LOGS_TYPE_CONSOLE, "Unexpected command from console");
    }
    return 0;
}


THREAD void *QPS_talk(QPS_talk_data *qps_talk_data) {
    qps_talk_data->server_socket = init_socket(QPS_TALK_SOCKET_NAME);
	socket_server_bind(qps_talk_data->server_socket, 
                       QPS_TALK_SOCKET_PATH,
                       QPS_TALK_SOCKET_NAME);
	char cmd[128] = {0};

	while(2) {
		qps_talk_data->client_socket = 
          socket_server_wait_for_connection(qps_talk_data->server_socket,
                                          MAX_CONNECTIONS,
                                          QPS_TALK_SOCKET_NAME);

        if ( qps_talk_data->client_socket == -1 ) {
            WARN(LOGS_TYPE_CONSOLE, "Unexpected fail during waiting for socket connection");
        }
        else {
            while(3) {
                memset(cmd, 0, sizeof(cmd));
                int n = 
                  blocking_read_from_socket(qps_talk_data->client_socket,
                                            cmd,
                                            sizeof(cmd));
                INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Console input(size: %d): %s", n, cmd);
                if(n == -1) {
                    WARN(LOGS_TYPE_CONSOLE, "Unexpected problem during socket read");
                    close_server_socket(qps_talk_data->client_socket,
                                        QPS_TALK_SOCKET_NAME);
                } 
                else {
                    int result = QPS_execute_cmd(cmd, qps_talk_data);
                    if (result == -1) {
                        break;
                    }
                }
            }
        }
    }
}
