/*
 * qps-recognizer.c
 *
 *  Created on: Apr 9, 2019
 *      Author: Fixed
 */

/* a server in the unix domain.  The pathname of
   the socket address is passed as an argument */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/un.h>
#include <stdio.h>
#include "qps-recognizer.h"

void error_2(const char *msg)
{
    perror(msg);
}

int QPS_init_recognizer_socket() {
	INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Initializing recognizer socket");
	int sockfd;
	struct sockaddr_un;
	if ((sockfd = socket(AF_UNIX,SOCK_STREAM,0)) < 0)
		error_2("creating socket");
	return sockfd;
}
void QPS_init_recognizer_client_conn(int socket) {
#if USE_RECOG 
	INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Initializing recognizer connection ");
	int servlen;
    struct sockaddr_un  serv_addr;

    bzero((char *)&serv_addr,sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, RECOG_SOCKET_PATH);
    servlen = strlen(serv_addr.sun_path) +
				 sizeof(serv_addr.sun_family);
   
    if (connect(socket, (struct sockaddr *)
						 &serv_addr, servlen) < 0)
	error_2("Connecting");
#endif
}
int QPS_init_recognizer_connection(int socket) {
	int sockfd = socket;
	int newsockfd, servlen;
	socklen_t clilen;
	struct sockaddr_un  cli_addr, serv_addr;
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, RECOG_SOCKET_PATH);
	servlen=strlen(serv_addr.sun_path) +
					 sizeof(serv_addr.sun_family);
	if(bind(sockfd,(struct sockaddr *)&serv_addr,servlen)<0)
		error_2("binding socket");
	listen(sockfd,1);
	clilen = sizeof(cli_addr);
	newsockfd = accept(
	   sockfd,(struct sockaddr *)&cli_addr,&clilen);
	if (newsockfd < 0)
	error_2("accepting");
	return newsockfd;
}
void QPS_init_recognizer(char* recognizer) {
    system(recognizer);
}

int QPS_ask_recognizer(int socket, char* cmd, char *item, uint8_t item_size) {
	int n;
	write(socket, cmd, 1);
	n = read(socket,item,item_size);
	return n;
}

void QPS_close_recognizer_socket(int socket) {
	close(socket);
}
