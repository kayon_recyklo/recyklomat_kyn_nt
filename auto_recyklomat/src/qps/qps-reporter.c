// serwer

/*
*	*** Błędy drukarki:
*	pnf - nie znaleziono drukarki
*	we - błąd zapisu
*	re - błąd odczytu
*	wc - błędna lista argumentów wywołania
*	pto - Printhead Thermistor Open
*	ifc - Invalid Firmware Config
*	pde - Printhead Detection Error
*	bpe - Bad Printhead Element
*	mot - Motor Over Temperature
*	pot - Printhead Over Temperature
*	cf - Cutter Fault
*	ho - Head Open
*	ro - Ribbon Out
*	mo - Media Out
*	cppf - Clear Paper Path Failed
*	pfe - Paper Feed Error
*	pnr - Presenter Not Running
*	pjdr - Paper Jam during Retract
*	bmnf - Black Mark not Found
*	bmce - Black Mark Calibrate Error
*	rfto - Retract Function timed out
*	p - Paused
*
*	*** Warningi drukarki	
*	pnes - Paper-near-end Sensor
*	rp - Replace Printhead
*	cp - Clean Printhead
*	ntcm - Need to Calibrate Media
*	s1 - Sensor 1 (Paper before head)
*	s2 - Sensor 2 (Black mark)
*	s3 - Sensor 3 (Paper after head)
*	s4 - Sensor 4 (loop ready)
*	s5 - Sensor 5 (presenter)
*	s6 - Sensor 6 (retract ready)
*	s7 - Sensor 7 (in retract)
*	s8 - Sensor 8 (at bin)
*
*	*** info drukarki
*	drs - odpowiedź na nakaz resetu
*
*	*** Błędy skanera
*	to - timeout skanowania
*	ato - timeout ACK
*	we - bład zapisu
*	oe - błąd otwarcia urządzenia
*	cp - błąd konfiguracji portu COM
*	ae - błędna lista parametrów
*
*	*** info skanera
*	ok - wszystko działa	
*/


#include "sockets.h"
#include "util.h"
#include "qps-ext-device-errors.h"
#include "qps-ext-device-protocol.h"

#define SOCKET_NAME "reporter-socket"
#define MAX_CONNECTIONS 5

#define LOG_FORMAT "\"%s\" from device \"%s\""

void process_error_report( uint8_t type, char * device, char * report )
{
	if( report == NULL ) return;

	switch( type )
	{
		case GET_ERROR: ERROR(LOG_FORMAT, report, device);
		break;

		case GET_WARNING: WARN(LOG_FORMAT, report, device);
		break;
		 
		case GET_INFO: INFO(LOGS_TYPE_DRIVER, LOG_FORMAT, report, device);
		break;
	}
}

THREAD void * error_serwer( errors_data_t * data )
{
	unlink(ERROR_SOCKET_PATH);
	int serwer = init_socket(SOCKET_NAME);
	socket_server_bind(serwer, ERROR_SOCKET_PATH, SOCKET_NAME);

	report_t report;
	report_init(&report, "serwer", NULL, process_error_report);
	char dane[1024] = {0};

	while(2)
	{
		int klient = socket_server_wait_for_connection(serwer,
			       	MAX_CONNECTIONS, SOCKET_NAME);

		blocking_read_from_socket(klient, dane, sizeof(dane));

		report_process(&report, dane);
	
		close_server_socket(klient, SOCKET_NAME);
		bzero(dane, sizeof(dane));	
	}
}

