#include "qps-command.h"
#include "util.h"
#define QPS_DEBUG_NAMES
#include "iomap.h"
typedef struct QPSCommand {
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *command;
    void (*on_success)(NOAIO_FRAME *, struct QPSCommand *self);
    void (*on_failure)(NOAIO_FRAME *, struct QPSCommand *self);
    void *data;
} QPSCommand;

#define PTRY(pcall) if(pcall) { ERROR(LOGS_TYPE_CODE, #pcall " failed"); }

static void
QPS_unblock(NOAIO_FRAME *frame, QPSCommand *command) {
    PTRY(pthread_queue_push_back((pthread_queue_t *) command->data, NULL));
}

static void
QPS_nocopy_unblock(NOAIO_FRAME *frame, QPSCommand *command) {
    PTRY(pthread_queue_push_back((pthread_queue_t *) command->data, frame));
}

static void
QPS_copy_unblock(NOAIO_FRAME *frame, QPSCommand *command) {
    PTRY(pthread_queue_push_back((pthread_queue_t *) command->data,
                                 NOAIO_copy_frame(frame)));
}

bool
QPS_blocking_command_success(IODriver *device,
                     NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *cmd) {
    pthread_queue_t queue;
    PTRY(pthread_queue_init(&queue));

    QPSCommand command = {
        .command = cmd,
        .on_success = QPS_nocopy_unblock,
        .on_failure = QPS_unblock,
        .data = &queue
    };
    PTRY(pthread_queue_push_back(&device->UART->command_queue, &command));

    bool result = (bool) !!pthread_queue_pop_front(&queue);
    PTRY(pthread_queue_destroy(&queue));
    return result;
}


NOAIO_FRAME *
QPS_blocking_command(IODriver *device,
                     NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *cmd) {
    pthread_queue_t queue;
    PTRY(pthread_queue_init(&queue));
    
    QPSCommand command = {
        .command = cmd,
        .on_success = QPS_copy_unblock,
        .on_failure = QPS_unblock,
        .data = &queue
    };
    PTRY(pthread_queue_push_back(&device->UART->command_queue, &command));

    NOAIO_FRAME *frame = (NOAIO_FRAME *) pthread_queue_pop_front(&queue);
    PTRY(pthread_queue_destroy(&queue));
    return frame;
}

static void
QPS_release_command(NOAIO_FRAME *frame, QPSCommand *command) {
    free(command->command);
    free(command);
}

void
QPS_nonblocking_command(IODriver *device,
                        NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *cmd) {
    PTRY(pthread_queue_push_back(&device->UART->command_queue,
                                 MAKE(QPSCommand,
                                      .command = cmd,
                                      .on_success = QPS_release_command,
                                      .on_failure = QPS_release_command,
                                      .data = NULL)));
}

void
QPS_execute_pending_command(UARTInterface *uart) {
    QPSCommand *command = pthread_queue_pop_front(&uart->command_queue);
    NOAIO_FRAME *frame = NOAIO_request(uart->out, uart->in,
                                       command->command,
                                       &uart->incomming_frame,
                                       CAN_RESPONSE_TIMEOUT_MS);
    NOAIO_REQUEST original = command->command->noaio.header.request;
    if (!frame
        || (frame->header.request != NOAIO_RESPONSE_TO(original))) {
        command->on_failure(frame, command);
    }
    else {
        command->on_success(frame, command);
    }
}

#if NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STRATEGY_OVERWRITE

# if USE_PTHREAD_TIMER

void QPS_send_then_release(QPSRemote *remote) {
    if(!QPS_blocking_command_success(remote->io, &remote->command)) {
        WARN(LOGS_TYPE_CAN, "delayed frame received no response");
    }
    free(remote);
}

# endif

void
QPS_disable(IODriver *io, NOAIO_CHANNEL channel,
                  AttitudeTowardsResponse afterIssuingCommand) {
    /*
     * TODO tutaj sobie zakladamy, ze polecenie ENABLE_FOR_PERIOD
     * wykonane kilkukrotnie na tym samym wejsciu ma spowodowac
     * przedluzenie czasu, w ktorym wyjscie ma byc ustawone (wg ustalen
     * Lisa programista IO Drivera zakladal inne rozwiazanie, ale
     * to sie wydaje prostsze w implementacji i uzyteczniejsze)
     */
//	INFO("disabling channel %x at dev %x - %s", channel, io->index, QPS_out_names[io->index][channel]);
	if (afterIssuingCommand == WaitForResponse) {
	        QPS_WITH_COMMAND_RESPONSE(response, {
	                (void) response;
	            }, io, DISABLE_OUTPUT, .channel = channel);
	    }
	else {
		assert(afterIssuingCommand == DontWaitForResponse);
		QPS_COMMAND_NONBLOCK(io, DISABLE_OUTPUT, .channel = channel);
	}
}

void
QPS_enable(IODriver *io, NOAIO_CHANNEL channel,
                  AttitudeTowardsResponse afterIssuingCommand) {
    /*
     * TODO tutaj sobie zakladamy, ze polecenie ENABLE_FOR_PERIOD
     * wykonane kilkukrotnie na tym samym wejsciu ma spowodowac
     * przedluzenie czasu, w ktorym wyjscie ma byc ustawone (wg ustalen
     * Lisa programista IO Drivera zakladal inne rozwiazanie, ale
     * to sie wydaje prostsze w implementacji i uzyteczniejsze)
     */
//	INFO("enabling channel %x at dev %x - %s", channel, io->index, QPS_out_names[io->index][channel]);
	if (afterIssuingCommand == WaitForResponse) {
	        QPS_WITH_COMMAND_RESPONSE(response, {
	                (void) response;
	            }, io, ENABLE_OUTPUT, .channel = channel);
	    }
	else {
		assert(afterIssuingCommand == DontWaitForResponse);
		QPS_COMMAND_NONBLOCK(io, ENABLE_OUTPUT, .channel = channel);
	}
}

void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand) {
    /*
     * TODO tutaj sobie zakladamy, ze polecenie ENABLE_FOR_PERIOD
     * wykonane kilkukrotnie na tym samym wejsciu ma spowodowac
     * przedluzenie czasu, w ktorym wyjscie ma byc ustawone (wg ustalen
     * Lisa programista IO Drivera zakladal inne rozwiazanie, ale
     * to sie wydaje prostsze w implementacji i uzyteczniejsze)
     */
	//INFO("enabling channel %d at dev %d - %s for %d",
//			channel, io->index,
//			QPS_out_names[io->index][channel],
//			period_ms);
	if (afterIssuingCommand == WaitForResponse) {
        QPS_WITH_COMMAND_RESPONSE(response, {
                (void) response;
            }, io, ENABLE_FOR_PERIOD,
            .channel = channel,
            .period_ms = period_ms);
    }
    else {
        assert(afterIssuingCommand == DontWaitForResponse);
        QPS_COMMAND_NONBLOCK(io,
                             ENABLE_FOR_PERIOD,
                             .channel = channel,
                             .period_ms = period_ms);
    }
}

void
QPS_disable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand) {
    /*
     * TODO tutaj sobie zakladamy, ze polecenie ENABLE_FOR_PERIOD
     * wykonane kilkukrotnie na tym samym wejsciu ma spowodowac
     * przedluzenie czasu, w ktorym wyjscie ma byc ustawone (wg ustalen
     * Lisa programista IO Drivera zakladal inne rozwiazanie, ale
     * to sie wydaje prostsze w implementacji i uzyteczniejsze)
     */
//	INFO("disabling channel %d at dev %d - %s for %d",
//			channel,
//			io->index,
//			QPS_out_names[io->index][channel],
//			period_ms);
    if (afterIssuingCommand == WaitForResponse) {
        QPS_WITH_COMMAND_RESPONSE(response, {
                (void) response;
            }, io, DISABLE_FOR_PERIOD,
            .channel = channel,
            .period_ms = period_ms);
    }
    else {
        assert(afterIssuingCommand == DontWaitForResponse);
        QPS_COMMAND_NONBLOCK(io, DISABLE_FOR_PERIOD,
                             .channel = channel,
                             .period_ms = period_ms);
    }
}


#elif NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND

# if USE_PTHREAD_TIMER

typedef struct {
    IODriver *io;
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX command;
} QPSRemote;

void QPS_send_then_release(QPSRemote *remote) {
    if(!QPS_blocking_command_success(remote->io, &remote->command)) {
        WARN("delayed frame received no response");
    }
    free(remote);
}

void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand) {
    if (afterIssuingCommand == WaitForResponse) {
        QPS_WITH_COMMAND_RESPONSE(response, {
                (void) response;
            }, io, ENABLE_OUTPUT, .channel = channel);
    }
    else {
        assert(afterIssuingCommand == DontWaitForResponse);
        QPS_COMMAND_NONBLOCK(io, ENABLE_OUTPUT, .channel = channel);
    }

    QPSRemote *remote = NEW(QPSRemote);
    NOAIO_DECLARE_REQUEST_FRAME(command, DISABLE_OUTPUT, io->address,
                                .channel = channel);
    remote->io = io;
    memcpy(&remote->command, &command, sizeof(command));
    pthread_after_ms(period_ms, (delayed_procedure_t) QPS_send_then_release, remote);
}

# else // !USE_PTHREAD_TIMER

void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand) {
    if (afterIssuingCommand == WaitForResponse) {
        QPS_WITH_COMMAND_RESPONSE(response, {
                (void) response;
            }, io, ENABLE_OUTPUT, .channel = channel);
    }
    else {
        assert(afterIssuingCommand == DontWaitForResponse);
        QPS_COMMAND_NONBLOCK(io, ENABLE_OUTPUT, .channel = channel);
    }

    NOAIO_DECLARE_REQUEST_FRAME(command, DISABLE_OUTPUT, io->address,
                                .channel = channel);
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *frame =
        (NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *) COPY(command);

    pthread_queue_push_back(&io->for_period[channel].queue,
                            MAKE(PeriodTimerCommand,
                                 .command = frame,
                                 .period_ms = period_ms,
                                 .device = io));
}

THREAD void *
QPS_waiting_for_period(PeriodCommandTimer *timer) {
    //INFO(LOGS_TYPE_CODE"started QPS_waiting_for_period output %d", timer->output_index);
    while (1) {
        PeriodTimerCommand *command = pthread_queue_pop_front(&timer->queue);
       //INFO("output %d waiting for %d before command", timer->output_index,
             command->period_ms);
        if (pthread_queue_has_been_empty(&timer->queue, command->period_ms)) {
            //INFO("output %d timeout %d passed, issuing command",
                 timer->output_index, command->period_ms);
            QPS_nonblocking_command(command->device, command->command);
        }
        else {
            //INFO("output %d timer interrupted", timer->output_index);
        }
        free(command);
    }
    return NULL;
}




# endif // !USE_PTHREAD_TIMER

#endif // NOA_IO_FOR_PERIOD_STRATEGY

