#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <errno.h>
#include <sys/file.h>

#include "qps-command.h"
#include "recyc.h"
#include "util.h"
#include "textparams.h"
#include "qps-recognizer.h"

static void
QPS_lock(Recyc *self) {
    /*QPSRecycData *qps = (QPSRecycData *) self->data;

    QPS_WITH_COMMAND_RESPONSE(response, {
            (void) response;
        },  &qps->devices[QPS_DROPBOX_DOOR_LOCK_DEVICE],
    	DISABLE_OUTPUT, .channel = NOAIO_output_channel(QPS_DROPBOX_DOOR_LOCK_OUT));
*/}

static void
QPS_unlock(Recyc *self) {
  /*  QPSRecycData *qps = (QPSRecycData *) self->data;

    QPS_WITH_COMMAND_RESPONSE(response, {
            (void) response;
        }, &qps->devices[QPS_DROPBOX_DOOR_LOCK_DEVICE],
    	ENABLE_OUTPUT, .channel = NOAIO_output_channel(QPS_DROPBOX_DOOR_LOCK_OUT));
*/}

static bool
QPS_isClosed(Recyc *self) {
    QPSRecycData *qps = (QPSRecycData *) self->data;


    bool closed = false;
    QPS_WITH_COMMAND_RESPONSE(response, {
            closed = !!(response->input_state & QPS_DROPBOX_DOOR_DETECTOR_IN);
            //OUT("input: %02X mask: %02X", response->input_state, QPS_DROPBOX_DOOR_DETECTOR_IN);
        }, &qps->devices[QPS_DROPBOX_DOOR_DETECTOR_DEVICE],
        GET_STATE);
    return closed;
}

static void
QPS_invokeOnClosedHandler(IODriver *io, int input_index, bool new_value) {
    const NOAIO_CHANNEL detector_channel
        = NOAIO_input_channel(QPS_DROPBOX_DOOR_DETECTOR_IN);
    const int detector_index
        = NOAIO_input_channel_index(detector_channel);
    assert(detector_index == input_index);
    void (*action)(Recyc *) // fajne rzutowanie, co nie?
        = (void (*)(Recyc *)) io->on_change[input_index].arg;
    assert(action);
    Recyc *recyc = (Recyc *) io->on_change[input_index].arg2;
    QPSRecycData *qps = (QPSRecycData *) recyc->data;
    time_t actual_time;
    time(&actual_time);
    //INFO("On closed time - %ld, block_time - %ld", actual_time, qps->block_doors_reaction_time);
    if((actual_time - qps->block_doors_reaction_time) > QPS_DOOR_SAFETY_TIME){
		if (new_value) {
			time(&qps->block_doors_reaction_time);
			//time(&qps->mock_recognition->start_transaction_time);
			INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Doors have been closed!");
			recyc->supervisor->report(recyc->supervisor, ReportDoorsClosed);
			action(recyc);
		}
		else {
			//qps->mock_recognition->start_transaction_time = 0;
			INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Doors have been opened");
			recyc->supervisor->report(recyc->supervisor, ReportDoorsOpened);
		}
    }
}

static void
QPS_onClosed(Recyc *self, void (*action)(Recyc *)) {
	QPS_registerOnChangeEvent((QPSRecycData *)self->data,
			QPS_DROPBOX_DOOR_DETECTOR_DEVICE,
			QPS_DROPBOX_DOOR_DETECTOR_IN,
			QPS_invokeOnClosedHandler, action, self);
}


/*static ItemType
QPS_recognizeItem(Recyc *self) {
	QPSRecycData *qps = (QPSRecycData *) self->data;
	QPS_enable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
			   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
			   WaitForResponse);

    ItemType itemType = NULL;
    size_t size = 0;

    getline(&itemType, &size, qps->recognition); //alokuje stringa na stercie

    if (!itemType) {
        INFO("no item present in the chamber");
        return itemType;
    }

    char *newline = strchr(itemType, '\n');
    if (newline) {
        *newline = '\0';
    }

    newline = strchr(itemType, '\r');
    if (newline) {
        *newline = '\0';
    }

    INFO("recognized %s", itemType);
	QPS_disable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
			   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
			   WaitForResponse);
    return itemType;
}*/

void
QPS_recognizeItem(Recyc *self) {
	QPSRecycData *qps = (QPSRecycData *) self->data;
	//qps->recognition.mocking = true;
	//strcpy(qps->recognition.mocked_recognition, ItemTypePET);
	INFO(LOGS_TYPE_MOCK, LOGS_STATUS_NONE, "Mocking recognition: %d", qps->recognition.mocking);
	if(qps->recognition.mocking == true) {
		//qps->mock_recognition->counter = 0;
		QPS_enable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
			   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
			   WaitForResponse);
		int rolls_time_ms = (rand()%(QPS_DROPBOX_MAX_ROLLS_TIME_MS-QPS_DROPBOX_MIN_ROLLS_TIME_MS))+QPS_DROPBOX_MIN_ROLLS_TIME_MS;
					INFO(LOGS_TYPE_MOCK, LOGS_STATUS_NONE, "Generated rolls time: %d", rolls_time_ms);
					QPS_enable_for_period(&qps->devices[QPS_DROPBOX_ROLLS_DEVICE],
							  NOAIO_output_channel(QPS_DROPBOX_ROLLS_OUT),
							  rolls_time_ms,
							  WaitForResponse);
		usleep(1000L*rolls_time_ms);
		strcpy(qps->recognition.recognized_item, qps->recognition.mocked_recognition);
//		qps->recognition.mocking = false;
	}
	else {
		QPS_enable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
				   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
				   WaitForResponse);
		usleep(1000L*QPS_DROPBOX_LEDS_DELAY_MS);
		for(int recognize_iter = 0; recognize_iter < QPS_RECOGNITION_ITER; recognize_iter++) {
			int rolls_time_ms = (rand()%(QPS_DROPBOX_MAX_ROLLS_TIME_MS-QPS_DROPBOX_MIN_ROLLS_TIME_MS))+QPS_DROPBOX_MIN_ROLLS_TIME_MS;
			INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Generated rolls time: %d", rolls_time_ms);
			if(recognize_iter > 0) {
				QPS_enable_for_period(&qps->devices[QPS_DROPBOX_ROLLS_DEVICE],
					 	 NOAIO_output_channel(QPS_DROPBOX_ROLLS_OUT),
					  	rolls_time_ms,
					  	WaitForResponse);
				usleep(1000L * QPS_RECOGNITION_DELAY);

			}
			memset(qps->recognition.recognized_item, 0, NELEMS(qps->recognition.recognized_item));
			QPS_ask_recognizer(qps->recognition.socket, START_RECOGNITION,
							   qps->recognition.recognized_item,
							   NELEMS(qps->recognition.recognized_item)); //+1 - null terminator
			memset(qps->recognition.recognized_item+3, 0, 1);
			INFO(LOGS_TYPE_RECOGNIZER, LOGS_STATUS_NONE, "Recognized %s", qps->recognition.recognized_item);
			usleep(1000L * QPS_NEXT_RECOGNITION_DELAY);
			if((!strcmp(qps->recognition.recognized_item, ItemTypeOTH)) || (!strcmp(qps->recognition.recognized_item, ItemTypeEMP))) {
			}
			else {
				if(!strcmp(qps->recognition.recognized_item, ItemTypeBAR)) {
					strcpy(qps->recognition.recognized_item, ItemTypeDEO);
					INFO(LOGS_TYPE_RECOGNIZER, LOGS_STATUS_NONE, "Recognized BAR, changing to DEO, recognized item: %s", qps->recognition.recognized_item);
				}
				break;
			}
		}
		//if(!strcmp(qps->recognition.recognized_item, ItemTypeOTH)) {
		//	INFO("WRONG ITEM RECOGNIZED");
		//}
		QPS_disable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
				   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
				   WaitForResponse);
	}
	QPS_disable(&qps->devices[QPS_DROPBOX_LEDS_DEVICE],
		   NOAIO_output_channel(QPS_DROPBOX_LEDS_OUT),
		   WaitForResponse);
	INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Recognized %s", qps->recognition.recognized_item);
}

static void
QPS_takeOver(Recyc *self) {
    QPSRecycData *qps = (QPSRecycData *) self->data;
    
    pthread_join(qps->event_handler, NULL);

    for (int i = 0; i < qps->num_uarts; ++i) {
        pthread_join(qps->UART[i].thread, NULL);
    }
}

static void
QPS_processItem(Recyc *self, char *item) {
    QPSRecycData *qps = (QPSRecycData *) self->data;
    // przekazujemy obsluge do watku QPS_reflex
    INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "processing %s", item);
    pthread_queue_push_back(&qps->conveyor.pending_items, (void*)item);
}

#if 0
#define FLUSH_DEBUG 1
static void
QPS_flush_fd( int fd )
{
    int len, retval;
    retval = ioctl(fd, FIONREAD, &len);
#if FLUSH_DEBUG 
    printf("\n\n\nsize: %d retval: %d\n\n\n", len, retval); 
#endif
    uint8_t buf;
    
    for( int i=0; i<len; i++ )
    {
        read(fd, &buf, 1);
#if FLUSH_DEBUG 
        printf("0x%02X byte %d of %d\n", buf, i, len);
#endif
    }

#if FLUSH_DEBUG
    ioctl(fd, FIONREAD, &len);
    printf("\n\n\nsize: %d retval: %d\n\n\n", len, retval); 
    usleep(1000L*3000);
#endif
}
#else
static void
QPS_flush_fd( int fd )
{
    int flags = fcntl(fd, F_GETFD);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK); 

    uint8_t buf;
    int cnt=0;

    (void)cnt;
    while( read(fd, &buf, sizeof(buf)) == sizeof(buf) )
    {
//        printf("0x%02X - %03d\n", buf, cnt++);
    } 

    fcntl(fd, F_SETFL, flags); 
//    while(1);
}
#endif

// zdefiniowana w qps-reflex.c
extern void QPS_reflex_init(Recyc *);
//zdefiniowano w qps-reflex.c
extern void QPS_moving_conveyor_init(Recyc *);

static Recyc *
QPS_NewRecyc(const char *args) {
    //INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "QPS_NewRecyc(\"%s\")", args);
    QPSRecycData *data = NEW(QPSRecycData);
    assert(data);
    data->num_uarts = textparam_parse_int(parse_dec, args, "uarts=", 1);
    assert(data->num_uarts > 0);
    data->UART = ARRAY(UARTInterface, data->num_uarts);
    assert(data->UART);
    
    data->num_devices = textparam_parse_int(parse_dec, args, "devices=", 1);
    assert(data->num_devices > 0);
    data->devices = ARRAY(IODriver, data->num_devices);
    assert(data->devices);
    data->recognition.socket = QPS_init_recognizer_socket();
    data->recognition.mocking = false;
    time(&data->last_process_start_time);
    QPS_init_recognizer_client_conn(data->recognition.socket);
    
    for (int i = 0; i < data->num_uarts; ++i) {
        UARTInterface *UART = &data->UART[i];
        char param[16];

        int n = snprintf(param, NELEMS(param), "in%d=", i);
        assert(n <= NELEMS(param));
        UART->in_name = textparam(args, param);

        n = snprintf(param, NELEMS(param), "out%d=", i);
        assert(n <= NELEMS(param));
        UART->out_name = textparam(args, param);

        if (UART->in_name == NULL || UART->out_name == NULL) {
            assert(UART->in_name == NULL && UART->out_name == NULL);
            n = snprintf(param, NELEMS(param), "uart%d=", i);
            assert(n <= NELEMS(param));
            UART->in_name = UART->out_name = textparam(args, param);
        }

        if (UART->in_name == NULL || UART->out_name == NULL) {
            ERROR(LOGS_TYPE_INIT, "No path for uart%d  (e.g. uart%d=/dev/ttyS1) was given", i, i);
            goto release_uarts;
        }

        if (UART->in_name == UART->out_name || !strcmp(UART->in_name, UART->out_name)) {
            INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Opening(RW) %s", UART->in_name);
            UART->out = UART->in = open(UART->in_name, O_RDWR);

	    if( flock(UART->out, LOCK_EX | LOCK_NB) != 0 )
	    {
		fprintf(stderr, "Another instance is running!\n");
		exit(-1);
	    }

            // parametry transmisji portu szeregowego
            // TODO zakladamy tutaj, ze jezeli in i out to ten sam plik,
            // to wtedy jest to port szeregowy, ale tak naprawde wypadaloby
            // sprawdzic przy pomocy "stat", czy rzeczywiscie mamy do czynienia
            // z urzadzeniem znakowym (byc moze sens mialoby rowniez przekazywanie
            // tych parametrow w stringu "arg")
            struct termios tty = {0};
            cfmakeraw(&tty);
            cfsetospeed(&tty, CAN_BAUDRATE);
            cfsetispeed(&tty, CAN_BAUDRATE);
            tty.c_cflag |= CS8 | CREAD | CLOCAL;
            tty.c_cc[VMIN] = 1;
            tty.c_cc[VTIME] = 5;
            tcsetattr(UART->in, TCSANOW, &tty);


        }
        else {
            INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Opening(R) %s", UART->in_name);
            UART->in = open(UART->in_name, O_RDONLY);
            INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Opening(W) %s", UART->out_name);
            UART->out = open(UART->out_name, O_WRONLY);
        }
       
        QPS_flush_fd(UART->in);
        QPS_flush_fd(UART->out);

        if (UART->out < 0) {
            perror(UART->out_name);
            goto release_uarts;
        }

        if (UART->in < 0) {
            perror(UART->in_name);
            goto release_uarts;
        }
        

        // Tutaj marnujemy nieco pamieci
        UART->devices = ARRAY(IODriver *, data->num_devices);
        UART->num_devices = 0;


        pthread_queue_init(&UART->command_queue);
        // TODO trzeba jeszcze okreslic parametry transmisji
    }

    Recyc * recyc = NEW(Recyc);

    for (int i = 0; i < data->num_devices; ++i) {
        IODriver *device = &data->devices[i];
        device->index = i;

	device->recyc = recyc;
        char param[16];
        int n = snprintf(param, NELEMS(param), "device%d=", i);
        assert(n <= NELEMS(param));
        const char *value = strstr(args, param);
        if (!value) {
            ERROR(LOGS_TYPE_INIT, "Options for device%d were not given", i);
            goto release_devices;
        }
        
        int uart_index;
        if (sscanf(value+n, "%x@uart%d", &device->address, &uart_index) != 2) {
            ERROR(LOGS_TYPE_INIT, "Expected device format: device%d=address@uartN"
                  " (address-hex number, N-dec value)", i);
            goto release_devices;
        }

        assert(uart_index < data->num_uarts);

        for (int j = 0; j < NOAIO_INPUTS; ++j) {
            device->on_change[j] = (InputEventHandler) {
                .handler = QPS_no_reaction,
                .arg = NULL,
                .arg2 = NULL
            };
        }
        
        device->UART = &data->UART[uart_index];
        device->UART->devices[device->UART->num_devices++] = device;
        device->poll = QPS_poll_input;

        pthread_mutex_init(&device->input_mutex, NULL);
    }

    pthread_queue_init(&data->conveyor.pending_items);
    data->block_doors_reaction_time = 0;
    QPS_mock_recognition mock_recognition;
    //mock_recognition.counter = 0;
    //mock_recognition.start_transaction_time = 0;
    data->mock_recognition = &mock_recognition;
    assert(recyc);
    data->recyc = recyc;
    recyc->lock = QPS_lock;
    recyc->unlock = QPS_unlock;
    recyc->onClosed = QPS_onClosed;
    recyc->isClosed = QPS_isClosed;
    recyc->recognizeItem = QPS_recognizeItem;
    recyc->takeOver = QPS_takeOver;
    recyc->processItem = QPS_processItem;
    
    recyc->data = data;

    pthread_queue_init(&data->event_queue);
    typedef void *(*thread)(void (*));
    
    pthread_create(&data->event_handler, NULL, (thread) QPS_handle_events, data);
    
    for (int i = 0; i < data->num_uarts; ++i) {
        UARTInterface *uart = &data->UART[i];
        uart->event_queue = &data->event_queue;
        pthread_create(&uart->thread, NULL, (thread) QPS_poll_uart, uart);
    }

    QPS_reflex_init(recyc);

    return recyc;
    
 release_devices:
    
 release_uarts:
    for (int i = 0; i < data->num_uarts; ++i) {
        if (data->UART[i].in_name) {
            free(data->UART[i].in_name);
        }
        if (data->UART[i].out_name
            && data->UART[i].out_name != data->UART[i].in_name) {
            free(data->UART[i].out_name);
        }
    }
    free(data->devices);
    free(data->UART);
    //release_data:
    free(data);
    return NULL;
}

// Recyc_init("qps uarts=1 devices=3 uart0=/dev/ttyS6 device0=ddccbbaa@uart0 device1=047ca4b0@uart0 device2=017ca4b0@uart0")
double QPS_get_process_min_length(char* recognized_item) {
	if(!strcmp(recognized_item, ItemTypePET)){
		return QPS_PROCESS_PET_MIN_LENGTH;
	}
	else if(!strcmp(recognized_item, ItemTypeALU)){
		return QPS_PROCESS_ALU_MIN_LENGTH;
	}
	else if(!strcmp(recognized_item, ItemTypeGLA)){
		return QPS_PROCESS_GLA_MIN_LENGTH;
	}
	else if(!strcmp(recognized_item, ItemTypeDEO)){
		return QPS_PROCESS_DEO_MIN_LENGTH;
	}
	else if(!strcmp(recognized_item, ItemTypeBAT)){
		return QPS_PROCESS_BAT_MIN_LENGTH;
	}
	else if(!strcmp(recognized_item, ItemTypeSYR)){
		return QPS_PROCESS_SYR_MIN_LENGTH;
	}
	else{
		ERROR(LOGS_TYPE_MAIN_PROCESS, "Error while counting process length time");
		return 0;
	}
}
INITIALIZE {
    INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "QPS recyc module initializing");
    bool registeredQPSDriver = Recyc_register("qps", QPS_NewRecyc);
    assert(registeredQPSDriver);
}


