#include "qps-command.h"
#include "util.h"
#include "iomap.h"

#include "qps-conveyor-devices.h"

typedef struct {
    IODriver *source_device;
    int input_index;
    bool new_value;
} QPSInputEvent;

void stop_conveyor( Recyc* recyc);
void block_receiving(QPSRecycData* qps);

void 
IODriver_analog_update( IODriver * io, NOAIO_CHANNEL channel, uint16_t voltage )
{
    pthread_mutex_lock( &io->input_mutex );

    io->analog_inputs[channel-NOAIO_ANALOG_INPUT_A] = voltage;

    pthread_mutex_unlock( &io->input_mutex );       
}

void
IODriver_current_update( IODriver * io, NOAIO_CHANNEL channel, uint16_t current )
{
    pthread_mutex_lock( &io->input_mutex );

    io->output_currents[channel-NOAIO_CHANNEL_OUT0] = current;

    pthread_mutex_unlock( &io->input_mutex );       
} 

uint16_t 
IODriver_analog_get( IODriver * io, NOAIO_CHANNEL channel )
{
    uint16_t voltage;
    pthread_mutex_lock( &io->input_mutex );

    voltage = io->analog_inputs[channel-NOAIO_ANALOG_INPUT_A];

    pthread_mutex_unlock( &io->input_mutex );       
    return voltage;
}

bool 
IODriver_digital_get( IODriver * io, NOAIO_CHANNEL channel )
{
    uint8_t bit = 1<<(channel-NOAIO_CHANNEL_INA);
    return !!(io->previous_input & bit);
}

uint16_t
IODriver_current_get( IODriver * io, NOAIO_CHANNEL channel )
{
    uint16_t current;
    pthread_mutex_lock( &io->input_mutex );
    
    current = io->output_currents[channel-NOAIO_CHANNEL_OUT0];
    
    pthread_mutex_unlock( &io->input_mutex );       
    return current;
}

void
QPS_registerOnChangeEvent(QPSRecycData *qps,
		int device_index,
		NOAIO_INPUT_BITS input_bits,
		void (*handler)(IODriver *, int, bool),
		void *arg, void *arg2) {
    // rejestrowanie procedury obslugi
    const NOAIO_CHANNEL detector_channel
        = NOAIO_input_channel(input_bits);
    const int detector_index
        = NOAIO_input_channel_index(detector_channel);
    InputEventHandler *on_change
        = &qps->devices[device_index].on_change[detector_index];
    assert(on_change->handler == QPS_no_reaction);
    assert(on_change->arg == NULL);

    on_change->handler = handler;
    on_change->arg = arg;
    on_change->arg2 = arg2;
}

void
QPS_no_reaction(struct IODriver *io, int input_index, bool new_value) {
    INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "Ignoring event from %x (%d=%d)", io->address, input_index, new_value);
}

void
check_crusher_overcurrent( IODriver * device, uint16_t vol, NOAIO_ANALOG_CHANNEL in )
{
	iodriver_input_info_t * info = device->in_config[in-NOAIO_ANALOG_INPUT_A];

	if( info == NULL ) return;

	if( vol > info->max_voltage_mv )
	{
		info->ovc_cnt++;

		if( info->max_ovc_cnt == info->ovc_cnt )
		{
			if( info->ovc_callback ) info->ovc_callback(info);
		}
	}
	else
	{
		info->ovc_cnt = 0;
	}
}

void conveyor_current_ok_callback( iodriver_config * config, Recyc * recyc )
{
	(void)recyc;

	time_t now = time(NULL);

	if( difftime(now, config->last_ovc_time) > QPS_CONVEYOR_MAX_BLOCKED_TIME_S )
	{
		config->first_ovc_time = 0;
	}
}

void conveyor_out_ovc_callback( iodriver_config * config, Recyc * recyc )
{
	time_t * first_ovc_time = &config->first_ovc_time;
	time_t now = time(NULL);
	config->last_ovc_time = now;

	if( *first_ovc_time )
	{
		if( difftime(now, *first_ovc_time) > QPS_CONVEYOR_MAX_BLOCKED_TIME_S )
		{
			ERROR(LOGS_TYPE_CONV, "Conveyor overcurrent\t[FAIL]");
			stop_conveyor(recyc);
			block_receiving(recyc->data);
		}	
	}
	else
	{
		WARN(LOGS_TYPE_SERVICE, "Conveyor blocked");
		*first_ovc_time = now;
	}
}

void default_out_ovc_callback( iodriver_config * config, Recyc * recyc )
{
	NOAIO_CHANNEL channel = NOAIO_output_channel(config->out);
	ERROR(LOGS_TYPE_LIMIT, "Overcurrent dev %d out %d - val: %dmA, max: %dmA", config->device, channel, (int)config->arg, config->soft_max_limit_mA);
}

void 
QPS_poll_out_current( IODriver * device )
{
    uint16_t val, soft_max;
	
    for( uint8_t i=0; i<NOAIO_OUTPUTS; i++ )
    {
	    iodriver_config * config = device->out_config[i];
	    if( config == NULL ) return;
	    if( config->current_measure == 0 ) continue;

        QPS_WITH_RESPONSE(info, {
	val = info->current;
	soft_max = config->soft_max_limit_mA;
            
	IODriver_current_update(device, i, val);

	if( val > soft_max ) 
	{
                config->arg = (void*)(uint32_t)val;
		config->ovc_callback(config, device->recyc);
	}
	else if( config->current_ok_callback ) config->current_ok_callback(config, device->recyc);

	}, device, GET_OUTPUT_INFO, {},  i);	
    }
}

void
QPS_poll_digital_input(IODriver *device) {
    QPS_WITH_RESPONSE(state, {
            for (int i = 0; i < NOAIO_INPUTS; ++i) {
                NOAIO_INPUT_BITS previous = (device->previous_input & (1 << i));
                NOAIO_INPUT_BITS current = state->input_state & (1 << i);
                if (current != previous) {
                    QPSInputEvent *event = MAKE(QPSInputEvent,
                                                .source_device = device,
                                                .input_index = i,
                                                .new_value = !!current);
                    pthread_queue_push_back(device->UART->event_queue, event);
                }
            }
            device->previous_input = state->input_state;
        }, device, GET_STATE, {
	// tu kod odpalany, jak nie uda się odebrać ramki
	}); 
}

void
QPS_poll_analog_input( IODriver * device )
{
    if( device->is_redriver )
    {
        for( NOAIO_ANALOG_CHANNEL i=NOAIO_ANALOG_INPUT_A; i<=NOAIO_ANALOG_INPUT_D; i++)
        {
            QPS_WITH_RESPONSE(info, {
	    uint16_t vol = info->input_voltage;		
            IODriver_analog_update(device, i, vol);
	    
		check_crusher_overcurrent(device, vol, i);

            }, device, GET_INPUT_INFO, {}, i); 
        }
    }
}

void
QPS_poll_input( IODriver * device ) 
{
#if 1

	switch( device->cmd_cnt )
	{
		case 0 ... 4: 
			QPS_poll_digital_input(device);
			QPS_poll_analog_input(device);
		break;

                default: 
		QPS_poll_out_current(device);
		device->cmd_cnt = 0;
		return;
	}

	device->cmd_cnt++;
#else
	QPS_poll_digital_input(device);
	QPS_poll_analog_input(device);
#endif
}

THREAD void *
QPS_handle_events(QPSRecycData *data) {
    INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Started QPS_handle_events thread");
    while (true) {
        QPSInputEvent *event
            = (QPSInputEvent *) pthread_queue_pop_front(&data->event_queue);
        IODriver *device = event->source_device;
        int index = event->input_index;
        device->on_change[index].handler(device, index, event->new_value);
        free(event);
    }
    return NULL; // never reached
}

void get_ver(IODriver *device){

	QPS_WITH_RESPONSE(response, {
			device->version.major = response->version_major;
			device->version.minor = response->version_minor;
			INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "IODriver %x version %d.%d", device->address,
				device->version.major, device->version.minor);
	}, device, GET_VERSION, {});
}

typedef enum{
	CORRECT = 0,
	WRONG = 1
}check_t;

void set_and_check_vol_limits(IODriver *device){
#define DEFAULT_MIN_VOLTAGE_MV (device->voltage_limits->min_voltage_mv)
#define DEFAULT_MAX_VOLTAGE_MV (device->voltage_limits->max_voltage_mv)
	check_t vol_limits;
	QPS_WITH_RESPONSE(response, {			// SPRAWDZ CZY LIMITY SA JUZ USTAWIONE POPRAWNIE
			if(response->minimum_ex_voltage == DEFAULT_MIN_VOLTAGE_MV && response->maximum_ex_voltage == DEFAULT_MAX_VOLTAGE_MV){
				vol_limits = CORRECT;
				INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Initializing voltage limits");
			}
			else{
				vol_limits = WRONG;			// JESLI LIMITY NIE SA WLASCIWE TO USTAW POPRAWNE LIMITY
				QPS_SEND_REQUEST(device, SET_VOLTAGE_LIMITS,
									.minimum_ex_voltage = DEFAULT_MIN_VOLTAGE_MV,
											.maximum_ex_voltage = DEFAULT_MAX_VOLTAGE_MV);
			}
	}, device, GET_VOLTAGE_LIMITS, {});

	if(vol_limits){
		QPS_WITH_RESPONSE(response, {		// SPRAWDZ CZY LIMITY ZOSTALY POPRAWIONE
			if(response->minimum_ex_voltage == DEFAULT_MIN_VOLTAGE_MV && response->maximum_ex_voltage == DEFAULT_MAX_VOLTAGE_MV){
				INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Initializing voltage limits");
			}
			else{
				ERROR(LOGS_TYPE_INIT, "Initializing voltage limits");
			}
		}, device, GET_VOLTAGE_LIMITS, {});
	}
}

void init_prev(IODriver* device){
	// inicjalizacja previous_input
        QPS_WITH_RESPONSE(state, {
                device->previous_input = state->input_state;
            }, device, GET_STATE, {});

	// oznaczenie modulow z wejsciami analogowymi
	device->is_redriver =
		((device->address & IODRIVER_TYPE_MASK) == IODRIVER_TYPE_REDRIVER);
}

void get_output_state(IODriver* device){
	uint8_t maska = 0;

	check_t output_correct;
	for(uint8_t i=0; i<8; i++) maska |= (device->out_config[i]->init_state << i);

		QPS_WITH_RESPONSE(response, {									// SPRAWDZ CZY LIMITY SA JUZ POPRAWNE
				if(response->output_latch == maska ){
					output_correct = CORRECT;
					INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Initializing iodriver's outs");
				}
				else{
					output_correct = WRONG;
					QPS_SEND_REQUEST(device, SET_OUTPUT, .output_latch = maska);	// JESLI NIE SA WLASCIWE TO POPRAW JE
				}
		}, device, GET_STATE, {});
		if(output_correct){
			QPS_WITH_RESPONSE(response, {						// SPRAWDZ CZY LIMITY ZOSTALY POPRAWIONE
					if(response->output_latch == maska ){
						INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Initializing iodriver's outs");
					}
					else{
						ERROR(LOGS_TYPE_INIT, "Initializing iodriver's outs");
					}
			}, device, GET_STATE, {});
		}
}

int set_and_check_cur_limit(IODriver* device, iodriver_config * config, int device_num){

		check_t current_limit;

        if( config->device != device_num || config->out == NOAIO_UNINITIALIZED ) return 0;
        uint8_t channel = NOAIO_output_channel(config->out);
        QPS_WITH_RESPONSE(response, {									// SPRAWDZ CZY LIMITY SA JUZ USTAWIONE POPRAWNIE
        			if(response->minimum_ex_current == config->min_limit_mA && response->maximum_ex_current == config->max_limit_mA){
        				current_limit = CORRECT;
        			}
        			else{
					current_limit = WRONG;
        				QPS_SEND_REQUEST(device, SET_CURRENT_LIMITS, .channel = channel,	// // JESLI LIMITY NIE SA WLASCIWE TO USTAW POPRAWNE LIMITY
        											.minimum_ex_current = config->min_limit_mA, .maximum_ex_current = config->max_limit_mA);
        			}
        }, device, GET_OUTPUT_INFO, {}, .channel = channel);
        if(current_limit){
    		QPS_WITH_RESPONSE(response, {									// SPRAWDZ CZY LIMITY ZOSTALY POPRAWIONE
					if(response->minimum_ex_current == config->min_limit_mA && response->maximum_ex_current == config->max_limit_mA){
						current_limit = CORRECT;
					}
					else{
						ERROR(LOGS_TYPE_INIT, "Initializing out no. %d with current limits", channel);
						current_limit = WRONG;
					}
    		}, device, GET_OUTPUT_INFO, {}, .channel = channel);
        }

	if( config->ovc_callback == NULL ) config->ovc_callback = default_out_ovc_callback;
    	device->out_config[channel] = config;

    	if(current_limit == CORRECT)
    		return 0;
    	else
    		return 1;
}

#define SET_AND_CHECK_CUR_LIMIT(config) if(set_and_check_cur_limit(device, &config, device_num)){	\
											ERROR(LOGS_TYPE_INIT, "Initialize device %s\t[FAIL]", #config);			\
											return WRONG;											\
										}else														\
											return CORRECT											\

int init_pet_crusher_open(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(pet_crusher_open);
}
int init_pet_crusher_close(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(pet_crusher_close);
}
int init_alu_crusher_open(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(alu_crusher_open);
}
int init_alu_crusher_close(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(alu_crusher_close);
}
int init_conveyor_start(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(conveyor_start);
}
int init_dropbox_conveyor(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_conveyor);
}
int init_dropbox_director(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_director);
}
/*
int init_dropbox_door_lock(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_door_lock);
}
*/
int init_dropbox_leds(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_leds);
}
int init_dropbox(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox);
}
int init_dropbox_lock(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_lock);
}
int init_dropbox_rolls(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(dropbox_rolls);
}
int init_qps_display(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(qps_display);
}
int init_qps_pwr(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(qps_pwr);
}
int init_printer(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(printer);
}
int init_conveyor_speed_a(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(conveyor_speed_a);
}
int init_conveyor_speed_b(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(conveyor_speed_b);
}
int init_conveyor_speed_c(IODriver* device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(conveyor_speed_c);
}
int init_conveyor_dir(IODriver * device, int device_num){
	SET_AND_CHECK_CUR_LIMIT(conveyor_dir);
}

void default_ovc_callback( iodriver_input_info_t * info )
{
	conveyor_device_t * crusher = (conveyor_device_t*)info->arg;
	ERROR(LOGS_TYPE_LIMIT, "%s\t[OVERCURRENT]", crusher->name);
}

void init_input( IODriver * device, int device_num, iodriver_input_info_t * config )
{
	if( device_num != config->device ) return;

	config->ovc_callback = default_ovc_callback;
	device->in_config[config->in-NOAIO_ANALOG_INPUT_A] = config;
}

int init_pet_crusher(IODriver* device, int device_num){
	uint8_t wrong_limits = 0;
	wrong_limits += init_pet_crusher_open(device, device_num);
	wrong_limits += init_pet_crusher_close(device, device_num);

	init_input( device, device_num, &pet_crusher_current_sense );

	return wrong_limits;
}
int init_alu_crusher(IODriver* device, int device_num){
	uint8_t wrong_limits = 0;
	wrong_limits += init_alu_crusher_open(device, device_num);
	wrong_limits += init_alu_crusher_close(device, device_num);

	init_input( device, device_num, &alu_crusher_current_sense );

	return wrong_limits;
}
int init_conveyor(IODriver* device, int device_num){
	uint8_t wrong_limits = 0;
	wrong_limits += init_conveyor_start(device, device_num);
	wrong_limits += init_conveyor_dir(device, device_num);
	wrong_limits += init_conveyor_speed_a(device, device_num);
	wrong_limits += init_conveyor_speed_b(device, device_num);
	wrong_limits += init_conveyor_speed_c(device, device_num);
	return wrong_limits;
}
int init_dropbox_devices(IODriver* device, int device_num){
	uint8_t wrong_limits = 0;
	wrong_limits += init_dropbox_conveyor(device, device_num);
	wrong_limits += init_dropbox_director(device, device_num);
	//wrong_limits += init_dropbox_door_lock(device, device_num);
	wrong_limits += init_dropbox_leds(device, device_num);
	wrong_limits += init_dropbox(device, device_num);
	wrong_limits += init_dropbox_lock(device, device_num);
	wrong_limits += init_dropbox_rolls(device, device_num);
	return wrong_limits;
}

int init_default_outputs( IODriver * device, int device_num )
{
	uint8_t wrong_limits = 0;
    for( uint8_t i=0; i<NOAIO_OUTPUTS; i++ )
    {
        if( device->out_config[i] == NULL )
        {
            default_config.device = device_num;
            default_config.out = (1<<i);

	    wrong_limits += set_and_check_cur_limit(device, &default_config, device_num);
        }
    } 
    return wrong_limits;
}

void init_devices(IODriver* device, int device_num){

        memset(device->out_config, 0, sizeof(device->out_config));
	memset(device->in_config, 0, sizeof(device->in_config));
//        device->wdt_info = {0};    

    uint8_t wrong_limits = 0;	// zlicza ilosc zle zainicjalizowanych limitow wyjsc iodriverow
    wrong_limits += init_pet_crusher(device, device_num);
    wrong_limits += init_alu_crusher(device, device_num);
    wrong_limits += init_conveyor(device, device_num);
    wrong_limits += init_dropbox_devices(device, device_num);

    wrong_limits += init_default_outputs(device, device_num);
    wrong_limits += init_qps_display(device, device_num);
    wrong_limits += init_qps_pwr(device, device_num);
    wrong_limits += init_printer(device, device_num);

	if(!wrong_limits){
		INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Initializing current limits");
	}else
		ERROR(LOGS_TYPE_INIT, "Initializing current limits");
}

void QPS_init_watchdog( IODriver * device )
{
    watchdog_info_t wdt = {
	.mode = QPS_WATCHDOG_MODE,
	.bitmask = QPS_WATCHDOG_OUT,
	.interval = QPS_WATCHDOG_INTERVAL	
    };

    bool is_ok = 0;

    QPS_WITH_RESPONSE(response, {
		if( 
			response->watchdog_mode == wdt.mode &&
			response->output_mask == wdt.bitmask &&
			response->watchdog_interval == wdt.interval
		) is_ok = 1;
	}, device, GET_WATCHDOG, {});

	if( is_ok == 0 )
	{
	
	    QPS_SEND_REQUEST(device, SET_WATCHDOG, .watchdog_mode = wdt.mode, .output_mask = wdt.bitmask, .watchdog_interval = wdt.interval);

	    QPS_WITH_RESPONSE(response, {
			if( 
				response->watchdog_mode == wdt.mode &&
				response->output_mask == wdt.bitmask &&
				response->watchdog_interval == wdt.interval
			) is_ok = 1;
	    }, device, GET_WATCHDOG, {});

	    if( is_ok == 0 ) 
	    {
		ERROR(LOGS_TYPE_INIT, "Watchdog set");
		return;
	    }
	}
	INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Watchdog set");
}

void QPS_reset_watchdog( IODriver * device )
{
    QPS_SEND_REQUEST(device, WATCHDOG_RESET);
}

void QPS_set_baudrate( IODriver * device, NOAIO_BAUDRATE baudrate )
{
    QPS_SEND_REQUEST(device, SET_BAUDRATE, .baud_rate_index = baudrate);
}

static inline void
QPS_initial_poll(UARTInterface *uart) {
    for (int i = 0; i < uart->num_devices; ++i) {
    	IODriver *device = uart->devices[i];        // spytanie IODrivera co on za jeden
	device->voltage_limits = &iodriver_voltage_limits[i];

    	get_ver(device);
        set_and_check_vol_limits(device);		// initializing devices with default voltage limits

        init_devices(device, i);	// inicjalizuje urzadzenia

        get_output_state(device);

        init_prev(device);	//	initializes previous_input and marks modules with analog input

    }

    QPS_init_watchdog( uart->devices[QPS_WATCHDOG_DEVICE] );
}

THREAD void *
QPS_poll_uart(UARTInterface *uart) {
    INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "started QPS_poll_uart thread", uart->in_name);
    assert(uart && uart->num_devices > 0);

    QPS_initial_poll(uart);
    
    int current_device = 0;

    while (true) {
        if (pthread_queue_has_been_empty(&uart->command_queue,
        								 QPS_CAN_POLLING_PERIOD_MS
                                         / uart->num_devices) ){
            IODriver *device = uart->devices[current_device];
            device->poll(device);
            current_device = (current_device+1) % uart->num_devices;
        }
        else {
            QPS_execute_pending_command(uart);
        }
    }
    return NULL; // never reached
}
