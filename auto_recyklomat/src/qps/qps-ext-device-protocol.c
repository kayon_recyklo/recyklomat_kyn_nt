#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "qps-ext-device-protocol.h"

void buffer_init( write_buffer_t * buf )
{
	memset( buf->data, 0, MAX_BUFFER_SIZE );
	buf->write_ptr = buf->data;
}

void buffer_add( write_buffer_t * buf, char * str )
{
	buf->write_ptr += sprintf(buf->write_ptr, "%s ", str);
}

void buffer_close( write_buffer_t * buf )
{
	// usuwa ostatnią spację
	if( buf->write_ptr != buf->data ) *(buf->write_ptr-1) = 0;
}

void report_init( report_t * rep, char * dev_name, report_sender_t sender, report_line_processor_t reciver )
{
	buffer_init( &rep->err_buf );
	buffer_init( &rep->warn_buf );
	buffer_init( &rep->info_buf );

	rep->sender = sender;
	rep->reciver = reciver;
	rep->dev_name = dev_name;
}

void report_add( report_t * rep, uint8_t type, char * str )
{
	switch( type )
	{
		case ADD_ERROR: buffer_add( &rep->err_buf, str);
		break;	

		case ADD_WARNING: buffer_add( &rep->warn_buf, str);
		break;

		case ADD_INFO: buffer_add( &rep->info_buf, str);
		break;
	}
}

void report_reset( report_t * rep )
{
	buffer_init( &rep->err_buf );
	buffer_init( &rep->warn_buf );
	buffer_init( &rep->info_buf );
}

void report_close( report_t * rep )
{
	buffer_close( &rep->err_buf );
	buffer_close( &rep->warn_buf );
	buffer_close( &rep->info_buf );
}

void report_send( report_t * rep )
{
	char buffer[4*MAX_BUFFER_SIZE];

	report_close(rep);

	sprintf(buffer, "DEV: %s\nERR: %s\nWARN: %s\nINFO: %s\n",
		       	rep->dev_name,
	       		rep->err_buf.data,
	 		rep->warn_buf.data,
			rep->info_buf.data);		

	if( rep->sender ) rep->sender( buffer );

	report_reset( rep );
}

#define EMPTY_NULL(str) (str[0]?str:NULL)

void report_process( report_t * rep, char * buffer )
{
	char * dev_name = strtok(buffer, "\n");
	dev_name = strchr(dev_name, ' ')+1;

	char * errors = strtok(NULL, "\n");
	errors = strchr(errors, ' ')+1;

	char * warnings = strtok(NULL, "\n");
	warnings = strchr(warnings, ' ')+1;
	
	char * info = strtok(NULL, "\n");
	info = strchr(info, ' ')+1;

	rep->dev_name = dev_name;

	if( rep->reciver )
	{
		rep->reciver( GET_ERROR, dev_name, EMPTY_NULL(errors) );
		rep->reciver( GET_WARNING, dev_name, EMPTY_NULL(warnings) );
		rep->reciver( GET_INFO, dev_name,EMPTY_NULL(info) );
	}
}









