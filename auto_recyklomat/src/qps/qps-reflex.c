#include "qps-command.h"
#include "util.h"
#include "pthread-queue.h"
#include "pthread-timer.h"
#include "pthread-fifo.h"
#define QPS_DEFINE_REFLEX_TABLE
#include "iomap.h"

#define PTRY(pcall) if(pcall) { ERROR(LOGS_TYPE_CODE, #pcall " failed"); }

#define QPS_REFLEX_HISTORY_SIZE         5
#define ADD_ITEM        0
#define REMOVE_ITEM     1
#define FALLING_EDGE    2
#define EXTEND_TIMEOUT 	3
#define STOP_TIME	4
#define RESUME_TIME	5

#define CONTROL_ON 1

#define TEST_HW_GLA_ONLY 1

#define DEBUG_ERROR_HANDLING 1
#define CONVEYOR_ENABLE 1

typedef enum PACKED {
    QPSDetectorActionSkip,
    QPSDetectorActionPerform
} QPSDetectorAction;


typedef struct {
    QPSDetectorAction action;

    time_t start_time;
    int max_process_time;
} QPSDetectorItem;

typedef struct 
{
    pthread_queue_t * cmd_queue;
} timeout_thread_info_t;

typedef struct
{
    uint8_t cmd;
    void * arg;
} timeout_cmd_t;

typedef struct
{
	time_t item_start_time;
	time_t timeout_change;
} extend_timeout_info_t;

void QPS_reflex_add_to_history( QPSReflex * reflex, QPSDetectorItem * item );
void conveyor_move_forward(Recyc* recyc, int ms);
void conveyor_move_backward(Recyc* recyc, int ms);
bool conveyor_cleanup( Recyc * recyc );
void emergency_director_on_error(conveyor_device_t* device);
void emergency_director_on_phase_complete( conveyor_device_t * device );
void director_on_error(conveyor_device_t* device);
void director_on_phase_complete( conveyor_device_t * self );
void conveyor_set_speed( Recyc * recyc, uint8_t speeden );

static void QPS_timeout_send_cmd( uint8_t cmd, void * arg );

ASSERT(sizeof(QPSDetectorAction) <= sizeof(void *));

// ***************************************************************************************** //
// **************************** Obsluga czujnikow optycznych ******************************* //
// ***************************************************************************************** //

time_t t1 = 0;
time_t t2 = 0;
uint16_t cnt=0;

// kolejka komend wątku od timeouta
pthread_queue_t timeout_cmd_queue;

//licznik wszystkich odpadów
volatile int item_counter;
pthread_mutex_t counter_mutex;

void QPS_reflex_change_item_counter( int val )
{
    pthread_mutex_lock(&counter_mutex);
    item_counter += val;
    pthread_mutex_unlock(&counter_mutex);    
}

bool QPS_reflex_check_item_counter( void )
{
    int cnt;

    pthread_mutex_lock(&counter_mutex);
    cnt = item_counter;
    pthread_mutex_unlock(&counter_mutex);    

    if( cnt != 0 )
    {
        ERROR(LOGS_TYPE_OPTO, "Item IO opto desynchronized");
    }

    return (cnt==0);
}

void QPS_reflex_handle_fault_counter( QPSReflex * reflex, bool is_ok )
{
	if( is_ok ) reflex->fault_counter = 0;
	else reflex->fault_counter++;

	if( reflex->fault_counter > QPS_REFLEX_FAULT_CNT_MAX )
	{
		ERROR(LOGS_TYPE_OPTO, "Opto %s failed %d times in a row", reflex->itemType, reflex->fault_counter);
	}
}

QPSDetectorItem * QPS_reflex_delete_expected( QPSReflex * reflex, QPSDetectorItem * item )
{
	QPSDetectorItem * _item, * to_remove;
	PTHREAD_QUEUE_FOR_EACH(&reflex->expectations, _item, {

		if( item->start_time == _item->start_time )
		{
			to_remove = _item;
		}
	});

	_item = pthread_queue_remove(&reflex->expectations, to_remove);

	return _item;
}

// sprawdzenie, czy w historii poprzedniego czujnika był aktualnie wykryty odpad
void
QPS_reflex_check_prev( QPSReflex * reflex, QPSDetectorItem * current_item )
{
    if( reflex == NULL ) return;

    QPSDetectorItem * item;
    bool found = 0;
    PTHREAD_FIFO_FOR_EACH(&reflex->history, item, {
        if( item->start_time == current_item->start_time )
        {
            found = 1;
        }

    });

    if( found == 0 )
    {
	item = QPS_reflex_delete_expected(reflex, current_item);

	if( item != NULL ) QPS_reflex_add_to_history(reflex, item);
        ERROR(LOGS_TYPE_OPTO, "%s opto - item missed", reflex->itemType);

	QPS_reflex_handle_fault_counter(reflex, 0);
    }
    else
	QPS_reflex_handle_fault_counter(reflex, 1);
}

void
QPS_reflex_add_to_history( QPSReflex * reflex, QPSDetectorItem * item )
{
// nie kopiuję, wskaźnik przechodzi z jednej kolejki na drugą, a potem jest usuwany przez 
// logikę FIFO, kiedy nadejdzie jego czas
//    QPSDetectorItem * history_item = COPY(*item);
    pthread_fifo_push_back( &reflex->history, item );
}

// Obecnie nie uzywana, pozostalosc starej logiki
void
QPS_reflex_extend_timeout( time_t item, time_t extra_perioid )
{
	extend_timeout_info_t * info = MAKE(extend_timeout_info_t, 
					.item_start_time = item,
					.timeout_change = extra_perioid);

	QPS_timeout_send_cmd(EXTEND_TIMEOUT, info);
}

static void
QPS_on_detector_trigger(IODriver *input, int input_index, bool new_value) {

    // reflex - opisuje czujnik
    // qps - wiadomo co
    QPSReflex *reflex = (QPSReflex *) input->on_change[input_index].arg;
//    QPSRecycData *qps = (QPSRecycData *) input->on_change[input_index].arg2;
	
    // zbocze opadające, olewamy je
    if (!new_value) {
        INFO(LOGS_TYPE_OPTO, LOGS_STATUS_NONE, "Falling edge - in: %d add: %x", input_index,
             input->address);
        reflex->rising_edge_time = 0;    
        return;
    }

    // obsługa błędów - licznik IO
    QPS_reflex_change_item_counter(-1);

       // time( &reflex->rising_edge_time );

    reflex->rising_edge_time = time_milis();
    INFO(LOGS_TYPE_OPTO, LOGS_STATUS_NONE, "Rising edge - in: %d add: %x", input_index, input->address);

    if (pthread_queue_is_empty(&reflex->expectations)) {
        ERROR(LOGS_TYPE_OPTO, "Unexpected signal from detector");
    }
    else {
        QPSDetectorItem * item
            = (QPSDetectorItem*) pthread_queue_pop_front(&reflex->expectations);

        // obsługa błędów - poprzedni czujnik
#if CONTROL_ON
        QPS_reflex_check_prev( reflex->prev, item);
        QPS_reflex_add_to_history(reflex, item);
#endif
    
        switch (item->action) {
        case QPSDetectorActionSkip:
	    //qps->mock_recognition->counter++;
	    //if (qps->mock_recognition->counter == 1) {
	    //	recyc->supervisor->report(recyc->supervisor, ReportRecognizedItem, qps->recognition.recognized_item);
	    //}
            INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Skipping reaction for input %d", input_index);
//	    QPS_reflex_extend_timeout(item->start_time, QPS_REFLEX_TIMEOUT);
            break;
        case QPSDetectorActionPerform:
	{
		conveyor_device_t * director = (conveyor_device_t*)reflex->director;
		
		if( director )
			director->start_working_cycle(director);
            break;
	}
        default:
            assert(false && "unrecognized action");
        }
    }
}

// chcemy przekazywac ItemType przez kolejke pending_items
ASSERT(sizeof(ItemType) <= sizeof(void *));

static inline int
QPS_small_item_index(ItemType itemType) {
    for (int i = 0; i < NELEMS(QPS_small_items); ++i) {
        if (!strcmp(QPS_small_items[i].itemType, itemType)) {
            return i;
        }
    }
    return -1;
}

static inline int
QPS_reflex_index(ItemType itemType) {
    for (int i = 0; i < NELEMS(QPS_reflex_table); ++i) {
        if (!strcmp(QPS_reflex_table[i].itemType, itemType)) {
            return i;
        }
    }
    return -1;
}

static void
QPS_reflex_add_expected( QPSReflex * reflex, QPSDetectorAction action, time_t now)
{
    QPSDetectorItem * item = NEW(QPSDetectorItem);
    item->action = action;
    item->start_time = now;
    item->max_process_time = QPS_REFLEX_TIMEOUT;
    
    pthread_queue_push_back(&reflex->expectations, item);
}

static void
QPS_timeout_send_cmd( uint8_t cmd, void * arg )
{
    timeout_cmd_t * _cmd = NEW(timeout_cmd_t);
    _cmd->cmd = cmd;
    _cmd->arg = arg;

    pthread_queue_push_back(&timeout_cmd_queue, _cmd);      
}

bool
QPS_reflex_handle_small_item( Recyc *recyc, QPSRecycData *qps, ItemType item )
{
	int index;	
	if ((index = QPS_small_item_index(item)) >= 0) 
	{
            INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "%s is a small item (index %d)", item, index);
            QPS_small_items[index].action(recyc, item);
            QPS_enable_for_period(&qps->devices[QPS_DROPBOX_CONVEYOR_DEVICE],
                                  NOAIO_output_channel(QPS_DROPBOX_CONVEYOR_OUT),
								  QPS_DROPBOX_CONVEYOR_PASSAGE_TIME_MS,
                                  WaitForResponse);
            usleep(1000L * (useconds_t) QPS_DROPBOX_CONVEYOR_PASSAGE_TIME_MS);

		return 1;
        }

	return 0;
}

void QPS_reflex_handle_first_normal_item( QPSRecycData *qps, int index )
{
	QPSReflex *reflex = &QPS_reflex_table[index];
 	conveyor_device_t * director = reflex->director;

	if( director )
		director->start_working_cycle(director);
}

void QPS_reflex_handle_last_normal_item( int index, time_t now )
{
	INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Last reflex item");
	for (int i = QPS_REFLEX_FIRST_SENSOR; i <= index; ++i) 
	{
		INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "%s expects to skip", QPS_reflex_table[i].itemType);
                QPS_reflex_add_expected(&QPS_reflex_table[i], QPSDetectorActionSkip, now);
                QPS_reflex_change_item_counter(+1);
	}
}

void QPS_reflex_handle_std_normal_item( int index, time_t now )
{
	for (int i = QPS_REFLEX_FIRST_SENSOR; i < index; ++i) 
	{
		INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "%s expects to skip", QPS_reflex_table[i].itemType);
                QPS_reflex_add_expected(&QPS_reflex_table[i], QPSDetectorActionSkip, now);
                QPS_reflex_change_item_counter(+1);
	}
	
	INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "%s expects to react", QPS_reflex_table[index].itemType);
        QPS_reflex_add_expected(&QPS_reflex_table[index], QPSDetectorActionPerform, now);
        QPS_reflex_change_item_counter(+1);
}

bool QPS_reflex_handle_normal_item( Recyc *recyc, QPSRecycData *qps, ItemType item )
{
	int index;
        if ((index = QPS_reflex_index(item)) >= 0) 
	{
		if( QPS_reflex_table[index].is_enabled == FRACTION_DISABLED ) 
		{
			index = NELEMS(QPS_reflex_table)-1; 
			INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "%s redirected to %s", item, QPS_reflex_table[index].itemType);
		}
		else
			INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "%s is a regular item (index %d)", item, index);
#if CONVEYOR_ENABLE
	    time_t last_process_end_time;
            double time_difference;

            do 
	    {
            	usleep(1000L);
            	time(&last_process_end_time);
            	time_difference = difftime(last_process_end_time, qps->last_process_start_time);
            } while(time_difference < qps->process_min_length);

		// załączenie taśmociągu
		conveyor_set_speed(recyc, CONVEYOR_STD_SPEED);
		conveyor_move_forward(recyc, QPS_CONVEYOR_PASSAGE_TIME_MS);
#endif
            time_t now = time(NULL);
            QPSDetectorItem * it = MAKE(QPSDetectorItem, 
                                        .start_time = now,                                
                                        .max_process_time = QPS_REFLEX_TIMEOUT_FIRST + (index-QPS_REFLEX_FIRST_SENSOR)*QPS_REFLEX_TIMEOUT 
                                        );
#if CONTROL_ON
      	    QPS_timeout_send_cmd(ADD_ITEM, it); 
#endif
	    switch( index )
	    {
		case QPS_REFLEX_FIRST_ARM:
			QPS_reflex_handle_first_normal_item(qps, index);
			break;
		
		case QPS_REFLEX_LAST:
			QPS_reflex_handle_last_normal_item(index, now);
			break;

		default:
			QPS_reflex_handle_std_normal_item(index, now);
	    }

            
	    INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Releasing dropbox after %f", time_difference);
            QPS_enable_for_period(&qps->devices[QPS_DROPBOX_LOCK_DEVICE],
                                  NOAIO_output_channel(QPS_DROPBOX_LOCK_OUT),
								  QPS_DROPBOX_LOCK_TIME_MS,
                                  WaitForResponse);

            usleep(1000L * (useconds_t) QPS_DROPBOX_DELAY);
            QPS_enable_for_period(&qps->devices[QPS_DROPBOX_DEVICE],
                                  NOAIO_output_channel(QPS_DROPBOX_OUT),
								  QPS_DROPBOX_TIME_MS,
                                  WaitForResponse);

            usleep(1000L * (useconds_t) QPS_DROPBOX_TIME_MS);
            
	    time(&qps->last_process_start_time);
            usleep(1000L * (useconds_t) QPS_DROPBOX_LOCK_TIME_MS);
           
	    return 1;
        }
 	return 0;
}

THREAD void *
QPS_reflex(Recyc *recyc) {
    QPSRecycData *qps = (QPSRecycData *) recyc->data;
    INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Started QPS_reflex thread");
    bool block = false;

    while (true) {

	// oczekiwanie, aż coś wejdzie na kolejkę
        ItemType item
            = (ItemType) pthread_queue_pop_front(&qps->conveyor.pending_items);

        if( strcmp(item, "block") == 0 )
        {
		QPS_timeout_send_cmd(STOP_TIME, NULL);
            block = true;
            continue;
        }
        else if( strcmp(item, "resume") == 0 )
        {
		QPS_timeout_send_cmd(RESUME_TIME, NULL);
            block = false;
            continue;
        }

        if( block ) continue;

	// ogarnięcie strzykawek i baterii
        if( QPS_reflex_handle_small_item(recyc, qps, item) == 0 )
	{ 
		// ogarnięcie zwykłych śmieci
		if( QPS_reflex_handle_normal_item(recyc, qps, item) == 0 )
		{
			// nieznany obiekt
           		WARN(LOGS_TYPE_DRIVER, "Unsupported item type: %s", item);
		}
	}

        //free(item);
        //INFO("Unlocking doors");
        recyc->unlock(recyc);

        recyc->supervisor->report(recyc->supervisor, ReportStatusIdle);
    }
    return NULL;
}

// 0 - odpad pozostał na którejś kolejce czujnika
// 1 - odpad był tylko w systemie
bool QPS_reflex_remove_item( pthread_queue_t * system_queue, QPSDetectorItem * item )
{
    bool retval = 1;
    // zabranie odpadu ze wszystkich kolejek
    for( uint8_t i=QPS_REFLEX_FIRST_SENSOR; i<NELEMS(QPS_reflex_table); i++ )
    {
        QPSDetectorItem * expected;
        pthread_queue_t * expectations = &QPS_reflex_table[i].expectations;

        PTHREAD_QUEUE_FOR_EACH(expectations, expected, {
            if( expected->start_time == item->start_time )
            {
                pthread_queue_remove(expectations, expected);

                free(expected);
                retval = 0;

		QPS_reflex_handle_fault_counter( &QPS_reflex_table[i], 0 );
            }
        });
    }

    item = pthread_queue_remove(system_queue, item);

    if( item ) free(item);

    return retval;
}

static void handle_timeout( pthread_queue_t * items, bool time_stopped )
{
    time_t now = time(NULL);
    int actual_time_ms = time_milis();
    QPSDetectorItem * item;
    PTHREAD_QUEUE_FOR_EACH(items, item,{
        if( difftime(now, item->start_time) > item->max_process_time )
        {
            if( QPS_reflex_remove_item(items, item) == 0 && time_stopped == 0 )
                ERROR(LOGS_TYPE_OPTO, "Reflex item timeout");
        }
    });

    for( uint8_t i=QPS_REFLEX_FIRST_SENSOR; i<NELEMS(QPS_reflex_table); i++ )
    {
        if( actual_time_ms - QPS_reflex_table[i].rising_edge_time > QPS_REFLEX_HIGH_STATE_TIMEOUT  && QPS_reflex_table[i].rising_edge_time != 0 )
        {
		if( time_stopped == 0 )
		{
            		ERROR(LOGS_TYPE_OPTO, "%s opto falling edge timeout", QPS_reflex_table[i].itemType);
		}

	    QPS_reflex_table[i].rising_edge_time = 0;
        }
    }
}

// zostawione na wypadek, gdyby kiedys trzeba bylo robic snapshota
#if 0
static void timeout_store_elapsed( pthread_queue_t * items )
{
	time_t now = time(NULL);
    	QPSDetectorItem * item;
	PTHREAD_QUEUE_FOR_EACH(items, item, {
		item->start_time = difftime(now, item->start_time);
	});

    for( uint8_t i=QPS_REFLEX_FIRST_SENSOR; i<NELEMS(QPS_reflex_table); i++ )
    {
	    if( QPS_reflex_table[i].rising_edge_time )
		QPS_reflex_table[i].rising_edge_time = now-QPS_reflex_table[i].rising_edge_time;
    }
}

static void timeout_restore_elapsed( pthread_queue_t * items )
{
	time_t now = time(NULL);
    	QPSDetectorItem * item;
	PTHREAD_QUEUE_FOR_EACH(items, item, {
		item->start_time = now - item->start_time;
	});

    for( uint8_t i=QPS_REFLEX_FIRST_SENSOR; i<NELEMS(QPS_reflex_table); i++ )
    {
	    if( QPS_reflex_table[i].rising_edge_time )
		QPS_reflex_table[i].rising_edge_time = now - QPS_reflex_table[i].rising_edge_time;
    }
}
#endif

void
extend_timeout_process_cmd( pthread_queue_t * items, timeout_cmd_t * cmd )
{
	extend_timeout_info_t * info = cmd->arg;

	QPSDetectorItem * item;
	PTHREAD_QUEUE_FOR_EACH(items, item, {

		if( item->start_time == info->item_start_time ) 
		{
			item->max_process_time += info->timeout_change;	
		}
	});

	free(info);
}

THREAD void *
QPS_reflex_timeout( timeout_thread_info_t * info  )
{
	INFO(LOGS_TYPE_INIT, LOGS_STATUS_OK, "Started QPS_reflex_timeout thread");
    pthread_queue_t * cmd_queue = info->cmd_queue;    

    pthread_queue_t item_list;
    pthread_queue_init(&item_list);

    bool time_stopped = 0;

    while(2)
    {
#if CONTROL_ON
        usleep(100*1000L);
        // obsługa komend
        if( pthread_queue_is_empty(cmd_queue) == 0 )
        {
            timeout_cmd_t * cmd = pthread_queue_pop_front(cmd_queue);

	    switch( cmd->cmd )
            {
            case ADD_ITEM:
                pthread_queue_push_back(&item_list, cmd->arg);
            break;

            case REMOVE_ITEM:

            break;

            case FALLING_EDGE:

            break;

	    case EXTEND_TIMEOUT:
		extend_timeout_process_cmd(&item_list, cmd);		
	    break;

	    case STOP_TIME:
		//timeout_store_elapsed(&item_list);
		time_stopped = 1;
	    break;

	    case RESUME_TIME:
		//timeout_restore_elapsed(&item_list);
		time_stopped = 0;
	    break;
            }
           
	    // nie usuwam cmd->arg, bo on ma siedziec na liscie wszystkich odpadow az minie jego timeout 
            free(cmd);
        }
        else
        {
            handle_timeout(&item_list, time_stopped);
        }     
#endif	
    }
}

// **************************************************************************** //
// ********************* Obsluga urzadzen tasmociagu ************************** //
// **************************************************************************** //

// ustawia callback od czujnika optycznego
void QPS_reflex_set_callback( Recyc * recyc, void (*handler)(struct IODriver*, int, bool) )
{
    	QPSRecycData *qps = (QPSRecycData *) recyc->data;

	for( uint8_t i=QPS_REFLEX_FIRST_SENSOR; i<NELEMS(QPS_reflex_table); i++ )
	{
    		QPSReflex *reflex = &QPS_reflex_table[i];
	        NOAIO_CHANNEL input_channel = NOAIO_input_channel(reflex->input.bit);
        	int input_index = NOAIO_input_channel_index(input_channel);
		
		qps->devices[reflex->input.device_index].on_change[input_index]
            		= (InputEventHandler) {
            		.handler = handler,
            		.arg = reflex,
            		.arg2 = qps
        		};
	}

}

// sekcja obslugujaca zatrzymanie kierownicy po sygnale z czujnika
// aktualnie nie uzywana
void position_event_handler( struct IODriver * input, int channel, bool new_state )
{
	IODriver_out_t * pin = (IODriver_out_t*)input->on_change[channel].arg;
	QPSRecycData * qps = (QPSRecycData*)input->on_change[channel].arg2;

	if( new_state )
	{
	//	INFO("Disabling channel %d on device %08X", channel, input->address);
		conveyor_device_set_pin_state(pin, qps, 0);
	}
//	else
//		INFO("Ignoring falling edge channel %d on device %08X", channel, input->address);
}

void conveyor_device_set_input_callback( IODriver_in_t * in, QPSRecycData * qps, void(*callback)(struct IODriver*,int,bool), IODriver_out_t * out )
{
	if( in->channel == NOAIO_UNINITIALIZED ) return;

	uint8_t in_idx = NOAIO_input_channel_index(in->channel);

	qps->devices[in->device_idx].on_change[in_idx]
            		= (InputEventHandler) {
            		.handler = callback,
            		.arg = out,
            		.arg2 = qps
        		};
}

void door_clear_callback( QPSRecycData * qps, InputEventHandler * old )
{
    uint8_t in_idx = NOAIO_input_channel_index( NOAIO_input_channel(QPS_DROPBOX_DOOR_DETECTOR_IN) );

    InputEventHandler * handler = &qps->devices[QPS_DROPBOX_DOOR_DETECTOR_DEVICE].on_change[in_idx];

    memcpy(old, handler, sizeof(InputEventHandler));

    *handler = (InputEventHandler) {
        .handler = QPS_no_reaction,
        .arg = NULL,
        .arg2 = NULL
    };
}

void door_restore_callback( QPSRecycData * qps, InputEventHandler * old )
{
    uint8_t in_idx = NOAIO_input_channel_index( NOAIO_input_channel(QPS_DROPBOX_DOOR_DETECTOR_IN) );

    InputEventHandler * handler = &qps->devices[QPS_DROPBOX_DOOR_DETECTOR_DEVICE].on_change[in_idx];

    memcpy(handler, old, sizeof(InputEventHandler));
}

void conveyor_device_off_sensor_init( conveyor_device_t * device, QPSRecycData * qps )
{
	conveyor_device_set_input_callback( &device->open_sensor, qps, position_event_handler, &device->open_out);	
	conveyor_device_set_input_callback( &device->close_sensor, qps, position_event_handler, &device->close_out);	
}

// wlaczenie czujnikow optycznych
void QPS_reflex_attach_opto_sensors( Recyc * recyc )
{
	QPS_reflex_set_callback(recyc, QPS_on_detector_trigger);
}

// wylaczenie czujnikow optycznych
void QPS_reflex_detach_opto_sensors( Recyc * recyc )
{
	QPS_reflex_set_callback(recyc, QPS_no_reaction);
}

// sprawdzanie stanow urzdzen tasmociagu
// aktualnie stany posrednie sa zawsze ok
bool crusher_check_state( conveyor_device_t * device )
{
	IODriver_in_t * pin = &device->close_sensor;
	IODriver * driver = &device->qps->devices[pin->device_idx];

	uint16_t vol = IODriver_analog_get(driver, pin->channel);

	conveyor_device_status_value_t status = device->get_status(device);
	switch( status )
	{
		case CONVEYOR_DEVICE_OPENING:
			return 1;
	       	break;

		case CONVEYOR_DEVICE_OPENED:
			if( vol < 2600 ) return 1;
		break;

		case CONVEYOR_DEVICE_CLOSING:
			return 1;
		break;

		case CONVEYOR_DEVICE_CLOSED:
			if( vol > 4000 ) return 1;
		break;

		default:
			return 0;		
	}

	return 0;
}

bool director_check_state( conveyor_device_t * device )
{
    IODriver_in_t * pin = &device->open_sensor;
    IODriver * driver = &device->qps->devices[pin->device_idx];
    bool open_sensor = IODriver_digital_get(driver, pin->channel);

    pin = &device->close_sensor;
    driver = &device->qps->devices[pin->device_idx];
    bool close_sensor = IODriver_digital_get(driver, pin->channel);

    conveyor_device_status_value_t status = device->get_status(device);
    switch( status )
    {
        case CONVEYOR_DEVICE_OPENING:
        case CONVEYOR_DEVICE_CLOSING:
            //if( !open_sensor && !close_sensor ) return 1;
	    return 1;
        break;

        case CONVEYOR_DEVICE_OPENED:
            if( open_sensor && !close_sensor ) return 1;
        break;

        case CONVEYOR_DEVICE_CLOSED:
            if( !open_sensor && close_sensor ) return 1;
        break;

        default:
            return 0;
    }

    return 0;
}

void stop_conveyor( Recyc* recyc){	// zatrzymuje tasmociag na podany czas
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "Stopping conveyor");
	QPSRecycData * qps = (QPSRecycData *)recyc->data;
	QPS_disable(&qps->devices[QPS_CONVEYOR_DEVICE], 
			NOAIO_output_channel(QPS_CONVEYOR_START_OUT),
		       	DontWaitForResponse);

}

void conveyor_move_backward(Recyc* recyc, int ms){	// cofa tasmociag przez wyznaczony czas
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "Moving conveyor backward");
	QPSRecycData * qps = (QPSRecycData *)recyc->data;

	QPS_enable(&qps->devices[QPS_CONVEYOR_DEVICE],
			  NOAIO_output_channel(QPS_CONVEYOR_DIR_OUT),
			  DontWaitForResponse);

	QPS_enable_for_period(&qps->devices[QPS_CONVEYOR_DEVICE],
			  NOAIO_output_channel(QPS_CONVEYOR_START_OUT),
			  ms,
			  DontWaitForResponse);
}

void conveyor_move_forward(Recyc* recyc, int ms){		// przesowa tasmociag do przodu przez wyznaczony czas
	INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Starting conveyor");
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "Moving conveyor forward");
	QPSRecycData * qps = (QPSRecycData *)recyc->data;

	QPS_disable(&qps->devices[QPS_CONVEYOR_DEVICE],
			  NOAIO_output_channel(QPS_CONVEYOR_DIR_OUT),
			  DontWaitForResponse);

	QPS_enable_for_period(&qps->devices[QPS_CONVEYOR_DEVICE],
			  NOAIO_output_channel(QPS_CONVEYOR_START_OUT),
			  ms,
			  DontWaitForResponse);
}

// max prednosc - 7, min - 0
void conveyor_set_speed( Recyc * recyc, uint8_t speeden )
{
	QPSRecycData * qps = (QPSRecycData *)recyc->data;
	NOAIO_CHANNEL ch_sa = NOAIO_output_channel(QPS_CONVEYOR_SPEED_A_OUT);
	NOAIO_CHANNEL ch_sb = NOAIO_output_channel(QPS_CONVEYOR_SPEED_B_OUT);
	NOAIO_CHANNEL ch_sc = NOAIO_output_channel(QPS_CONVEYOR_SPEED_C_OUT);

	if( speeden & 0x01 )
		QPS_enable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sc, DontWaitForResponse);
	else
		QPS_disable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sc, DontWaitForResponse);

	if( speeden & 0x02 )
		QPS_enable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sb, DontWaitForResponse);
	else
		QPS_disable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sb, DontWaitForResponse);

	if( speeden & 0x04 )
		QPS_enable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sa, DontWaitForResponse);
	else
		QPS_disable(&qps->devices[QPS_CONVEYOR_DEVICE], ch_sa, DontWaitForResponse);
}

InputEventHandler door_handler;

void block_receiving(QPSRecycData* qps){		// blokuje wrzucanie odpadow
	INFO(LOGS_TYPE_SERVICE, LOGS_STATUS_NONE, "Stopping items receiving");

	//char* block = "block";
	//pthread_queue_push_back(&qps->conveyor.pending_items, block);

        door_clear_callback(qps, &door_handler);
}
void resume_receiving(QPSRecycData* qps){	/* Recyc* recyc */	// wznawia przyjmowanie odpadow
#if DEBUG_ERROR_HANDLING
	INFO(LOGS_TYPE_SERVICE, LOGS_STATUS_NONE, "Resuming items receiving");
#endif
	//char* resume = "resume";
	//pthread_queue_push_back(&qps->conveyor.pending_items, resume);

        door_restore_callback(qps, &door_handler);
}

bool close_all_directors(){
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "All directors closing");

	int size = NELEMS(QPS_reflex_table);
	for(int i = 0; i < size; i++){
		if(QPS_reflex_table[i].director)
			QPS_reflex_table[i].director->close_now(QPS_reflex_table[i].director);
	}

	return 1;
}

void open_all_directors( void )
{
	INFO(LOGS_TYPE_DRIVER, LOGS_STATUS_NONE, "All directors opening");
	for( uint8_t i=0; i<NELEMS(QPS_reflex_table); i++ )
	{
		conveyor_device_t * director = QPS_reflex_table[i].director;

		if(director)
		{
			director->open_now(director);
		}
	}
}

bool wait_for_directors_closed( conveyor_device_t * not_checked )
{
	for(int i = 0; i < NELEMS(QPS_reflex_table); i++){

		usleep(1000L*100);
		conveyor_device_t * director = QPS_reflex_table[i].director;

		if( director && director != not_checked )
		{
			conveyor_device_status_value_t status = director->get_status(director);

			switch(status)
			{
				case CONVEYOR_DEVICE_ERROR_STATUS:
					WARN(LOGS_TYPE_DIR, "Repair error: %s", director->name);
					return 0;
				case CONVEYOR_DEVICE_READY:
					break;
				default:
					i = -1;
					continue;
			}
		}
	}
	return 1;
}

Recyc * directors_return_to_normal_state( void )
{
	Recyc * recyc;
	for( uint8_t i=0; i<NELEMS(QPS_reflex_table); i++ )
	{
		conveyor_device_t * director = QPS_reflex_table[i].director;

		if( director )
		{
			recyc = director->recyc;

			director->set_error_mode(director, CONVEYOR_DEVICE_NEM);
			director->error_info.local_error = 0;
			director->error_info.try_number = 0;

			director->on_error = director_on_error;
			director->on_phase_complete = director_on_phase_complete;
		}
	}

	return recyc;
}

void directors_set_error_state( conveyor_device_error_mode_t state, conveyor_device_t * missed )
{
	for(int i = 0; i < NELEMS(QPS_reflex_table); i++){
		conveyor_device_t * director = QPS_reflex_table[i].director;
		
		if(director && director != missed)
		{
			if( state == CONVEYOR_DEVICE_HEM )
			{
				director->on_error = emergency_director_on_error;
				director->on_phase_complete = emergency_director_on_phase_complete;
			}
			director->set_error_mode(director, state);
		}
	}
}

void finish_advanced_repair(char * name, bool ok )
{
	if( ok == 0 ) 
        {
            ERROR(LOGS_TYPE_DIR, "%s repair", name);
            return;
        }

        INFO(LOGS_TYPE_DIR, LOGS_STATUS_OK, "%s repair", name);

	Recyc * recyc = directors_return_to_normal_state();

	resume_receiving( (QPSRecycData*)recyc->data );
	QPS_reflex_attach_opto_sensors(recyc); 
}

#define MAX_CLOSE_TRY 2

THREAD void* advanced_repair_procedure( conveyor_device_t * fault_device){

	Recyc * recyc = fault_device->recyc;
	uint8_t try_nr;
	bool ok = 0;
	for( try_nr=0; try_nr < MAX_CLOSE_TRY; try_nr++ )
	{
		conveyor_set_speed(recyc, 3);
		conveyor_move_backward(recyc, QPS_CONVEYOR_STEP_BACK_TIME);
		usleep(1000L*QPS_CONVEYOR_STEP_BACK_TIME);

		close_all_directors();
		if( wait_for_directors_closed(NULL) == 1 ) break;
	}

	if( try_nr == MAX_CLOSE_TRY )
	{
		ERROR(LOGS_TYPE_DIR, "%s repair", fault_device->name);
	}
	else
	{
		ok = conveyor_cleanup(recyc);
	}

	finish_advanced_repair(fault_device->name, ok);

	return NULL;
}

void conveyor_start_advanced_repair( conveyor_device_t * fault_device )			// CIEZKI TRYB AWARYJNY
{
	WARN("Start advanced repair\nreason: %s", fault_device->name);

	QPS_reflex_detach_opto_sensors( fault_device->recyc );
	
	directors_set_error_state(CONVEYOR_DEVICE_HEM, NULL);

	pthread_t a;
	pthread_create(&a, NULL, (void*(*)(void*))advanced_repair_procedure, fault_device);
}

bool conveyor_cleanup( Recyc * recyc ){
	//rusz tasmociagiem
	conveyor_set_speed(recyc, CONVEYOR_MAX_SPEED);
	conveyor_move_forward(recyc, QPS_CONVEYOR_CLEAN_TIME);

	usleep(1000L*QPS_CONVEYOR_CLEAN_TIME);
	conveyor_set_speed(recyc, CONVEYOR_STD_SPEED);
	//odpal cykle wszystkich kierownic i czeka na statusy czy wsszystkie dzialaja
	
	open_all_directors();

	return wait_for_directors_closed(NULL);
}

void conveyor_finish_initial_repair(conveyor_device_t* fault_device){

	directors_return_to_normal_state(); // reset error_info

#if 1
	directors_set_error_state(CONVEYOR_DEVICE_HEM, NULL); // awaryjne phase_complete i on_error
	if( conveyor_cleanup(fault_device->recyc) )	
	{
		resume_receiving( fault_device->qps );
		QPS_reflex_attach_opto_sensors( fault_device->recyc );

		directors_return_to_normal_state();
	}
	else
	{
		conveyor_start_advanced_repair(fault_device);
	}
#else
	QPS_reflex_attach_opto_sensors( fault_device->recyc );

	directors_return_to_normal_state();
#endif
}

bool conveyor_start_initial_repair( conveyor_device_t * fault_device )			// LEKKI TRYB AWARYJNY
{
	directors_set_error_state(CONVEYOR_DEVICE_LEM, NULL);

	WARN(LOGS_TYPE_DIR, "Start initial repair\nreason: %s", fault_device->name);
	block_receiving(fault_device->qps);

	if( wait_for_directors_closed(fault_device) == 0 )
		conveyor_start_advanced_repair(fault_device);

	stop_conveyor(fault_device->recyc);
	QPS_reflex_detach_opto_sensors( fault_device->recyc );
	return 1;
}

void emergency_director_on_error(conveyor_device_t* device) 
{
	WARN(LOGS_TYPE_DIR, "%s emergency phase", device->name);

	device->set_status(device, CONVEYOR_DEVICE_ERROR_STATUS);
}

void emergency_director_on_phase_complete( conveyor_device_t * device )
{
	INFO(LOGS_TYPE_DIR, LOGS_STATUS_OK, "%s emergency phase", device->name);

	conveyor_device_status_value_t status = device->get_status(device);

	if( status == CONVEYOR_DEVICE_CONFIRMED_OPENED ) device->close_now(device);
}

// local error to jest poprzedni blad ktory wystapil
void director_on_error(conveyor_device_t* device)
{
	switch(device->error_info.local_error){

		// blad w pierwszym ruchu	0
		case CONVEYOR_DEVICE_NO_ERROR:
			WARN(LOGS_TYPE_DIR, "%s first move", device->name);
			device->repeat_phase(device);		// DOPCHNIJ
			device->error_info.local_error = CONVEYOR_DEVICE_FIRST_MOVE_FAILED; // tak, a nie ++, żeby uniezaleznic od zmian kolejnosci
			break;

			// jest blad w dopychaniu, byl blad w pierwszym ruchu	1
		case CONVEYOR_DEVICE_FIRST_MOVE_FAILED:
			WARN(LOGS_TYPE_DIR, "%s first push", device->name);
			conveyor_start_initial_repair(device);
			device->start_other_phase(device);
			device->error_info.local_error = CONVEYOR_DEVICE_FIRST_PUSH_FAILED;
			break;	

			// powrot sie nie udal, a wczesniej nie udalo sie dopchniecie	2
		case CONVEYOR_DEVICE_FIRST_PUSH_FAILED:
			WARN(LOGS_TYPE_DIR, "%s return", device->name);
			device->repeat_phase(device);
			device->error_info.local_error = CONVEYOR_DEVICE_RETURN_FAILED;
			break;	
			
			// nie udalo sie dopchniecie powrotne	3
		case CONVEYOR_DEVICE_RETURN_FAILED:
			WARN(LOGS_TYPE_DIR, "%s return push", device->name);
			device->error_info.local_error = CONVEYOR_DEVICE_ULTIMATE_ERROR;
			conveyor_start_advanced_repair(device);
			break;	

		case CONVEYOR_DEVICE_ULTIMATE_ERROR:
		default:
			ERROR(LOGS_TYPE_DIR, "problem with device.local_error");
	}
}

void director_finish_cycle( conveyor_device_t * device, conveyor_device_status_value_t status )
{
	if( status == CONVEYOR_DEVICE_READY && device->status.is_normal_cycle == 0 )
	{
		INFO(LOGS_TYPE_DIR, LOGS_STATUS_NONE, "%s: start next device", device->name);
		device->start_next(device);
		// zalacz zgniatarke
		conveyor_device_error_mode_t mode = device->get_error_mode(device);

		if( mode != CONVEYOR_DEVICE_NEM )
			conveyor_finish_initial_repair(device);
	}

}

void director_repair_cycle_complete_no_error( conveyor_device_t * device )
{
	conveyor_device_status_value_t status = device->get_status(device);

	if(status == CONVEYOR_DEVICE_CONFIRMED_OPENED){
		device->close(device);
		conveyor_move_forward(device->recyc, QPS_CONVEYOR_RETURN_TIME);
	} 
	else 
		director_finish_cycle(device, status);
}

void director_normal_push_ok( conveyor_device_t * device )
{
	conveyor_device_status_value_t status = device->get_status(device);

	director_finish_cycle(device, status);

	if( status == CONVEYOR_DEVICE_CONFIRMED_OPENED && device->status.is_normal_cycle == 0 ) 
		device->close(device);
}

bool director_return_push_ok( conveyor_device_t * device )
{
	conveyor_move_backward(device->recyc, QPS_CONVEYOR_STEP_BACK_TIME);
	if( device->error_info.try_number == device->error_info.max_try_number ) {
		// INFO("try_number: %d\tmax_try_number: %d", device->error_info.try_number, device->error_info.max_try_number);
		return 1;		// zwraca info ze wystapil blad i przechodzi do conveyor_start_advanced_repair()
	}

	msleep(QPS_CONVEYOR_STEP_BACK_TIME);
	device->start_other_phase(device);

	return 0;
}

// zwraca czy wymagana powazna naprawa
bool director_phase_complete_normal(conveyor_device_t* device)
{
	bool retval = 0;

	switch(device->error_info.local_error){

		case CONVEYOR_DEVICE_NO_ERROR:
			director_repair_cycle_complete_no_error(device);
			break;

		case CONVEYOR_DEVICE_FIRST_MOVE_FAILED:
			INFO(LOGS_TYPE_DIR, LOGS_STATUS_OK, "%s push", device->name);
			director_normal_push_ok(device);
			break;

		case CONVEYOR_DEVICE_RETURN_FAILED:
		case CONVEYOR_DEVICE_FIRST_PUSH_FAILED:
			INFO(LOGS_TYPE_DIR, LOGS_STATUS_OK, "%s return", device->name);
			retval = director_return_push_ok(device);
			device->error_info.try_number += !retval;
			break;
		default:
			ERROR(LOGS_TYPE_DIR, "faza nieprawidlowa(%d)", device->error_info.local_error);
	}

	device->error_info.local_error = 0;
	return retval;
}

void director_on_phase_complete( conveyor_device_t * self )
{
	conveyor_device_status_value_t status = self->get_status(self);
	INFO(LOGS_TYPE_DIR, LOGS_STATUS_OK, "%s %s phase finish", self->name, (status==CONVEYOR_DEVICE_READY)?"closing":"opening");

	if( director_phase_complete_normal(self) )
	{
		conveyor_start_advanced_repair(self);
	}
}

void crusher_exit_error_mode( conveyor_device_t * device )
{
	INFO(LOGS_TYPE_CRUSHER, LOGS_STATUS_OK, "%s repair", device->name);
	conveyor_move_forward(device->recyc, QPS_CONVEYOR_PASSAGE_TIME_MS);
	resume_receiving(device->qps);
	QPS_reflex_attach_opto_sensors(device->recyc);
	device->set_error_mode(device, CONVEYOR_DEVICE_NEM);
}

void reflex_disable_by_crusher( conveyor_device_t * device )
{
	for(uint8_t i = 0; i < NELEMS(QPS_reflex_table); i++){
		QPSReflex* reflex = &QPS_reflex_table[i];
		if(reflex->crusher == device){
			INFO(LOGS_TYPE_CRUSHER, LOGS_STATUS_NONE, "%s is disabled", reflex->itemType);
			reflex->is_enabled = FRACTION_DISABLED;
			return;
		}
	}
}

void reflex_enable_by_crusher( conveyor_device_t * device )
{
	for(uint8_t i = 0; i < NELEMS(QPS_reflex_table); i++){
		QPSReflex* reflex = &QPS_reflex_table[i];
		if(reflex->crusher == device){
			INFO(LOGS_TYPE_CRUSHER, LOGS_STATUS_NONE, "%s is enabled", reflex->itemType);
			reflex->is_enabled = FRACTION_ENABLED;
			return;
		}
	}
}

void crusher_on_error(conveyor_device_t* device)
{
	if( device->error_info.try_number == 0 )
	{
		device->set_error_mode(device, CONVEYOR_DEVICE_LEM);
		block_receiving(device->qps);
		reflex_disable_by_crusher(device);
		//stop_conveyor(device->recyc);
		//QPS_reflex_detach_opto_sensors(device->recyc);
	}

	if(device->error_info.try_number < device->error_info.max_try_number )
	{
		device->start_other_phase(device);
		device->error_info.try_number++;
	}
	else
	{
		ERROR(LOGS_TYPE_CRUSHER, "%s", device->name);
		//reflex_disable_by_crusher(device);
		crusher_exit_error_mode(device);
	}
}

void crusher_on_phase_complete( conveyor_device_t * device )
{
	conveyor_device_status_value_t status = device->get_status(device);
	INFO(LOGS_TYPE_CRUSHER, LOGS_STATUS_OK, "%s %s phase finish", device->name, (status==CONVEYOR_DEVICE_READY)?"closing":"opening");
	
	if(device->error_info.try_number != 0)
	{
		if(status == CONVEYOR_DEVICE_CONFIRMED_OPENED)
		{
			device->close_now(device);
			device->error_info.try_number = 0;
		}
		else
		{
			device->start_other_phase(device);
		}
	}
	else if( device->get_error_mode(device) == CONVEYOR_DEVICE_LEM && 
		 device->error_info.try_number < device->error_info.max_try_number )
	{
		reflex_enable_by_crusher(device);
		crusher_exit_error_mode(device);
	}
}

void
QPS_reflex_init_conveyor_devices( QPSReflex * reflex, Recyc * recyc )
{
	if( reflex->director == NULL ) return;

	QPSRecycData * qps = (QPSRecycData *)recyc->data;

	conveyor_device_init( reflex->director, qps );
	conveyor_device_init( reflex->crusher, qps );



	conveyor_device_off_sensor_init( reflex->director, qps );

	if( reflex->director ){
		reflex->director->on_error = director_on_error;
		reflex->director->recyc = recyc;
#if TEST_HW_GLA_ONLY
		if( strcmp("GLA director", reflex->director->name) == 0 )
		{
			reflex->director->hw_status_ok = director_check_state;
			reflex->director->on_phase_complete = director_on_phase_complete;
		}
#else

		reflex->director->hw_status_ok = director_check_state;
		reflex->director->on_phase_complete = director_on_phase_complete;
#endif
	}

	if( reflex->crusher ) 
	{
		reflex->crusher->recyc = recyc;
		reflex->crusher->on_error = crusher_on_error;
		reflex->director->next_device = reflex->crusher;

#if TEST_HW_GLA_ONLY
		if( strcmp("PET crusher", reflex->crusher->name) == 0 )
		{
			reflex->crusher->on_phase_complete = crusher_on_phase_complete;
			reflex->crusher->hw_status_ok = crusher_check_state;
		}
#else
		reflex->crusher->on_phase_complete = crusher_on_phase_complete;
		reflex->crusher->hw_status_ok = crusher_check_state;
#endif
	}
}

void
QPS_reflex_init(Recyc *recyc) {

	QPS_reflex_attach_opto_sensors(recyc);

    QPSRecycData *qps = (QPSRecycData *) recyc->data;
	
    QPS_reflex_init_conveyor_devices( &QPS_reflex_table[QPS_REFLEX_FIRST_ARM], recyc);
    for (int i = QPS_REFLEX_FIRST_SENSOR; i < NELEMS(QPS_reflex_table); ++i) {
        QPSReflex *reflex = &QPS_reflex_table[i];
       // NOAIO_CHANNEL input_channel = NOAIO_input_channel(reflex->input.bit);
       // int input_index = NOAIO_input_channel_index(input_channel);
        pthread_queue_init(&reflex->expectations);
        pthread_fifo_init(&reflex->history, QPS_REFLEX_HISTORY_SIZE);
        
	reflex->rising_edge_time = 0;

	QPS_reflex_init_conveyor_devices(reflex, recyc);
    }
    typedef void *(*thread)(void (*));
#if NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND && !USE_PTHREAD_TIMER
    for (int i = 0; i < qps->num_devices; ++i) {
        IODriver *device = &qps->devices[i];
        for (int j = 0; j < NOAIO_OUTPUTS; ++j) {
            PeriodCommandTimer *timer = &device->for_period[j];
            pthread_queue_init(&timer->queue);
            pthread_create(&timer->thread, NULL,
                           (thread) QPS_waiting_for_period,
                           timer);
            timer->output_index = j;
        }
    }
#endif // NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND
    pthread_create(&qps->conveyor.control, NULL, (thread) QPS_reflex, recyc);

    static timeout_thread_info_t info;
    pthread_queue_init(&timeout_cmd_queue);
    info.cmd_queue = &timeout_cmd_queue; 

    pthread_t timeout;
    pthread_create(&timeout, NULL, (thread)QPS_reflex_timeout, &info);

    pthread_mutex_init(&counter_mutex, NULL);
}

