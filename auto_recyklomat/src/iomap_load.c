#include <time.h>
#include <termios.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ini.h"
#include "iomap.h"
#include "util.h"

#define PRINT_DEBUG 0
#define LOAD_IO_CONFIG 1

#define SWITCH(str, code) { \
    const char * __str = (const char*)str; \
    code; \
}

#define CASE(str) if(strcmp(str, __str)==0)

// dla wszystkich wejsc i wyjsci dane w iomap.ini sa jako kanaly( 0-7, A-D )
typedef enum{UINT8, UINT16, NOAIO_CH_IN, NOAIO_CH_OUT, NOAIO_BIT_IN, NOAIO_BIT_OUT, 
		QPS_STRING, VOLTAGE_IDX, VOLTAGE_MIN, VOLTAGE_MAX, BAUDRATE} value_type_t;

typedef struct 
{
    const char * section;
    const char * name;
    const char * subname;

    void * val_dst;
    value_type_t value_type;
    
    bool was_used;
} ini_config_record_t;

char qps_arg[2048] = "qps";

#define STRING(x) XSTRING(x)
#define XSTRING(x) #x

#define OUT_CONFIG(param, type) { \
    .section = STRING(SECTION_NAME), \
    .value_type = type, \
    .val_dst = &DEVICE_NAME.param, \
    .name = STRING(DEVICE_NAME), \
    .subname = STRING(param)} 
 
#define IN_CONFIG(param, type) { \
    .section = STRING(SECTION_NAME), \
    .value_type = type, \
    .val_dst = &DEVICE_NAME.param, \
    .name = STRING(DEVICE_NAME), \
    .subname = STRING(param)} 

#define VOLTAGE_CONFIG(param, type) { \
    .section = STRING(SECTION_NAME), \
    .value_type = type, \
    .val_dst = iodriver_voltage_limits, \
    .name = STRING(DEVICE_NAME), \
    .subname = STRING(param)} 

#define TIMING_CONFIG(value) { \
    .section = STRING(SECTION_NAME), \
    .value_type = UINT16, \
    .val_dst = &DEVICE_NAME.value, \
    .name = STRING(value), \
    .subname = NULL}

#define QPS_CONFIG(_name, value) { \
    .section = STRING(SECTION_NAME), \
    .value_type = QPS_STRING, \
    .val_dst = value, \
    .name = STRING(_name), \
    .subname = NULL}

#define MISC_CONFIG(value, type) { \
    .section = STRING(SECTION_NAME), \
    .value_type = type, \
    .val_dst = &DEVICE_NAME.value, \
    .name = STRING(value), \
    .subname = NULL}

ini_config_record_t config_table[] = {
#define SECTION_NAME QPS_PARAMS
  
    QPS_CONFIG(qps_system_parameters, qps_arg),

#if LOAD_IO_CONFIG == 1
#undef SECTION_NAME
#define SECTION_NAME OUT

#define DEVICE_NAME conveyor_dir
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME pet_crusher_open
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME pet_crusher_close
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME alu_crusher_open
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME alu_crusher_close
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME conveyor_start
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME conveyor_speed_a
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME conveyor_speed_b
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME conveyor_speed_c
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME dropbox_conveyor
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),
#undef DEVICE_NAME
#define DEVICE_NAME dropbox_director
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),
/*
#undef DEVICE_NAME
#define DEVICE_NAME dropbox_door_lock
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),
*/
#undef DEVICE_NAME
#define DEVICE_NAME dropbox_leds
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME dropbox
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME dropbox_lock
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME dropbox_rolls
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME default_config
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME qps_display
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME qps_pwr
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME printer
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME gla_director_open
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME gla_director_close
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME pet_director_open
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME pet_director_close
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME alu_director_open
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef DEVICE_NAME
#define DEVICE_NAME alu_director_close
    OUT_CONFIG(device, UINT8),
    OUT_CONFIG(out, NOAIO_BIT_OUT),
    OUT_CONFIG(init_state, UINT8),
    OUT_CONFIG(min_limit_mA, UINT16),
    OUT_CONFIG(max_limit_mA, UINT16),
    OUT_CONFIG(soft_min_limit_mA, UINT16),
    OUT_CONFIG(soft_max_limit_mA, UINT16),

#undef SECTION_NAME
#define SECTION_NAME IN

#undef DEVICE_NAME
#define DEVICE_NAME alu_crusher_current_sense
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),
    IN_CONFIG(max_voltage_mv, UINT16),
    IN_CONFIG(max_ovc_cnt, UINT8),

#undef DEVICE_NAME
#define DEVICE_NAME pet_crusher_current_sense
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),
    IN_CONFIG(max_voltage_mv, UINT16),
    IN_CONFIG(max_ovc_cnt, UINT8),

#undef DEVICE_NAME
#define DEVICE_NAME pet_director_open_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME pet_director_close_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME alu_director_open_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME alu_director_close_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME gla_director_open_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME gla_director_close_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME pet_crusher_open_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME pet_crusher_close_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME alu_crusher_open_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME alu_crusher_close_sensor_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME pet_opto_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME alu_opto_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME oth_opto_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_CH_IN),

#undef DEVICE_NAME
#define DEVICE_NAME door_input
    IN_CONFIG(device, UINT8),
    IN_CONFIG(in, NOAIO_BIT_IN),

#endif

#undef SECTION_NAME
#define SECTION_NAME VOLTAGE

#undef DEVICE_NAME
#define DEVICE_NAME iodriver_voltage_limits
    VOLTAGE_CONFIG(device, VOLTAGE_IDX),
    VOLTAGE_CONFIG(min_voltage_mv, VOLTAGE_MIN),
    VOLTAGE_CONFIG(max_voltage_mv, VOLTAGE_MAX),

#undef SECTION_NAME
#define SECTION_NAME DROPBOX_TIMING

#undef DEVICE_NAME
#define DEVICE_NAME dropbox_times
    TIMING_CONFIG(delay_ms),
    TIMING_CONFIG(open_time_ms),
    TIMING_CONFIG(min_rolls_time_ms),
    TIMING_CONFIG(max_rolls_time_ms),
    TIMING_CONFIG(lock_time_ms),
    TIMING_CONFIG(rolls_time_ms),
    TIMING_CONFIG(conveyor_passage_time_ms),
    TIMING_CONFIG(led_time_ms),
    TIMING_CONFIG(door_safety_time_s),
    TIMING_CONFIG(recognition_delay_ms),
    TIMING_CONFIG(next_recognition_delay_ms),

    TIMING_CONFIG(process_pet_min_length_s),
    TIMING_CONFIG(process_alu_min_length_s),
    TIMING_CONFIG(process_deo_min_length_s),
    TIMING_CONFIG(process_bat_min_length_s),
    TIMING_CONFIG(process_syr_min_length_s),
    TIMING_CONFIG(process_gla_min_length_s),

#undef SECTION_NAME
#define SECTION_NAME CONVEYOR_TIMING

#undef DEVICE_NAME
#define DEVICE_NAME conveyor_times
    TIMING_CONFIG(conveyor_passage_time_ms),
    TIMING_CONFIG(reflex_timeout_s),
    TIMING_CONFIG(first_reflex_timeout_s),
    TIMING_CONFIG(reflex_high_state_timeout_s),
    
    TIMING_CONFIG(arm_time_ms),
    TIMING_CONFIG(first_arm_closing_delay_ms),
    TIMING_CONFIG(arm_closing_delay_ms),
    TIMING_CONFIG(crashing_time_ms),
    TIMING_CONFIG(releasing_time_ms),
    TIMING_CONFIG(crashing_delay_ms),
    TIMING_CONFIG(releasing_delay_ms),
    TIMING_CONFIG(crasher_test_offset_ms),
    TIMING_CONFIG(director_test_offset_ms),
    TIMING_CONFIG(conveyor_max_blocked_time_s),

    TIMING_CONFIG(conveyor_return_time_ms),
    TIMING_CONFIG(conveyor_step_back_time_ms),
    TIMING_CONFIG(conveyor_clean_time_ms),
    TIMING_CONFIG(director_error_delay_ms),
    TIMING_CONFIG(director_close_delay_s),
    TIMING_CONFIG(director_open_delay_s),

#undef SECTION_NAME
#define SECTION_NAME OTHER_SETTINGS

#undef DEVICE_NAME
#define DEVICE_NAME recyc_config
    MISC_CONFIG(can_pooling_perioid_ms, UINT16),
    MISC_CONFIG(crusher_max_voltage_mv, UINT16),
    MISC_CONFIG(crusher_max_ovc_cnt, UINT8),
    MISC_CONFIG(opto_max_error_cnt, UINT8),
    MISC_CONFIG(can_baudrate, BAUDRATE),
    MISC_CONFIG(watchdog_mode, UINT8),
    MISC_CONFIG(watchdog_interval, UINT8),
    MISC_CONFIG(maintenance_start_hour, UINT8),
    MISC_CONFIG(maintenance_stop_hour, UINT8),
};

static inline char * ini_get_value( ini_config_record_t * record, const char * section, const char * name, const char * value )
{
    bool section_match = strcmp( record->section, section ) == 0;
    bool name_match = strcmp(record->name, name) == 0;
    bool subname_match = 1;    

    char * value_ptr = (char*)value;

    if( record->subname )
    {
        char * subname_end = strchr(value, ':');

        if( subname_end == NULL ) subname_match = 0;
        else
        {
            uint8_t len = subname_end-value;
            subname_match = strncmp(record->subname, value, len) == 0;
        }
        
        value_ptr = subname_end+1;
    }

    if( section_match && name_match && subname_match ) 
    {   
#if PRINT_DEBUG
        printf("%s %s %s $%s\n", section, name, value, value_ptr);
#endif
        return value_ptr;
    }
    else 
        return NULL;
}

static void save_value_u8( void * ptr, const char * value )
{
    (*(uint8_t*)ptr) = atoi(value);
}

static void save_value_u16( void * ptr, const char * value )
{
    (*(uint16_t*)ptr) = atoi(value);
}

bool check_uninitialized( void * ptr, const char * value )
{
	if( strcmp("NONE", value) == 0 ) 
	{
		(*(NOAIO_CHANNEL*)ptr) = NOAIO_UNINITIALIZED; 
		return 1;
	}

	return 0;
}

static void save_value_ch_in( void * ptr, const char * value )
{
	if( check_uninitialized(ptr, value) ) return;

    (*(NOAIO_CHANNEL*)ptr) = *value - 'A' + 10;
}

static void save_value_ch_out( void * ptr, const char * value )
{
	if( check_uninitialized(ptr, value) ) return;
    save_value_u8(ptr, value);
}

static void save_value_bit_in( void * ptr, const char * value )
{
	if( check_uninitialized(ptr, value)  || value[0] < 'A' || value[0] > 'D') return;

	uint8_t channel = value[0] - 'A';
    NOAIO_INPUT_BITS bit = 1<<channel;

    (*(NOAIO_INPUT_BITS*)ptr) = bit;
}

static void save_value_bit_out( void * ptr, const char * value )
{   
	if( check_uninitialized(ptr, value) ) 
	{
		return;
	}

    NOAIO_OUTPUT_BITS bit;

    uint8_t channel = atoi(value) & (NOAIO_OUTPUTS-1);
    bit = 1<<channel;

    (*(NOAIO_OUTPUT_BITS*)ptr) = bit;
}

static void save_value_qps_string( void * ptr, const char * value )
{
	if( strlen(value) == 0 ) return;

	// wydobycie poczatku docelowej lokalizacji danych
	char * dst = (char*)ptr;
	size_t old_len = strlen(dst);
		
	sprintf(dst+old_len, " %s", value);

	char * colon_ptr = strchr(dst+old_len, ':');
	if( colon_ptr ) *colon_ptr = '=';
}

static uint8_t vol_limit_idx = 0;

static void save_value_vol_idx( void * ptr, const char * value )
{
	vol_limit_idx = atoi(value);
	iodriver_voltage_limits_t * limits = (iodriver_voltage_limits_t*)ptr + vol_limit_idx;
	limits->device = vol_limit_idx;	
}

static void save_value_vol_min( void * ptr, const char * value )
{
	iodriver_voltage_limits_t * limits = (iodriver_voltage_limits_t*)ptr + vol_limit_idx;

        limits->min_voltage_mv = atoi(value);
}

static void save_value_vol_max( void * ptr, const char * value )
{
	iodriver_voltage_limits_t * limits = (iodriver_voltage_limits_t*)ptr + vol_limit_idx;

        limits->max_voltage_mv = atoi(value);
}

static void save_value_baudrate( void * ptr, const char * value )
{
    speed_t * baud = ptr;

    SWITCH(value, {
        CASE("2400") *baud = B2400;
        CASE("4800") *baud = B4800;
        CASE("9600") *baud = B9600;
        CASE("19200") *baud = B19200;
        CASE("38400") *baud = B38400;
        CASE("57600") *baud = B57600;
        CASE("115200") *baud = B115200;
        CASE("500000") *baud = B500000;
        CASE("1000000") *baud = B1000000;
    });
}

static void save_value( ini_config_record_t * record, const char * value )
{
//    if( strcmp(value, "NONE") == 0 ) return;

    void(*save_functions[])(void*,const char*) = {
        save_value_u8, save_value_u16, 
        save_value_ch_in, save_value_ch_out,
        save_value_bit_in, save_value_bit_out,
	save_value_qps_string, save_value_vol_idx,
        save_value_vol_min, save_value_vol_max,
        save_value_baudrate
    };

    save_functions[record->value_type](record->val_dst, value);
}

static int handler( void * arg, const char * section, const char * name, const char * value )
{
    for( uint16_t i=0; i<NELEMS(config_table); i++ )
    {
        ini_config_record_t * record = &config_table[i];
        char * val_ptr;

        if( (val_ptr = ini_get_value(record, section, name, value )) )
        {
            save_value(record, val_ptr);
            record->was_used = 1;
        }
    }

    return 1;
}

static void check_complete( void )
{
    bool was_any_missed = 0;

    for( uint16_t i=0; i<NELEMS(config_table); i++ )
    {
        ini_config_record_t * record = &config_table[i];

        if( record->was_used == 0 )
        {
            was_any_missed = 1;

            if( record->subname )
                WARN("%s:%s was not found in ini file", record->name, record->subname);
            else
                WARN("%s was not found in ini file", record->name);
        } 
    }

    if( was_any_missed )
    {
        WARN("Ini file not complete%c", '!');
    }
    else
    {
        INFO("Ini file loaded %s", "[OK]");
    }
}

void show_values( void )
{
    for( uint16_t i=0; i<NELEMS(config_table); i++ )
    {
        ini_config_record_t * record = &config_table[i];

        if( record->value_type == UINT16 )
            printf("%s:%s = %d\n", record->name, record->subname, *(uint16_t*)record->val_dst);
        else
            printf("%s:%s = %d\n", record->name, record->subname, *(uint8_t*)record->val_dst);
        
    }
}

char * load_iomap_config( const char * filename )
{       
	INFO("Loading ini file");
    ini_parse(filename, handler, NULL);
    check_complete();

    update_conveyor_devices_config();

#if PRINT_DEBUG 
    printf("\n\n\n");
    show_values();
    while(1);
#endif

    return qps_arg;
}       
