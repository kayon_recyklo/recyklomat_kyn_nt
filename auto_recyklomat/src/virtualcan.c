
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define virtualcan
#include "util.h"

#include "pthread-queue.h"	// =======
#include "qps-logs.h"

typedef struct {
    int in;
    int out;
} channel;

int main(int argc, char *argv[]) {
    bool verbose = false;
    if (!strcmp(argv[1], "-v")) {
        verbose = true;
        --argc;
        ++argv;
    }
    
    int num_channels = (argc - 1) / 2;
    channel *channels = ARRAY(channel, num_channels);
    
    int nfds = -1;
        
    for (int i = 0; i < num_channels; ++i) {
        channels[i].out = open(argv[2*i+2], O_WRONLY);
        INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "opened output %s", argv[2*i+2]);

        channels[i].in = open(argv[2*i+1], O_RDONLY);
        INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "opened input %s", argv[2*i+1]);
        
        nfds = max(channels[i].in, nfds);
    }
    ++nfds;

    while (1) {
        fd_set inputs;
        FD_ZERO(&inputs);
        for (int i = 0; i < num_channels; ++i) {
            FD_SET(channels[i].in, &inputs);
        }
        select(nfds, &inputs, NULL, NULL, NULL);
        for (int i = 0; i < num_channels; ++i) {
            if (FD_ISSET(channels[i].in, &inputs)) {
                char buffer[1024];
                int n = read(channels[i].in, buffer, sizeof(buffer));
                if (verbose) {
                    printf("%s: ", argv[2*i+1]);
                    DUMPHEX(buffer, n);
                }
                //assert(n > 0);
                for (int j = 0; j < num_channels; ++j) {
                    int m = write(channels[j].out, buffer, n);
                    assert(n == m);
                    if (verbose) {
                        printf("wrote %d bytes to %s\n", m, argv[2*j+2]);
                    }
                }
            }
        }
    }
    
    return 0;
}

