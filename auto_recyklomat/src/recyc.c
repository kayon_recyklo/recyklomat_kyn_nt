#include "recyc.h"
#include "util.h"
#include "qps.h"

/*
 * Logika sterownika recyklomatu. To wlasnie w tym miejscu
 * (Recyc_onClosed) znajduje sie logika sterowania.
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl>
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */


static void
ignore_report(Supervisor *self, Report condition, ...) { }

Supervisor *ignoramus = &(Supervisor) {
    .report = ignore_report,
    .data = NULL
};

typedef struct {
    const char *prefix;
    Recyc *(*constructor)(const char *args);
} RecycConstructor; 

static RecycConstructor registered_types[RECYC_MAX_REGISTERED_TYPES];

static int num_registered_types = 0;

bool
Recyc_register(const char *prefix,
                    Recyc *(*constructor)(const char *args)) {
    if (num_registered_types < RECYC_MAX_REGISTERED_TYPES) {
        registered_types[num_registered_types++] = (RecycConstructor) {
            .prefix = prefix,
            .constructor = constructor
        };
        return true;
    }
    return false;
}

void
Recyc_onClosed(Recyc *self) {
    // tutaj piszemy sobie logike sterowania na wysokim poziomie abstrakcji:
     	time_t t = time(NULL); \
     	struct tm tm = *localtime(&t);
	int maintenance = 0;
	if(MAINTENANCE_START_HOUR < MAINTENANCE_STOP_HOUR) {
		if(tm.tm_hour >= MAINTENANCE_START_HOUR && tm.tm_hour < MAINTENANCE_STOP_HOUR) {
			maintenance = 1;
		}
		else {
			maintenance = 0;
		}
	}
	else if(MAINTENANCE_START_HOUR > MAINTENANCE_STOP_HOUR) {
		if(tm.tm_hour >= MAINTENANCE_START_HOUR || tm.tm_hour < MAINTENANCE_STOP_HOUR) {
			maintenance = 1;
		}
		else {
			maintenance = 0;
		}
	}
	if(!maintenance){	
		self->lock(self);
		QPSRecycData *qps = (QPSRecycData *) self->data;
		self->recognizeItem(self);
    
    		if (qps->recognition.recognized_item == ItemTypeNone) {
        		self->unlock(self);
			return;
    		}
    		qps->process_min_length = QPS_get_process_min_length(qps->recognition.recognized_item);
    		self->supervisor->report(self->supervisor, ReportRecognizedItem, qps->recognition.recognized_item);
//    self->lock(self);
    		self->supervisor->report(self->supervisor, ReportStatusProcessing);

    		if (!self->isClosed(self)) {
        		self->unlock(self);
        		self->supervisor->report(self->supervisor, ReportStatusIdle);
        		return;
    		}
   		self->processItem(self, qps->recognition.recognized_item);
	}
	else {
		INFO(LOGS_TYPE_SERVICE, LOGS_STATUS_NONE, "Maintenance in progress %d - %d", MAINTENANCE_START_HOUR, MAINTENANCE_STOP_HOUR);
	}
}

Recyc *
Recyc_init(const char *prefix_with_args, Supervisor *supervisor) {
    for (int i = 0; i < num_registered_types; ++i) {
        const char *args = is_prefix_token(registered_types[i].prefix,
                                           prefix_with_args);

	MESSAGE("init before if(args)");

        if (args) {
		MESSAGE("init before recyc=");
            Recyc *recyc = registered_types[i].constructor(skip_chars(isspace, args));
            MESSAGE("init after recyc=");

	    if (recyc) {
                recyc->supervisor = supervisor;
                recyc->onClosed(recyc, Recyc_onClosed);

		MESSAGE("init after onClosed");
            }

	    MESSAGE("init return recyc");
            return recyc;
        }
    }
    MESSAGE("init return NULL");
    return NULL;
}


