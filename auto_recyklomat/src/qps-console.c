/*
 * qps-console.c
 *
 *  Created on: May 30, 2019
 *      Author: mliszewskn
 */
#define qpsconsole

#include "qps.h"
#include "qps-console.h"
#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "sockets.h"

#define QUIT_CONSOLE "QC"
#define TEST_CMD "TC"
#define PET_RECOG "PR"
#define ALU_RECOG "AR"
#define GLA_RECOG "GR"
#define DEO_RECOG "DR"
#define BAT_RECOG "BR"
#define SYR_RECOG "SR"
#define OTH_RECOG "OR"
#define NO_RECOG "NR"
#define CLOSE_DOORS "CD"
#define OPEN_DOORS "OD"
#define RELOAD_CONFIG "RC"
#define EMPTY_MESSAGE ""
#define RELOAD_IOMAP "RI"

static void help(const char *args, int socket);

static void test(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "TESTING CONSOLE");
    write(socket, TEST_CMD, strlen(TEST_CMD));
}

static void mock_PET_recognition(const char *args, int socket) {
   INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating PET recognition"); 
   write(socket, PET_RECOG, strlen(PET_RECOG));
}

static void mock_ALU_recognition(const char *args, int socket) {
   INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating ALU recognition"); 
   write(socket, ALU_RECOG, strlen(ALU_RECOG));
}

static void mock_GLA_recognition(const char *args, int socket) {
   INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating GLA recognition"); 
   write(socket, GLA_RECOG, strlen(GLA_RECOG));
}

static void mock_DEO_recognition(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating DEO recognition");
    write(socket, DEO_RECOG, strlen(DEO_RECOG));
}

static void mock_BAT_recognition(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating BAT recognition");
    write(socket, BAT_RECOG, strlen(BAT_RECOG));
}

static void mock_SYR_recognition(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating SYR recognition");
    write(socket, SYR_RECOG, strlen(SYR_RECOG));
}

static void mock_OTH_recognition(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating OTH recognition");
    write(socket, OTH_RECOG , strlen(OTH_RECOG));
}

static void mock_NO_recognition(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Turning off recognition simulation");
    write(socket, NO_RECOG, strlen(NO_RECOG));
}

static void mock_closing_doors(const char *args, int socket) {
	INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating closing doors");
    write(socket, CLOSE_DOORS , strlen(CLOSE_DOORS));
}

static void mock_opening_doors(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Simulating opening doors");
    write(socket, OPEN_DOORS , strlen(OPEN_DOORS));
}


static void quit_console(const char *args, int socket) {
   write(socket, QUIT_CONSOLE , strlen(QUIT_CONSOLE));
   close_socket_conn(socket, "talk_socket");
   exit(0);
}
static void reload_iomap(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Reloading io configuration");
    write(socket, RELOAD_IOMAP, strlen(RELOAD_IOMAP));
}


static qps_console_command_t commands[] = {
	{ .prefix = "test", .action = test, "test console" },
	{ .prefix = "close doors", .action = mock_closing_doors, "simulation of closing doors" },
	{ .prefix = "open doors", .action = mock_opening_doors, "simulation of opening doors" },
	{ .prefix = "recognize PET", .action = mock_PET_recognition, "simulation of PET recognition during next operation" },
	{ .prefix = "recognize ALU", .action = mock_ALU_recognition, "simulation of ALU recognition during next operation" },
	{ .prefix = "recognize GLA", .action = mock_GLA_recognition, "simulation of GLA recognition during next operation" },
	{ .prefix = "recognize DEO", .action = mock_DEO_recognition, "simulation of DEO recognition during next operation" },
	{ .prefix = "recognize BAT", .action = mock_BAT_recognition, "simulation of BAT recognition during next operation" },
	{ .prefix = "recognize SYR", .action = mock_SYR_recognition, "simulation of SYR recognition during next operation" },
	{ .prefix = "recognize OTH", .action = mock_OTH_recognition, "simulation of OTH recognition during next operation" },
	{ .prefix = "recognize NONE", .action = mock_NO_recognition, "turn off recognition mock"},
	{ .prefix = "quit", .action = quit_console, "closing socket and quiting console program" },
	{ .prefix = "reload iomap", .action = reload_iomap, "reload io configuration" },
	{ .prefix = "help", .action = help, "list available commands" },
	{ .prefix = "cd", .action = mock_closing_doors, "simulation of closing doors" },
	{ .prefix = "od", .action = mock_opening_doors, "simulation of opening doors" },
	{ .prefix = "rp", .action = mock_PET_recognition, "simulation of PET recognition during next operation" },
	{ .prefix = "ra", .action = mock_ALU_recognition, "simulation of ALU recognition during next operation" },
	{ .prefix = "rg", .action = mock_GLA_recognition, "simulation of GLA recognition during next operation" },
	{ .prefix = "rd", .action = mock_DEO_recognition, "simulation of DEO recognition during next operation" },
	{ .prefix = "rb", .action = mock_BAT_recognition, "simulation of BAT recognition during next operation" },
	{ .prefix = "rs", .action = mock_SYR_recognition, "simulation of SYR recognition during next operation" },
	{ .prefix = "ro", .action = mock_OTH_recognition, "simulation of OTH recognition during next operation" },
	{ .prefix = "rn", .action = mock_NO_recognition, "turn off recognition mock"},

};

static void help(const char *args, int socket) {
    INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "Available commands:");
    for (int i = 0; i < NELEMS(commands); ++i) {
        INFO(LOGS_TYPE_CONSOLE, LOGS_STATUS_NONE, "  %s -- %s", commands[i].prefix, commands[i].help);
    }
}

static void command_dispatch(const char *line, int socket) {
    for (int i = 0; i < NELEMS(commands); ++i) {
        const char *args;
        if ((args = is_prefix_token(commands[i].prefix, line))) {
            return commands[i].action(skip_chars(isspace, args), socket);
        }
    }
    WARN(LOGS_TYPE_CONSOLE, "Unrecognized command: `%s'. Type `help' to list available commands.",
         line);
}

int main(int argc, char* argv[]) {
    INFO(LOGS_TYPE_INIT, LOGS_STATUS_NONE, "Starting QPS talk program");
    int fd = init_socket("talk_socket");
    while( socket_client_conn(fd, "/tmp/talk-socket", "talk_socket") == 0 ) usleep(2000*1000L);
    while(1) {
		char *line = readline(NULL);
		command_dispatch(line, fd);
		free(line);
	}
	return 0;
}

