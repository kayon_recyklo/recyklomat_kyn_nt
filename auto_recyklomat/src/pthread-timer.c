#include <pthread.h>
#include "util.h"

#ifdef NDEBUG
#  define PTRY(pcall) (void) pcall
#else // !defined NDEBUG
#  define PTRY(pcall) if (pcall) { ERROR(LOGS_TYPE_CODE, #pcall " failed"); }
#endif // !defined NDEBUG

typedef struct {
    void (*action)(void *);
    void *arg;
    int time_ms;
    pthread_t thread;
} delayed_action_t;

static void *
run_delayed_action(delayed_action_t *delayed) {
    PTRY(pthread_detach(delayed->thread));
    usleep(1000 * delayed->time_ms);
    delayed->action(delayed->arg);
    free(delayed);
    return NULL;
}

void
pthread_after_ms(int time_ms, void (*action)(void *), void *arg) {
    delayed_action_t *delayed = MAKE(delayed_action_t,
                                     .action = action,
                                     .arg = arg,
                                     .time_ms = time_ms);
    PTRY(pthread_create(&delayed->thread, NULL,
                        (void *(*)(void *)) run_delayed_action,
                        delayed));
}
