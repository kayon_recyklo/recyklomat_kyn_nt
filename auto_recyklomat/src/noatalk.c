#define _GNU_SOURCE

#define noatalk

#include <readline/readline.h>
#include <readline/history.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include "util.h"
#include <sys/ioctl.h>
#include <getopt.h>
#include <linux/serial.h>

#ifndef NOAIO_H		// noio.h jest includowane w iomap.h
#include "noaio.h"
#endif

#include "textparams.h"

#include "iomap.h"	// <-- w iomap.h znajduja sie wartosi domyslne limitow napiecia i pradowe


typedef struct {
    enum { Running, Finished } mode;
    uint32_t address;
    int in;
    int out;
} ProgramState;

typedef struct {
    const char *prefix;
    ProgramState (*action)(const char *command, ProgramState state);
    const char *help;
} command_t;

typedef struct 
{
    NOAIO_BAUDRATE noaio_baud;
    speed_t baud;
    char * name;
} speed_info_t;

static ProgramState help(const char *args, ProgramState state);

static ProgramState finish(const char *args, ProgramState state) {
    INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Exiting program");
    state.mode = Finished;
    return state;
}

static NOAIO_INCOMMING_FRAME incomming_frame = {
    .next = 0,
    .shift = 0
};

static speed_info_t baudrates[] = { {NOAIO_BAUD_2400, B2400, "2400"}, {NOAIO_BAUD_4800, B4800, "4800"}, 
    {NOAIO_BAUD_9600, B9600, "9600"}, {0, 0, "none"}, {NOAIO_BAUD_19200, B19200, "19200"}, 
    {0, 0, "none"}, {NOAIO_BAUD_38400, B38400, "38400"}, {NOAIO_BAUD_57600, B57600, "57600"}, 
    {0, 0, "none"}, {NOAIO_BAUD_115200, B115200, "115200"}, {NOAIO_BAUD_250000, 0, "250000"},
    {NOAIO_BAUD_500000, B500000, "500000"}, {NOAIO_BAUD_1000000, B1000000, "1000000"}};

#define NOAIO_SEND_REQUEST(out, in, addr, req, ...) ({                  \
            NOAIO_DECLARE_REQUEST_FRAME(local_frame,                    \
                                        req, addr,                      \
                                        ## __VA_ARGS__);                \
            NOAIO_request(out, in,                                      \
                          (NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *)        \
                          &local_frame, &incomming_frame,               \
                          CAN_RESPONSE_TIMEOUT_MS);                     \
        })

static ProgramState purge(const char *args, ProgramState state) {
    int n;
    do {
        n = read_with_timeout(state.in,
                              incomming_frame.buffer,
                              sizeof(incomming_frame.buffer),
                              CAN_RESPONSE_TIMEOUT_MS);
        DUMPHEX(incomming_frame.buffer, n);
    } while (n > 0);
    incomming_frame.next = 0;
    incomming_frame.shift = 0;
    return state;
}


#define WITH_RESPONSE_FRAME(frame, req, framename, action)              \
    if (frame) {                                                        \
        if (frame->header.request == NOAIO_RESPONSE_##req) {            \
            NOAIO_RESPONSE_FRAME_##req *framename                       \
                = &frame->RESPONSE.req;                                 \
            action;                                                     \
        }                                                               \
        else {                                                          \
            WARN(LOGS_TYPE_NOAIO, "Invalid response");                                    \
        }                                                               \
    }

#define WITH_RESPONSE(framename, action, state, request, ...)  {         \
        NOAIO_FRAME *raw_frame = NOAIO_SEND_REQUEST(state.out,          \
                                                    state.in,           \
                                                    state.address,      \
                                                    request,            \
                                                    ## __VA_ARGS__);    \
        WITH_RESPONSE_FRAME(raw_frame, request, framename, action);     \
    }

static ProgramState version(const char *args, ProgramState state) {
    WITH_RESPONSE(response, {
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "NoaTech IO Driver %x version %d.%d", state.address,
                response->version_major, response->version_minor);
        }, state, GET_VERSION);
    return state;
}


static ProgramState status(const char *args, ProgramState state) {
       
    if( (state.address & IODRIVER_TYPE_MASK) == IODRIVER_TYPE_REDRIVER )
    { 
        for( NOAIO_ANALOG_CHANNEL i=NOAIO_ANALOG_INPUT_A; i<=NOAIO_ANALOG_INPUT_D; i++ )
        {    
            WITH_RESPONSE(info, {
                INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "analog in%c=%dmV", i-NOAIO_ANALOG_INPUT_A+'A', info->input_voltage);
            }, state, GET_INPUT_INFO, i); 
        }
    }

    WITH_RESPONSE(response, {
            for (int i = 0; i < NOAIO_INPUTS; ++i) {
                INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "in%c = %d", i+'A', !!(response->input_state & (1 << i)));
            }
            
            for (int i = 0; i < NOAIO_OUTPUTS; ++i) {
                INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "out%d = %d (%d)", i, !!(response->output_latch & (1 << i)),
                    !!(response->output_state & (1 << i)));
            }
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "system voltage = %dmV", response->system_voltage);
        }, state, GET_STATE);
    return state;
}

static ProgramState set_watchdog( const char * args, ProgramState state )
{
    char * mode_s = strtok( (char*)args, " ");
    char * mask_s = strtok(NULL, " ");
    char * interval_s = strtok(NULL, " ");

    uint8_t mode = atoi(mode_s);
    uint8_t mask = atoi(mask_s);
    uint8_t interval = atoi(interval_s);

    printf("mode: %d, mask: %d, interval: %d\n", mode, mask, interval);

    (void) NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_WATCHDOG,
                              .watchdog_mode = mode, .output_mask = mask, .watchdog_interval= interval);
    return state;
}

static ProgramState get_watchdog( const char * args, ProgramState state )
{
    uint8_t mode, mask, interval;
    WITH_RESPONSE(response, {
        mode = response->watchdog_mode;
        mask = response->output_mask;
        interval = response->watchdog_interval;
    }, state, GET_WATCHDOG);

    printf("mode: %d, mask: %d, interval: %d\n", mode, mask, interval);
    return state;
}

static ProgramState reset_watchdog( const char * args, ProgramState state )
{
    (void) NOAIO_SEND_REQUEST(state.out, state.in, state.address, WATCHDOG_RESET);
    return state;
}

static ProgramState set_baudrate( const char * args, ProgramState state )
{
    NOAIO_BAUDRATE speed = 0;
    for( uint8_t i=0; i<NELEMS(baudrates); i++ )
    {
        if( strcmp(args, baudrates[i].name) == 0 )
        {
            speed = baudrates[i].noaio_baud;
            break;
        } 
    }

    if( speed )
    {
        (void) NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_BAUDRATE, .baud_rate_index = speed);
    }
    else
    {       
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Unsupported baudrate!");
    }
    return state;
}

static ProgramState supported_baudrates( const char * args, ProgramState state )
{
    printf("supported baudrates:\n");
    for( uint8_t i=0; i<NELEMS(baudrates); i++ ) 
    {
        if( baudrates[i].baud )
            printf("%s, ", baudrates[i].name);
    }

    printf("\n");   

    return state; 
}

static inline NOAIO_OUTPUT_BITS set_bit(NOAIO_OUTPUT_BITS outputs, int bit) {
    return outputs | (1 << bit);
}

static inline NOAIO_OUTPUT_BITS clear_bit(NOAIO_OUTPUT_BITS outputs, int bit) {
    return outputs & ~(1 << bit);
}

static inline ProgramState
transform_outputs(NOAIO_OUTPUT_BITS (*f)(NOAIO_OUTPUT_BITS, int),
                  const char *args, ProgramState state) {
    NOAIO_OUTPUT_BITS outputs;
    WITH_RESPONSE(response, {
            outputs = response->output_latch;
        }, state, GET_STATE);

    for (const char *p = args; *p; p = skip_chars(isspace,
                                                  skip_chars(isgraph, p))) {
        int n;
        if(sscanf(p, "%d", &n) != 1) {
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Invalid argument: %s", p);
            return state;
        }
        assert(0 <= n && n < NOAIO_OUTPUTS);
        outputs = f(outputs, n);
    }
    
    (void) NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_OUTPUT,
                              .output_latch = outputs);
    
    return state;
}

static ProgramState set_outputs(const char *args, ProgramState state) {
    return transform_outputs(set_bit, args, state);
}

static ProgramState clear_outputs(const char *args, ProgramState state) {
    return transform_outputs(clear_bit, args, state);
}

static ProgramState events(const char *args, ProgramState state) {
    WITH_RESPONSE(response, {
            uint8_t num_events = NOAIO_num_events(response);
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "%d events", num_events);
            for (int i = 0; i < num_events; ++i) {
                INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "event %d: %02x at %d", i, response->events[i].event,
                    FROM_LITTLE_ENDIAN_3(response->events[i].time));
            }
        }, state, GET_EVENTS);
    return state;
}

static ProgramState change_address(const char *args, ProgramState state) {
    int address;
    if (sscanf(args, "%x", &address) == 1) {
        state.address = address;
    }
    else {
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Invalid argument: %s", args);
    }
    return state;

}

static ProgramState get_defaults(const char *args, ProgramState state) {
    WITH_RESPONSE(response, {
            for (int i = 0; i < NOAIO_OUTPUTS; ++i) {
                INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "default out%d = %d", i, !!(response->output_latch & (1 << i)));
            }
        }, state, GET_DEFAULT_OUTPUT);
    return state;
}

static ProgramState get_current_limits(const char *args, ProgramState state){						// wyswietla obecne limity pradowe
	for(int channel=0; channel<=7; channel++){
		WITH_RESPONSE(response, {
			INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Current limits on out no. %d:\tMIN: %dmA\tMAX: %dmA\tcurrent peak:%d", response->channel, response->minimum_ex_current, response->maximum_ex_current, response->peak_current);
		}, state, GET_OUTPUT_INFO, .channel = channel);
	}
	return state;
}

static ProgramState set_current_limits(const char *args, ProgramState state){				// ustawia limity pradowe na wartosci podane przez uzytkownika
	
	
	int min;
	int max;
	printf("set current limits (in mA)\n");
	for(int channel=0; channel<8; channel++){
		printf("out no. %d\nMINIMUM: ", channel);
		scanf("%d", &min);

		printf("MAXIMUM: ");
		scanf("%d", &max);
		NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_CURRENT_LIMITS, .channel = channel, .minimum_ex_current = min, .maximum_ex_current = max);
		INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Setting new current limits for out no. %d MIN: %dmA\tMAX: %dmA", channel, min, max);
	}
	
	
	return state;
}

static ProgramState get_voltage_limits(const char *args, ProgramState state){						// wyswietla obecne limity napiecia
	WITH_RESPONSE(response, {
		INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Voltage limits:\tMIN: %dmV\tMAX: %dmV", response->minimum_ex_voltage, response->maximum_ex_voltage);
	}, state, GET_VOLTAGE_LIMITS);
	return state;
}


static ProgramState set_voltage_limits(const char *args, ProgramState state){		// ustawia limity napiecia na wartosci podane uzytkownika
	
	int min;
	int max;
	printf("set voltage limits (in mV)\nMINIMUM: ");
	scanf("%d", &min);

	printf("MAXIMUM: ");
	scanf("%d", &max);
	NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_VOLTAGE_LIMITS, .minimum_ex_voltage = min, .maximum_ex_voltage = max);
	INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Setting new voltage limits MIN: %dmV\tMAX: %dmV", min, max);
	
	return state;
}
/*
static ProgramState set_voltage_limits_default(const char *args, ProgramState state){		// ustawia limity napieciowe na wartosci domyslne

	
	NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_VOLTAGE_LIMITS, .minimum_ex_voltage = DEFAULT_MIN_VOLTAGE_MV, .maximum_ex_voltage = DEFAULT_MAX_VOLTAGE_MV);
	INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Voltage limits set to defautl values");

	return state;
}

static ProgramState set_current_limits_default(const char *args, ProgramState state){		// ustawia limity pradowe na wartosci domyslne
	for(int channel = 0; channel<=7; channel++){
		NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_CURRENT_LIMITS, .channel = channel, .minimum_ex_current = DEFAULT_MIN_CURRENT_MA, .maximum_ex_current = DEFAULT_MAX_CURRENT_MA);
		//OUT("Current limits set to defautl values");
	}
	return state;
}
*/
static inline ProgramState
transform_defaults(NOAIO_OUTPUT_BITS (*f)(NOAIO_OUTPUT_BITS, int),
                          const char *args, ProgramState state) {
    NOAIO_OUTPUT_BITS outputs;
    WITH_RESPONSE(response, {
            outputs = response->output_latch;
        }, state, GET_DEFAULT_OUTPUT);

    for (const char *p = args; *p; p = skip_chars(isspace,
                                                  skip_chars(isgraph, p))) {
        int n;
        if(sscanf(p, "%d", &n) != 1) {
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Invalid argument: %s", p);
            return state;
        }
        assert(0 <= n && n < NOAIO_OUTPUTS);
        outputs = f(outputs, n);
    }
    
    (void) NOAIO_SEND_REQUEST(state.out, state.in, state.address, SET_DEFAULT_OUTPUT,
                              .output_latch = outputs);
    
    return state;
}


static ProgramState set_defaults(const char *args, ProgramState state) {
    return transform_defaults(set_bit, args, state);
}

static ProgramState clear_defaults(const char *args, ProgramState state) {
    return transform_defaults(clear_bit, args, state);
}

static command_t commands[] = {
    { .prefix = "exit", .action = finish, "exit program" },
    { .prefix = "purge", .action = purge, "flush data from serial port" },
    { .prefix = "version", .action = version, "retrieve and print device version" },
    { .prefix = "status", .action = status, "query device status" },
    { .prefix = "events", .action = events, "read events from device" },
    { .prefix = "set", .action = set_outputs, "set output values" },
    { .prefix = "clear", .action = clear_outputs, "clear output values" },
    { .prefix = "default", .action = get_defaults, "get default outputs" },

    { .prefix = "set_current_limits", .action = set_current_limits, "set current limits " },				// funkcje ustawiania wartosci limitow
    { .prefix = "set_voltage_limits", .action = set_voltage_limits, "set voltage limits" },    

    { .prefix = "current_limits", .action = get_current_limits, "get current limits" },					// funkcje wyswietlajace aktualne limity
    { .prefix = "voltage_limits", .action = get_voltage_limits, "get voltage limits" },    
// funkcje ustawiajace limity na wartosci domyslne z pliku iomap.h
//    { .prefix = "set_current_limits_default", .action = set_current_limits_default, "set current limits to default" },
//    { .prefix = "set_voltage_limits_default", .action = set_voltage_limits_default, "set voltage limits to default" },    

    { .prefix = "set_default", .action = set_defaults, "set default values" },
    { .prefix = "clear_default", .action = clear_defaults, "clear default values" },
    
    { .prefix = "address", .action = change_address, "change target device address" },
    { .prefix = "help", .action = help, "list available commands" },

    { .prefix = "wdt_set", .action = set_watchdog, "set watchdog timer" },
    { .prefix = "wdt_get", .action = get_watchdog, "get watchdog settings" },
    { .prefix = "wdt_reset", .action = reset_watchdog, "reset watchdog timer" },

    { .prefix = "set_baudrate", .action = set_baudrate, "change CAN baudrate" },
    { .prefix = "supported_baudrates", .action = supported_baudrates, "get list of supported baudrates"}
};


static speed_t get_baudrate( char * arg )
{
    for( uint8_t i=0; i<NELEMS(baudrates); i++ )
    {
        if( strcmp(baudrates[i].name, arg) == 0 )
        {
            INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "speed: %s", arg);
            return baudrates[i].baud;
        }
    }

    assert( false && "unsupported baudrate" );
    return 0;
}

static ProgramState help(const char *args, ProgramState state) {
    INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Available commands:");
    for (int i = 0; i < NELEMS(commands); ++i) {
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "  %s -- %s", commands[i].prefix, commands[i].help);
    }
    return state;
}

static ProgramState command_dispatch(const char *line, ProgramState state) {
    for (int i = 0; i < NELEMS(commands); ++i) {
        const char *args;
        if ((args = is_prefix_token(commands[i].prefix, line))) {
            return commands[i].action(skip_chars(isspace, args), state);
        }
    }
    WARN(LOGS_TYPE_NOAIO, "Unrecognized command: `%s'. Type `help' to list available commands.",
         line);

    return state;
}

void process_history( char * line )
{
    HIST_ENTRY * last = current_history();

    if( (last != NULL) && (strcmp(line, last->line) == 0) ) return;
    
    add_history(line);
}

int main(int argc, char *argv[]) {
    if (argc < 2 || argc > 5) {
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Usage: %s <SPEED> <IN> [<OUT>] [<xADDR>]", argv[0]);
        return -1;
    }

    const char *infile = argv[2];
    const char *outfile = infile;
    
    ProgramState programState = {
        .mode = Running,
        .address = (argc > 3) ? parse_hex(argv[3], 0) : 0,
    };

    if (argc > 4) {
        if (sscanf(argv[argc-1], "%x", &programState.address) == 1) {
            outfile = argv[argc-2];
        }
        else {
            programState.address = 0;
            outfile = argv[argc-1];
        }
    }

    if (infile == outfile) {
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Opening a single file");
        programState.in = programState.out = open(infile, O_RDWR);
    }
    else {
        INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Opening pair of files");
        programState.in = open(infile, O_RDONLY);
        programState.out = open(outfile, O_WRONLY);
    }

    assert(programState.in > 0);
    assert(programState.out > 0);
    
    if (programState.in == programState.out) {
#if 0
	if( set_custom_baudrate( programState.in, atol(argv[1]) ) == -1 )
	{
		INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Unable to sed baudrate %s", argv[1]);
		return -1;
	}
	INFO(LOGS_TYPE_NOAIO, LOGS_STATUS_NONE, "Baudrate: %s", argv[1]);    
#else
	speed_t speed = get_baudrate(argv[1]);
#endif
        struct termios uart = {0};
        cfmakeraw(&uart);
        cfsetospeed(&uart, speed);
        cfsetispeed(&uart, speed);
        uart.c_cflag |= CS8 | CREAD | CLOCAL;
        uart.c_cc[VMIN] = 1;
        uart.c_cc[VTIME] = 5;
        tcsetattr(programState.in, TCSANOW, &uart);
    }

    using_history();

    char prompt[32];
        
    while (programState.mode != Finished) {
        snprintf(prompt, sizeof(prompt), "%08x@%s> ", programState.address, infile);
        char *line = readline(prompt);
        if (!line) {
            break; // EOF
        }

        process_history(line);

        const char *command = skip_chars(isspace, line);
        if (*command && *command != '#') {
            programState = command_dispatch(command, programState);
        }

        free(line);
    }

    return 0;
}
