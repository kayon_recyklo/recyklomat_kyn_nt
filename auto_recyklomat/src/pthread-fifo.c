#include "util.h"
#include "pthread-fifo.h"

void pthread_fifo_init( pthread_fifo_t * fifo, uint16_t max_len )
{
    pthread_queue_init(&fifo->container);
    pthread_mutex_init(&fifo->mutex, NULL);
    fifo->max_size = max_len;
    fifo->size = 0;
}

void pthread_fifo_push_back( pthread_fifo_t * fifo, void * data )
{
    pthread_mutex_lock(&fifo->mutex);

    if( fifo->size == fifo->max_size )
    {
        void * old_data = pthread_queue_pop_front(&fifo->container);
        free(old_data);
    }
    else    
        fifo->size++;

    pthread_queue_push_back(&fifo->container, data);

    pthread_mutex_unlock(&fifo->mutex);
}

void * pthread_fifo_pop_front( pthread_fifo_t * fifo )
{
    void * data = NULL;
    pthread_mutex_lock(&fifo->mutex);

    if( fifo->size == 0 ) goto RETURN;

    data = pthread_queue_pop_front(&fifo->container);
    fifo->size--;

RETURN:
    pthread_mutex_unlock(&fifo->mutex);
    return data;
}

