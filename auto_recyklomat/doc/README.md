# Dokumentacja systemowa sterowinka QPS

Celem niniejszego dokumentu jest zaprezentowanie założeń projektowych
sterownika QPS. 

# Architektura sterownika

Sterownik składa się z dwóch warstw. Najniższą warstwę stanowi
warstwa zdarzeń i komunikacji, odpowiedzialna za odpytywanie urządzeń NoaTech
IO Driver i sterowanie nimi. Ponad tą warstwą zaimplementowany został "system
odruchów", odpowiedzialny za konkretne sterowanie.

Obie te warstwy razem dostarczają realizację interfejsu sterownika.

## Warstwa zdarzeń i komunikacji

Do dokonywania jakichkolwiek zmian w sterowniku istotne jest zrozumienie jego
niskopoziomowej architektury. Architektura ta zobrazowana została na następującym
diagramie:

![Architektura warstwy zdarzeń](https://bitbucket.org/kayon_recyklo/recyc/raw/db8c42aea80d19b20fbc13f33579e274597ef6e2/doc/events-commands.svg)

Jak widać, do sterownika QPS może być podłączonych wiele urządzeń IO Driver.
Podłącza się je za pośrednictwem portu szeregowego (np. FTDI). Sterownik
obsługuje dowolnie wiele portów szeregowych (ale nie mniej niż jeden), a do każdego
z nich można podłączyć dowolnie wiele urządzeń IO Driver.

Dla każdego użytego portu szeregowego tworzony jest odrębny wątek `QPS_poll_uart`.
Jego rola jest dwojaka: po pierwsze, cyklicznie odpytuje urządzenia IO Driver
o stan ich wejść, i w przypadku zmiany generuje odpowiednie zdarzenie. Po drugie,
przetwarza asynchroniczne polecenia przychodzące z warstwy aplikacji (i ewentualnie
przekazuje do niej odpowiedzi od urządzeń).

**UWAGA!** Kod aplikacji nigdy nie powinien wysyłać zapytań bezpośrednio do urządzenia.
Wszystkie zapytania powinny być przesyłane za pośrednictwem wątku `QPS_poll_uart`.
Do tego celu służą funkcje `QPS_blocking_command` i `QPS_nonblocking_command` opisane
w nagłówku `qps-command.h`, oraz owijające je makra `QPS_COMMAND_NONBLOCK`
i `QPS_WITH_COMMAND_RESPONSE`.

**UWAGA!** W przypadku korzystania z funkcji `QPS_blocking_command` jej użytkownik
jest odpowiedzialny za zwolnienie otrzymanej pamięci. W praktyce lepiej stosować
makro `QPS_WITH_COMMAND_RESPONSE`, które "samo" zajmuje się tą kwestią.

Jeżeli na którymś z wejść jakiegoś IO Drivera wystąpiła zmiana, `QPS_poll_uart`
przekazuje informację o tym do wątku `QPS_handle_events` za pośrednictwem kolejki
`event_queue` (zob. `struct QPSRecycData` w pliku `qps.h`). Wątek `QPS_handle_events`
wywołuje odpowiednie funkcje obsługi zdarzeń.

**UWAGA!** Wszystkie funkcje obsługi zdarzeń są wywoływane z jednego wątku
`QPS_handle_events`. Z tego powodu wykonująca się w danym momencie procedura
obsługi zdarzenia blokuje możliwość obsługi kolejnych zdarzeń, w związku z czym
należy w procedurach obsługi zdarzeń unikać działań mogących zabierać dużo czasu,
i w miarę możliwości delegować je do innych wątków.

## System odruchów

Aby zmaksymalizować ilość przedmiotów, które mogą się jednocześnie znaleźć
na pasie transmisyjnym, zaimplementowany został "system odruchów", działający
w sposób następujący:

Na pasie transmisyjnym znajdują się pary (detektor, aktuator). Detektor
jest odpowiedzialny za wykrycie obiektu, natomiast aktuator - za zepchnięcie
wykrytego obiektu do odpowiadającego mu pojemnika.

Z każdym detektorem skojarzona jest kolejka działań, przy czym działania mogą
być dwojakiego rodzaju:
- możemy zignorować obiekt (w sytuacji, gdy miałby on być obsłużony przez
którąś z kolejnych par (detektor, aktuator)), albo
- możemy nakazać aktuatorowi zepchnięcie obiektu.

Z technicznego punktu widzenia możliwa jest sytuacja, że na wejściu detektora
pojawi się sygnał, na który detektor nie jest przygotowany. Na obecną chwilę
nie mamy sensownej strategii obsługi tego rodzaju sytuacji.

Ilość i kolejność par (detektor, aktuator) określona jest w tabeli
`QPS_reflex_table` zdefiniowanej w pliku `iomap.h`. W pliku tym można również
znaleźć tablicę `QPS_small_items`, określającą sposób obsługi tzw. "małych
przedmiotów", które nie trafiają na pas transmisyjny.

## Interfejs sterownika

Pierwotny projekt systemu obsługi recyklomatu zakładał możliwość
definiowania różnych rodzajów sterowników. W związku z tym, plik `recyc.h`
zawiera zdefiniowany interfejs, który poszczególne sterowniki powinny
realizować, natomiast plik `qps/qps-recyc.c` w istocie dostarcza
implementację tego interfejsu.

W toku prac okazało się jednak, że taki poziom abstrakcji nie jest do niczego
potrzebny, zaś sam interfejs sterownika jest zdefiniowany w sposób
niezadowalający. Interfejs sterownika, który na dzień dzisiejszy wymaga
zdefiniowania funkcji `lock`, `isClosed`, `unlock`, `onClosed`, `recognizeItem`,
`takeOver` oraz `processItem`, można na dzień dzisiejszy traktować jako
ślepe ogniwo ewolucji.

Ponieważ jednak narzut wnoszony przez tę dodatkową warstwę abstrakcji
jest niewielki, nie została ona usunięta z kodu źródłowego. Być może
na jakimś etapie projektu okaże się przydatna.

# Analiza działania

Punktem wejścia do programu sterownika jest oczywiście funkcja `main`, zdefiniowana
w pliku `qps.c`. Znajduje się w niej wywołanie funkcji `Recyc_init`, które z kolei
powoduje wywołanie funkcji `QPS_NewRecyc`, zdefiniowanej w pliku `qps/qps-recyc.c`.

Po zainicjalizowaniu sterownik oczekuje na zdarzenie zamknięcia drzwi.
Spowoduje ono wywołanie funkcji `Recyc_onClosed`, które pobierze typ rozpoznanego
obiektu (`QPS_recognizeItem`) i przekaże go (`QPS_processItem`) za pośrednictwem
kolejki `conveyor.pending_items` do wątku `QPS_reflex` (`qps/qps-reflex.c`).

Wątek ten sprawdzi, czy typ obiektu jest zdefiniowany w tablicy `QPS_small_items`,
i jeśli tak, zastosuje strategię obsługi małych obiektów.

W przeciwnym razie wątek sprawdzi, czy typ obiektu znajduje się w tablicy
`QPS_reflex_table`, i jeśli tak, doda odpowiednie odruchy do kolejek oczekiwań
poszczególnych czujników na pasie transmisyjnym.

# Niedoskonałości

Z uwagi na ograniczony czas, w jakim powstał sterownik, pewne funkcjonalności
nie zostały zaimplementowane w sposób idealny. Warto mieć to na względzie,
i w przypadku, gdyby zaistniała kiedyś taka możliwość, dobrze byłoby te
niedoskonałości poprawić.

## Obsługa ramek

Protokół komunikacji ze sterownikiem IO Driver opisany jest w pliku `noaio.h`.
Całość jest zasadniczo zrobiona dobrze, poza jednym małym mankamentem:
bajty startu, stopu oraz sumy kontrolnej nie zostały określone jako część
ramki. Spowodowało to konieczność zdefiniowania dodatkowego typu o nazwie
`NOAIO_TRANSPORT_LAYER_FRAME_PREFIX`, zawierającego dodatkowy bajt startu.

Gdyby dodać bajt startu do nagłówka ramki, można by było z tego typu
w ogóle zrezygnować, co dodatkowo uprościłoby również kod w kilku miejscach.

## Strategie na wypadek wadliwego działania

Jak wspomniano w ramach omawiania "systemu odruchów", technicznie
możliwa jest sytuacja, w której wystąpi wykrycie nieoczekiwanego obiektu.
Wydaje się, że bez dostępu do działającego prototypu, trudno wymyślić
zadowalającą strategię radzenia sobie z tego rodzaju sytuacjami.

## Sugestie dalszego rozwoju

Niniejszy dokument dotyczy pierwszej wydanej wersji sterownika. Oczywiście
sterownik powinien docelowo zachowywać się w pełni automatycznie i nie wymagać
żadnych ingerencji ze strony ludzkiego nadzorcy. Tym niemniej, zanim dojdzie
do tego etapu, warto rozważyć, w jaki sposób można by było ułatwić rozwijanie
i introspekcję stanu sterownika.

### Interpreter komend

Jednym z prostszych pomysłów byłoby dodanie interpretera linii komend,
za pomocą których użytkownik mógłby badać stan sterownika. Aktualnie
sterownik wywołuje funkcję `takeOver`, która oczekuje na zakończenie
wszystkich wątków warstwy obsługi zdarzeń, jednak dobrą alternatywą
byłoby po prostu wczytywanie kolejnych linii wejścia i odpowiednie
przetwarzanie ich (tak jak to ma miejsce w pliku `noatalk.c`).

### Implementacja sterownika ReDriver

Warto zwrócić uwagę, że na dzień dzisiejszy sterownik jest w stanie
obsłużyć jedynie urządzenia NoaTech IO Driver. Tymczasem do działania
projektu najprawdopodobniej konieczne będzie również obsłużenie urządzeń
NoaTech ReDriver (wyposarzonych w wejścia analogowe).

Aby obsłużyć te urządzenia, należałoby:

- napisać odpowiednią funkcję odpytującą (por. `QPS_poll_digital_input`
z `qps/qps-events.c`)
- dodać obsługę parametru pozwalającego wybrać rodzaj urządzenia
do funkcji `QPS_NewRecyc`, a następnie w tejże funkcji zmodyfikować linię

    device->poll = QPS_poll_digital_input;

w taki sposób, żeby rodzaj używanej funkcji `IODriver::poll` zależał
od owego parametru.
