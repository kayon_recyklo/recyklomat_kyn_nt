#!/bin/bash
LOG=/home/pi/logs/recyc.log
to_log() {
  # split output to stdout/log, prepend every log line with date
  # usage: cmd | to_log
  while read LINE; do
    echo "$LINE"
    echo "[$(date +'%F %T,%3N')][recyc] $LINE" >> $LOG
  done
}


rm /tmp/be-socket 2>/dev/null
sudo /home/pi/recyc/bin/./qps uarts=1 devices=3 uart0=/dev/can0 device0=010000aa@uart0 device1=020000aa@uart0 device2=030000aa@uart0 recognizer="" | to_log
