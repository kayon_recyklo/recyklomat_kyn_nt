#ifndef UTIL_H
#define UTIL_H

#define _GNU_SOURCE

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "pthread-queue.h"
#include "kayon.h"
#include "qps-logs.h"

#define SHOW_IN_COMMAND_LINE 1  // 0 wylacza opcje wyswietlania errorów itp. w command linie (wszystko inne wlacza)
#define SEND_TO_THE_FILE     1  // 0 wylacza opcje zapisywania erroru/ warninga/ outa w pliku (wszystko inne wlacza)
#define ENABLE_SET_PATH      1  // 0 wylacza opcje ustawienia konkretnej sciezki z #define ponizej

#define NOAIO_DEBUG 0 // komunikaty od warstwy komunikacyjej

#include <stdio.h>

#define LOG_PATH "/home/pi/ml_auto_recyklomat/auto_recyklomat/logs/"  // sciezka pod która chcesz zapisywac tresci errorów, warningow i outow


#define SIZE_OF_PATH 40

/*	ERROR("")	wystapil blad i program nie ma szans na samodzielna naprawe bledu / czesc proejktu nie bedzie w stanie dzialac / program powienno sie przerwac
 * 	WARN("")	wystapil blad, ale program ma szanse na samodzielne naprawienie bledu
 * 	INFO("")	informacja
 */

char today[SIZE_OF_PATH];

FILE* file; // = NULL;

int size;

#define SIZE_OF_LOG_NAME 26
char recyc_new[SIZE_OF_LOG_NAME];
char recyc_old[SIZE_OF_LOG_NAME];
#define NAZWA "%04d-%02d-%02d.log"
#define NAZWA_OLD "%04d-%02d-%02d_old.log"

#define LOG_B_LIMIT 4*1024*1024 // 4MB = 4 * 1024 * 1024
#define GET_FILE_SIZE(file) fseek(file, 0 , SEEK_END); \
                            size = ftell(file); \
                            fseek(file, 0, SEEK_SET)

extern QPS_write_to_file logs_str;

#define SET_NEW_TIME time_t t = time(NULL); \
        			struct tm tm = *localtime(&t)

#define LOGS_TYPE_CAN				"CAN"
#define LOGS_TYPE_MAIN_PROCESS			"MAIN"
#define LOGS_TYPE_MOCK				"MOCK"
#define LOGS_TYPE_RECOGNIZER			"RECOG"
#define LOGS_TYPE_INIT				"INIT"
#define LOGS_TYPE_OPTO				"OPTO"
#define LOGS_TYPE_DRIVER			"DRIVE"
#define LOGS_TYPE_SERVICE			"SERV"
#define LOGS_TYPE_SV				"SVIS"
#define LOGS_TYPE_BE				"B_END"
#define LOGS_TYPE_CONSOLE			"CONS"
#define LOGS_TYPE_SOCKET			"SOCK"
#define LOGS_TYPE_NOAIO				"NOAIO"
#define LOGS_TYPE_CODE				"CODE"
#define LOGS_TYPE_DIR				"DIR"
#define LOGS_TYPE_CRUSHER			"CRUSH"
#define LOGS_TYPE_CONV				"CONV"
#define LOGS_TYPE_LIMIT				"LIMIT"

#define LOGS_STATUS_OK				"[ OK ]"
#define LOGS_STATUS_FAIL			"[ FAIL ]"
#define LOGS_STATUS_NONE			NULL

/*
#define ERROR_CODE_USAGE                "[WRONG USAGE]"
#define ERROR_CODE_FAILED               "[ACTION FAILED]"
#define ERROR_CONVEYOR_DEVICE           "[CONVEYOR DEVICE]"
#define ERROR_DIRECTOR                  "[DIRECTOR]"
#define ERROR_CRUSHER                   "[CRUSHER]"
#define ERROR_CRUSHER_LIMIT             "[CRUSHER LIMIT]"
#define ERROR_OPTO                      "[OPTO]"
#define ERROR_RECOGNIZER                "[RECOGNIZER]"
#define ERROR_INIT                      "[INIT]"
#define ERROR_HARD_FAULT                "[HARD FAULT]"
#define ERROR_LIMIT                     "[LIMIT]"
*/

#if defined(virtualcan) || defined(noatalk) || defined(qpsconsole)

#define INFO(category, status, msg, ...)  	if(SHOW_IN_COMMAND_LINE){ \
					SET_NEW_TIME; \
                                	fprintf(stderr, "%04d-%02d-%02d %02d:%02d:%02d INFO %s "msg"\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, category, ##__VA_ARGS__); \
                                fflush(stderr); \
			}

#define ERROR(category, msg, ...) if(SHOW_IN_COMMAND_LINE){ \
				SET_NEW_TIME; \
                                fprintf(stderr, "%04d-%02d-%02d %02d:%02d:%02d ERROR %s"msg"\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, category, ## __VA_ARGS__); \
                                fflush(stderr); \
                        }

#define WARN(category, msg, ...)  if(SHOW_IN_COMMAND_LINE){ \
				SET_NEW_TIME; \
                                fprintf(stderr, "%04d-%02d-%02d %02d:%02d:%02d WARN %s "msg"\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, category, ##__VA_ARGS__); \
                                fflush(stderr); \
                        }
#else
void ERROR(const char* category, const char* msg, ...);
void WARN(const char* category, const char* msg, ...);
void INFO(const char* category, const char * status, const char* msg, ...);		//definincje sa w pliku qps-logs.c

#endif

#define MESSAGE(msg) //fprintf(stderr, msg "\n"); fflush(stderr)


#define NEW(type) (type *) malloc(sizeof(type))

#define MAKE(type, ...) ({                      \
            type *__item__ = NEW(type);             \
            *__item__ = (type) { __VA_ARGS__ };     \
            __item__;                               \
        })

#define ARRAY(type, n) (type *) calloc(n, sizeof(type))

#define COPY(object) ({                                                 \
            typeof(object) *copy = malloc(sizeof(object));              \
            memcpy(copy, &object, sizeof(object));                      \
            copy;                                                       \
        })

#define FROM_LITTLE_ENDIAN_2(array) (((byte *) array)[0] \
                                     | (((byte *) array)[1] << 8))

#define FROM_LITTLE_ENDIAN_3(array) (FROM_LITTLE_ENDIAN_2(array) \
                                     | (((byte *) array)[2]<<16))

#define FROM_LITTLE_ENDIAN_4(array) (FROM_LITTLE_ENDIAN_3(array) \
                                     | (((byte *) array)[3]<<24))

#define FROM_BIG_ENDIAN_3(array) ((((byte *) array)[2] << 16)    \
                                  |((((byte *) array)[1]) << 8) \
                                  |(((byte *) array)[0]))

// typ do rzutowania funkcji porownujacej przekazywanej do qsort
typedef int (*comparison)(const void *, const void *);

static inline int max(int a, int b) {
    return (a > b) ? a : b;
}

static inline int min(int a, int b) {
    return (a < b) ? a : b;
}

static inline const char *is_prefix_token(const char *prefix, const char *string) {
    while(*prefix) {
        if(!*string || (*prefix++ != *string++)) {
            return NULL;
        }
    }
    if (isalnum(*string) || *string == '_') {
        return NULL;
    }
    return string;
}

static inline const char *skip_chars(int (*ischar)(int), const char *input) {
    while (ischar(*input)) {
        ++input;
    }
    return input;
}


static inline int
read_with_timeout(int fd, byte *buffer, size_t size, clock_t timeout_ms) {
    assert(fd < FD_SETSIZE);
    assert(timeout_ms >= 0);
    struct timeval tv = {
        tv.tv_sec  = timeout_ms / 1000,
        tv.tv_usec = (timeout_ms % 1000)*1000
    };

    fd_set in;
    FD_ZERO(&in);
    FD_SET(fd, &in);
    int result = select(fd+1, &in, NULL, NULL, &tv);
    if (result < 0) {
        perror(__FUNCTION__);
        return result;
    }

    if (result == 0) {
        return result;
    }

    return read(fd, buffer, size);
}

static inline long long time_us() {
    struct timeval tv;
    bool failure = !!gettimeofday(&tv, NULL);
    assert(!failure);
    return (long long) 1000000*tv.tv_sec + tv.tv_usec;
}

static inline int time_milis() {
    int time_ms = time_us()/1000;
    return time_ms;
}

/*
 * released robi to samo, co free, z ta roznica, ze dodatkowo zwraca NULL.
 * Celem wprowadzania takiego dziwactwa jest zachecenie do stosowania idiomu:
 *
 *     x = released(x);
 */
static inline void *released(void *memory) {
    free(memory);
    return NULL;
}


static inline void fprinthex(FILE *out, byte *array, size_t size) {
    for (int i = 0; i < size; ++i) {
        fprintf(out, "%02X ", array[i]);
    }
}

static inline void msleep( uint32_t ms )
{
	usleep(1000L*ms);
}

#define printhex(array, size) fprinthex(stdout, array, size)

#define DUMPHEX(array, size) {                                  \
        printf(# array "[" # size "=%d]: ", (uint8_t) (size));      \
        printhex((byte *) array, (size_t) size);                \
        printf("\n");                                           \
    }


#define INITIALIZE __attribute__((constructor)) static void init(void)

extern const char * qps_exe_path;

#endif // UTIL_H
