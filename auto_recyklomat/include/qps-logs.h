/*
 * qps_logs.h
 *
 *  Created on: Aug 6, 2019
 *      Author: osboxes
 */

#ifndef QPS_LOGS_H_
#define QPS_LOGS_H_

#include "qps-backend.h"
#include "pthread-queue.h"

typedef struct {						// struktura do kolejki stringow ktore potem beda brane po kolei i zapisywane w pliku
	pthread_queue_t to_write_queue;
    char* str;
    pthread_t thread;	// uzylem do inicjalizacji w qps.c
} QPS_write_to_file;


void *QPS_save_to_file(void* arg);			// zapis do plikow .log THREAD

#endif /* QPS_LOGS_H_ */
