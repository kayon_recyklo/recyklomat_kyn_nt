#ifndef QPS_CONVEYOR_DEVICES_H
#define QPS_CONVEYOR_DEVICES_H

typedef enum PACKED
{
	DIGITAL = 1,
	ANALOG = 2,
	INPUT = 4,
	OUTPUT = 8
} IODriver_pin_type_t;

/*
 * CONVEYOR_DEVICE_DISABLED	- urzadzenie wylaczone
 *	CONVEYOR_DEVICE_READY	- urzadzenie zamkniete, czeka na polecenia
 *	CONVEYOR_DEVICE_OPENING - urzadzenie dostalo polecenie otwarcia i zaczyna otwieranie
 *	CONVEYOR_DEVICE_CONFIRMED_OPENING	- urzadzenie rzeczywiscie sie otwierala, dostalismy sygnal ze jest otwarte (stan tymaczasowy)
 *	CONVEYOR_DEVICE_OPENED				- urzadzenie otwarte (musi jeszcze przejsc test)
 *	CONVEYOR_DEVICE_CONFIRMED_OPENED	- urzadzenie otwarte (potwierdzone)
 *	CONVEYOR_DEVICE_CLOSING				- urzadzenie rozpoczyna procedure zamykania
 *	CONVEYOR_DEVICE_CONFIRMED_CLOSING	- dotarl sygnal ze rzeczywiscie sie zamyka (stan tymczasowy)
 *	CONVEYOR_DEVICE_CLOSED				- urzadzenie zamkniete (musi jeszcze przejsc test)
 *	CONVEYOR_DEVICE_CONFIRMED_CLOSED 	- urzadzenie zamkniete (potwierdzone)
 */

typedef enum PACKED
{
	CONVEYOR_DEVICE_DISABLED,
	CONVEYOR_DEVICE_READY,
	CONVEYOR_DEVICE_OPENING,
	CONVEYOR_DEVICE_CONFIRMED_OPENING,
	CONVEYOR_DEVICE_OPENED,
	CONVEYOR_DEVICE_CONFIRMED_OPENED,
	CONVEYOR_DEVICE_CLOSING,
	CONVEYOR_DEVICE_CONFIRMED_CLOSING,
	CONVEYOR_DEVICE_CLOSED,
	CONVEYOR_DEVICE_CONFIRMED_CLOSED,
	CONVEYOR_DEVICE_ERROR_STATUS
} conveyor_device_status_value_t;

typedef enum PACKED
{
	CONVEYOR_DEVICE_PHASE_OPENING,
	CONVEYOR_DEVICE_PHASE_CLOSING,
	CONVEYOR_DEVICE_PHASE_NONE
} conveyor_device_phase_t;

typedef struct
{
        // is_normal_cycle mowi, czy system wszedl w obsluge bledow
        // wywolanie start_cycle daje jej wartosc 1, wywolanie *_now daje 0
        // funkcja on_cycle_complete i odpalenie kolejnego urzadzenia w lancuchu startuja tylko przy is_normal_cycle == 1  
        // po pomyslnym zakonczeniu obslugi bledu trzeba recznie wywolac start_next() ze wskaznika w urzadzeniu
        bool is_normal_cycle;
	conveyor_device_status_value_t value;
	conveyor_device_status_value_t next_status;
	pthread_mutex_t mutex;
} conveyor_device_status_t;

typedef struct 
{
	NOAIO_CHANNEL channel;
	uint8_t device_idx;
} IODriver_out_t;

typedef struct 
{
	NOAIO_CHANNEL channel;
	uint8_t device_idx;
} IODriver_in_t;

//typedef enum{
//	OK = 0,			// nie wystapily bledy
//	WARNING = 1,	// wystapily bledy i urzadzenie moze sobie z nimi poradzic
//	ERROR = 2		// wystapily bledy i urzadzenie nie poradzi sobie z nimi
//}state_t;

typedef enum{
	GLA_DIRECTOR = 0,
	PET_DIRECTOR = 1,
	ALU_DIRECTOR = 2,
	PET_CRUSHER = 3,
	ALU_CRUSHER = 4,
} device_ID_t;

typedef enum{
	CONVEYOR_DEVICE_NO_ERROR,
	CONVEYOR_DEVICE_FIRST_MOVE_FAILED,
	CONVEYOR_DEVICE_FIRST_PUSH_FAILED,
	CONVEYOR_DEVICE_RETURN_FAILED,
	CONVEYOR_DEVICE_RETURN_PUSH_FAILED,
	CONVEYOR_DEVICE_ULTIMATE_ERROR = 255
} conveyor_device_local_error_t;

typedef enum{
	CONVEYOR_DEVICE_HEM,	// ciezki tryb a...
	CONVEYOR_DEVICE_LEM,
	CONVEYOR_DEVICE_NEM,
} conveyor_device_error_mode_t;

typedef struct
{
	conveyor_device_local_error_t local_error;	// ERROR
	conveyor_device_error_mode_t error_mode;	// CZYSZCZENIE / COFANIE / NORMAL
	uint8_t try_number;				// GLOBAL_ERROR
	uint8_t max_try_number;				// max ilosc prob dopychania
	pthread_mutex_t mutex;
	
} conveyor_device_error_info_t;

typedef struct conveyor_device_t 
{
	char * name;
	device_ID_t device_id;
	conveyor_device_status_t status;

	conveyor_device_error_info_t error_info;

	uint16_t before_open_delay;
	uint16_t opening_time;
	uint16_t before_close_delay;
	uint16_t closing_time;
	uint16_t test_offset;

	IODriver_out_t open_out;
	IODriver_in_t open_sensor;
	IODriver_out_t close_out;
	IODriver_in_t close_sensor;

	Recyc* recyc;

	void(*start_working_cycle)(struct conveyor_device_t * self);
	void(*open_now)(struct conveyor_device_t * self);
	void(*close_now)(struct conveyor_device_t * self);	
	void(*open)(struct conveyor_device_t * self);
	void(*close)(struct conveyor_device_t * self);	
	void(*start_other_phase)(struct conveyor_device_t * self);
	void(*repeat_phase)(struct conveyor_device_t * self);

	conveyor_device_status_value_t(*get_next_status)(struct conveyor_device_t * self);
	conveyor_device_status_value_t(*get_status)(struct conveyor_device_t * self);
	void(*set_status)(struct conveyor_device_t * self, conveyor_device_status_value_t status);
	conveyor_device_error_mode_t(*get_error_mode)(struct conveyor_device_t* self);
	void(*set_error_mode)(struct conveyor_device_t* self, conveyor_device_error_mode_t error_mode);
	conveyor_device_phase_t(*get_current_phase)(struct conveyor_device_t * self);

        void(*start_next)(struct conveyor_device_t * self);
	bool(*hw_status_ok)(struct conveyor_device_t * self);
	void(*on_error)(struct conveyor_device_t * self);
        void(*on_phase_complete)(struct conveyor_device_t * self);
	void(*on_cycle_complete)(struct conveyor_device_t * self);

	QPSRecycData * qps;
	void * arg;

	struct conveyor_device_t * next_device;
} conveyor_device_t;

#define CONVEYOR_DEVICE_ASAP 0
#define CONVEYOR_DEVICE_FOREVER ((uint16_t)-1)
#define CONVEYOR_DEVICE_UNINITIALIZED {.value = CONVEYOR_DEVICE_DISABLED, .mutex = PTHREAD_MUTEX_INITIALIZER}

void conveyor_device_init( conveyor_device_t * device, QPSRecycData * qps );
void conveyor_device_set_pin_state( IODriver_out_t * pin, QPSRecycData * qps, bool state );
#endif
