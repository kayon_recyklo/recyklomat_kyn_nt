#ifndef QPS_H
#define QPS_H

/**
 * \file qps.h
 * \brief Podstawowy interfejs komunikacji z peryferiami QPS
 */

#include <stdio.h> // zeby moc zadeklarowac FILE *
#include <unistd.h>
#include <termios.h>
#include "recyc.h"
#include "pthread-queue.h"
#include "noaio.h"
#include "iomap.h"

/*
 * Uzywamy sobie tutaj slowa THREAD do wyroznienia funkcji,
 * ktore sa uruchamiane jako watki. Jest to uklon w strone
 * osob czytajacych kod.
 */
#define THREAD

struct IODriver;
struct QPSRecycData;

/**
 * `UARTInterface` przechowuje informacje o urzadzeniach
 * podlaczonych do danego UARTa
 */
typedef struct {
    NOAIO_INCOMMING_FRAME incomming_frame;
    pthread_t thread;
    pthread_queue_t command_queue;
    pthread_queue_t *event_queue;
    struct IODriver **devices; // urzadzenia (IO Driver) komunikujace sie
    int num_devices; // za posrednictwem tego UARTa (kolejnosc przypadkowa)
    char *in_name;
    char *out_name;
    int in;
    int out;
} UARTInterface;

typedef struct {
    void (*handler)(struct IODriver *io, int input_index, bool new_value);
    void *arg; // dodatkowe dane do wykorzystania przez handler
    void *arg2; // i jeszcze jedno, zeby nie musiec alokowac
} InputEventHandler;


#if NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND
typedef struct {
    pthread_t thread;
    pthread_queue_t queue;
    int output_index;
} PeriodCommandTimer;
#endif // NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND

typedef struct 
{
    NOAIO_WATCHDOG_MODE mode;
    NOAIO_OUTPUT_BITS bitmask;
    uint8_t interval;
} watchdog_info_t;


/**
 * `IODriver` reprezentuje konkretna plytke NoaTech IODriver,
 * w szczegolnosci ostatni znany stan wejsc oraz wskazniki
 * na funkcje obslugi zdarzen.
 */
typedef struct IODriver {
    uint32_t address;
    UARTInterface *UART;
    NOAIO_INPUT_BITS previous_input;
    InputEventHandler on_change[NOAIO_INPUTS];

#if NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND

    PeriodCommandTimer for_period[NOAIO_OUTPUTS];

#endif // NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND

    void (*poll)(struct IODriver *self);
    
    struct { // takie tam pitu pitu
        uint8_t major;
        uint8_t minor;
    } version;
    uint8_t index;

    uint8_t cmd_cnt;
    bool is_redriver;

    pthread_mutex_t input_mutex;    
    uint16_t analog_inputs[NOAIO_INPUTS];
    uint16_t output_currents[NOAIO_OUTPUTS];

    struct iodriver_config * out_config[NOAIO_OUTPUTS];
    watchdog_info_t wdt_info;
    iodriver_input_info_t * in_config[NOAIO_INPUTS];

    iodriver_voltage_limits_t * voltage_limits;
    Recyc * recyc;
} IODriver;


typedef struct {
    pthread_queue_t pending_items;
    pthread_t control;
} QPSConveyor;

typedef struct{
	int socket;
	int connetion;
	char recognized_item[5];
	char mocked_recognition[5];
	bool mocking;
} RecognizerData;

typedef struct {
	int counter;
	time_t start_transaction_time;
} QPS_mock_recognition;

typedef struct QPSRecycData {
    UARTInterface *UART;
    int num_uarts;
    IODriver *devices; // jezeli mamy gdzies device_index (np. z iomap.h),
    int num_devices;   // to chodzi wlasnie o indeks w tej tablicy
    pthread_queue_t event_queue;
    pthread_t event_handler;

    char *recognizer;
    RecognizerData recognition;
    time_t last_process_start_time;
    double process_min_length;
    time_t block_doors_reaction_time;
    QPS_mock_recognition* mock_recognition;
    Recyc* recyc;
    QPSConveyor conveyor;

    
} QPSRecycData;

#define QPS_SEND_REQUEST(dev, req, ...) ({                              \
            NOAIO_DECLARE_REQUEST_FRAME(local_frame,                    \
                                        req, (dev)->address,            \
                                        ## __VA_ARGS__);                \
            NOAIO_request((dev)->UART->out, (dev)->UART->in,            \
                          (NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *)        \
                          &local_frame, &(dev)->UART->incomming_frame,  \
                          CAN_RESPONSE_TIMEOUT_MS);                     \
        })

#define QPS_WITH_RESPONSE_FRAME(frame, req, framename, action, action_fail)          \
    if (frame) {                                                        \
        if (frame->header.request == NOAIO_RESPONSE_##req) {            \
            NOAIO_RESPONSE_FRAME_##req *framename                       \
                = &frame->RESPONSE.req;                                 \
            action;                                                     \
        }                                                               \
        else {                                                          \
            WARN(LOGS_TYPE_CAN,"Invalid response");         				\
		action_fail;							\
        }                                                               \
    } else action_fail;

#define QPS_WITH_RESPONSE(framename, action, dev, request, action_fail, ...)  {      \
        NOAIO_FRAME *raw_frame = QPS_SEND_REQUEST(dev, request,         \
                                                  ## __VA_ARGS__);      \
        QPS_WITH_RESPONSE_FRAME(raw_frame, request,                     \
                                framename, action, action_fail);        \
    }

/**
 * `QPS_no_reaction` to domyslna funkcja obslugi zdarzen,
 * (IODriver.on_change.handler) ktora nic nie robi.
 * Inicjalizowana w pliku `qps.c` w funkcji `QPS_NewRecyc`.
 */
extern void
QPS_no_reaction(struct IODriver *io, int input_index, bool new_value);

/**
 * `QPS_poll_digital_input` to jedna (na razie jedyna)
 * z dostepnych funkcji, ktora moze byc uzyta jako wartosc
 * IODriver.poll, inicjalizowana w pliku `qps.c` w funkcji
 * `QPS_NewRecyc`.
 */
extern void
QPS_poll_digital_input(IODriver *device);


/**
 * `QPS_handle_events` to watek, ktory odbiera zdarzenia
 * od watkow `QPS_poll_uart` i wykonuje procedury obslugi zdarzen
 * (tj. IODriver.on_change.handler). Uruchamiany w funkcji
 * `QPS_NewRecyc` w pliku `qps.c`.
 */
extern THREAD void *
QPS_handle_events(QPSRecycData *data);

/**
 * `QPS_poll_uart` to watki (moze ich byc wiele), ktore komunikuja
 * sie poprzez UART z urzadzeniami IO Driver. Kazdemu uzytemu
 * UARTowi odpowiada jeden watek, ale do jednego UARTa moze byc
 * podpietych wiele urzadzen.
 *
 * Watki sa uruchamiane w zaleznosci od konfiguracji
 * (ilosc UARTow i urzadzen do nich podpietych) z funkcji
 * `QPS_NewRecyc` w pliku `qps.c`.
 */
extern THREAD void *
QPS_poll_uart(UARTInterface *uart);

extern void
QPS_registerOnChangeEvent(QPSRecycData *, int device_index,
		NOAIO_INPUT_BITS, void (*)(IODriver *, int, bool),
		void *arg, void *arg2);

extern uint16_t 
IODriver_analog_get( IODriver * io, NOAIO_CHANNEL channel );

extern uint16_t
IODriver_current_get( IODriver * io, NOAIO_CHANNEL channel );

void exceptions_handler_init( void );

extern bool 
IODriver_digital_get( IODriver * io, NOAIO_CHANNEL channel );
#endif // QPS_H
