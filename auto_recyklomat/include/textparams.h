#ifndef TEXTPARAMS_H
#define TEXTPARAMS_H

/*
 * Funkcje do pracy z parametrami tekstowymi
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl>
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */



#include "util.h"

static inline char *textparam(const char *string, const char *name) {
    assert(string);
    assert(name && strlen(name) > 0);
    const char *namevalue = strstr(string, name);
    if (!namevalue) {
        return NULL;
    }
    const char *value = namevalue + strlen(name);
    const char *end = skip_chars(isgraph, value);
    return strndup(value, end-value);
}

static inline int textparam_parse_int(int (*parse)(const char *, int),
                                      const char *string,
                                      const char *name,
                                      int default_int) {
    char *value = textparam(string, name);
    if (!value) {
        return default_int;
    }
    int result = parse(value, default_int);
    free(value);
    return result;
}

static inline int parse_dec(const char *str, int dflt) {
    int n;
    if(sscanf(str, "%d", &n) != 1) {
        return dflt;
    }
    return n;
}


static inline int parse_hex(const char *str, int dflt) {
    int n;
    if(sscanf(str, "%x", &n) != 1) {
        return dflt;
    }
    return n;
}


#endif // TEXTPARAMS_H
