#ifndef NOAIO_H
#define NOAIO_H


#include "kayon.h"
/*
 * Specyfikacja protokolu komunikacyjnego NoaTech IO Driver,
 * na podstawie dokumentacji dostarczonej przez NoaTech.
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl> listopad 2018
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */


#define CAN_RESPONSE_TIMEOUT_MS 300

#define NOAIO_FLUSH_MAX_ITERATIONS 5
#define NOAIO_FLUSH_TIMEOUT 150

/*
 * Poniewaz pole kodujace dlugosc ramki ma 1 bajt,
 * dlugosc ramki nie moze przekroczyc 255
 */
#define NOAIO_MAX_FRAME_LENGTH 255

#define IODRIVER_TYPE_MASK 0xff
#define IODRIVER_TYPE_IODRIVER 0xaa
#define IODRIVER_TYPE_REDRIVER 0xb0

typedef enum PACKED {
    NOAIO_REQUEST_GET_VERSION = 0x00,
    NOAIO_REQUEST_GET_STATE = 0x01,
    NOAIO_REQUEST_SET_CURRENT_LIMITS = 0x02,
    NOAIO_REQUEST_GET_OUTPUT_INFO = 0x03,
    NOAIO_REQUEST_SET_VOLTAGE_LIMITS = 0x04,
    NOAIO_REQUEST_GET_VOLTAGE_LIMITS = 0x05,
    NOAIO_REQUEST_ENABLE_OUTPUT = 0x06,
    NOAIO_REQUEST_DISABLE_OUTPUT = 0x07,
    NOAIO_REQUEST_SET_OUTPUT = 0x08,
    NOAIO_REQUEST_SET_DEFAULT_OUTPUT = 0x09,
    NOAIO_REQUEST_GET_DEFAULT_OUTPUT = 0x0A,
    NOAIO_REQUEST_ENABLE_FOR_PERIOD = 0x0B,
    NOAIO_REQUEST_DISABLE_FOR_PERIOD = 0x0C,
    NOAIO_REQUEST_GET_EVENTS = 0x0D,
    NOAIO_REQUEST_GET_INPUT_INFO = 0x0E,
    NOAIO_REQUEST_SET_WATCHDOG = 0x11,
    NOAIO_REQUEST_GET_WATCHDOG = 0x12,
    NOAIO_REQUEST_WATCHDOG_RESET = 0x10,

    NOAIO_REQUEST_SET_BAUDRATE = 0x20,
    NOAIO_REQUEST_ENTER_PROGRAM = 0x21,
    NOAIO_REQUEST_ENTER_BOOTLOADER = 0x22,
    NOAIO_REQUEST_WRITE_FLASH_PAGE = 0x23,
    
    NOAIO_REQUEST_MASK = 0x3F,
    NOAIO_REQUEST_FLAG_ERROR = (1 << 6),
    NOAIO_REQUEST_FLAG_RESPONSE = (1 << 7),

    NOAIO_RESPONSE_GET_VERSION = (NOAIO_REQUEST_GET_VERSION
                                  | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_STATE = (NOAIO_REQUEST_GET_STATE
                                | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_SET_CURRENT_LIMITS = (NOAIO_REQUEST_SET_CURRENT_LIMITS
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_OUTPUT_INFO = (NOAIO_REQUEST_GET_OUTPUT_INFO
                                      | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_SET_VOLTAGE_LIMITS = (NOAIO_REQUEST_SET_VOLTAGE_LIMITS
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_VOLTAGE_LIMITS = (NOAIO_REQUEST_GET_VOLTAGE_LIMITS
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_ENABLE_OUTPUT = (NOAIO_REQUEST_ENABLE_OUTPUT
                                    | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_DISABLE_OUTPUT = (NOAIO_REQUEST_DISABLE_OUTPUT
                                     | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_SET_OUTPUT = (NOAIO_REQUEST_SET_OUTPUT
                                 | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_SET_DEFAULT_OUTPUT = (NOAIO_REQUEST_SET_DEFAULT_OUTPUT
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_DEFAULT_OUTPUT = (NOAIO_REQUEST_GET_DEFAULT_OUTPUT
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_ENABLE_FOR_PERIOD = (NOAIO_REQUEST_ENABLE_FOR_PERIOD
                                        | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_DISABLE_FOR_PERIOD = (NOAIO_REQUEST_DISABLE_FOR_PERIOD
                                         | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_EVENTS = (NOAIO_REQUEST_GET_EVENTS
                                 | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_INPUT_INFO = (NOAIO_REQUEST_GET_INPUT_INFO
		    		 | NOAIO_REQUEST_FLAG_RESPONSE),

    NOAIO_RESPONSE_SET_WATCHDOG = (NOAIO_REQUEST_SET_WATCHDOG
                                    | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_GET_WATCHDOG = (NOAIO_REQUEST_GET_WATCHDOG
                                    | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_WATCHDOG_RESET = (NOAIO_REQUEST_WATCHDOG_RESET
                                    | NOAIO_REQUEST_FLAG_RESPONSE),
    NOAIO_RESPONSE_SET_BAUDRATE = (NOAIO_REQUEST_SET_BAUDRATE
                                    | NOAIO_REQUEST_FLAG_RESPONSE),
} NOAIO_REQUEST;

ASSERT(sizeof(NOAIO_REQUEST) == sizeof(uint8_t));

typedef enum PACKED
{
    NOAIO_BAUD_2400 = 0,
    NOAIO_BAUD_4800 = 1,
    NOAIO_BAUD_9600 = 2,
    NOAIO_BAUD_14400 = 3,
    NOAIO_BAUD_19200 = 4,
    NOAIO_BAUD_28800 = 5,
    NOAIO_BAUD_38400 = 6,
    NOAIO_BAUD_57600 = 7,
    NOAIO_BAUD_76800 = 8,
    NOAIO_BAUD_115200 = 9,
    NOAIO_BAUD_250000 = 10,
    NOAIO_BAUD_500000 = 11,
    NOAIO_BAUD_1000000 = 12
} NOAIO_BAUDRATE;


#define NOAIO_RESPONSE_TO(request) ((request) | NOAIO_REQUEST_FLAG_RESPONSE)

#define NOAIO_REQUEST_FOR(response) ((response) & (~NOAIO_REQUEST_FLAG_RESPONSE))

typedef enum PACKED {
    NOAIO_TX_START = 0x02,
    NOAIO_TX_END = 0x03
} NOAIO_TX;

ASSERT(sizeof(NOAIO_TX) == sizeof(uint8_t));

typedef struct PACKED {
    uint8_t length;
    uint32_t address;
    NOAIO_REQUEST request;
} NOAIO_HEADER;

ASSERT(sizeof(NOAIO_HEADER)
       == sizeof(uint8_t) + sizeof(uint32_t) + sizeof(NOAIO_REQUEST));

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_VERSION;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_GET_VERSION) == sizeof(NOAIO_HEADER));

typedef struct PACKED {
    NOAIO_HEADER header;
    uint8_t version_major;
    uint8_t version_minor;
} NOAIO_RESPONSE_FRAME_GET_VERSION;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_GET_VERSION)
       == sizeof(NOAIO_HEADER) + 2*sizeof(uint8_t));

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_STATE;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_GET_STATE) == sizeof(NOAIO_HEADER));

typedef enum PACKED {
    NOAIO_INPUT_A = (1 << 0),
    NOAIO_INPUT_B = (1 << 1),
    NOAIO_INPUT_C = (1 << 2),
    NOAIO_INPUT_D = (1 << 3),
    NOAIO_INPUTS = 4,
    
    NOAIO_INPUT_INVALID = 0,
} NOAIO_INPUT_BITS;

ASSERT(sizeof(NOAIO_INPUT_BITS) == sizeof(uint8_t));

typedef enum PACKED {

	NOAIO_ANALOG_INPUT_A = 0x0A,
	NOAIO_ANALOG_INPUT_B = 0x0B, 
	NOAIO_ANALOG_INPUT_C = 0x0C,
	NOAIO_ANALOG_INPUT_D = 0x0D,
	NOAIO_ANALOG_INPUTS = 4,

       	NOAIO_ANALOG_INPUT_INVALID = 0,	
} NOAIO_ANALOG_CHANNEL;

ASSERT(sizeof(NOAIO_ANALOG_CHANNEL) == sizeof(uint8_t));

typedef enum PACKED {
    NOAIO_OUTPUT_0 = (1 << 0),
    NOAIO_OUTPUT_1 = (1 << 1),
    NOAIO_OUTPUT_2 = (1 << 2),
    NOAIO_OUTPUT_3 = (1 << 3),
    NOAIO_OUTPUT_4 = (1 << 4),
    NOAIO_OUTPUT_5 = (1 << 5),
    NOAIO_OUTPUT_6 = (1 << 6),
    NOAIO_OUTPUT_7 = (1 << 7),
    NOAIO_OUTPUTS = 8,

    NOAIO_OUTPUT_INVALID = 0,
	NOAIO_UNINITIALIZED = 255
} NOAIO_OUTPUT_BITS;

#define NOAIO_UNINITIALIZED 255

ASSERT(sizeof(NOAIO_OUTPUT_BITS) == sizeof(uint8_t));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_INPUT_BITS input_state;
    NOAIO_OUTPUT_BITS output_state;
    NOAIO_OUTPUT_BITS output_latch;
    uint16_t system_voltage;
} NOAIO_RESPONSE_FRAME_GET_STATE;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_GET_STATE)
       == (sizeof(NOAIO_HEADER) + sizeof(NOAIO_INPUT_BITS)
           + 2*sizeof(NOAIO_OUTPUT_BITS) + sizeof(uint16_t)));

typedef enum PACKED {
    NOAIO_CHANNEL_OUT0 = 0,
    NOAIO_CHANNEL_OUT1 = 1,
    NOAIO_CHANNEL_OUT2 = 2,
    NOAIO_CHANNEL_OUT3 = 3,
    NOAIO_CHANNEL_OUT4 = 4,
    NOAIO_CHANNEL_OUT5 = 5,
    NOAIO_CHANNEL_OUT6 = 6,
    NOAIO_CHANNEL_OUT7 = 7,

    NOAIO_CHANNEL_INA = 0x0A,
    NOAIO_CHANNEL_INB = 0x0B,
    NOAIO_CHANNEL_INC = 0x0C,
    NOAIO_CHANNEL_IND = 0x0D,

    NOAIO_CHANNEL_BASE_OFFSET = NOAIO_CHANNEL_INA,
    NOAIO_CHANNEL_INVALID = (uint8_t) -1
} NOAIO_CHANNEL;

ASSERT(sizeof(NOAIO_CHANNEL) == sizeof(uint8_t));

INLINE int
NOAIO_input_channel_index(NOAIO_CHANNEL input_channel) {
    assert(NOAIO_CHANNEL_INA <= input_channel
           && input_channel <= NOAIO_CHANNEL_IND);
    int index = (int)input_channel - NOAIO_CHANNEL_BASE_OFFSET;
    assert(0 <= index && index < NOAIO_INPUTS);
    return index;
}

INLINE NOAIO_CHANNEL
NOAIO_input_index_channel(int index) {
    assert(0 <= index && index < NOAIO_INPUTS);
    NOAIO_CHANNEL input_channel
        = (NOAIO_CHANNEL) index + NOAIO_CHANNEL_BASE_OFFSET;
    assert(NOAIO_CHANNEL_INA <= input_channel
           && input_channel <= NOAIO_CHANNEL_IND);
    return input_channel;
}

INLINE NOAIO_INPUT_BITS
NOAIO_input(NOAIO_CHANNEL channel) {
    switch (channel) {
    case NOAIO_CHANNEL_INA:
        return NOAIO_INPUT_A;
    case NOAIO_CHANNEL_INB:
        return NOAIO_INPUT_B;
    case NOAIO_CHANNEL_INC:
        return NOAIO_INPUT_C;
    case NOAIO_CHANNEL_IND:
        return NOAIO_INPUT_D;
    default:
        assert(false && "invalid input channel");
        return NOAIO_INPUT_INVALID;
    }
}

INLINE NOAIO_CHANNEL
NOAIO_input_channel(NOAIO_INPUT_BITS bit) {
    switch (bit) {
    case NOAIO_INPUT_A:
        return NOAIO_CHANNEL_INA;
    case NOAIO_INPUT_B:
        return NOAIO_CHANNEL_INB;
    case NOAIO_INPUT_C:
        return NOAIO_CHANNEL_INC;
    case NOAIO_INPUT_D:
        return NOAIO_CHANNEL_IND;
    default:
        assert(false && "invalid input bit");
        return NOAIO_CHANNEL_INVALID;
    }
}

INLINE NOAIO_OUTPUT_BITS
NOAIO_output(NOAIO_CHANNEL channel) {
    switch (channel) {
    case NOAIO_CHANNEL_OUT0:
        return NOAIO_OUTPUT_0;
    case NOAIO_CHANNEL_OUT1:
        return NOAIO_OUTPUT_1;
    case NOAIO_CHANNEL_OUT2:
        return NOAIO_OUTPUT_2;
    case NOAIO_CHANNEL_OUT3:
        return NOAIO_OUTPUT_3;
    case NOAIO_CHANNEL_OUT4:
        return NOAIO_OUTPUT_4;
    case NOAIO_CHANNEL_OUT5:
        return NOAIO_OUTPUT_5;
    case NOAIO_CHANNEL_OUT6:
        return NOAIO_OUTPUT_6;
    case NOAIO_CHANNEL_OUT7:
        return NOAIO_OUTPUT_7;
    default:
        assert(false && "invalid output channel");
        return NOAIO_OUTPUT_INVALID;
    }
}

INLINE NOAIO_CHANNEL
NOAIO_output_channel(NOAIO_OUTPUT_BITS bit) {
    switch (bit) {
    case NOAIO_OUTPUT_0:
        return NOAIO_CHANNEL_OUT0;
    case NOAIO_OUTPUT_1:
        return NOAIO_CHANNEL_OUT1;
    case NOAIO_OUTPUT_2:
        return NOAIO_CHANNEL_OUT2;
    case NOAIO_OUTPUT_3:
        return NOAIO_CHANNEL_OUT3;
    case NOAIO_OUTPUT_4:
        return NOAIO_CHANNEL_OUT4;
    case NOAIO_OUTPUT_5:
        return NOAIO_CHANNEL_OUT5;
    case NOAIO_OUTPUT_6:
        return NOAIO_CHANNEL_OUT6;
    case NOAIO_OUTPUT_7:
        return NOAIO_CHANNEL_OUT7;
    case NOAIO_UNINITIALIZED:
	return NOAIO_CHANNEL_INVALID;
    default:
        assert(false && "invalid output bit");
        return NOAIO_CHANNEL_INVALID;
    }
}

INLINE speed_t
NOAIO_baudrate_to_speed( NOAIO_BAUDRATE baud )
{
    switch(baud)
    {
        case NOAIO_BAUD_2400: return B2400;
        case NOAIO_BAUD_4800: return B4800;
        case NOAIO_BAUD_9600: return B9600;
        case NOAIO_BAUD_19200: return B19200;
        case NOAIO_BAUD_38400: return B38400;
        case NOAIO_BAUD_57600: return B57600;
        case NOAIO_BAUD_115200: return B115200;

        default: assert(false && "unsupported baudrate");
    }
}

INLINE NOAIO_BAUDRATE
NOAIO_number_to_baudrate( uint32_t baud )
{
    switch(baud)
    {
        case 2400: return NOAIO_BAUD_2400;
        case 4800: return NOAIO_BAUD_4800;
        case 9600: return NOAIO_BAUD_9600;
        case 14400: return NOAIO_BAUD_14400;
        case 19200: return NOAIO_BAUD_19200;
        case 28800: return NOAIO_BAUD_28800;
        case 38400: return NOAIO_BAUD_38400;
        case 57600: return NOAIO_BAUD_57600;
        case 76800: return NOAIO_BAUD_76800;
        case 115200: return NOAIO_BAUD_115200;
        case 250000: return NOAIO_BAUD_250000;
        case 500000: return NOAIO_BAUD_500000;
        case 1000000: return NOAIO_BAUD_1000000;

        default: assert(false && "unsupported baudrate");
    }
}

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t minimum_ex_current;
    uint16_t maximum_ex_current;
} NOAIO_REQUEST_FRAME_SET_CURRENT_LIMITS;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_SET_CURRENT_LIMITS)
       == sizeof(NOAIO_HEADER) + sizeof(NOAIO_CHANNEL) + 2*sizeof(uint16_t));


typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_RESPONSE_FRAME_SET_CURRENT_LIMITS;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_SET_CURRENT_LIMITS) == sizeof(NOAIO_HEADER));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
} NOAIO_REQUEST_FRAME_GET_OUTPUT_INFO;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_GET_OUTPUT_INFO)
       == sizeof(NOAIO_HEADER) + sizeof(NOAIO_CHANNEL));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t current;
    uint16_t peak_current;
    uint16_t minimum_ex_current;
    uint16_t maximum_ex_current;
} NOAIO_RESPONSE_FRAME_GET_OUTPUT_INFO;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_GET_OUTPUT_INFO)
       == sizeof(NOAIO_HEADER) + sizeof(NOAIO_CHANNEL) + 4*sizeof(uint16_t));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL input_channel;
} NOAIO_REQUEST_FRAME_GET_INPUT_INFO;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_GET_INPUT_INFO)
       == sizeof(NOAIO_HEADER) + sizeof(NOAIO_CHANNEL));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL input_channel;
    uint16_t input_voltage;
} NOAIO_RESPONSE_FRAME_GET_INPUT_INFO;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_GET_INPUT_INFO)
       == sizeof(NOAIO_HEADER) + sizeof(NOAIO_CHANNEL) + sizeof(uint16_t));

typedef struct PACKED {
    NOAIO_HEADER header;
    uint16_t minimum_ex_voltage;
    uint16_t maximum_ex_voltage;
} NOAIO_REQUEST_FRAME_SET_VOLTAGE_LIMITS;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_SET_VOLTAGE_LIMITS)
       == sizeof(NOAIO_HEADER) + 2*sizeof(uint16_t));

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_RESPONSE_FRAME_SET_VOLTAGE_LIMITS;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_SET_VOLTAGE_LIMITS)
       == sizeof(NOAIO_HEADER));

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_VOLTAGE_LIMITS;

ASSERT(sizeof(NOAIO_REQUEST_FRAME_GET_VOLTAGE_LIMITS)
       == sizeof(NOAIO_HEADER));

typedef struct PACKED {
    NOAIO_HEADER header;
    uint16_t minimum_ex_voltage;
    uint16_t maximum_ex_voltage;
} NOAIO_RESPONSE_FRAME_GET_VOLTAGE_LIMITS;

ASSERT(sizeof(NOAIO_RESPONSE_FRAME_GET_VOLTAGE_LIMITS)
       == sizeof(NOAIO_HEADER) + 2*sizeof(uint16_t));

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
} NOAIO_REQUEST_FRAME_ENABLE_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
} NOAIO_RESPONSE_FRAME_ENABLE_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
} NOAIO_REQUEST_FRAME_DISABLE_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
} NOAIO_RESPONSE_FRAME_DISABLE_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_OUTPUT_BITS output_latch;    
} NOAIO_REQUEST_FRAME_SET_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_OUTPUT_BITS output_latch;
} NOAIO_RESPONSE_FRAME_SET_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_OUTPUT_BITS output_latch;    
} NOAIO_REQUEST_FRAME_SET_DEFAULT_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_OUTPUT_BITS output_latch;
} NOAIO_RESPONSE_FRAME_SET_DEFAULT_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_DEFAULT_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_OUTPUT_BITS output_latch;
} NOAIO_RESPONSE_FRAME_GET_DEFAULT_OUTPUT;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t period_ms;
} NOAIO_REQUEST_FRAME_ENABLE_FOR_PERIOD;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t period_ms;
} NOAIO_RESPONSE_FRAME_ENABLE_FOR_PERIOD;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t period_ms;
} NOAIO_REQUEST_FRAME_DISABLE_FOR_PERIOD;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_CHANNEL channel;
    uint16_t period_ms;
} NOAIO_RESPONSE_FRAME_DISABLE_FOR_PERIOD;

typedef struct PACKED {
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_EVENTS;

typedef enum PACKED {
    NOAIO_EVENT_CH0_OVERLOAD = 0x00,
    NOAIO_EVENT_CH1_OVERLOAD = 0x01,
    NOAIO_EVENT_CH2_OVERLOAD = 0x02,
    NOAIO_EVENT_CH3_OVERLOAD = 0x03,
    NOAIO_EVENT_CH4_OVERLOAD = 0x04,
    NOAIO_EVENT_CH5_OVERLOAD = 0x05,
    NOAIO_EVENT_CH6_OVERLOAD = 0x06,
    NOAIO_EVENT_CH7_OVERLOAD = 0x07,
    NOAIO_EVENT_CHX_UNDERLOAD = 0x10,
    NOAIO_EVENT_CH0_UNDERLOAD = 0x10,
    NOAIO_EVENT_CH1_UNDERLOAD = 0x11,
    NOAIO_EVENT_CH2_UNDERLOAD = 0x12,
    NOAIO_EVENT_CH3_UNDERLOAD = 0x13,
    NOAIO_EVENT_CH4_UNDERLOAD = 0x14,
    NOAIO_EVENT_CH5_UNDERLOAD = 0x15,
    NOAIO_EVENT_CH6_UNDERLOAD = 0x16,
    NOAIO_EVENT_CH7_UNDERLOAD = 0x17,

    NOAIO_EVENT_INPUTA_RISING_EDGE = 0x0A,
    NOAIO_EVENT_INPUTB_RISING_EDGE = 0x0B,
    NOAIO_EVENT_INPUTC_RISING_EDGE = 0x0C,
    NOAIO_EVENT_INPUTD_RISING_EDGE = 0x0D,
    NOAIO_EVENT_INPUTA_FALLING_EDGE = 0x1A,
    NOAIO_EVENT_INPUTB_FALLING_EDGE = 0x1B,
    NOAIO_EVENT_INPUTC_FALLING_EDGE = 0x1C,
    NOAIO_EVENT_INPUTD_FALLING_EDGE = 0x1D,
    NOAIO_EVENT_UNDERVOLTAGE = 0x20,
    NOAIO_EVENT_OVERVOLTAGE = 0x21,
    NOAIO_EVENT_UNEXPECTED_PWR_LOSS = 0x22
} NOAIO_EVENT;

ASSERT(sizeof(NOAIO_EVENT) == sizeof(uint8_t));

typedef struct PACKED {
    NOAIO_EVENT event;
    uint8_t time[3];
} NOAIO_EVENT_TIME;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_EVENT_TIME events[];
} NOAIO_RESPONSE_FRAME_GET_EVENTS;

typedef struct PACKED {
    NOAIO_HEADER header;
    NOAIO_EVENT_TIME events[(NOAIO_MAX_FRAME_LENGTH - (sizeof(NOAIO_HEADER)
                                                       +sizeof(uint8_t)
                                                       +2*sizeof(NOAIO_TX)))
                            / sizeof(NOAIO_EVENT_TIME)];
} NOAIO_RESPONSE_FRAME_GET_EVENTS_MAX;

// *** watchdog

typedef enum PACKED 
{
    DISABLED = 0,
    ENABLED_RESET_CMD = 1,
    ENABLED_ANY_CMD = 2
} NOAIO_WATCHDOG_MODE;

// set
typedef struct PACKED 
{
    NOAIO_HEADER header;
    NOAIO_WATCHDOG_MODE watchdog_mode;
    NOAIO_OUTPUT_BITS output_mask;
    uint8_t watchdog_interval;
} NOAIO_REQUEST_FRAME_SET_WATCHDOG;

typedef struct PACKED
{
    NOAIO_HEADER header;
} NOAIO_RESPONSE_FRAME_SET_WATCHDOG;

// get
typedef struct PACKED  
{
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_GET_WATCHDOG;

typedef struct PACKED
{
    NOAIO_HEADER header;
    NOAIO_WATCHDOG_MODE watchdog_mode;
    NOAIO_OUTPUT_BITS output_mask;
    uint8_t watchdog_interval;
} NOAIO_RESPONSE_FRAME_GET_WATCHDOG;

// reset
typedef struct PACKED
{
    NOAIO_HEADER header;
} NOAIO_REQUEST_FRAME_WATCHDOG_RESET;

typedef struct PACKED
{
    NOAIO_HEADER header;
} NOAIO_RESPONSE_FRAME_WATCHDOG_RESET;

// *** baudrate
typedef struct PACKED
{
    NOAIO_HEADER header;
    NOAIO_BAUDRATE baud_rate_index;
} NOAIO_REQUEST_FRAME_SET_BAUDRATE;

typedef struct PACKED
{
    NOAIO_HEADER header;
    NOAIO_BAUDRATE baud_rate_index;
} NOAIO_RESPONSE_FRAME_SET_BAUDRATE;

typedef union PACKED {
    NOAIO_HEADER header;
    NOAIO_REQUEST_FRAME_GET_VERSION GET_VERSION;
    NOAIO_REQUEST_FRAME_GET_STATE GET_STATE;
    NOAIO_REQUEST_FRAME_SET_CURRENT_LIMITS SET_CURRENT_LIMITS;
    NOAIO_REQUEST_FRAME_GET_OUTPUT_INFO GET_OUTPUT_INFO;
    NOAIO_REQUEST_FRAME_GET_INPUT_INFO GET_INPUT_INFO;
    NOAIO_REQUEST_FRAME_SET_VOLTAGE_LIMITS SET_VOLTAGE_LIMITS;
    NOAIO_REQUEST_FRAME_GET_VOLTAGE_LIMITS GET_VOLTAGE_LIMITS;
    NOAIO_REQUEST_FRAME_ENABLE_OUTPUT ENABLE_OUTPUT;
    NOAIO_REQUEST_FRAME_DISABLE_OUTPUT DISABLE_OUTPUT;
    NOAIO_REQUEST_FRAME_SET_OUTPUT SET_OUTPUT;
    NOAIO_REQUEST_FRAME_SET_DEFAULT_OUTPUT SET_DEFAULT_OUTPUT;
    NOAIO_REQUEST_FRAME_GET_DEFAULT_OUTPUT GET_DEFAULT_OUTPUT;
    NOAIO_REQUEST_FRAME_ENABLE_FOR_PERIOD ENABLE_FOR_PERIOD;
    NOAIO_REQUEST_FRAME_DISABLE_FOR_PERIOD DISABLE_FOR_PERIOD;
    NOAIO_REQUEST_FRAME_GET_EVENTS GET_EVENTS;
    NOAIO_REQUEST_FRAME_SET_BAUDRATE SET_BAUDRATE;
    NOAIO_REQUEST_FRAME_SET_WATCHDOG SET_WATCHDOG;
    NOAIO_REQUEST_FRAME_GET_WATCHDOG GET_WATCHDOG;
    NOAIO_REQUEST_FRAME_WATCHDOG_RESET WATCHDOG_RESET;
} NOAIO_REQUEST_FRAME;

typedef union PACKED {
    NOAIO_HEADER header;
    NOAIO_RESPONSE_FRAME_GET_VERSION GET_VERSION;
    NOAIO_RESPONSE_FRAME_GET_STATE GET_STATE;
    NOAIO_RESPONSE_FRAME_SET_CURRENT_LIMITS SET_CURRENT_LIMITS;
    NOAIO_RESPONSE_FRAME_GET_OUTPUT_INFO GET_OUTPUT_INFO;
    NOAIO_RESPONSE_FRAME_GET_INPUT_INFO GET_INPUT_INFO;
    NOAIO_RESPONSE_FRAME_SET_VOLTAGE_LIMITS SET_VOLTAGE_LIMITS;
    NOAIO_RESPONSE_FRAME_GET_VOLTAGE_LIMITS GET_VOLTAGE_LIMITS;
    NOAIO_RESPONSE_FRAME_ENABLE_OUTPUT ENABLE_OUTPUT;
    NOAIO_RESPONSE_FRAME_DISABLE_OUTPUT DISABLE_OUTPUT;
    NOAIO_RESPONSE_FRAME_SET_OUTPUT SET_OUTPUT;
    NOAIO_RESPONSE_FRAME_SET_DEFAULT_OUTPUT SET_DEFAULT_OUTPUT;
    NOAIO_RESPONSE_FRAME_GET_DEFAULT_OUTPUT GET_DEFAULT_OUTPUT;
    NOAIO_RESPONSE_FRAME_ENABLE_FOR_PERIOD ENABLE_FOR_PERIOD;
    NOAIO_RESPONSE_FRAME_DISABLE_FOR_PERIOD DISABLE_FOR_PERIOD;
    NOAIO_RESPONSE_FRAME_GET_EVENTS GET_EVENTS;
    NOAIO_RESPONSE_FRAME_SET_BAUDRATE SET_BAUDRATE;
    NOAIO_RESPONSE_FRAME_SET_WATCHDOG SET_WATCHDOG;
    NOAIO_RESPONSE_FRAME_GET_WATCHDOG GET_WATCHDOG;
    NOAIO_RESPONSE_FRAME_WATCHDOG_RESET WATCHDOG_RESET;
} NOAIO_RESPONSE_FRAME;

typedef union PACKED {
    NOAIO_HEADER header;
    NOAIO_REQUEST_FRAME REQUEST;
    NOAIO_RESPONSE_FRAME RESPONSE;
} NOAIO_FRAME;

typedef struct PACKED {
    NOAIO_TX stx;
    NOAIO_FRAME noaio;
} NOAIO_TRANSPORT_LAYER_FRAME_PREFIX;

static inline uint8_t
NOAIO_checksum(const NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *frame) {
    assert(frame->stx == NOAIO_TX_START);
    const byte *const buffer = (const byte *) frame;
    const int length = frame->noaio.header.length - 2;
    uint8_t lrc = 0;
    for (int i = 0; i < length; ++i) {
        lrc ^= buffer[i];
    }
    return lrc;
}

static inline uint8_t
NOAIO_retrieve_checksum(const NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *frame) {
    const byte *const bytes = (byte *) frame;
    return bytes[frame->noaio.header.length-2];
}

#define NOAIO_CHECKSUM(b)                                       \
    NOAIO_checksum((NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *) b)

#define NOAIO_DECLARE_REQUEST_FRAME(frame, req, addr, ...)              \
    struct {                                                            \
        NOAIO_TX stx;                                                   \
        NOAIO_REQUEST_FRAME_##req data;                                 \
        uint8_t lrc;                                                    \
        NOAIO_TX etx;                                                   \
    } frame = {                                                         \
        .stx = NOAIO_TX_START,                                          \
        .data = {                                                       \
            .header = {                                                 \
                .length = (sizeof(NOAIO_REQUEST_FRAME_##req)            \
                           + sizeof(uint8_t) + 2*sizeof(NOAIO_TX)),     \
                .address = addr,                                        \
                .request = NOAIO_REQUEST_##req                          \
            },                                                          \
            ## __VA_ARGS__                                              \
        },                                                              \
        .etx = NOAIO_TX_END                                             \
    };                                                                  \
    frame.lrc = NOAIO_CHECKSUM(&frame)

typedef struct {
    byte buffer[NOAIO_MAX_FRAME_LENGTH];
    int next;
    int shift;
} NOAIO_INCOMMING_FRAME;

extern const char *NOAIO_request_name(NOAIO_REQUEST request);
extern uint8_t NOAIO_num_events(NOAIO_RESPONSE_FRAME_GET_EVENTS *frame); 
extern NOAIO_FRAME *NOAIO_get_frame(int fd, NOAIO_INCOMMING_FRAME *f,
                                    clock_t timeout_ms);
extern NOAIO_FRAME *NOAIO_request(int out, int in,
                                  NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *outgoing,
                                  NOAIO_INCOMMING_FRAME *incomming,
                                  clock_t timeout_ms);
extern NOAIO_FRAME *NOAIO_copy_frame(NOAIO_FRAME *frame);
extern void NOAIO_flush_input(int fd);


int set_custom_baudrate( int uart, uint32_t baud );
#endif // NOAIO_H
