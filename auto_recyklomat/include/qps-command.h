#ifndef QPS_COMMAND_H
#define QPS_COMMAND_H

#include "qps.h"
/**
 * UWAGA! W przypadku sukcesu, `QPS_blocking_command` zwraca
 * kopie ramki (w przeciwnym razie dostajemy `NULL`).
 * Obowiazkiem wolajacego jest zwolnienie tej pamieci
 * (najlepiej uzywac makra `QPS_WITH_COMMAND_RESPONSE`, ktore
 * - uzyte poprawnie - zajmuje sie ta kwestia za uzytkownika).
 */
extern NOAIO_FRAME *
QPS_blocking_command(IODriver *, NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *);

extern bool
QPS_blocking_command_success(IODriver *, NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *);

extern void
QPS_nonblocking_command(IODriver *, NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *);

extern void
QPS_execute_pending_command(UARTInterface *uart);

extern void
QPS_poll_input( IODriver * device );

/**
 * UWAGA! Poniewaz makro `QPS_COMMAND_BLOCK` przekazuje wynik
 * funkcji `QPS_blocking_command`, uwagi dotyczace tej funkcji
 * stosuja sie rowniez do uzyc tego makra.
 */
#define QPS_COMMAND_BLOCK(io, req, ...) ({                              \
            NOAIO_DECLARE_REQUEST_FRAME(qps_local_frame,                \
                                        req, (io)->address,             \
                                        ## __VA_ARGS__);                \
            QPS_blocking_command((io),                                  \
                                 ((NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *) \
                                  &qps_local_frame));                   \
        })


#define QPS_COMMAND_NONBLOCK(io, req, ...) {                            \
        NOAIO_DECLARE_REQUEST_FRAME(qps_local_frame,                    \
                                    req, (io)->address,                 \
                                    ## __VA_ARGS__);                    \
        QPS_nonblocking_command((io),                                   \
                                ((NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *) \
                                 COPY(qps_local_frame)));               \
    }

/**
 * UWAGA! Blok `action` makra `QPS_WITH_COMMAND_RESPONSE` nie moze
 * zawierac instrukcji wyprowadzajacej przeplyw sterowania (takiej jak
 * `goto`, `return`, `break` itp.) poza ten blok, gdyz spowoduje to
 * wyciek pamieci.
 */
#define QPS_WITH_COMMAND_RESPONSE(frmname, action, dev, request, ...) { \
        NOAIO_FRAME *raw_frame = QPS_COMMAND_BLOCK(dev, request,        \
                                                   ## __VA_ARGS__);     \
        QPS_WITH_RESPONSE_FRAME(raw_frame, request,                     \
                                frmname, action, {});                       \
        if (raw_frame) { free(raw_frame); }                             \
    }

# if USE_PTHREAD_TIMER

typedef struct {
    IODriver *io;
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX command;
} QPSRemote;

extern void QPS_send_then_release(QPSRemote *remote);

# endif

typedef enum {
    DontWaitForResponse = 0,
    WaitForResponse = 1
} AttitudeTowardsResponse;

#if NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STRATEGY_OVERWRITE

void
QPS_disable(IODriver *io, NOAIO_CHANNEL channel,
                  AttitudeTowardsResponse afterIssuingCommand);

extern void
QPS_enable(IODriver *io, NOAIO_CHANNEL channel,
                  AttitudeTowardsResponse afterIssuingCommand);

extern void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand);

extern void
QPS_disable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand);

#elif NOA_IO_FOR_PERIOD_STRATEGY == NOA_IO_STARTEGY_APPEND

# if USE_PTHREAD_TIMER

typedef struct {
    IODriver *io;
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX command;
} QPSRemote;

extern void QPS_send_then_release(QPSRemote *remote);

extern void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand);

# else // !USE_PTHREAD_TIMER

typedef struct {
    NOAIO_TRANSPORT_LAYER_FRAME_PREFIX *command;
    uint16_t period_ms;
    IODriver *device;
} PeriodTimerCommand;

extern void
QPS_enable_for_period(IODriver *io, NOAIO_CHANNEL channel, uint16_t period_ms,
                      AttitudeTowardsResponse afterIssuingCommand);

extern THREAD void *
QPS_waiting_for_period(PeriodCommandTimer *timer);



# endif // !USE_PTHREAD_TIMER

#endif // NOA_IO_FOR_PERIOD_STRATEGY

#endif // QPS_COMMAND_H
