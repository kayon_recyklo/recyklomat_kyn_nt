/*
 * qps-backend.h
 *
 *  Created on: Mar 15, 2019
 *      Author: Fixed
 */

#ifndef INCLUDE_QPS_BACKEND_H_
#define INCLUDE_QPS_BACKEND_H_

#include "util.h"
#include "recyc.h"
#include "pthread-queue.h"

#define THREAD
#define BE_SOCKET_PATH "/tmp/be-socket"

typedef struct {
	char id[4];
}RecognizedItem;

#define MAX_RECOGNIZED_ITEMS 100

typedef struct {
	pthread_t thread;
	pthread_queue_t queue;
} BackendData;

extern Supervisor *konfident;

void process_report(Supervisor *self, Report condition, ...);
extern THREAD void *QPS_backend();

#endif /* INCLUDE_QPS_BACKEND_H_ */
