/*
 * qps-recognizer.h
 *
 *  Created on: Apr 9, 2019
 *      Author: Fixed
 */

#ifndef INCLUDE_QPS_RECOGNIZER_H_
#define INCLUDE_QPS_RECOGNIZER_H_

#include "util.h"
#include "pthread-timer.h"
#include "qps.h"

#define USE_RECOG 1

#define THREAD
#define RECOG_SOCKET_PATH "/tmp/recog-socket"

#define START_RECOGNITION "\n"
#define RECOGNIZE_PET "PET\n"
#define RECOGNIZE_ALU "ALU\n"
#define RECOGNIZE_GLA "GLA\n"
#define RECOGNIZE_OTH "OTH\n"

extern THREAD void QPS_recognizer();
extern void QPS_init_recognizer(char* recognizer);
extern int QPS_init_recognizer_socket();
extern void QPS_init_recognizer_client_conn(int socket);
extern int QPS_ask_recognizer(int socket, char* cmd, char *item, uint8_t item_size);

extern void QPS_init_recog_buttons(Recyc *self, void (*petaction)(Recyc *)
		, void (*aluaction)(Recyc *), void (*glaaction)(Recyc *));

extern void QPS_recognize_PET(IODriver *io, int input_index, bool new_value);
extern void QPS_on_mockup_PET_recognition(Recyc* self);

extern void QPS_recognize_ALU(IODriver *io, int input_index, bool new_value);
extern void QPS_on_mockup_ALU_recognition(Recyc* self);

extern void QPS_recognize_GLA(IODriver *io, int input_index, bool new_value);
extern void QPS_on_mockup_GLA_recognition(Recyc* self);

#endif /* INCLUDE_QPS_RECOGNIZER_H_ */
