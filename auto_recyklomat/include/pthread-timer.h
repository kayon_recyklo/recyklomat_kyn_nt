#ifndef PTHREAD_TIMER_H
#define PTHREAD_TIMER_H

typedef void (*delayed_procedure_t)(void *);

extern void
pthread_after_ms(int time_ms, delayed_procedure_t action, void *arg);

#endif // PTHREAD_TIMER_H

