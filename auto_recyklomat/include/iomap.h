#ifndef IOMAP_H
#define IOMAP_H

/*
 * Odwzorowanie logicznych wejsc i wyjsc sterownika w fizyczne
 * wejscia i wyjscia na plytce NoaTech IO Driver.
 *
 * UWAGA! Na koncu pliku znajduje sie odrebna sekcja,
 * ktora jest warunkowo zalaczana z pliku qps-reflex.c.
 * Celem takiego nieklasycznego rozwiazania jest trzymanie
 * wszystkich danych konfiguracyjnych w jednym pliku.
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl> 2018
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */

#include "noaio.h"
#include "recyc.h"

#define IOMAP_INI_PATH "/home/pi/rk_work/iomap.ini"

// konfiguracja predkosci CAN
#define CAN_BAUDRATE recyc_config.can_baudrate

// konfiguracja przerwy nocnej
#define MAINTENANCE_START_HOUR 	recyc_config.maintenance_start_hour
#define MAINTENANCE_STOP_HOUR 	recyc_config.maintenance_stop_hour

// strategie realizacji poleceni *_FOR_PERIOD przez IO Driver:
#define NOA_IO_STRATEGY_APPEND 1 // pierwotna strategia NoaTechu
#define NOA_IO_STRATEGY_OVERWRITE 2 // "przedluzanie" okresu przelaczenia
#define NOA_IO_FOR_PERIOD_STRATEGY NOA_IO_STRATEGY_OVERWRITE //OVERWRITE

#define USE_PTHREAD_TIMER 						1
#define QPS_RECOGNITION_ITER 						3

#define CONVEYOR_STD_SPEED						4
#define CONVEYOR_MAX_SPEED						7
//timing

// czasy czekania na 'kielichu' + czas swiecenia diod 'kielicha'
#define QPS_DROPBOX_DELAY						dropbox_times.delay_ms
// czas swiecenia diod we wrzutni
#define QPS_DROPBOX_LEDS_DELAY_MS					dropbox_times.led_time_ms
// jak czesto odpytujemy kazde urzadzenie prze UART
#define QPS_CAN_POLLING_PERIOD_MS 					recyc_config.can_pooling_perioid_ms

#define QPS_PROCESS_PET_MIN_LENGTH					dropbox_times.process_pet_min_length_s
#define QPS_PROCESS_ALU_MIN_LENGTH					dropbox_times.process_alu_min_length_s
#define QPS_PROCESS_GLA_MIN_LENGTH					dropbox_times.process_gla_min_length_s
#define QPS_PROCESS_DEO_MIN_LENGTH					dropbox_times.process_deo_min_length_s
#define QPS_PROCESS_BAT_MIN_LENGTH					dropbox_times.process_bat_min_length_s
#define QPS_PROCESS_SYR_MIN_LENGTH					dropbox_times.process_syr_min_length_s

#define QPS_DOOR_SAFETY_TIME						dropbox_times.door_safety_time_s
#define QPS_RECOGNITION_DELAY						dropbox_times.recognition_delay_ms
#define QPS_NEXT_RECOGNITION_DELAY					dropbox_times.next_recognition_delay_ms
// czasy przerzucania odpadow do 'kielicha'
#define QPS_DROPBOX_TIME_MS						dropbox_times.open_time_ms
#define QPS_DROPBOX_MIN_ROLLS_TIME_MS					dropbox_times.min_rolls_time_ms
#define QPS_DROPBOX_MAX_ROLLS_TIME_MS					dropbox_times.max_rolls_time_ms
#define QPS_DROPBOX_LOCK_TIME_MS					dropbox_times.lock_time_ms
#define QPS_DROPBOX_ROLLS_TIME_MS					dropbox_times.rolls_time_ms
#define QPS_DROPBOX_CONVEYOR_PASSAGE_TIME_MS				dropbox_times.conveyor_passage_time_ms

#define QPS_CONVEYOR_PASSAGE_TIME_MS 					conveyor_times.conveyor_passage_time_ms

// maksymalny czas drogi odpadu miedzy czujnikami[s]
#define QPS_REFLEX_TIMEOUT                                              conveyor_times.reflex_timeout_s
#define QPS_REFLEX_TIMEOUT_FIRST					conveyor_times.first_reflex_timeout_s
#define QPS_REFLEX_HIGH_STATE_TIMEOUT					conveyor_times.reflex_high_state_timeout_s

// maksymalna dopuszczalna ilosc bledow pod rzad bez wywolywania ERROR()
#define QPS_REFLEX_FAULT_CNT_MAX					recyc_config.opto_max_error_cnt

#define QPS_CONVEYOR_MAX_BLOCKED_TIME_S					conveyor_times.conveyor_max_blocked_time_s

// czas potrzebny na zrzucenie wszystkich odpadow z conveyora w milisekundach
#define QPS_CONVEYOR_STEP_BACK_TIME					conveyor_times.conveyor_step_back_time_ms
#define QPS_CONVEYOR_RETURN_TIME					conveyor_times.conveyor_return_time_ms
// czas potrzebny na zrzucenie wszystkich odpadow z conveyora w milisekundach
#define QPS_CONVEYOR_CLEAN_TIME						conveyor_times.conveyor_clean_time_ms
// czas kiedy conveyor jest zatrzymany zeby device mogl bezpiecznie obsluzyc swoj blad
#define QPS_DIRECTOR_ERROR_DELAY					conveyor_times.director_error_delay_ms
// czas conveyora na probe ponownego zamkniecia przy obsludze bledow podany w sekundach
#define QPS_DIRECTOR_CLOSE_DELAY_SEC					conveyor_times.director_close_delay_s
#define QPS_DIRECTOR_OPEN_DELAY_SEC					conveyor_times.director_open_delay_s

// io map

typedef struct dropbox_time_info_t
{
	uint16_t process_pet_min_length_s;
	uint16_t process_alu_min_length_s;
	uint16_t process_gla_min_length_s;
	uint16_t process_deo_min_length_s;
	uint16_t process_bat_min_length_s;
	uint16_t process_syr_min_length_s;

	uint16_t delay_ms;
	uint16_t open_time_ms;
	uint16_t min_rolls_time_ms;
	uint16_t max_rolls_time_ms;
	uint16_t lock_time_ms;
	uint16_t rolls_time_ms;
	uint16_t conveyor_passage_time_ms;
	uint16_t led_time_ms;
	uint16_t door_safety_time_s;
	uint16_t recognition_delay_ms;
	uint16_t next_recognition_delay_ms;
} dropbox_time_info_t;

typedef struct conveyor_time_info_t
{
	uint16_t conveyor_passage_time_ms;
	uint16_t reflex_timeout_s;
	uint16_t first_reflex_timeout_s;
	uint16_t reflex_high_state_timeout_s;

	uint16_t arm_time_ms;
	uint16_t first_arm_closing_delay_ms;
	uint16_t arm_closing_delay_ms;
	
	uint16_t crashing_time_ms;
	uint16_t releasing_time_ms;
	
	uint16_t crashing_delay_ms;
	uint16_t releasing_delay_ms;

	uint16_t crasher_test_offset_ms;
	uint16_t director_test_offset_ms;

	uint16_t conveyor_max_blocked_time_s;

	uint16_t conveyor_step_back_time_ms;
	uint16_t conveyor_return_time_ms;
	uint16_t conveyor_clean_time_ms;
	uint16_t director_error_delay_ms;

	uint16_t director_close_delay_s;
	uint16_t director_open_delay_s;
} conveyor_time_info_t;

typedef struct recyc_config_params_t
{
	uint16_t can_pooling_perioid_ms;
	uint16_t crusher_max_voltage_mv;
	uint8_t opto_max_error_cnt;
	uint8_t crusher_max_ovc_cnt;
        speed_t can_baudrate;
	uint8_t watchdog_mode;
	uint8_t watchdog_interval;

	uint8_t maintenance_start_hour;
	uint8_t maintenance_stop_hour;
} recyc_config_params_t;
// OPEN = zgniec
// CLOSE = pusc

#define QPS_WATCHDOG_DEVICE				qps_pwr.device
#define QPS_WATCHDOG_OUT				qps_pwr.out
#define QPS_WATCHDOG_MODE				recyc_config.watchdog_mode
#define QPS_WATCHDOG_INTERVAL 				recyc_config.watchdog_interval

// przypisanie outputow do makr zgniatacza butelek PET
#define QPS_PET_CRUSHER_DEVICE 				pet_crusher_open.device
#define QPS_PET_CRUSHER_OPEN_OUT 			pet_crusher_open.out
#define QPS_PET_CRUSHER_OPEN_INIT_STATE		        pet_crusher_open.init_state
#define QPS_PET_CRUSHER_CLOSE_OUT			pet_crusher_close.out
#define QPS_PET_CRUSHER_CLOSE_INIT_STATE	        pet_crusher_close.init_state

// przypisanie outputow do makr tasmociagu
#define QPS_CONVEYOR_DEVICE 				conveyor_dir.device
#define QPS_CONVEYOR_START_OUT 				conveyor_start.out
#define QPS_CONVEYOR_START_INIT_STATE		        conveyor_start.init_state
#define QPS_CONVEYOR_DIR_OUT 				conveyor_dir.out
#define QPS_CONVEYOR_DIR_INIT_STATE			conveyor_dir.init_state
#define QPS_CONVEYOR_SPEED_A_OUT			conveyor_speed_a.out
#define QPS_CONVEYOR_SPEED_A_INIT_STATE		        conveyor_speed_a.init_state
#define QPS_CONVEYOR_SPEED_B_OUT			conveyor_speed_b.out
#define QPS_CONVEYOR_SPEED_B_INIT_STATE		        conveyor_speed_b.init_state
#define QPS_CONVEYOR_SPEED_C_OUT			conveyor_speed_c.out
#define QPS_CONVEYOR_SPEED_C_INIT_STATE		        conveyor_speed_c.init_state

// przypisanie outputow do makr zgniatacza puszek aluminiowyhc
#define QPS_ALU_CRASHER_DEVICE				alu_crusher_open.device
#define QPS_ALU_CRASHER_OPEN_OUT			alu_crusher_open.out
#define QPS_ALU_CRASHER_OPEN_INIT_STATE		        alu_crusher_open.init_state
#define QPS_ALU_CRASHER_CLOSE_OUT			alu_crusher_close.out
#define QPS_ALU_CRASHER_CLOSE_INIT_STATE	        alu_crusher_close.init_state

// makra zamkniecia drzwiczek 'kielicha'
/*
#define QPS_DROPBOX_DOOR_LOCK_DEVICE			dropbox_door_lock.device
#define QPS_DROPBOX_DOOR_LOCK_OUT			dropbox_door_lock.out
#define QPS_DROPBOX_DOOR_LOCK_INIT_STATE	        dropbox_door_lock.init_state
*/
// czujnik drzwiczek kielicha
#define QPS_DROPBOX_DOOR_DETECTOR_DEVICE		door_input.device
#define QPS_DROPBOX_DOOR_DETECTOR_IN			door_input.in

// zamkniecie 'kielicha'
#define QPS_DROPBOX_LOCK_DEVICE				dropbox_lock.device
#define QPS_DROPBOX_LOCK_OUT				dropbox_lock.out
#define QPS_DROPBOX_LOCK_INIT_STATE			dropbox_lock.init_state

// 'KIELICH'
#define QPS_DROPBOX_DEVICE				dropbox.device
#define QPS_DROPBOX_OUT					dropbox.out
#define QPS_DROPBOX_INIT_STATE			        dropbox.init_state

// TASMOCIAG
#define QPS_DROPBOX_CONVEYOR_DEVICE			dropbox_conveyor.device
#define QPS_DROPBOX_CONVEYOR_OUT			dropbox_conveyor.out
#define QPS_DROPBOX_CONVEYOR_INIT_STATE		        dropbox_conveyor.init_state

// oswietlenie 'kielicha'
#define QPS_DROPBOX_LEDS_DEVICE				dropbox_leds.device
#define QPS_DROPBOX_LEDS_OUT				dropbox_leds.out
#define QPS_DROPBOX_LEDS_INIT_STATE			dropbox_leds.init_state

// 'kielich'
#define QPS_DROPBOX_DIRECTOR_DEVICE			dropbox_director.device
#define QPS_DROPBOX_DIRECTOR_OUT			dropbox_director.out
#define QPS_DROPBOX_DIRECTOR_INIT_STATE		        dropbox_director.init_state

// ROLKI 'kielicha'
#define QPS_DROPBOX_ROLLS_DEVICE			dropbox_rolls.device
#define QPS_DROPBOX_ROLLS_OUT				dropbox_rolls.out
#define QPS_DROPBOX_ROLLS_INIT_STATE		        dropbox_rolls.init_state

// ekran
#define QPS_DISPLAY_DEVICE				qps_display.device
#define QPS_DISPLAY_OUT					qps_display.out
#define QPS_DISPLAY_INIT_STATE		                qps_display.init_state

// QPS
#define QPS_QPS_PWR_DEVICE				qps_pwr.device
#define QPS_QPS_PWR_OUT					qps_pwr.out
#define QPS_QPS_PWR_INIT_STATE		                qps_pwr.init_state

// drukarka
#define QPS_PRINTER_DEVICE				printer.device
#define QPS_PRINTER_OUT					printer.out
#define QPS_PRINTER_INIT_STATE		                printer.init_state

#define CRUSHER_MAX_VOLTAGE_MV		                recyc_config.crusher_max_voltage_mv
#define CRUSHER_MIN_OVC_SAMPLES_ERR	                recyc_config.crusher_max_ovc_cnt

typedef enum init_state{
	OFF = 0,
	ON = 1
}init_state;

//typedef struct iodriver_config;
typedef struct iodriver_config{
	uint8_t device;
	NOAIO_OUTPUT_BITS out;
	init_state init_state;
	uint16_t min_limit_mA;
	uint16_t max_limit_mA;
        
        uint16_t soft_min_limit_mA;
        uint16_t soft_max_limit_mA;

	bool current_measure;

	time_t first_ovc_time;
	time_t last_ovc_time;

        void * arg;

	void(*ovc_callback)(struct iodriver_config*, Recyc*);
	void(*current_ok_callback)(struct iodriver_config*, Recyc*);
} iodriver_config;

typedef struct iodriver_input_info_t
{
	uint8_t device;
	NOAIO_ANALOG_CHANNEL in;
	uint16_t max_voltage_mv;
	uint8_t ovc_cnt;
	uint8_t max_ovc_cnt;
	void * arg;

	void(*ovc_callback)(struct iodriver_input_info_t * info);
} iodriver_input_info_t;

typedef struct iodriver_voltage_limits_t
{
	uint8_t device;
	uint16_t min_voltage_mv;
	uint16_t max_voltage_mv;
} iodriver_voltage_limits_t;

extern iodriver_config conveyor_dir;
extern iodriver_config pet_crusher_open;
extern iodriver_config pet_crusher_close;
extern iodriver_config alu_crusher_open;
extern iodriver_config alu_crusher_close;
extern iodriver_config conveyor_start;
extern iodriver_config conveyor_speed_a;
extern iodriver_config conveyor_speed_b;
extern iodriver_config conveyor_speed_c;
extern iodriver_config dropbox_conveyor;
extern iodriver_config dropbox_director;
//extern iodriver_config dropbox_door_lock;
extern iodriver_config dropbox_leds;
extern iodriver_config dropbox;
extern iodriver_config dropbox_lock;
extern iodriver_config dropbox_rolls;
extern iodriver_config conveyor_speed_a;
extern iodriver_config conveyor_speed_b;
extern iodriver_config conveyor_speed_c;
extern iodriver_config default_config;
extern iodriver_config qps_display;
extern iodriver_config qps_pwr;
extern iodriver_config printer;
extern iodriver_config gla_director_open;
extern iodriver_config gla_director_close;
extern iodriver_config pet_director_open;
extern iodriver_config pet_director_close;
extern iodriver_config alu_director_open;
extern iodriver_config alu_director_close;

extern iodriver_input_info_t alu_crusher_current_sense;
extern iodriver_input_info_t pet_crusher_current_sense;

extern iodriver_input_info_t pet_opto_input;
extern iodriver_input_info_t alu_opto_input;
extern iodriver_input_info_t oth_opto_input;
extern iodriver_input_info_t gla_director_close_sensor_input;
extern iodriver_input_info_t gla_director_open_sensor_input;
extern iodriver_input_info_t pet_director_open_sensor_input;
extern iodriver_input_info_t pet_director_close_sensor_input;
extern iodriver_input_info_t alu_director_open_sensor_input;
extern iodriver_input_info_t alu_director_close_sensor_input;
extern iodriver_input_info_t pet_crusher_open_sensor_input;
extern iodriver_input_info_t pet_crusher_close_sensor_input;
extern iodriver_input_info_t alu_crusher_open_sensor_input;
extern iodriver_input_info_t alu_crusher_close_sensor_input;
extern iodriver_input_info_t door_input;

extern iodriver_voltage_limits_t iodriver_voltage_limits[];

extern dropbox_time_info_t dropbox_times;
extern conveyor_time_info_t conveyor_times;
extern recyc_config_params_t recyc_config;

char * load_iomap_config( const char * filename );
void update_conveyor_devices_config( void );

#endif // IOMAP_H

#ifdef QPS_DEBUG_NAMES
char* QPS_out_names[3][8] = {
		{
			"empty",
			"alu_crasher_close",
			"alu_crasher_release",
			"pet_crasher_close",
			"pet_crasher_release",
			"conveyor",
			"small_conveyor",
			"bat_syr_director",
		},
		{
			"empty",
			"gla_director_close",
			"gla_director_open",
			"alu_director_close",
			"alu_director_open",
			"pet_director_close",
			"pet_director_open",
			"leds",
		},
		{
			"empty",
			"conv_speed_a",
			"conv_speed_b",
			"conv_speed_c",
			"conv_dir",
			"dropbox_solenoid",
			"dropbox_electrolock",
			"dropbox_rolls",
		},
};

char *QPS_in_names[3][4] = {
		{
			"opto1",
			"opto2",
			"opto3",
			"opto_last",
		},
		{
			"conveyor_info_1",
			"conveyor_info_2",
			"empty",
			"empty",
		},
		{
			"door_detector",
			"empty",
			"empty",
			"empty",
		},
};
#endif //DEBUG_NAMES

#ifdef QPS_DEFINE_REFLEX_TABLE

/*
 * Ta sekcja pliku jest widoczna tylko w qps-reflex.c 
 */

#include "qps-conveyor-devices.h"

typedef struct {
    int device_index;
    NOAIO_INPUT_BITS bit;
} QPS_input;

typedef enum{
	FRACTION_ENABLED = 0,	
	FRACTION_DISABLED = 1	
} QPS_reflex_status_t;

typedef struct QPSReflex {
    ItemType itemType;
    pthread_queue_t expectations;
    pthread_fifo_t history;
    QPS_input input;
    QPS_reflex_status_t is_enabled;

    conveyor_device_t * director;
    conveyor_device_t * crusher;

    struct QPSReflex * prev;
    time_t rising_edge_time;

    uint16_t fault_counter;
} QPSReflex;

typedef struct {
    ItemType itemType;
    void (*action)(Recyc *, ItemType);
} QPSInitialAction;

/**
 * Tabela `QPS_reflex_table` jest zorganizowana w taki sposob,
 * ze wczesniejsze wpisy powinny odpowiadac detektorom i aktuatorom
 * znajdujacym sie blizej drzwi.
 *
 * Jest to istotne, poniewaz jezeli chcemy, zeby np. czwarty czujnik
 * (liczac od drzwi) spowodowal zepchniecie obiektu z tasmy, to
 * wczesniejsze czujniki trzeba poinstruowac, zeby zignorowaly
 * wykryty obiekt. Taka strategia sterowania pozwala na jednoczesna
 * obsluge wielu obiektow na tasmie.
 */
#define QPS_REFLEX_FIRST 0 // index pierwszego elementu w tabeli QPS_reflex_table
#define QPS_REFLEX_LAST 3 // index ostatniego elementu w tabeli QPS_reflex_table

#define QPS_ALL_OPTOS 0
#define QPS_NO_FIRST_OPTO 1
#define QPS_OPTOS QPS_NO_FIRST_OPTO

#define QPS_REFLEX_FIRST_ARM 0
#define QPS_REFLEX_FIRST_SENSOR QPS_OPTOS  	// index pierwszego elementu w tabeli QPS_reflex_table
#define QPS_REFLEX_LAST 3 		// index ostatniego elementu w tabeli QPS_reflex_table

<<<<<<< HEAD
extern QPSReflex QPS_reflex_table[QPS_REFLEX_LAST+1];
extern QPSInitialAction QPS_small_items[2];
=======
QPSReflex QPS_reflex_table[] = {
	{ 
		.itemType = "GLA", 
		.input = { .device_index = NOAIO_UNINITIALIZED , .bit = NOAIO_UNINITIALIZED },

		.director = &gla_director,
		//.crusher = &pet_crusher,	// UWAGA TO FDO TESTOW
		.prev = NULL
	},
	
	{ 
		.itemType = "PET", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_A },
		
		.director = &pet_director,
		.crusher = &pet_crusher,
#if QPS_OPTOS != QPS_NO_FIRST_OPTO
          	.prev = &QPS_reflex_table[0]
#else
          	.prev = NULL
#endif
    	},

    	{ 
		.itemType = "ALU", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_B },
    	
		.director = &alu_director,
		.crusher = &alu_crusher,
		.prev = &QPS_reflex_table[1]
    	},
        
	{ 
		.itemType = "DEO", 
		.input = { .device_index = 0, .bit = NOAIO_INPUT_C },
          
		.director = NULL,
		.crusher = NULL,
		.prev = &QPS_reflex_table[2]
	}
};

static void QPS_setup_syringe_container(Recyc *recyc, ItemType itemType) {
    QPSRecycData *qps = (QPSRecycData *) recyc->data;
    INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Setting small items director to SYR");
    QPS_WITH_COMMAND_RESPONSE(response, {
            (void) response;
        }, &qps->devices[QPS_DROPBOX_DIRECTOR_DEVICE],
        ENABLE_OUTPUT,
        .channel = NOAIO_output_channel(QPS_DROPBOX_DIRECTOR_OUT));
}

static void QPS_setup_battery_container(Recyc *recyc, ItemType itemType) {
    QPSRecycData *qps = (QPSRecycData *) recyc->data;

    INFO(LOGS_TYPE_MAIN_PROCESS, LOGS_STATUS_NONE, "Setting small items director to BAT");
    
    QPS_WITH_COMMAND_RESPONSE(response, {
            (void) response;
        }, &qps->devices[QPS_DROPBOX_DIRECTOR_DEVICE],
        DISABLE_OUTPUT,
        .channel = NOAIO_output_channel(QPS_DROPBOX_DIRECTOR_OUT));
}

QPSInitialAction QPS_small_items[] = {
    { .itemType = "SYR", .action = QPS_setup_syringe_container },
    { .itemType = "BAT", .action = QPS_setup_battery_container },
};
>>>>>>> test_master

#endif // QPS_DEFINE_REFLEX_TABLE
