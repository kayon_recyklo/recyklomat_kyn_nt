#ifndef PTHREAD_FIFO_H
#define PTHREAD_FIFO_H

#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "pthread-queue.h"

typedef struct _fifo
{
    pthread_queue_t container;
    uint16_t max_size;
    uint16_t size;

    pthread_mutex_t mutex;
} pthread_fifo_t;

void * pthread_fifo_pop_front( pthread_fifo_t * fifo );
void pthread_fifo_push_back( pthread_fifo_t * fifo, void * data );
void pthread_fifo_init( pthread_fifo_t * fifo, uint16_t max_len );

#define PTHREAD_FIFO_FOR_EACH(fifo_ptr, data, action)   \
    pthread_mutex_lock(&((fifo_ptr)->mutex));           \
    pthread_queue_to_begin(&((fifo_ptr)->container));   \
    while( (data=pthread_queue_current_data(&((fifo_ptr)->container))) != NULL ) \
    {   \
        action; \
        pthread_queue_to_next(&((fifo_ptr)->container));        \
    }	\
    pthread_mutex_unlock(&((fifo_ptr)->mutex));           \

#endif // PTHREAD_FIFO_H
