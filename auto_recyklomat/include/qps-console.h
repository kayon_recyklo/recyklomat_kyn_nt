/*
 * qps-console.h
 *
 *  Created on: May 30, 2019
 *      Author: mliszewski
 */

#ifndef INCLUDE_QPS_CONSOLE_H_
#define INCLUDE_QPS_CONSOLE_H_

#include "util.h"
#include "recyc.h"
#include "pthread-queue.h"

typedef struct {
    const char *prefix;
    void (*action)(const char *command, int socket);
    const char *help;
} qps_console_command_t;

#endif /* INCLUDE_QPS_CONSOLE_H_ */
