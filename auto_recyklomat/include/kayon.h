#ifndef KAYON_H
#define KAYON_H

/*
 * Podstawowe typy i makra stosowane w projektach firmy Kayon.
 * Niniejszy naglowek nie powinien miec zbyt wielu zaleznosci
 * (w szczegolnosci nie chcemy tu miec <stdlib.h> ani <stdio.h>).
 *
 * Bardziej rozbudowany zbior utensyliow mozna znalezc w "utils.h".
 */
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#define NELEMS(a) (sizeof(a)/sizeof(a[0]))


#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#define ASSERT_MSG(COND,MSG) typedef char static_assertion_##MSG[(!!(COND))*2-1]
#define ASSERT_LINE_(X,L) ASSERT_MSG(X,static_assertion_at_line_##L)
#define ASSERT_LINE(X,L) ASSERT_LINE_(X,L)
#define ASSERT(X) ASSERT_LINE(X,__LINE__)

#define JUST(x) x

#define SWITCH(str, code) { \
    const char * __str = (const char*)str; \
    code; \
}

#define CASE(str) if(strcmp(str, __str)==0)

#define PACKED __attribute__((packed))
#define INLINE static const inline __attribute__((const, always_inline))

typedef uint8_t byte;




#endif // KAYON_H
