/*
 * qps-talk.h
 *
 *  Created on: May 30, 2019
 *      Author: mliszewski
 */

#ifndef INCLUDE_QPS_TALK_H_
#define INCLUDE_QPS_TALK_H_

#include "util.h"
#include "recyc.h"
#include "pthread-queue.h"

#define THREAD

typedef struct {
	pthread_t thread;
	pthread_queue_t queue;
	int server_socket;
    int client_socket;
    Recyc *recyc;
}QPS_talk_data;

typedef struct {
    const char *prefix;
    void (*action)(const char *command);
    const char *help;
} qps_talk_command_t;

THREAD void *QPS_talk(QPS_talk_data *qps_talk_data);

#endif /* INCLUDE_QPS_CONSOLE_H_ */
