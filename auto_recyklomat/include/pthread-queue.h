#ifndef PTHREAD_QUEUE_H
#define PTHREAD_QUEUE_H

#include <pthread.h>
#include <stdbool.h>
/*
 * Interfejs obslugi wielowatkowosci.
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl>, listopad 2018
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */

typedef struct list_t {
    void *head;
    struct list_t *tail;
} list_t;

typedef struct {
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    list_t *front;
    list_t *back;
    list_t *current;
} pthread_queue_t;

extern int pthread_queue_init(pthread_queue_t *queue);
extern int pthread_queue_destroy(pthread_queue_t *queue);
extern bool pthread_queue_is_empty(pthread_queue_t *queue);
extern bool pthread_queue_has_been_empty(pthread_queue_t *queue, clock_t ms);
extern int pthread_queue_push_back(pthread_queue_t *queue, void *data);
extern void *pthread_queue_pop_front(pthread_queue_t *queue);
void *pthread_queue_remove( pthread_queue_t *queue, void *elem );
void * pthread_queue_current_data( pthread_queue_t *queue );
bool pthread_queue_to_next( pthread_queue_t *queue );
void pthread_queue_to_begin( pthread_queue_t *queue );

#define PTHREAD_QUEUE_FOR_EACH(queue_ptr, data, action) \
    pthread_queue_to_begin((queue_ptr));\
    while( (data=pthread_queue_current_data((queue_ptr))) != NULL )\
        {       \
            action;\
            pthread_queue_to_next((queue_ptr));\
        }

#endif // PTHREAD_QUEUE_H
