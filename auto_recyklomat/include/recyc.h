#ifndef RECYC_H
#define RECYC_H

#include "kayon.h"

/*
 * Interfejs sterownika recyklomatu.
 *
 * Autor: Panicz Maciej Godek <maciej.godek@kayon.pl>
 * Program jest wlasnoscia firmy Kayon. Wszystkie prawa zastrzezone.
 */

#define RECYC_MAX_REGISTERED_TYPES 5


typedef char *ItemType;

#define ItemTypeNone NULL
#define ItemTypePET "PET"
#define ItemTypeALU "ALU"
#define ItemTypeGLA "GLA"
#define ItemTypeDEO "DEO"
#define	ItemTypeBAT "BAT"
#define ItemTypeSYR "SYR"
#define ItemTypeOTH "OTH"
#define ItemTypeEMP "EMP"
#define ItemTypeBAR "BAR"

typedef enum Report {
    ReportOK,
    ReportUnableToLockChamberDoor,
    ReportFailedAssertion,

	ReportRecognizedItem,

	ReportNewUserID,

	ReportDoorsClosed,
	ReportDoorsOpened,

	ReportStatusIdle,
	ReportStatusProcessing,
	ReportStatusError,
  
    ReportUnknownError // Uwaga! Nigdy nie nalezy wywolywac funkcji
    // supervisor_report z argumentem ReportUnknownError. Jezeli chcesz
    // zglosic jakikolwiek blad, po prostu dodaj odpowiednia wartosc
    // do tego enuma (i w miare mozliwosci obsluz po stronie supervisora).
} Report;

typedef struct Supervisor {
    void (*report)(struct Supervisor *self, Report condition, ...);
    void *data;
} Supervisor;

typedef struct Recyc {
    void (*lock)(struct Recyc *self);
    bool (*isClosed)(struct Recyc *self);
    void (*unlock)(struct Recyc *self);
    void (*onClosed)(struct Recyc *self, void (*callback)(struct Recyc *self));
    void (*recognizeItem)(struct Recyc *self);
    void (*takeOver)(struct Recyc *self);
    /**
     * `processItem` powinno docelowo powodowac wywolanie `unlock`
     */
    void (*processItem)(struct Recyc *self, ItemType item);

    // manipulowanie tasmociagiem przez okreslony czas
    void (*conveyorStop)(struct Recyc* self, int ms);
    void (*conveyorForward)(struct Recyc* self, int ms);
    void (*conveyorBackward)(struct Recyc* self, int ms);

    Supervisor *supervisor;
    void *data;
} Recyc;


extern Supervisor *ignoramus;

/*
 * Pierwszy parametr do Recyc_init jest stringiem postaci:
 *
 * "nazwa_sterownika parametr1=wartosc1 parametr2=wartosc2 ..."
 * 
 * Nazwy i znaczenie poszczegolnych parametrow zaleza
 * od konkretnego sterownika (sterowniki powinny znajdowac sie
 * w plikach o nazwach postaci `recyc-*.c`).
 *
 * Prawdopodobnie niezbedne bedzie przestudiowanie zrodel,
 * poniewaz - z uwagi na dynamike rozwoju - nie ma na razie
 * co liczyc na inna dokumentacje.
 * 
 */
extern Recyc *Recyc_init(const char *args, Supervisor *supervisor);

extern bool Recyc_register(const char *type,
                           Recyc *(*constructor)(const char *args));

extern double QPS_get_process_min_length(char* recognized_item);

#endif // RECYC_H
