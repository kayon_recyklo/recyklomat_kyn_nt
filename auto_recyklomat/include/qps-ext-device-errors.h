#ifndef ERRORS_H
#define ERRORS_H

#include "util.h"
#include "recyc.h"
#include "pthread-queue.h"
#include <stdint.h>

#define ERROR_SOCKET_PATH "/tmp/errors"

#define THREAD

typedef struct {
	pthread_t thread;
	pthread_queue_t queue;
} errors_data_t;

/*
typedef stuct
{
	uint8_t dupa;
} errors_info_t;
*/
THREAD void * error_serwer( errors_data_t * data );

typedef uint8_t errors_info_t;
#endif
