# Drukarka_recyklo
test drukarki:
wymaga uprawnień roota do dostępu do urządzenia
wywołany bez parametrów zwraca aktualny status drukarki, lub błędy komunikacji, jeśli występują

 Błędy drukarki:

pnf - nie znaleziono drukarki
we - błąd zapisu
re - błąd odczytu
wc - błędna lista argumentów wywołania
pto - Printhead Thermistor Open
ifc - Invalid Firmware Config
pde - Printhead Detection Error
bpe - Bad Printhead Element
mot - Motor Over Temperature
pot - Printhead Over Temperature
cf - Cutter Fault
ho - Head Open
ro - Ribbon Out
mo - Media Out
cppf - Clear Paper Path Failed
pfe - Paper Feed Error
pnr - Presenter Not Running
pjdr - Paper Jam during Retract
bmnf - Black Mark not Found
bmce - Black Mark Calibrate Error
rfto - Retract Function timed out
p - Paused

 Warningi drukarki	

pnes - Paper-near-end Sensor
rp - Replace Printhead
cp - Clean Printhead
ntcm - Need to Calibrate Media
s1 - Sensor 1 (Paper before head)
s2 - Sensor 2 (Black mark)
s3 - Sensor 3 (Paper after head)
s4 - Sensor 4 (loop ready)
s5 - Sensor 5 (presenter)
s6 - Sensor 6 (retract ready)
s7 - Sensor 7 (in retract)
s8 - Sensor 8 (at bin)

 info drukarki

drs - odpowiedź na nakaz resetu

wszystkie informacje są zwracane na socket w formacie zgodnym ze standardowym protokołem
po wywołaniu testu z argumentem reset(sudo ./test reset) jest wykonywany reset drukarki
