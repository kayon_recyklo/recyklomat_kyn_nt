#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>

#include "protocol.h"
#include "sockets.h"

#define PRINTER_PATH "/dev/usb/lp0"

#define OPEN_DEVICE_ERROR 	"pnf"
#define WRITE_ERROR 		"we"
#define READ_ERROR		"re"
#define CMD_ERROR		"wc"
#define DEVICE_RESET		"drs"

#define GET_STATUS "~HQES"
#define RESET "~JR"

#define ASSERT_RETVAL(val, err) if(val<0)send_err(err)

#define ERROR_SOCKET "/tmp/errors"

report_t report;

void result( char * res )
{
	int fd = init_socket("sender");
	socket_client_conn(fd, ERROR_SOCKET, "sender");

	blocking_write_to_socket(fd, res, strlen(res));
	close_socket_conn(fd, "sender");

        printf("%s", res);

	exit(0);
}

void send_err( char * err )
{
	report_add(&report, ADD_ERROR, err);
	report_send(&report);
}

void send_info( char * err )
{
	report_add(&report, ADD_INFO, err);
	report_send(&report);
}

int open_printer( void )
{
	int fd = open( PRINTER_PATH, O_RDWR );

	if( fd < 0 )
	{
		send_err( OPEN_DEVICE_ERROR );
	}

	return fd;
}

void printer_send_cmd( int fd, char * cmd )
{
	ssize_t bytes = write( fd, cmd, strlen(cmd) );
	ASSERT_RETVAL(bytes, WRITE_ERROR);
}

void printer_get_response( int fd, char * res, uint16_t exp_size )
{
	ssize_t bytes = read( fd, res, 200 );
	if( bytes != exp_size ) send_err(READ_ERROR);
}

char * after_token( char * str, char * tok )
{
	int len = strlen(tok);

	char * retval = strstr(str, tok);

	if( retval == NULL ) return NULL;

	return retval + len;
}

uint8_t is_blank( char c )
{
	return ( (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
	 || (c >= '0' && c <= '9') );
}

char * remove_blank( char * str )
{
	while( *str != '0' && *str != '1'  ) str++;

	return str;
}

void to_numbers( char * str )
{
	while( *str ) (*str++) -= '0';
}

void check_err_flag( char byte, uint8_t flag, char * err )
{
	if( byte & flag ) report_add(&report, ADD_ERROR, err);
}

void check_warn_flag( char byte, uint8_t flag, char * warn )
{
	if( byte & flag ) report_add(&report, ADD_WARNING, warn);
}

void resolve_error( char * err_1, char * err_2 )
{
	to_numbers(err_2);

	check_err_flag(err_2[5], 0x02, "pnf");
	check_err_flag(err_2[5], 0x01, "ifc");
	check_err_flag(err_2[6], 0x08, "pde");
	check_err_flag(err_2[6], 0x04, "bpe");
	check_err_flag(err_2[6], 0x02, "mot");
	check_err_flag(err_2[6], 0x01, "pot");
	check_err_flag(err_2[7], 0x08, "cf");
	check_err_flag(err_2[7], 0x04, "ho");
	check_err_flag(err_2[7], 0x02, "ro");
	check_err_flag(err_2[7], 0x01, "mo");

	check_err_flag(err_2[4], 0x08, "cppf");
	check_err_flag(err_2[4], 0x04, "pfe");
	check_err_flag(err_2[4], 0x02, "pnr");
	check_err_flag(err_2[4], 0x01, "pjdr");
	check_err_flag(err_2[3], 0x08, "bmnf");
	check_err_flag(err_2[3], 0x04, "bmce");
	check_err_flag(err_2[3], 0x02, "rfto");
	check_err_flag(err_2[3], 0x01, "p");
}

void resolve_warning( char * warn_1, char * warn_2 )
{
	to_numbers(warn_2);

	check_warn_flag(warn_2[7], 0x08, "pnes");
	check_warn_flag(warn_2[7], 0x04, "rp");
	check_warn_flag(warn_2[7], 0x02, "cp");
	check_warn_flag(warn_2[7], 0x01, "ntcm");
	check_warn_flag(warn_2[6], 0x01, "s1");
	check_warn_flag(warn_2[6], 0x02, "s2");
	check_warn_flag(warn_2[6], 0x04, "s3");
	check_warn_flag(warn_2[6], 0x08, "s4");
	check_warn_flag(warn_2[5], 0x01, "s5");
	check_warn_flag(warn_2[5], 0x02, "s6");
	check_warn_flag(warn_2[5], 0x04, "s7");
	check_warn_flag(warn_2[5], 0x08, "s8");
}

void printer_process_response( char * res )
{
	char * err = after_token(res, "ERRORS:");
	char * warn = after_token(res, "WARNINGS:");

	err = remove_blank(err);
	warn = remove_blank(warn);

	char err_flag, warn_flag;
       	char err_1[10], err_2[10], warn_1[10], warn_2[10];

	sscanf(err, "%c%s%s", &err_flag, err_1, err_2);
	sscanf(warn, "%c%s%s", &warn_flag, warn_1, warn_2);

	if( err_flag == '1' )
		resolve_error(err_1, err_2);

	if( warn_flag == '1' )
		resolve_warning(warn_1, warn_2);

	report_send(&report);
}
	

int main( int argc, char ** argv )
{
	report_init(&report, "printer", result); 

	int fd = open_printer();

	if( argc == 2 )
	{
		if( strcmp(argv[1], "reset") == 0 )
		{
			printer_send_cmd(fd, RESET);
			send_info(DEVICE_RESET);
		}
		else
			send_err(CMD_ERROR);
	}
	else
	if( argc > 2 ) 
		send_err(CMD_ERROR);

	printer_send_cmd(fd, GET_STATUS);

	char buf[200];
	printer_get_response(fd, buf, 132);

	printer_process_response(buf);

	close(fd);
	return 0;
}
