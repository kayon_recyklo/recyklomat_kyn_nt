#define MAX_BUFFER_SIZE 128

#define ADD_ERROR 0
#define ADD_WARNING 1
#define ADD_INFO 2

typedef void(*report_sender_t)(char*);
typedef void(*report_line_processor_t)(uint8_t, char*);

typedef struct _write_buf
{
	char data[MAX_BUFFER_SIZE];
	char * write_ptr;
} write_buffer_t;

typedef struct _report
{
	write_buffer_t err_buf;
	write_buffer_t warn_buf;	
	write_buffer_t info_buf;
	
	report_sender_t sender;
	report_line_processor_t reciver;

	char * dev_name;
} report_t;

void report_add( report_t * rep, uint8_t type, char * str );
void report_init( report_t * rep, char * dev_name, report_sender_t sender );
void report_send( report_t * rep );
