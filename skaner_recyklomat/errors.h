#ifndef ERRORS_H
#define ERRORS_H

#define ERROR_TIMEOUT 	"to"
#define ERROR_ACK 	"ato"
#define ERROR_WRITE	"we"
#define ERROR_OPEN	"oe"
#define ERROR_COM	"cp"
#define ERROR_NONE	"ok"
#define ERROR_ARG	"ae"

#endif

