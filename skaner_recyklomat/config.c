#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <time.h>

#include "protocol.h"
#include "sockets.h"
#include "errors.h"

#define USE_BEEP_PARAM 1

typedef enum _trigger_mode	{LEVEL=0, PRESENTATION=7, HOST=8, SOFTWARE=15} trigger_mode;
typedef enum _beeper_mode	{AUTO=1, MANUAL=0} beeper_mode; // odpowiada opcjom "Beep After Good Decode" i "Do Not Beep After Good Decode"
typedef enum _aim_mode		{AIM_OFF=0, AIM_ON=2} aim_mode;
typedef enum _illumination_mode {LIGHT_OFF=0, LIGHT_ON=1} illumination_mode;
typedef enum _parameter_mode	{PARAM_OFF=0, PARAM_ON=1} parameter_mode;

typedef struct _parametry
{
	char * urzadzenie;
	uint8_t illumination_bright;
	uint8_t aim_bright;
	illumination_mode  illumination_enable;
	aim_mode  aim_enable;

	trigger_mode trig;
	beeper_mode beep;
} parametry_t;

report_t report;

#define ILLUMINATION 		298
#define AIM			306

// 0 -10
#define AIM_BRIGHT		668

// 0 - 30
#define ILLUMINATION_BRIGHT	669
#define CONFIG_BEEPER		56
#define CONFIG_TRIGGER		138
#define PARAMETER_SCANNING	236


#if USE_BEEP_PARAM
#define BEEP 0x00
#else
#define BEEP 0xFF
#endif

#include "common.h"

void skaner_set_param( int fd, uint16_t param, uint8_t value )
{
	// komenda, nadawca, flagi(zmiana stała), czy brzęczyć
	uint8_t dane[8] = {0xC6, 0x04, 0x08, BEEP};
	
	uint8_t * ptr = &dane[4];	
#define ADD_TO_BUF(x) *ptr++=x

	switch(param)
	{
	case 0 ... 239:
		ADD_TO_BUF(param);
		break;
	case 256 ... 495:
		ADD_TO_BUF(0xF0);
		ADD_TO_BUF(param - 256);
		break;
	case 512 ... 751:
		ADD_TO_BUF(0xF1);
		ADD_TO_BUF(param - 512);
		break;
	case 768 ... 1007:
		ADD_TO_BUF(0xF2);
		ADD_TO_BUF(param - 768);
		break;
	default:
		ADD_TO_BUF(0xF8);
		ADD_TO_BUF((param >> 8) & 0xFF);
		ADD_TO_BUF(param & 0xFF);
	}

	ADD_TO_BUF(value);

	SSI_transmit(fd, dane, ptr-dane);
}

int main( int argc, char ** argv  )
{
	report_init(&report, "scanner-config", sender);

	if( argc != 2 )
	{
		send_err(ERROR_ARG);	
	}

	int fd = open(argv[1], O_RDWR | O_NONBLOCK);

	if( fd < 0  )
	{
		send_err(ERROR_OPEN);
	}
	else
	{
//		 printf("Urzadzenie otwarte\n");
		ustaw_port(fd, 9600);

		skaner_set_param(fd, CONFIG_BEEPER, MANUAL);
		skaner_set_param(fd, CONFIG_TRIGGER, LEVEL);
		skaner_set_param(fd, AIM, AIM_ON);
		skaner_set_param(fd, ILLUMINATION, LIGHT_OFF);
		skaner_set_param(fd, AIM_BRIGHT, 0);
		skaner_set_param(fd, PARAMETER_SCANNING, PARAM_OFF);

		send_info(ERROR_NONE);
	}

	close(fd);

	return 0;
}
