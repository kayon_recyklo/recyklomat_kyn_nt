#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <time.h>

#include "sockets.h"
#include "protocol.h"
#include "errors.h"

#define USE_BEEP_PARAM 1

typedef struct _parametry
{
	uint16_t timeout;
	uint16_t skan_odstep;
	char * urzadzenie;
} parametry_t;

typedef enum _trigger_mode	{LEVEL=0, PRESENTATION=7, HOST=8, SOFTWARE=15} trigger_mode;
typedef enum _beeper_mode	{AUTO=1, MANUAL=0} beeper_mode; // odpowiada opcjom "Beep After Good Decode" i "Do Not Beep After Good Decode"
typedef enum _aim_mode		{AIM_OFF=0, AIM_ON=2} aim_mode;
typedef enum _illumination_mode {LIGHT_OFF=0, LIGHT_ON=1} illumination_mode;

#define ILLUMINATION 		298
#define AIM			306
#define AIM_BRIGHT		668
#define ILLUMINATION_BRIGHT	669
#define CONFIG_BEEPER		56
#define CONFIG_TRIGGER		138

#if USE_BEEP_PARAM
#define BEEP 0x00
#else
#define BEEP 0xFF
#endif

time_t scan_time, ack_time;
report_t report;

#include "common.h"

void skaner_set_param( int fd, uint16_t param, uint8_t value )
{
	uint8_t dane[8] = {0xC6, 0x04, 0x00, BEEP};
	
	uint8_t * ptr = &dane[4];	
#define ADD_TO_BUF(x) *ptr++=x

	switch(param)
	{
	case 0 ... 239:
		ADD_TO_BUF(param);
		break;
	case 256 ... 495:
		ADD_TO_BUF(0xF0);
		ADD_TO_BUF(param - 256);
		break;
	case 512 ... 751:
		ADD_TO_BUF(0xF1);
		ADD_TO_BUF(param - 512);
		break;
	case 768 ... 1007:
		ADD_TO_BUF(0xF2);
		ADD_TO_BUF(param - 768);
		break;
	default:
		ADD_TO_BUF(0xF8);
		ADD_TO_BUF((param >> 8) & 0xFF);
		ADD_TO_BUF(param & 0xFF);
	}

	ADD_TO_BUF(value);

	SSI_transmit(fd, dane, ptr-dane);
}

void brzeczyk( int fd )
{
	uint8_t dane[] = {0xE6, 0x04, 0x00, 0x14};

	SSI_transmit(fd, dane, 4);
}

void skanuj( int fd )
{
	uint8_t dane[] = {0xE4, 0x04, 0x00};

	time(&scan_time);

	if( ack_time == 0 ) time(&ack_time);

//	printf("scan\n");
	SSI_transmit(fd, dane, 3);
}

uint8_t odbierz_bajt( char bajt, char * cel )
{
        static uint8_t ile_opuscic=0;
	

	if( ile_opuscic ) 
	{
		ile_opuscic--;
		return 0;
	}

	// ACK	
	if( bajt < ' ' ) 
	{
		time(&ack_time);
//		printf("ack");

		ile_opuscic = bajt+1;
		return 0;
	} 

	*cel = bajt;

	return 1;
}

void sprawdz_ack_timeout( void )
{
	if( difftime(scan_time, ack_time) > 3 )
	{
		send_err(ERROR_ACK);
	}
}

void skaner_proces( int fd, parametry_t * parametry )
{
	static char kod[1024];
	static int16_t skan_licznik=0, znak_idx=0;
	
	char znak;

	sprawdz_ack_timeout();
	
		if( read(fd, &znak, 1) != -1 ) // odczyt jednego bajtu
		{
			znak_idx += odbierz_bajt(znak, &kod[znak_idx]);

			usleep(1000);
			skan_licznik++;
		}
		else
		{
			if( znak_idx  ) // koniec ramki
			{
				if(  skan_licznik >= 0 && skan_licznik <= parametry->skan_odstep ) // omiń zdublowane skany
				{	
					skan_licznik = -5*parametry->skan_odstep;
					brzeczyk(fd);

					close(fd);
					
					send_info(kod);
				}

				memset(kod, 0, sizeof(kod));
				znak_idx = 0;
			
			}

			usleep(20*1000L);
			skan_licznik += 20;
			
			if( skan_licznik > parametry->skan_odstep )
			{
				skan_licznik = 0;
				skanuj(fd);
			}
		}
}

void pokaz_pomoc( void )
{
	printf("arg1 - sciezka do urzadzenia\narg2 - odstep miedzy skanami w ms\narg3 - timeout skanowania w s\n");
}

uint8_t parsuj_parametry( int argc, char ** argv, parametry_t * parametry )
{
	if( argc != 4 ) 
	{
		pokaz_pomoc();
		send_err(ERROR_ARG);
	}
	
	parametry->urzadzenie = argv[1];
	parametry->skan_odstep = atoi(argv[2]);
	parametry->timeout = atoi(argv[3]);

	return 0;
}

int main( int argc, char ** argv  )
{
	time_t start_time;

	report_init(&report, "scanner-scan", sender);

        time(&start_time);
	parametry_t parametry;
	if( parsuj_parametry(argc, argv, &parametry) ) return -1;
	
	int fd = open(parametry.urzadzenie, O_RDWR | O_NONBLOCK);

	if( fd < 0  )
	{
		send_err(ERROR_OPEN);
	}
	else
	{
		//if(debug) printf("Urzadzenie otwarte\n");
		ustaw_port(fd, 9600);

	}

	while(1)
	{
		time_t actual_time;
	        time(&actual_time);
		double diff_time  = difftime(actual_time, start_time);
		
		if(diff_time >= parametry.timeout) 
		{
			close(fd);
			send_err(ERROR_TIMEOUT);	
		}

		skaner_proces(fd, &parametry);
	}

	return 0;
}
