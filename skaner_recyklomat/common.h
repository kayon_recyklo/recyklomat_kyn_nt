#ifndef COMMON_H
#define COMMON_H

#define ERROR_SOCKET "/tmp/errors"

void sender( char * res )
{
	int fd = init_socket("sender");
	socket_client_conn(fd, ERROR_SOCKET, "sender");

	blocking_write_to_socket(fd, res, strlen(res));
	close_socket_conn(fd, "sender");

#ifdef SCANNER_TEST
        printf("%s", res);
#endif

	exit(0);
}	

void send_err( char * err )
{
	report_add(&report, ADD_ERROR, err);
	report_send(&report); 
}

void send_info( char * info )
{
	report_add(&report, ADD_INFO, info);
	report_send(&report);
}

void ustaw_port(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0)
    {
	send_err(ERROR_COM);
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* Simple Serial Interface Programmer's Guideno hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) 
    {
	    send_err(ERROR_COM);
    }
}

void SSI_transmit( int fd, uint8_t * dane, uint8_t len )
{
	uint8_t buf[100];
	buf[0] = len+1;

	uint8_t * buf_ptr = &buf[1];
	uint16_t crc= -(len+1);

	for( uint8_t i=0; i<len; i++ )
	{
		crc -= dane[i];
		*(buf_ptr++) = dane[i];
	}
	
	*(buf_ptr++) = (crc >> 8)&0xff;
	*(buf_ptr) = crc & 0xff;

	int retval = write(fd, buf, len+3);

	if( retval == -1 )
	{
		send_err(ERROR_WRITE);
	}
}

#endif

