config:
Ustawia docelową konfigurację, poza SSI CDC, które trzeba ustawić skanując kod

Argumenty: ścieżka do urządzenia

scanner:
Odpowiada za skanowanie

Argumenty: ścieżka do urządzenia, odstęp między skanami[ms] i timeout skanowania[s]

test:
Sprawdza, czy skaner odpowiada

Argumenty: ścieżka do urządzenia

Zgłaszane komunikaty:

 Błędy skanera

to - timeout skanowania
ato - timeout ACK
we - bład zapisu
oe - błąd otwarcia urządzenia
cp - błąd konfiguracji portu COM
ae - błędna lista parametrów

 info skanera

ok - wszystko działa

WSZYSTKIE 3 WYMAGAJĄ UPRAWNIEŃ ROOTA!
