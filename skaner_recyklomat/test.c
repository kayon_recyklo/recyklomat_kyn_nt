#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <time.h>

#include "sockets.h"
#include "protocol.h"
#include "errors.h"

typedef struct _parametry
{
	uint16_t skan_odstep;
	char * urzadzenie;
} parametry_t;

time_t scan_time, ack_time;
report_t report;

#define SCANNER_TEST
#include "common.h"

void test( int fd )
{
	uint8_t dane[] = {0xC4, 0x04, 0x00};

	time(&scan_time);

	if( ack_time == 0 ) time(&ack_time);

	SSI_transmit(fd, dane, 3);
}

uint8_t odbierz_bajt( char bajt, char * cel )
{
	if( bajt < ' ' ) 
	{
		send_info(ERROR_NONE);
	} 

	*cel = bajt;

	return 1;
}

void sprawdz_ack_timeout( void )
{
	if( difftime(scan_time, ack_time) > 2 )
	{
		send_err(ERROR_ACK);
	}
}

void skaner_proces( int fd )
{
	static int16_t skan_licznik=0, znak_idx=0;
	
	char znak;

	sprawdz_ack_timeout();
	
	if( read(fd, &znak, 1) != -1 ) // odczyt jednego bajtu
	{
		znak_idx += odbierz_bajt(znak, &znak);

		usleep(1000);
		skan_licznik++;
	}
	else
	{
		usleep(20*1000L);
		skan_licznik += 20;
			
		if( skan_licznik > 200 )
		{
			skan_licznik = 0;
			test(fd);
		}
	}
}

int main( int argc, char ** argv  )
{
	if( argc != 2 ) return -1;

	time_t start_time;
        time(&start_time);

	report_init(&report, "scanner-test", sender); 
	
	int fd = open(argv[1], O_RDWR | O_NONBLOCK);

	if( fd < 0  )
	{
		send_err(ERROR_OPEN);
	}
	else
	{
		//if(debug) printf("Urzadzenie otwarte\n");
		ustaw_port(fd, 9600);

	}

	while(1)
	{
		time_t actual_time;
	        time(&actual_time);
		double diff_time  = difftime(actual_time, start_time);
		
		if(diff_time >= 5) 
		{
			send_err(ERROR_TIMEOUT);
		}
		skaner_proces(fd);
	}

	return 0;
}
