#!/bin/bash

server_ip="10.7.0.1"
inet_ip="8.8.8.8"
ping_count=1
logs_path="/home/pi/ml_auto_recyklomat/auto_recyklomat/logs"           
limit_temp=80 #80 st.
prec_limit_temp=$limit_temp*1000

logfile=$(date +"%Y-%m-%d.log")

hostname=$(hostname)
detailed_logpath="/home/pi/service_logs/logs"
detailed_logfile=$(date +"${hostname}_det_%Y-%m-%d_%H-%M.log")
logs="${logs_path}/${logfile}"
detailed_log="${detailed_logpath}/${detailed_logfile}"
hour=$(date +"%H")

if [[ -f "$logs" ]]; then
    logs_exist=true
else
    logs_exist=false
fi

dev_name=(  "main usb hub" 
            "ethernet hub"
            "ftdi hub"
            "gsm"
            "touchscreen"
            "scanner QR"
            "camera"
            "printer" 
            "monitor"
            "internet"
            "server connection"
            "temperature < $limit_temp deg"
            "recognizer"
            "GLA director"
	    "PET director"
	    "ALU director"
	    "current limits"
	    "PET crusher current"
	    "ALU crusher current"
            "PET crusher position"
	    "ALU crusher position"
            "PET detector"
	    "ALU detector"
	    "DEO detector"
	    "conveyor" )

dev_cmd=(   "check_main_usb_hub"
            "check_ethernet_hub"
            "check_ftdi_hub" 
            "check_gsm"
            "check_touchscreen"
            "check_scanner_QR"
            "check_camera"
            "check_printer"
            "check_monitor"
            "check_inet"
            "check_server"
            "check_temp"
            "check_recognizer"
            "check_GLA_director"
	    "check_PET_director"
	    "check_ALU_director"
	    "check_limits"
            "check_PET_crusher_current"
	    "check_ALU_crusher_current"
	    "check_PET_crusher_position"
	    "check_ALU_crusher_position"
	    "check_detector_PET"
	    "check_detector_ALU"
	    "check_detector_DEO"
	    "check_conveyor" )

check_main_usb_hub() {
    if lsusb -d 0424:9514 &>/dev/null ; then
    	echo "no errors"
    fi
} 

check_ethernet_hub() {
    if lsusb -d 0424:ec00 &>/dev/null ; then
	echo "no errors"
    fi
}
 
check_ftdi_hub() {
    if lsusb -d 0403:6011 &>/dev/null ; then
	echo "no errors"
    fi
}
 
check_gsm() {
    if lsusb -d 413c:81a3 &>/dev/null ; then
	echo "no errors"
    fi
}

check_touchscreen() {
    if lsusb -d 6615:0001 &>/dev/null ; then
	echo "no errors"
    fi
}

check_scanner_QR() {
    if lsusb -d 05e0:1701 &>/dev/null ; then
	echo "no errors"
    fi
}

check_camera() {
    if lsusb -d 05a3:8830 &>/dev/null ; then
	echo "no errors"
    fi
}

check_printer() {
    if lsusb -d 0a5f:00b1 &>/dev/null ; then
	echo "no errors"
    fi
}	

check_monitor() {
    if tvservice -s | grep -q "state 0xa" ; then
        echo "no errors"
    fi
}

check_inet() {
    if ping -q -c $ping_count -W 5 $inet_ip &>/dev/null; then
        echo "no errors"
    fi
}

check_server() {
    if ping -q -c $ping_count -W 5 $server_ip &>/dev/null; then
        echo "no errors"
    fi
}

check_temp() {
    temp=$(cat /sys/class/thermal/thermal_zone0/temp)
    if (( $temp < $prec_limit_temp )); then
        echo "no errors"
    fi
}

check_recognizer() {
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ps aux | grep "[r]ecognition.py" &>/dev/null ; then
        echo "no errors"
    fi
}

check_GLA_director() {
    error_cmd="GLA director repair"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}


check_PET_director() {
    error_cmd="PET director repair"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}


check_ALU_director() {
    error_cmd="ALU director repair"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_limits() {
    error_cmd="Overcurrent"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_PET_crusher_current() {
    error_cmd="OVERCURRENT"
    error_cmd_2="PET"

    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs | grep -qa $error_cmd_2 ; then
        echo "no errors"
    fi
}

check_ALU_crusher_current() {
    error_cmd="OVERCURRENT"
    error_cmd_2="ALU"

    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs | grep -qa $error_cmd_2 ; then
        echo "no errors"
    fi
}

check_PET_crusher_position() {
    error_cmd="PET crusher"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_ALU_crusher_position() {
    error_cmd="ALU crusher"

    if [ ! -f "$logs" ]; then
    	echo "no file"
    	return
    fi

    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
    	echo "no errors"
    fi
}

check_detector_PET() {
    error_cmd="Opto PET"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_detector_ALU() {
    error_cmd="Opto ALU"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_detector_DEO() {
    error_cmd="Opto DEO"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}

check_conveyor() {
    error_cmd="Conveyor overcurrent"
     
    if [ ! -f "$logs" ]; then
    	echo "no file"
	return
    fi
    
    if ! grep -qa "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\].*ERROR.*$error_cmd" $logs ; then
        echo "no errors"
    fi
}
#tput clear

cmd_returns_anything () {
    ret=$($@)
    if [[ "no errors" == "$ret" ]] ; then
	echo "no_err"
    elif [[ "no file" == "$ret" ]] ; then
	echo "no_file"
    else
	echo "err"
    fi
}

echo_logs_datetime () {
    printf "%s" "$(date +"%Y-%m-%d [%T] ")"
}

echo_FAILED () {
    printf "[\e[91m%s\e[39m]\n" "FAILED"
}

echo_OK () {
    printf "[\e[92m%s\e[39m]\n" "  OK  "
}

echo_NO_FILE () {
    printf "[\e[93m%s\e[39m]\n" "NO LOG"

}
print_line() {
    for ((i=1;i<=80;i++))
    do
        printf "="
    done
    printf "\n"
}
##tvservice -n -s -M

print_line
printf "DEVICES STATUS $(date +"%Y-%m-%d %H:%M"):\n"
print_line

for i in ${!dev_name[@]}; do
    echo_logs_datetime
    printf "Checking %*s" -40 "${dev_name[$i]}"
    ret=$(cmd_returns_anything ${dev_cmd[$i]})
    if [[ $ret == "no_err" ]]; then 
        echo_OK
    elif [[ $ret == "no_file" ]]; then
    	echo_NO_FILE
    else
        echo_FAILED
    fi
done    

print_line
printf "If there were errors, detailed logs can be found in:\n${detailed_logfile}\n"
print_line

grep "\[$hour:[[:digit:]]\{2\}:[[:digit:]]\{2\}\]" $logs | grep -E "ERROR" &> $detailed_log || rm $detailed_log

