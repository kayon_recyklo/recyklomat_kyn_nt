#!/bin/bash
server_ip="10.7.0.1"
serv_user="service"
slogs_path="/home/pi/service_logs/logs/"

if [ ! -d "$slogs_path" ]; then
	mkdir -p "$slogs_path"
fi

slogs_srv_path="/home/service/logs/"
recyklomat_name=$(hostname)
data=$(date +"_%Y-%m-%d_%H-%M")
init="_init"
gen_logs_script= $(/home/pi/service_logs/service.sh &> /home/pi/service_logs/logs/$recyklomat_name$data$init)

while [ "$(ls -A $slogs_path | grep -v '\.gz')" ]; do
    file_name=$(ls -A $slogs_path | grep -v '\.gz' | head -n1 | cut -d " " -f1)
    gzip $slogs_path/$file_name
done

while [ "$(ls -A $slogs_path | grep '.gz')" ]; do
    file_name=$(ls -A $slogs_path | grep '.gz' | head -n1 | cut -d " " -f1)
    if scp -q -o ConnectTimeout=5 "$slogs_path/$file_name" "$serv_user@$server_ip:$slogs_srv_path" ; #&> /dev/null ;
    then
        rm "$slogs_path$file_name"
    fi
done
