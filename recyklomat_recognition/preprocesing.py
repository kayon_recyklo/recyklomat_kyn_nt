import cv2
import numpy as np
def process_image(img):
    img=cv2.resize(img,(32,32))
    img=img.astype(np.uint8)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    hsv[:,:,2] = cv2.equalizeHist(hsv[:,:,2])
    img = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    lap = cv2.Laplacian(yuv[:,:,0],cv2.CV_64F)
    lap = lap.reshape(-1,lap.shape[1],1)
    lap = lap/300.0
    img =  img/255.0-0.5

    img=np.concatenate((img,lap),axis=2)
    #img = lap
    #sobelx = np.absolute(cv2.Sobel(yuv[:,:,2], cv2.CV_64F, 1, 0,ksize=5))
    #
    return img
