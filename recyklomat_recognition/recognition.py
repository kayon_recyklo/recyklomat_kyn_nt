#!/usr/bin/python
import sys
import os
import logging
import socket
import json
import glob
import cv2
import numpy as np
from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array
import operator
from operator import itemgetter
from preprocesing import process_image
from time import time
import threading
from pyzbar.pyzbar import decode
from pyzbar.pyzbar import ZBarSymbol as zbs

#TODO: handle exceptions
with open(sys.argv[1]) as cfg_file:
	cfg = json.load(cfg_file)

socket_address = cfg['system']['socket']
log_filename = cfg['system']['log']
photo_dir = cfg['system']['photo_dir']
model_json = cfg['model']['config']
model_weights = cfg['model']['weights']
threshold = cfg['model']['threshold']
camera_dev = cfg['video']['device']
img_width = cfg['video']['width']
img_height = cfg['video']['height']
video_buf = cfg['video']['buffer']
roi_width = cfg['roi']['width']
roi_height = cfg['roi']['height']
roi_x0 = cfg['roi']['x']
roi_y0 = cfg['roi']['y']
with open(cfg['codes']) as db_file:
	code_db = json.load(db_file)

model = None
classes = None
classes_sq = ['OTH', 'ALU', 'GLA', 'PET']
video = None
sock = None
override = False
codes = []

def initLog(filename):
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    fh = logging.FileHandler(filename)
    fh.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
    root.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    root.addHandler(ch)

def initCamera(dev=0):
    video = cv2.VideoCapture(dev)
    video.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, img_width)
    video.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, img_height)
    #video.set()
    #set exposure?
    return video

def initModel():
    with open(model_json, 'r') as jfile:
        data = jfile.read()
        model = model_from_json(data)
        classes = json.loads(data)['output_cl']
    classes[0] = "OTH"
    model.compile("adam", "mse")
    model.load_weights(model_weights)
    return model, classes

def initSocket():
    try:
         os.unlink(socket_address)
    except OSError:
        if os.path.exists(socket_address):
             raise
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(socket_address)
    sock.listen(1)
    return sock

def log(message):
    logging.debug(message)
    print message

def takePicture():
    for i in range(video_buf): retval, mat = video.read() #4 frames in buffer?
    if mat.shape[1] == img_width and mat.shape[0] == img_height:
        output = mat[roi_y0:roi_y0+roi_height, roi_x0:roi_x0+roi_width, :]
    return output

def savePicture(mat):
    timestamp = time()
    photo_path = photo_dir + "/recyklomat-" + str(timestamp) + ".jpg"
    cv2.imwrite(photo_path, mat)
    return photo_path

def squeeze(results):
    #classes: ["OTH", "ALU-b", "ALU-g", "ALU-r", "ALU-w", "GLA", "PET-b", "PET-g", "PET-r", "PET-w", "PET-x"]
    #metaclasses: ["OTH", "ALU", "GLA", "PET"]
    meta={"OTH":0,"ALU":0,"GLA":0,"PET":0}
    for i in range(len(classes)):
      meta[classes[i][0:3]]+=results[i]
    return [meta['OTH'],meta['ALU'],meta['GLA'],meta['PET']]

def predict(mat):
    tstart = time()
    img = process_image(mat)
    timg = img.reshape(( 1,
       img.shape[0],
       img.shape[1],
       img.shape[2]))
    results = model.predict(timg, batch_size=1)
    results = results[0].tolist()
    result_str=""
    for i,val in enumerate(results):
      if val>0.01: result_str += "\n%.2f %s"%(val,classes[i])
    #log("Results (raw): "+str(results))
    log("Recognition done (%.4fs), result: %s"%(time()-tstart,result_str))
    results = squeeze(results)
    #results: list of probabilities for classes
    #logging.debug("results: " + str(results))
    result_best = max(enumerate(results), key=itemgetter(1))[0]
    result_val = results[result_best]
    if result_val > threshold:
       result_class = classes_sq[result_best]
    else:
       result_class = classes_sq[0] #OTH
    return result_class, results

def readcodes(img):
    codes = []
    tstart = time()
    d = decode(img,symbols=[zbs.EAN8, zbs.EAN13])
    for dd in d: codes.append(dd.data)
    log("Readcodes done (%.4fs), result: %s"%(time()-tstart,str(codes)))
    #log("Readcodes result: "+str(codes))

#to be launched in separate thread
def recognition(response_socket):
    pic = takePicture()
    path = savePicture(pic)
    log("Saved photo to " + path)
    code_thread = threading.Thread(target=readcodes,args=(pic,))
    code_thread.start()
    pred,results = predict(pic)
    result_str="[%.2f, %.2f, %.2f, %.2f]"%(results[0],results[1],results[2],results[3])
    log("Prediction result: " + pred + " " + result_str + (""," (suppressed)")[override])
    code_thread.join()
    #check if code in database
    for c in codes:
      if c in code_db: pred = code_db[c]; log("Code override: "+pred)
    if os.path.isfile("/tmp/recog.OTH"): pred="OTH"; log("File override (OTH)")
    if os.path.isfile("/tmp/recog.ALU"): pred="ALU"; log("File override (ALU)")
    if os.path.isfile("/tmp/recog.GLA"): pred="GLA"; log("File override (GLA)")
    if os.path.isfile("/tmp/recog.PET"): pred="PET"; log("File override (PET)")
    if not override:
      response_socket.sendall(pred+'\n')

#to be launched in separate thread
def recog_file(filename):
    pic = cv2.imread(filename)
    log("File loaded: "+str(pic.shape))
    code_thread = threading.Thread(target=readcodes,args=(pic,))
    code_thread.start()
    tstart = time()
    pred,results = predict(pic)
    log("Recognition done (%.4fs)"%(time()-tstart))
    log("Result: " + pred + " " + str(results) + (""," (suppressed)")[override])
    code_thread.join()
    log("Readcodes joined: "+str(codes))
    for c in codes:
      if c in code_db: pred = code_db[c]; log("Code override: "+pred)




#######################################################################

initLog(log_filename)

if not os.path.exists(photo_dir):
    log("Photo directory ("+photo_dir+") does not exist. Creating")
    os.makedirs(photo_dir)
log("Recognition starting")


video = initCamera(camera_dev)
model,classes = initModel()
log("Model initialized...")
log("Classes: "+str(classes))
#initial prediction (first one takes more time)
pic = takePicture()
predict(pic)

#all ready - open socket
sock = initSocket();
while True:
  log("Socket open at "+socket_address+", waiting for connection...")
  connection, client = sock.accept()
  try:
    log("Connected: " + str(client))
    while True:
      data = connection.recv(128)
      if not data:
        log("Client disconnect")
        break
      if data == '\n':
        log("Received request")
        override = False
        #perform recognition and send response
        t = threading.Thread(target=recognition,args=(connection,))
        t.start()
      else:
        log("Received override request: " + data) 
        #if recognition is not idle, this will make sure it stays silent
        override = True
        connection.sendall(data)
        #data = data[:-1] #strip newline
        #log("Check file: " + data)
        #t = threading.Thread(target=recog_file,args=(data,))
        #t.start()
        
  finally:
    connection.close()








