#!/usr/bin/python3
# Load the modules
import pickle
import math
import numpy as np
#import tensorflow as tf
import matplotlib.pyplot as plt
from tqdm import tqdm
# Import keras deep learning libraries
import json
from sklearn.model_selection import train_test_split
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.optimizers import SGD, Adam, RMSprop
from keras.callbacks import TensorBoard
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
import glob
import cv2
import time
import sys
#my image processing function
#from detect import process_image
from preprocesing import process_image
import os

import json
import os
import h5py


epoch_time = str(int(time.time()))
os.mkdir('models/'+epoch_time)
#set random seed for reproducibility
random_seed=123456
np.random.seed(random_seed)

# cl_list=sorted(glob.glob('foto_gazomat/*/'))
dataset = sys.argv[1]
#dataset = 'butle'
cl_list=sorted(glob.glob(dataset+'/*/'))
print('Classes: ', cl_list)

val_size=0.2

batch_size = 15
nb_classes = len(cl_list)
#nb_epoch = 55
nb_epoch = 100

X_train=[]
y_train=[]
X_train_extra=[]
y_train_extra=[]

X_train=[]
y_train=[]

count=[0]*nb_classes;
for i,cl in enumerate(cl_list):
    ss=cl+'*.jpg'
    img_list=glob.glob(ss)
    print(cl)
    for fimg in img_list:
        X_train.append(fimg)
        y_train.append(i)
        count[i]=count[i]+1
X_train = X_train[::1]
y_train = y_train[::1]

X_train, X_val, y_train, y_val = train_test_split(X_train,y_train,test_size=val_size,random_state=random_seed)

print('X_train length: ', len(X_train))
print('y_train length: ', len(y_train))
print('X_val length: ', len(X_val))
print('y_val length: ', len(y_val))

def trainGenerator():
    while True:
        batchx=[]
        batchy=[]
        for iii,(fname,cl) in enumerate(list(zip(X_train,y_train))):
            img=cv2.imread(fname)
            cl_one_hot=[0]*nb_classes
            cl_one_hot[cl]=1
            processed=process_image(img)
            batchx.append(processed)
            batchy.append(cl_one_hot)
            if len(batchx)==batch_size:
                bx=np.array(batchx)
                by=np.array(batchy)
                batchx=[]
                batchy=[]
                yield bx,by

def valGenerator():
    while True:
        batchx=[]
        batchy=[]
        for fname,cl in zip(X_val,y_val):
            img=cv2.imread(fname)
            cl_one_hot=[0]*nb_classes
            cl_one_hot[cl]=1
            processed=process_image(img)
            batchx.append(processed)
            batchy.append(cl_one_hot)
            if len(batchx)==batch_size:
                bx=np.array(batchx)
                by=np.array(batchy)
                batchx=[]
                batchy=[]
                yield bx,by

fig, bx = plt.subplots();
width=0.4;
ind = np.arange(len(count));
prev_count=[]
for cc in count:
    prev_count.append(cc)

bx.bar(ind,count,width=0.2,color='b');
bx.bar(ind+width,prev_count,width=0.2,color='r');
bx.set_ylabel('Number of images');
bx.set_title('Images count per label');
bx.set_xticks(ind+width);
bx.set_xticklabels([str(x) for x in list(range(len(count)))], rotation=90,ha='center');

input_shape=process_image(np.zeros(shape=(900,900,3))).shape

print(input_shape, 'input shape')

kernel_size = (3,3)
model = Sequential()

model.add(Convolution2D(16, kernel_size[0], kernel_size[1],
                        border_mode='valid',
                        input_shape=input_shape))
model.add(Activation('relu'))
model.add(Convolution2D(32, kernel_size[0], kernel_size[1]))
model.add(Activation('relu'))
model.add(Convolution2D(64, kernel_size[0], kernel_size[1]))
model.add(Activation('relu'))
model.add(Convolution2D(128, kernel_size[0], kernel_size[1]))
model.add(Activation('relu'))

model.add(Flatten())
model.add(Dropout(0.2))
model.add(Dense(32))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(Dense(32))
model.add(Activation('relu'))

model.add(Dense(nb_classes))
model.add(Activation('softmax'))

model.summary()

filepath1='models/'+epoch_time+"/weights-{epoch:02d}-{loss:.4f}.h5"
checkpoint1 = ModelCheckpoint(filepath1, monitor='loss', verbose=1, save_best_only=True, mode='min',period=1)
filepath2='./model.h5'
checkpoint2 = ModelCheckpoint(filepath2, monitor='loss', verbose=1, save_best_only=True, mode='min',period=1)

callbacks_list = [checkpoint1,checkpoint2]


# Save model as json file
json_string = json.loads(model.to_json())

cl_list_pretty = []
for cl in cl_list:
    cl_list_pretty.append(os.path.basename(os.path.normpath(cl)))

json_string["output_cl"] = cl_list_pretty
with open('model.json', 'w') as outfile:
	#json.dump(json_string, outfile)
	outfile.write(json.dumps(json_string))
	# save weights
	print("Overwrite Successful - json model")
model.save_weights('./model.h5')
print("Overwrite Successful - h5 weights")
print('Training...')
# train model
#model.fit_generator(Xy_generator(),samples_per_epoch=samples_per_epoch, nb_epoch=FLAGS.epochs, callbacks=callbacks_list)

model.compile(loss='categorical_crossentropy',
              optimizer=Adam() ,metrics=['accuracy'])


history = model.fit_generator(trainGenerator(),
                    validation_data=valGenerator(),
                    samples_per_epoch=len(X_train),
                    nb_val_samples=len(X_val),
                    nb_epoch=nb_epoch,
                    callbacks=callbacks_list,
                    verbose=1)
