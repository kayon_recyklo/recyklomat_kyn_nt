# recyklomat_recognition

Recognition system for recyklomat.

## Install prerequisites
```bash
apt install python-dev python-pip python-opencv python-setuptools python-h5py python-yaml python-scipy libzbar0 libzbar-dev
pip install Theano
pip install keras
pip install pyzbar
mkdir ~/.keras
echo '{"epsilon": 1e-07,"floatx": "float32","image_data_format": "channels_last","backend": "theano"}' > ~/.keras/keras.json
```

## Run
```bash
./recognition.py config.json
```
Config file must use absolute paths.
