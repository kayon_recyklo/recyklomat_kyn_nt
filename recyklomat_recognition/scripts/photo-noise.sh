#!/bin/bash

DIR=$1

#if [[ $# < 2 ]]; then echo "$0 <class-dir> <size>" ; exit ; fi
echo $DIR
cd $DIR

NFILES=$(ls -1q | wc -l)
COUNT=0
for F in `ls`; do
	convert $F -evaluate Gaussian-noise 0.5 ${F%.*}-g05.jpg
	convert $F -evaluate Gaussian-noise 0.7 ${F%.*}-g07.jpg
	convert $F -evaluate Gaussian-noise 1.0 ${F%.*}-g10.jpg
	convert $F -evaluate Gaussian-noise 1.1 ${F%.*}-g11.jpg
	convert $F -evaluate Laplacian-noise 1.0 ${F%.*}-l10.jpg
	convert $F -evaluate Poisson-noise 3.0 ${F%.*}-l30.jpg
	convert $F -evaluate Poisson-noise 4.0 ${F%.*}-l40.jpg
	convert $F -evaluate Poisson-noise 5.0 ${F%.*}-l50.jpg
	convert $F -evaluate Multiplicative-noise 1.0 ${F%.*}-m10.jpg
	((COUNT++))
	echo -ne "\rnoise $COUNT/$NFILES"
done
echo

NFILES=$(ls -1q | wc -l)
COUNT=0
for F in `ls`; do
	convert $F -crop 30x30+0+0 -resize 32 ${F%.*}-s01.jpg
	convert $F -crop 30x30+1+1 -resize 32 ${F%.*}-s02.jpg
	convert $F -crop 30x30+2+2 -resize 32 ${F%.*}-s03.jpg
	convert $F -crop 31x31+0+0 -resize 32 ${F%.*}-s11.jpg
	convert $F -crop 31x31+1+0 -resize 32 ${F%.*}-s12.jpg
	convert $F -crop 31x31+0+1 -resize 32 ${F%.*}-s13.jpg
	convert $F -crop 31x31+1+1 -resize 32 ${F%.*}-s14.jpg
	((COUNT++))
	echo -ne "\rstretch $COUNT/$NFILES"
done
echo


#width 3000 -> 32* 94 -> move by 100
#height 850 -> 32* ~27 -> move by 50

