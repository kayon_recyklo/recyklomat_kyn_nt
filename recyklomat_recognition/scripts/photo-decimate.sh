#!/bin/bash
N=$3
IN=$1
OUT=$2
mkdir $OUT

NFILES=$(ls -1q $IN | wc -l)
COUNT=0
for f in `ls $IN`; do
	if (( RANDOM % N == 0 )); then
		mv $IN/$f $OUT/
	fi
	((COUNT++))
	echo -ne "\r$COUNT/$NFILES"
done
echo
