#!/bin/bash

SIZE=32x32
#SIZE=$2
DIR=$1

#if [[ $# < 2 ]]; then echo "$0 <class-dir> <size>" ; exit ; fi

mkdir $DIR-$SIZE
for D in `ls $DIR`; do
	NFILES=$(ls -1q $DIR/$D | wc -l)
	COUNT=0
	echo "$D ($NFILES)"
	mkdir $DIR-$SIZE/$D
	for F in `ls $DIR/$D`; do
		#echo -n "."
		BF=${F##*/}
		BF=${BF%.*}
		convert $DIR/$D/$F -crop 3000x850+40+800 -resize $SIZE\! $DIR-$SIZE/$D/$BF-1.jpg
		convert $DIR/$D/$F -crop 3000x850+240+800 -resize $SIZE\! $DIR-$SIZE/$D/$BF-2.jpg
		convert $DIR/$D/$F -crop 3000x850+440+800 -resize $SIZE\! $DIR-$SIZE/$D/$BF-3.jpg
		convert $DIR/$D/$F -crop 3000x850+240+750 -resize $SIZE\! $DIR-$SIZE/$D/$BF-4.jpg
		convert $DIR/$D/$F -crop 3000x850+240+850 -resize $SIZE\! $DIR-$SIZE/$D/$BF-5.jpg
		((COUNT++))
		echo -ne "\r$COUNT/$NFILES"
	done
	echo
done

#width 3000 -> 32* 94 -> move by 100
#height 850 -> 32* ~27 -> move by 50

