#!/bin/bash
if [[ $# < 1 ]]; then echo "$0 <dir>"; exit; fi
cd $1
for D in $(ls) ; do
	if [[ -d $D ]]; then
		cd $D
		echo "**** $D **** $(pwd)"
		
		COUNT=0
		NFILES=$(ls -1q | wc -l)		
		for F in $(ls); do
			echo -ne "\rgamma $COUNT/$NFILES"
			#VAL=$(convert "$F" -colorspace gray -format "%[fx:image.mean]" info: )
			IN="${F%.*}"
			#if (( $(echo "$VAL>0.3" | bc -l ) )) && (( $(echo "$VAL<0.7" | bc -l ) )); then 
				convert -gamma 0.3  "$IN.jpg" "${IN}_dark2.jpg"
				convert -gamma 0.7 "$IN.jpg" "${IN}_dark1.jpg"
				convert -brightness-contrast +20x+20 "$IN.jpg" "${IN}_lite1.jpg"
				convert -brightness-contrast +40x+40 "$IN.jpg" "${IN}_lite2.jpg"
			#fi
			((COUNT++))
		done
		echo
		
		COUNT=0
		NFILES=$(ls -1q | wc -l)
		for F in $(ls); do
			echo -ne "\rrotate $COUNT/$NFILES"
			IN="${F%.*}"
			#convert -rotate "90"  "$IN.jpg" "${IN}_90.jpg"
			convert -rotate "180" "$IN.jpg" "${IN}_180.jpg"
			#convert -rotate "270" "$IN.jpg" "${IN}_270.jpg"
			((COUNT++))
		done
		echo

		#echo -n "flip "
		#for F in $(ls); do
		#	echo -n "."
		#	IN="${F%.*}"
		#	convert -flop  "$IN.jpg" "${IN}_flip.jpg"
		#done
		#echo
		
		cd ..
	fi
done
cd ..
