#!/usr/bin/python3
import sys
import json
import glob
import cv2
import numpy as np
#import tensorflow as tf
from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array
import operator;
from operator import itemgetter
import sys
#import matplotlib
#matplotlib.use('TkAgg')
#import matplotlib.pyplot as plt
from preprocesing import process_image
np.random.seed(123456)

model = None
dataset = sys.argv[1]
#dataset = 'butle-test'
modelfile = sys.argv[2]
#modelfile = 'tf-model.h5'

cl_list=sorted(glob.glob(dataset+'/*/'))[:]
cl_list=[x.split('/')[-2] for x in cl_list]
print(cl_list)
imgs =[]
for i,cl in enumerate(cl_list):
    ss=dataset+'/'+cl+'/*.jpg'
    img_list=glob.glob(ss)

    print(cl)
    for fimg in img_list:
        imgs.append(fimg)
imgs = sorted(imgs)
#print (imgs)


def detect(image_array):
    image_array=process_image(image_array)

    transformed_image_array = image_array.reshape(( 1,
                                           image_array.shape[0],
                                           image_array.shape[1],
                                           image_array.shape[2]))
    cl = model.predict(transformed_image_array, batch_size=1)
    ld=max(enumerate(cl[0]), key=itemgetter(1))[0]
    cl=list(cl[0])
    info = "%\t".join(['%7.1f'%(x*100.0) for x in cl])
    # for result in sorted(zip(cl,cl_list), key=lambda x:x[0], reverse=True):
    #     print("%.1f%% -> %s"%(result[0]*100.0,result[1]))
    result=max(list(zip(cl,cl_list)), key=lambda x:x[0])
    return result[1],info, ld

if __name__ == '__main__':
    with open('model.json', 'r') as jfile:
        data = jfile.read()
        model = model_from_json(data)


    model.compile("adam", "mse")
    model.load_weights(modelfile)
    #
    # export_model(model, 'example.model')

    good=0
    bad=0
    m  = np.zeros((len(imgs),len(cl_list)), dtype=np.int8)
    m2 = np.zeros((len(cl_list),len(cl_list)), dtype=np.int16)
    f = open("fails.log", "w+")

    print(' '*80,'\t\t','\t'.join(['%8s'%x for x in cl_list]))
    for i,fimg in enumerate(imgs):
        img=cv2.imread(fimg)
        for j,cl_name in enumerate(cl_list):
            if cl_name in fimg:
                 cl_true = j

        res,info, cl=detect(img)
        m2[cl_true, int(cl)] +=1
        if res in fimg:
            print('%16d %4s %60s'%(i, cl_list[int(cl)], fimg),'\t\033[32m[ OK ]\033[0m\t',info)
            good=good+1
            #print((i,int(cl)))
            m[i, int(cl)] = 1
        else:
            print('%16d %4s %60s'%(i, cl_list[int(cl)], fimg),'\t\033[31m[FAIL]\033[0m\t',info)
            f.write('%60s: %s\n'%(fimg,info))
            bad=bad+1
            m[i, int(cl)] = -1
            m[i, int(cl_true)] = 1


    _all=good+bad
    print('Good: %d (%.2f%%)  Bad: %d (%.2f%%)  Total: %d'%(good,100.0*good/_all,bad,100.0*bad/_all,_all))
    st = '%25s'%'True\\predict'+"".join(['%20s'%x for x in cl_list]) + '\n'
    for i in range (len(cl_list)):
        st = st + '%20s'%cl_list[i] + " ";
        for j in range (len(cl_list)):
            st = st + '%20d'%(m2[i,j]) +" "
        st = st + "\n"
    print (st)

    f.close()

