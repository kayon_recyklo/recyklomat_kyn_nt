import asyncio
from copy import copy
import logging
import logging.handlers
import datetime
import dateutil.tz
from config import Config


log_handler = None


def setup_logger(name: str) -> logging.Logger:
    global log_handler
    if log_handler is None:
        log_handler = logging.handlers.RotatingFileHandler(Config.LOG_FILE_PATH, Config.LOG_SIZE_BYTES, Config.LOG_COUNT)
        formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
        log_handler.setFormatter(formatter)
    log = logging.getLogger(name)
    log.addHandler(log_handler)
    log.setLevel(logging.INFO)
    return log


def parse_args(flags: list, argv: list) -> dict:
    argv = copy(argv[1:])
    argv.reverse()
    params = {}
    while argv:
        arg = argv.pop().strip()
        for flag in flags:
            if arg == flag:
                params[flag] = argv.pop()
            elif arg.startswith(flag):
                params[flag] = arg[len(flag):]

    return params


__all__ = ("exit_on_exception", "datetime_str", "call_with_interval", "setup_logger", "parse_args")


async def exit_on_exception(method, *args, task_name="", **kwargs):
    try:
        await method(*args, **kwargs)
    except:
        setup_logger("exit on exception").exception(f"exception occured in task {task_name}")
    finally:
        exit(-1)


def datetime_str():
    return datetime.datetime.now(dateutil.tz.tzlocal()).strftime("%Y-%m-%dT%H:%M:%S.000%z")


async def call_with_interval(interval, method, *args, **kwargs):
    while True:
        await asyncio.sleep(interval)
        method(*args, **kwargs)