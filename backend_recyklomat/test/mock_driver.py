from consts import DriverMessages, GarbageTypes
from config import Config
from test.test_util import post, create_socket, getch


commands = {
    's': 'POST: start',
    '1': DriverMessages.HatchOpen.value,
    '2': DriverMessages.HatchClose.value,
    '3': f"{DriverMessages.QRCode.value} 123456789012",
    '4': f"{DriverMessages.RecognitionFinished.value} {GarbageTypes.Other.value}",
    '5': f"{DriverMessages.RecognitionFinished.value} {GarbageTypes.Aluminium.value}",
    '6': f"{DriverMessages.RecognitionFinished.value} {GarbageTypes.Glass.value}",
    '7': f"{DriverMessages.RecognitionFinished.value} {GarbageTypes.Deodorant.value}",
    '8': 'POST: no_qr',
    '9': 'POST: continue_recycling',
    '0': 'POST: stop_recycling'
}

if __name__ == "__main__":
    x = 0
    with create_socket(Config.DRIVER_SOCKET_PATH) as s:
        while True:
            print(f"waiting for a connection on socket {Config.DRIVER_SOCKET_PATH} .")
            conn, addr = s.accept()
            with conn:
                print('Connected by', addr)

                while True:
                    print('\n'.join(f"{k}: {v}" for k, v in commands.items()))
                    print('q: quit mock driver')
                    print("choice: ")
                    try:
                        choice = getch()
                        if choice == 'q':
                            exit(0)
                        message_to_send = commands[choice]
                    except KeyError:
                        print("bad choice")
                        continue


                    print(f"-> {message_to_send}")
                    if message_to_send.startswith('POST: '):
                        _, path = message_to_send.split(' ', 1)
                        post(path)
                    else:
                        try:
                            conn.sendall(message_to_send.encode())
                        except BrokenPipeError:
                            print("Connection broken")
                            break
