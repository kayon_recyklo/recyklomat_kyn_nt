from time import sleep
import sys
import os
from pathlib import Path


dir_path = Path(os.path.dirname(os.path.realpath(__file__)))

test_file = dir_path.parent / 'test' / 'receipt.txt'

output = ' '.join(sys.argv[1:])
with open(str(test_file), 'w') as r:
    r.write(output)

sleep(10)
