import os

import consts


def print_receipt(config, state):
    cmd_args = {
        'POINTS': -1 if state.ecopoints is None else state.ecopoints,
        'TRANSACTION_TAG': '-1' if state.transaction_tag is None else state.transaction_tag
    }
    for garbage, count in state.collected_garbage.items():
        cmd_args[garbage.value] = count

    cmd = "{} &".format(config.PRINTER_COMMAND)
    cmd = cmd.format(**cmd_args)
    os.system(cmd)


if __name__ == "__main__":
    print('-' * 80)
    print("testing drivers/printer.py")
    print('-' * 80)

    from random import randint

    from time import time
    import state_machine

    class Config(object):
        PRINTER_COMMAND = "python3.6 ./test/mocked_printer.py {POINTS} {ALU} {GLA} {DEO} {SYR} {PET} {BAT} {TRANSACTION_TAG}"

    RecyklomatState = state_machine.RecyklomatState
    GarbageTypes = consts.GarbageTypes

    state = RecyklomatState(
        collected_garbage={
            GarbageTypes.Aluminium: randint(1, 100),
            GarbageTypes.Glass: randint(1, 100),
            GarbageTypes.Deodorant: randint(1, 100),
            GarbageTypes.Syringe: randint(1, 100),
            GarbageTypes.PET: randint(1, 100),
            GarbageTypes.Battery: randint(1, 100),
        },
        ecopoints=randint(1, 1000),
        transaction_tag='0123345678912'
    )

    expected_cmd = f"{state.ecopoints}" \
                   f" {state.collected_garbage[GarbageTypes.Aluminium]}" \
                   f" {state.collected_garbage[GarbageTypes.Glass]}" \
                   f" {state.collected_garbage[GarbageTypes.Deodorant]}" \
                   f" {state.collected_garbage[GarbageTypes.Syringe]}" \
                   f" {state.collected_garbage[GarbageTypes.PET]}" \
                   f" {state.collected_garbage[GarbageTypes.Battery]}" \
                   f" {state.transaction_tag}"

    print("Testing sending command to printer ...")
    test_file = "./test/receipt.txt"
    try:
        os.remove(test_file)
    except FileNotFoundError:
        pass

    start = time()
    print_receipt(Config, state)
    stop = time()
    assert stop - start < 0.01

    while True:
        try:
            with open(test_file, 'r') as r:
                actual_value = r.read().strip()
                assert actual_value == expected_cmd.strip(), f"\n\texpected cmd: {expected_cmd}\n\tactual cmd: {actual_value}"
                break
        except FileNotFoundError:
            if time() - start > 0.1:
                raise

    os.remove(test_file)
    print("... OK")
