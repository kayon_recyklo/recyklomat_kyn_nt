import asyncio


class QRReaderDriver(object):
    def __init__(self, cmd, callback, log):
        self.process = None
        self._killed = False
        self.callback = callback
        self.cmd = cmd
        self.log = log

    async def async_read(self):
        self._killed = False
        try:
            self.process = await asyncio.create_subprocess_exec(
                *self.cmd.split(' '),
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE
            )
        except:
            self.log.exception("error opening qr code reader driver")
            return

        self.log.info('reading qr code ...')

        try:
            qr = await self.process.stdout.readline()
            self.process = None
        except:
            if not self._killed:
                self.log.exception("exception during reading qr code:")
        else:
            qr = qr.decode('utf-8').strip()
            self.callback(qr)

    def kill(self):
        if self.process:
            self._killed = True
            try:
                self.process.kill()
                self.process = None
            except:
                self.log.exception('exception during killing qr code reader process:')


if __name__ == "__main__":
    import os
    from pathlib import Path
    from logging import getLogger

    print('-' * 80)
    print(__file__)
    print('-' * 80)

    async def test():
        q = asyncio.Queue()

        def callback(id):
            print(f'callback with id: {id}')
            q.put_nowait(('qr_code', id))

        cmd = Path(os.path.dirname(os.path.realpath(__file__))).parent / 'test' / 'mocked_scanner.py'
        cmd = f"python3.6 {cmd}"

        print(cmd)
        print("Testing proper qr read ...")
        d = QRReaderDriver(cmd, callback, getLogger('test - qr_code_reader'))
        asyncio.get_event_loop().create_task(d.async_read())
        await asyncio.sleep(0.5)
        d.kill()
        assert not q.empty()
        msg = q.get_nowait()
        assert msg == ('qr_code', '123456789012')
        print("... OK")

        print("Testing timeout on qr read ...")
        d = QRReaderDriver(cmd, callback, getLogger('test - qr_code_reader'))
        asyncio.get_event_loop().create_task(d.async_read())
        await asyncio.sleep(0.01)
        d.kill()
        assert q.empty()

        print("... OK")

    asyncio.get_event_loop().run_until_complete(test())
