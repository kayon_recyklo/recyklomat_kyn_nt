import socket
from os import times_result

from consts import driver_messegaes_to_recyklomat_events, States, Events
from state_machine import RecyklomatState
from util import setup_logger
from config import Config
from asyncio import Queue
import time
import json
import asyncio
from excetions import ReturnToPrevoiusState, EventNotSupported, NoStateChangeException
import datetime


def open_recyklomat_socket(path):
    socket_ = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0)
    socket_.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    socket_.setblocking(False)
    socket_.connect(path)
    return socket_


def has_timed_out(timeout, creation_time, now=None):
    now = now or time.time()
    if creation_time is None or timeout is None:
        return False

    return now > timeout + creation_time


class RecyklomatDriver(object):

    def load_cached_state(self, file):
        state = json.load(file)
        self._driver_state, self._driver_previous_state = \
            self._state_class.deserialize(state['_driver_state']),\
            self._state_class.deserialize(state['_driver_previous_state'])

    def __state_store(self, dumped_state):
        with open(Config.STATE_CACHING_FILE, 'w') as f:
            json.dump(dumped_state, f)

    def __init__(self,
                 socket_,
                 driver_state=None,
                 driver_previous_State=None,
                 state_class=RecyklomatState,
                 maintenance_period_start=None,
                 maintenance_period_stop=None,
                 maintenance_mode_allowed_in_states=[States.Ready],
                 _time=time.time,
                 _state_store=None,
                 _maintenance_state_name=States.Maintenance):
        self.log = setup_logger("driver")
        self._state_class = state_class
        self.maintenance_period_start = maintenance_period_start
        self.maintenance_period_stop = maintenance_period_stop
        self.maintenance_mode_allowed_in_states = maintenance_mode_allowed_in_states
        self._driver_state = driver_state or self._state_class()
        self._driver_previous_state = driver_previous_State or state_class()
        self._socket = socket_
        self._event_queue = Queue()
        self._time = _time
        self._state_class = state_class
        self._state_store = _state_store or self.__state_store
        self.state_callbacks = list()
        self._maintenance_state_name = _maintenance_state_name
        # kolejnosc ma znaczenie!
        self.event_handlers = [
            self._timer_tick_handler, self._current_state_event_handler
        ]

    def add_state_callback(self, state: States, callback):
        self.state_callbacks.append((state, callback))

    def restart(self):
        self.log.info("reseting recyklomat state")
        self.post_event(Events.ResetCounters)

    def get_prevoius_state(self):
        return self._driver_previous_state.serialize()

    def get_recyklomat_state(self):
        return self._driver_state.serialize()

    def post_event(self, event: Events, event_args: tuple = None):
        self._event_queue.put_nowait((event, event_args or (None, )))

    async def event_loop(self):
        while True:
            event_data = await self._event_queue.get()
            params = []
            event = event_data[0]
            if event == Events.Kill:
                break
            if len(event_data) > 1:
                params = event_data[1]
            self._event_loop_iteration(event, params)

    def _cache_state_on_disk(self):
        self._state_store({
            '_driver_state': self._driver_state.serialize(),
            '_driver_previous_state': self._driver_previous_state.serialize()
        })

    def _execute_state_callbacks(self):
        for state, callback in self.state_callbacks:
            if state == self._driver_state.name:
                self.log.info(f"executint state callback for state: {state.name}, {repr(callback)}")
                try:
                    callback(self._driver_state.copy())
                except:
                    self.log.exception("exception occured while running a state callback")

    def _current_state_event_handler(self, event, event_params):
        return self._driver_state.handle(event, event_params)

    def is_maintenance_period(self):
        if self.maintenance_period_stop is None or self.maintenance_period_start is None:
            return False

        now = datetime.datetime.fromtimestamp(self._time()).time()
        if self.maintenance_period_start > self.maintenance_period_stop:
            # przypadek gdy start jest przed polnoca, a stop po polnocy
            return now > self.maintenance_period_start or now < self.maintenance_period_stop
        return self.maintenance_period_start < now < self.maintenance_period_stop

    def _timer_tick_handler(self, event, event_params):
        if event != Events.Tick:
            raise EventNotSupported

        if self.is_maintenance_period() and self._driver_state.name in self.maintenance_mode_allowed_in_states:
            return self._state_class(self._maintenance_state_name)

        if self._driver_state.name == self._maintenance_state_name and not self.is_maintenance_period():
            return self._state_class()

        if has_timed_out(self._driver_state.error_timeout, self._driver_state.created_at_timestamp,
                         self._time()) and self._driver_state.name != self._maintenance_state_name:
            self.log.error(f"ERROR TIMEOUT. current_state: {str(self._driver_state.serialize())},"
                           f" previous_state: {str(self._driver_previous_state.serialize())}. Resetting state")
            return self._state_class()

        if has_timed_out(self._driver_state.user_interaction_timeout, self._driver_state.created_at_timestamp,
                         self._time()):
            self.log.info("User interaction timeout")
            self.post_event(Events.InteractionTimeout, (None, ))

        raise NoStateChangeException

    def _event_loop_iteration(self, event, event_params):
        try:
            for handler in self.event_handlers:
                try:
                    self._driver_state, self._driver_previous_state = handler(event, event_params), self._state_class()
                    self.log.info(f"recykomat state: {self._driver_state.name}")
                    self._cache_state_on_disk()
                    self._execute_state_callbacks()
                    break
                except EventNotSupported:
                    continue
                except NoStateChangeException:
                    break
            else:
                self.log.error(f"unhandled event '{event}' with params {event_params}.")
        except:
            self.log.exception(f"Exception occured while handling event '{event}' with params {event_params}.")

    async def driver_client(self):
        ioloop = asyncio.get_event_loop()
        while True:
            data = await ioloop.sock_recv(self._socket, 1024)
            if len(data) == 0:
                self.log.error("received empty message from the driver. Assuming the socket is closed")
                raise IOError('driver socket is closed')

            try:
                data = data.decode('utf-8').strip()
            except:
                self.log.exception(f"failed to decode message from the driver. Message: {data}")
            else:
                self.log.info("received data %s", data)
                for message in data.split('\n'):
                    event_name, *event_args = message.split(' ')
                    try:
                        event = driver_messegaes_to_recyklomat_events[event_name]
                    except KeyError:
                        self.log.error("unknown event %s", event_name)
                        continue
                    self.post_event(event, tuple(event_args))


if __name__ == "__main__":
    print('-' * 80)
    print(__file__)
    print('-' * 80)
    TEST_ERROR_TIMEOUT = 5
    TEST_USER_INTERATION_TIMEOUT = 2

    def PASS(*args, **kwargs):
        pass

    class TimeMock(object):
        def __init__(self):
            self.val = 0

        def __call__(self, *args, **kwargs):
            return self.val

    time_mock = TimeMock()

    from enum import Enum

    class TestStatesEnum(Enum):
        Idle = 'idle'
        Maitenance = 'maitenance'
        Working = 'working'

    class StateMock(object):
        def __init__(self, *args, **kwargs):
            self.init_args = args
            self.init_kwargs = kwargs
            self.handle_event = None
            self.handle_params = None
            self.created_at_timestamp = time_mock()
            self.error_timeout = TEST_ERROR_TIMEOUT
            self.user_interaction_timeout = TEST_USER_INTERATION_TIMEOUT
            self.name = args[0] if len(args) else TestStatesEnum.Idle

        def handle(self, event, params):
            self.handle_event = event
            self.handle_params = params
            return StateMock(created_by=self)

        def serialize(self):
            return "<serialize mock>"

    print("testing error timeout ...")
    r = RecyklomatDriver(object, state_class=StateMock, _time=time_mock, _state_store=PASS)
    s1 = r._driver_state
    s2 = r._driver_previous_state
    time_mock.val = TEST_ERROR_TIMEOUT + 1
    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._driver_state != s1
    assert r._driver_previous_state != s2
    assert r._driver_previous_state != s1
    assert r._driver_state.created_at_timestamp == time_mock.val
    assert r._driver_previous_state.created_at_timestamp == time_mock.val
    print("... OK")

    print("testing user interaction timeout ...")
    time_mock.val = 0
    r = RecyklomatDriver(object, state_class=StateMock, _time=time_mock, _state_store=PASS)
    s1 = r._driver_state
    s2 = r._driver_previous_state
    time_mock.val = TEST_USER_INTERATION_TIMEOUT + 1
    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._event_queue.get_nowait()[0] == Events.InteractionTimeout
    print("... OK")

    print("test saving state in case of a crash")
    class MockedStateStore(object):
        def __init__(self):
            self.history = []

        def __call__(self, *args, **kwargs):
            self.history.append((args, kwargs))

    state_store_mock = MockedStateStore()
    r = RecyklomatDriver(object, _time=time_mock, _state_store=state_store_mock)
    r._event_loop_iteration(Events.HatchOpened, (None, ))
    assert len(state_store_mock.history) == 1
    assert state_store_mock.history[0][0][0]['_driver_state']['name'] == 'working'
    assert state_store_mock.history[0][0][0]['_driver_previous_state']['name'] == 'ready'
    print("... OK")

    print("testing restoring state after crash ...")
    r = RecyklomatDriver(object)
    from io import StringIO
    from state_machine import States

    cached_state = json.dumps({
        '_driver_state': RecyklomatState(States.SummaryWithNoQr).serialize(),
        '_driver_previous_state': RecyklomatState(States.SendingReport).serialize()

    })
    r.load_cached_state(StringIO(cached_state))

    assert r._driver_state.name == States.SummaryWithNoQr
    assert r._driver_previous_state.name == States.SendingReport
    print("... OK")

    print('testing state callbacks')
    r = RecyklomatDriver(object)
    r._driver_state = RecyklomatState(States.WaitingForQR)


    class callable(object):
        def __init__(self):
            self.args = None
            self.kwargs = None
            self.calls = 0

        def __call__(self, *args, **kwargs):
            self.calls += 1
            self.args = args
            self.kwargs = kwargs


    fake_send_report = callable()
    QR_CODE='123152412'
    r.add_state_callback(States.SendingReport, fake_send_report)
    r._event_loop_iteration(Events.ScannedQR, (QR_CODE, ))
    assert fake_send_report.calls == 1
    assert fake_send_report.args[0].name == States.SendingReport
    assert fake_send_report.args[0].user_id == QR_CODE
    print("... OK")

    print("Testing NoStateChange")

    class StateThatDoesNotChange(StateMock):
        def handle(self, e, p):
            raise NoStateChangeException

    r = RecyklomatDriver(object())
    previous_state = object()
    current_state = StateThatDoesNotChange()
    r._driver_state, r._driver_previous_state = current_state, previous_state
    r._event_loop_iteration("random event", (None, ))
    assert r._driver_state is current_state
    assert r._driver_previous_state is previous_state
    print("... OK")

    print("testing entering maitenance mode ...")
    import datetime
    MORNING = datetime.time(8, 0)
    EVENING = datetime.time(20, 0)
    NOON = datetime.datetime(2019, 8, 10, 12, 0)
    MIDNIGHT = datetime.datetime(2019, 8, 10, 0, 0)
    time_mock = TimeMock()
    time_mock.val = MIDNIGHT.timestamp()

    r = RecyklomatDriver(object(),
                         maintenance_period_start=EVENING,
                         maintenance_period_stop=MORNING,
                         maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                         state_class=StateMock,
                         _time=time_mock,
                         _maintenance_state_name=TestStatesEnum.Maitenance
                         )
    idle_state = StateMock(name=TestStatesEnum.Idle)
    idle_state.created_at_timestamp = time_mock()

    r._driver_state, r._driver_previous_state = idle_state, previous_state
    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._driver_state.name == TestStatesEnum.Maitenance

    time_mock.val = NOON.timestamp()
    idle_state.created_at_timestamp = time_mock()
    r._driver_state, r._driver_previous_state = idle_state, idle_state
    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._driver_state is idle_state


    time_mock.val = MIDNIGHT.timestamp()

    r = RecyklomatDriver(object(),
                         maintenance_period_start=MORNING,
                         maintenance_period_stop=EVENING,
                         maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                         state_class=StateMock,
                         _time=time_mock,
                         _maintenance_state_name=TestStatesEnum.Maitenance
                         )

    time_mock.val = NOON.timestamp()
    r._driver_state = idle_state
    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._driver_state.name == TestStatesEnum.Maitenance


    r = RecyklomatDriver(object(),
                         maintenance_period_start=MORNING,
                         maintenance_period_stop=EVENING,
                         maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                         state_class=StateMock,
                         _time=time_mock,
                         _maintenance_state_name=TestStatesEnum.Maitenance
                         )
    time_mock.val = MIDNIGHT.timestamp()
    r = RecyklomatDriver(object(),
                         maintenance_period_start=MORNING,
                         maintenance_period_stop=EVENING,
                         maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                         state_class=StateMock,
                         _time=time_mock,
                         _maintenance_state_name=TestStatesEnum.Maitenance
                         )
    r._driver_state = idle_state

    r._event_loop_iteration(Events.Tick, (None, ))
    assert r._driver_state is idle_state


    working_state = StateMock(TestStatesEnum.Working)
    r = RecyklomatDriver(object(),
                     maintenance_period_start=EVENING,
                     maintenance_period_stop=MORNING,
                     maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                     state_class=StateMock,
                     _time=time_mock,
                     _maintenance_state_name=TestStatesEnum.Maitenance
                     )
    r._driver_state = working_state
    print("... OK")
    print("Testing exit from maintenace mode ...")
    time_mock.val = NOON.timestamp()
    r = RecyklomatDriver(object(),
                   maintenance_period_start=EVENING,
                   maintenance_period_stop=MORNING,
                   maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                   state_class=StateMock,
                   _time=time_mock,
                   _maintenance_state_name=TestStatesEnum.Maitenance
                   )
    current_state = StateMock(TestStatesEnum.Maitenance)
    r._driver_state = current_state
    r._event_loop_iteration(Events.Tick, (None,))
    assert r._driver_state.name == TestStatesEnum.Idle

    time_mock.val = MIDNIGHT.timestamp()
    r = RecyklomatDriver(object(),
                         maintenance_period_start=MORNING,
                         maintenance_period_stop=EVENING,
                         maintenance_mode_allowed_in_states=[TestStatesEnum.Idle],
                         state_class=StateMock,
                         _time=time_mock,
                         _maintenance_state_name=TestStatesEnum.Maitenance
                         )
    current_state = StateMock(TestStatesEnum.Maitenance)
    r._driver_state = current_state
    r._event_loop_iteration(Events.Tick, (None,))
    assert r._driver_state.name == TestStatesEnum.Idle
    print("... OK")

