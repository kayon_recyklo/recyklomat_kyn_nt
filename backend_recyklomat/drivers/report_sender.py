import urllib.request
import json
import logging.handlers
from pathlib import Path
from time import time
import random
from os import listdir, remove
from os.path import isfile
from config import Config
from tornado.httpclient import AsyncHTTPClient
from util import datetime_str

from state_machine import RecyklomatState
from consts import GarbageTypes

_LETTERS_AND_NUMBERS = 'abcdefghijklmnoprstuwxyz0123456789'
_HEADERS = {'content-type': 'application/json'}


class ReturnToPreviousState(Exception):
    """Exception used by the "handle_***" methods to indicate that the recyklomat should return to previous state."""


def random_alfanumeric_string():
    random_str = ''.join(random.choices(_LETTERS_AND_NUMBERS, k=32))
    return f"{int(time())}_{random_str}"


def post_to_uri(uri: str, data: str):
    response = urllib.request.urlopen(urllib.request.Request(
        uri,
        data=data.encode('utf-8'),
        headers=_HEADERS
    ))
    if response.getcode() != 200:
        raise Exception(f'Failed to send report to {uri} ; response code: {response.getcode()}')
    return response.read()


async def send_report(config: Config, log: logging.Logger, report: dict):
    log.info("sending report to %s , data: %s", config.URI, report)
    http_client = AsyncHTTPClient()
    data = json.dumps(report)
    try:
        rsp = await http_client.fetch(config.URI, method="POST", body=data, headers=_HEADERS)
        assert 'points' in json.loads(rsp.body)
        return rsp.body
    except:
        cached_report_path = Path(config.CACHE_FOLDER) / f"{random_alfanumeric_string()}.json"
        log.exception('Failed to send report to %s . Saving report for later to: %s',
                      config.URI, cached_report_path)
        with open(cached_report_path, 'w') as fh:
            fh.write(data)
    return -1


def cached_reports_files(directory):
    for node in listdir(directory):
        full_path = Path(directory) / node
        if isfile(full_path):
            yield full_path


def send_cached_reports(config: Config, log: logging.Logger):
    for file_path in cached_reports_files(config.CACHE_FOLDER):
        try:
            with open(file_path, 'r') as fh:
                log.info("sending cached report to %s , file: %s", config.URI, file_path)
                report = fh.read()
                post_to_uri(config.URI, report)
            remove(file_path)
        except:
            log.exception("failed to send cached report")


def transform_state_to_report(state: RecyklomatState):
    return {
        'timestamp': datetime_str(),
        'rvmId': Config.RVMID,
        'aluminiumCans': state.collected_garbage[GarbageTypes.Aluminium],
        'glassBottles': state.collected_garbage[GarbageTypes.Glass],
        'batteries': state.collected_garbage[GarbageTypes.Battery],
        'plastic': state.collected_garbage[GarbageTypes.PET],
        'syringes': state.collected_garbage[GarbageTypes.Syringe],
        'aerosols': state.collected_garbage[GarbageTypes.Deodorant],
        'customerId': state.user_id or Config.DEFAULT_USER_ID,
        'transactionTag': f"{Config.CODE}{str(int(time()))[-7:]}",
        'code':Config.CODE
    }


if __name__ == "__main__":
    from config import Config
    from util import setup_logger

    log = setup_logger('report_sender')
    log.info("sending cached reports")
    send_cached_reports(Config, log)
