import json
import logging
from string import Template

import tornado.web

from config import Config
from drivers.recyklomat import RecyklomatDriver
from consts import Events


class DebugHandler(tornado.web.RequestHandler):
    def initialize(self, recyklomat: RecyklomatDriver, log: logging.Logger, host, port) -> None:
        self.recyklomat = recyklomat
        self.log = log
        self.tpl_data = {
            'HOST': host, 'PORT': port
        }

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    def compute_etag(self):
        return None

    def get(self, path):
        if path == '/log':
            with open(Config.LOG_FILE_PATH, 'r') as l:
                self.write('\n'.join(reversed(l.readlines()[-20:])))
        else:
            with open('./web_debug/index.html', 'r') as f:
                self.write(Template(f.read()).substitute(**self.tpl_data))

    def post(self, path):
        data = json.loads(self.request.body)
        event = Events(data['event'])
        params = data['args']
        self.recyklomat.post_event(event, params)