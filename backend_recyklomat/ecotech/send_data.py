#!/usr/bin/env python3

import sys
import urllib.request
import json
from time import gmtime, strftime
time = strftime("%Y-%m-%dT%H:%M:%S.000+0000", gmtime())

alu = 0
gla = 0
bat = 0
pet = 0

if len(sys.argv) >= 3:
	alu = int(sys.argv[2])
if len(sys.argv) >= 4:
	gla = int(sys.argv[3])
if len(sys.argv) >= 5:
	bat = int(sys.argv[4])
if len(sys.argv) >= 2:
	pet = int(sys.argv[1])

url = 'http://18.194.89.149/api/v1/items'
data = {
    'timestamp': time,
    'aluminiumCans': alu,
    'glassBottles': gla,
    'batteries': bat,
    'plastic': pet,
    'rvmId': '5821fc17857aba0001fcf6e3',
    'customerId': '1011887766551'
}
#print(data)
response = urllib.request.urlopen(urllib.request.Request(
    url,
    data=json.dumps(data).encode('utf-8'),
    headers={'content-type': 'application/json'}
    )).read().decode('utf-8')
json_response = json.loads(response)
if 'errorMessage' in json_response:
	print(-1)
else:
	print(int(json_response['points'])) 
#print(json_response)
