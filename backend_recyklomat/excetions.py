class StateNotSupportedError(Exception):
    pass


class EventNotSupported(Exception):
    pass


class EventParameterNotSupported(Exception):
    pass


class ReturnToPrevoiusState(Exception):
    """Exception used by the `handle_xxx` methotds to signal the recyklomat driver to revert to previous state"""


class NoStateChangeException(Exception):
    """Event has been handled, but there is no need to do anything about it."""
