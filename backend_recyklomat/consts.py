from enum import Enum


class DriverMessages(Enum):
    HatchOpen = 'hatch_open'
    HatchClose = 'hatch_close'
    RecognitionFinished = 'recognized'
    QRCode = 'qr_code'


class Events(Enum):
    HatchOpened = 1
    HatchClosed = 2
    RecognitionFinished = 3
    ResetCounters = 4
    ContinueWithNoQR = 6
    ScannedQR = 7
    InteractionTimeout = 8
    FinishCollectingGarbage = 9
    ReportSent = 10
    SummaryTimeout = 11
    Tick = 12
    Kill = 13


driver_messegaes_to_recyklomat_events = {
    DriverMessages.HatchOpen.value: Events.HatchOpened,
    DriverMessages.HatchClose.value: Events.HatchClosed,
    DriverMessages.RecognitionFinished.value: Events.RecognitionFinished,
    DriverMessages.QRCode.value: Events.ScannedQR,
}


class States(Enum):
    Ready = 'ready'
    Working = 'working'
    Processing = 'processing'
    WaitingForQR = 'waiting_for_qr'
    RemoveUnknownGarbage = 'remove_unknown_garbage'
    SendingReport = 'sending_report'
    SummaryWithNoQr = 'sending_report_summary_without_qr'
    SummaryWithQR = 'summary_with_qr'
    Maintenance = 'maintenance'


class GarbageTypes(Enum):
    Glass = 'GLA'
    Aluminium = 'ALU'
    Deodorant = 'DEO'
    Syringe = 'SYR'
    PET = 'PET'
    Battery = 'BAT'
    Other = 'OTH'
    Empty = 'EMP'