#!/usr/bin/env bash

INITIAL_PWD="$(pwd)"
PID_FILE='/tmp/recyklomat_backend.pid'
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
ABSOLUTE_SCRIPT_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"

TRUE=1
FALSE=0

cd "${ABSOLUTE_PATH}"

function install_service {
echo "[Unit]
Description=Backend recyklomatu

[Install]
WantedBy=multi-user.target

[Service]
User=""$USER""
Type=forking
PIDFile=""$PID_FILE""
RemainAfterExit=no
Restart=on-failure
RestartSec=5s
ExecStart=""$PWD""/backend_control.sh --start
ExecStop=""$PWD""/backend_control.sh --kill
" > /tmp/recyklomat.service
sudo mv /tmp/recyklomat.service /etc/systemd/system
sudo systemctl daemon-reload
}

function is_app_running {
    if [[ -f "${PID_FILE}" ]]; then
        PID="$(cat $PID_FILE)"
        if [[ "$(ps -p "${PID}" -o cmd --no-headers)" ]]; then
            echo "${TRUE}"
        fi;
    fi;
}

if [[ "${#}" < 1 || "${1}" == "--help" ]] ; then
    cat << EndOfMessage
Witaj w skrypcie do kontroli recyklomatowego backendu.
    Użycie: ./backend_control.sh <opcja>
Uwaga! skrypt powinien być uruchamiany z poziomu uzytkownika docelowego!
Dostepne opcje:
    --help             wyświetl ten komunikat
    --start            uruchom backend
    --kill             zabij aktualnie uruchomiony backend
    --status           sprawdz czy backend jest uruchomiony
    --install          zainstaluj i uruchom usuluge recyklomatu oraz zainstaluj wszystkie potrzebne zaleznosci (tylko ubuntu/debian)
    --test             odpal skrócone testy integracyjne aby sprawdzić czy środowisko jest dobrze zestawione
    --debug            uruchom backend bez podlaczania sie do sterownika z uruchomiona konsola debugowa dostepna pod adresem http://<url>:<port>/debug
EndOfMessage

fi

function get_config_variable {
    python3.6 -c "import config; print(config.Config.${1})"
}

function create_folders {
    mkdir -p "$(get_config_variable CACHE_FOLDER)"
    mkdir -p "$(dirname $(get_config_variable LOG_FILE_PATH))"
}


function kill_recyklo {
    if [[ "$(is_app_running)" == "${TRUE}" ]]; then
        echo "killing recyklomat"
        kill -9 "$(cat ${PID_FILE})"
    else
        echo "recyklomat backend is already killed"
    fi;	
}

if [[ "${1}" = '--kill' ]]; then
    kill_recyklo
fi;

if [[ "${1}" = '--status' ]]; then
    if [[ -f "${PID_FILE}" ]]; then

        if [[ "$(is_app_running)" == "${TRUE}" ]]; then
            echo "recyklomat backend is running"
        else
            echo "recyklomat backend is NOT running"
        fi;

    else
        echo 'recyklomat backend is NOT running'
    fi;
fi;


function start {
	create_folders
    if [[ "$(is_app_running)" == "${TRUE}" ]]; then
        echo "recyklomat backend is already running"
        exit 1
    else
        python3.6 ./recyklomat_application.py ${1} &
        echo "$!" > "${PID_FILE}"
    fi;
}

if [[ "${1}" = "--start" ]]; then
    start
fi;

function download_and_compile_python {
    # http://www.knight-of-pi.org/installing-python3-6-on-a-raspberry-pi/
    sudo apt-get install python3-dev libffi-dev libssl-dev wget gcc make zlibc -y
    wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
    tar xJf Python-3.6.3.tar.xz
    cd Python-3.6.3
    ./configure
    make
    sudo make install
    sudo pip3 install --upgrade pip
    cd ..
    #sudo python3.6 -m pip install setuptools
    #sudo python3.6 -m pip install -r ./requirements.txt
}


function install_python36_ubuntu {
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt-get update
    sudo apt-get install python3.6 python3-pip
    #sudo python3.6 -m pip install setuptools
    #sudo python3.6 -m pip install -r ./requirements.txt
}

if [[ "${1}" == "--install" ]]; then
    set -e
    # konfiguracja systemu
    sudo locale-gen en_US.UTF-8
    # instalacja zaleznosci
    if [[ "$(command -v python3.6)" ]] ; then
        echo "python 3.6 already installed"
    else
        if [[ "$(lsb_release -a)" == *"Ubuntu"* ]]; then
            echo "ubuntu detectec"
            install_python36_ubuntu
        else
            echo "installing python 3.6 from source - it may take some time"
            download_and_compile_python
        fi;
    fi
    sudo python3.6 -m pip install setuptools
    sudo python3.6 -m pip install -r ./requirements.txt
    # zestawianie folderow
    report_cache_folder="$(get_config_variable CACHE_FOLDER)"
    mkdir -p -- "$report_cache_folder"
    # konfiguracja crontaba - wysylanie scachowanych raportow
    report_sender_cmd="python3.6 ${ABSOLUTE_PATH}/drivers/report_sender.py"
    # co godzine o losowej minucie
    minute="$(( ( RANDOM % 60 )  + 1 ))"
    (crontab -l 2>/dev/null; echo "${minute} * * * * PYTHONPATH=${ABSOLUTE_PATH} ${report_sender_cmd}") | crontab -
    # instalacja i uruchomienie uslugi
    install_service
    sudo systemctl start recyklomat.service
    set +e
    echo "recyklomat zostal poprawnie skonfigurowany, wejdz http://<ip>:8888/ i sprawdz czy wszystko dziala poprawnie, a jesli nie, to sprawdz w logach co sie stalo."
fi


function start_with_delay {
    sleep 1
    start
}

if [[ "${1}" == "--start-with-mock" ]]; then
    start_with_delay&
    PYTHONPATH="${ABSOLUTE_PATH}" python3.6 ./test/mock_driver.py -d true
    kill
fi

if [[ "${1}" == "--test" ]] ; then
	create_folders
    mkdir -p log
    rm ./log/*
    PYTHONPATH="${ABSOLUTE_PATH}" python3.6 ./state_machine.py
    PYTHONPATH="${ABSOLUTE_PATH}" python3.6 ./drivers/recyklomat.py
    PYTHONPATH="${ABSOLUTE_PATH}" python3.6 ./drivers/qr_code_reader.py
    PYTHONPATH="${ABSOLUTE_PATH}" python3.6 ./drivers/printer.py
    backend_state_file="$(get_config_variable STATE_CACHING_FILE)"
    rm "$backend_state_file"
fi

if [[ "${1}" == "--debug" ]] ; then
    start "-d true ""${2}"
fi

cd "${INITIAL_PWD}"
