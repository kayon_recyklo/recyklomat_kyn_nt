import logging

import tornado.web

from drivers.recyklomat import RecyklomatDriver
from consts import States, Events
from util import setup_logger
from config import Config
from time import time


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, recyklomat: RecyklomatDriver, log: logging.Logger) -> None:
        self.recyklomat = recyklomat
        self.log = log

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def options(self):
        self.set_status(204)
        self.finish()

    def compute_etag(self):
        return None

    def get(self, path):
        if path != 'status':
            self.clear()
            self.set_status(400)
            return
        prev_state = self.recyklomat.get_prevoius_state()
        state = self.recyklomat.get_recyklomat_state()

        if(prev_state['name'] in (States.Processing.value, States.SendingReport.value)
            and time() - prev_state['created_at_timestamp'] < Config.MINIMAL_TIME_OF_DISPLAYING_MESSAGE_S):
            rsp = prev_state
        else:
            rsp = state

        rsp['status'] = rsp['name']
        del rsp['name']
        self.write(rsp)

    def post(self, path):
        self.log.info("POST: %s , \nbody: %s", path, self.request.body)
        try:
            event = {
                'stop_recycling': Events.FinishCollectingGarbage,
                'no_qr': Events.ContinueWithNoQR
            }[path]
            self.recyklomat.post_event(event, tuple())

        except KeyError:
            self.clear()
            self.set_status(400)
            self.log.error(f"Bad post request: {path}")


def init_tornado_app(recyklomat, host, port, log, debug=False):
    handlers = [
        (r"/api/(.+)", MainHandler, dict(recyklomat=recyklomat,
                                         log=setup_logger("MainHandler"))),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "./web/static"}),
        (r"/(.*)", tornado.web.StaticFileHandler, {"path": 'web', "default_filename": "index.html"})
    ]

    if debug:
        from web_debug.debug_handler import DebugHandler
        # handler debugowy, ze wzgledu na kolizje z handlerem dla plikow statycznych, musi byc pierwszy na liscie
        handlers = [(
            r"/debug(/.*)?", DebugHandler, dict(recyklomat=recyklomat,
                                                log=setup_logger('DebugHandler'), host=host, port=port)
        )] + handlers

    log_str = '", "'.join(map(lambda e: e[0], handlers))
    log_str = '"' + log_str + '"'
    log.info(f"starting handlers for: {log_str}")
    app = tornado.web.Application(handlers)
    app.listen(port, address=host)
    return app