import tornado.ioloop
import tornado.web
import logging
import json
import asyncio

from time import time, sleep
from drivers.recyklomat import RecyklomatDriver, open_recyklomat_socket
from handlers import init_tornado_app
from state_machine import RecyklomatState
from consts import States, Events
from config import Config
from drivers import report_sender
from drivers.report_sender import transform_state_to_report
from util import setup_logger, exit_on_exception, call_with_interval
from drivers.qr_code_reader import QRReaderDriver
from drivers import printer


async def _send_report(config: Config, state) -> tuple:
    """
    :return: response dict, transaction tag str
    """
    log = setup_logger("_send_report")
    report = transform_state_to_report(state)
    rsp = await report_sender.send_report(config, log,report)
    log.info(f"Report sent. Response: {repr(rsp)}")
    return json.loads(rsp), report['transactionTag']


async def report_sender_loop(reports: asyncio.Queue, post_event_method, send_report) -> None:
    log = setup_logger("report_sender_loop")
    log.info('report sender loop start')
    while True:
        state_to_report = await reports.get()
        log.info('got state to report')
        try:
            rsp, transaction_tag = await send_report(state_to_report)
        except:
            log.exception("failed to send report")
        else:
            post_event_method(rsp, transaction_tag)


def get_cmd_args():
    global params
    import sys
    from util import parse_args
    log.info("starting recyclkomat backend")
    return parse_args(['-s', '-p', '-h', '-d'], sys.argv)


def setup_report_sender():
    states_to_report_queue = asyncio.Queue()

    async def send_state_report(state: RecyklomatState):
        """:return: responsedict, transaction tag"""
        log = setup_logger('async_report_sender')
        log.info('sending report')
        return await _send_report(Config, state)

    def event_sender_method(rsp, transaction_tag):
        points = int(rsp.get('points', -1))
        log.info(f'got response with points: {points}, transaction id {transaction_tag}')
        recyklomat.post_event(Events.ReportSent, (points, transaction_tag))

    return states_to_report_queue, [report_sender_loop(
        states_to_report_queue,
        event_sender_method,
        send_state_report
    )]


def setup_recyklomat(debug, socket_path, state_caching_file):
    tasks = []
    if debug:
        log.info("debug mode enabled - skipping connection to the driver")
        logging.getLogger("asyncio").setLevel(logging.DEBUG)
        recyklomat = RecyklomatDriver(
            None,
            state_class=RecyklomatState,
            maintenance_period_start=Config.MAINTENANCE_PERIOD_START,
            maintenance_period_stop=Config.MAINTENANCE_PERIOD_STOP,
            maintenance_mode_allowed_in_states=[States.Ready, ],
            _maintenance_state_name=States.Maintenance)
    else:
        log.info("connecting to the driver")
        start_time = time()
        while True:
            try:
                socket = open_recyklomat_socket(socket_path)
                break
            except:
                if time() - start_time > 60:
                    log.exception('Failed to connect to the driver.')
                    raise
                log.error("failed to connect to the driver, retrying in 5 seconds")
                sleep(5)

        recyklomat = RecyklomatDriver(socket, state_class=RecyklomatState,
                                      maintenance_period_start=Config.MAINTENANCE_PERIOD_START,
                                      maintenance_period_stop=Config.MAINTENANCE_PERIOD_STOP,
                                      maintenance_mode_allowed_in_states=[States.Ready, ],
                                      _maintenance_state_name=States.Maintenance)
        tasks.append(
            exit_on_exception(recyklomat.driver_client, task_name="recyklomat.driver_client"),
        )

    try:
        with open(state_caching_file, 'r') as f:
            recyklomat.load_cached_state(f)
        log.info("loaded stored state")
    except FileNotFoundError:
        pass

    tasks.extend([
        call_with_interval(1, recyklomat.post_event, Events.Tick, tuple()),
        exit_on_exception(recyklomat.event_loop, task_name="recyklomat.event_loop"),
    ])

    return recyklomat, tasks


def add_ioloop_tasks(tasks):
    asyncio_loop = tornado.ioloop.IOLoop.current().asyncio_loop
    for task in tasks:
        log.info(f"starting task: %s", repr(task))
        asyncio_loop.create_task(task)


def setup_qr_reader(recyklomat: RecyklomatDriver):
    log = setup_logger('QRReaderDriver')
    qr_code_reader_queue = asyncio.Queue()

    def callback(qr):
        log.info(f'got qr code: {qr}')
        recyklomat.post_event(Events.ScannedQR, (qr, ))

    driver = QRReaderDriver(Config.QR_CODE_DRIVER_CMD, callback, log)

    def waiting_for_qr_state_event_handler(*_):
        qr_code_reader_queue.put_nowait(None)

    def kill_reader_process_on_state_change(*_):
        driver.kill()

    async def qr_code_reader_loop():
        log.info('Starting event loop')
        while True:
            await qr_code_reader_queue.get()
            await driver.async_read()

    recyklomat.add_state_callback(States.WaitingForQR, waiting_for_qr_state_event_handler)
    recyklomat.add_state_callback(States.SendingReport, kill_reader_process_on_state_change)

    return driver, [qr_code_reader_loop()]


if __name__ == "__main__":
    log = setup_logger("backend")
    try:
        params = get_cmd_args()
        ioloop_tasks = []

        debug = params.get('-d', 'false').lower().strip() == 'true'

        socket_path = params.get('-s', Config.DRIVER_SOCKET_PATH)
        recyklomat, recyklomat_tasks = setup_recyklomat(debug, socket_path, Config.STATE_CACHING_FILE)

        states_to_report_queue, report_sender_tasks = setup_report_sender()
        recyklomat.add_state_callback(States.SendingReport, states_to_report_queue.put_nowait)

        recyklomat.add_state_callback(States.SummaryWithNoQr,
                                      lambda state: printer.print_receipt(Config, state))

        qr_reader, qr_reader_tasks = setup_qr_reader(recyklomat)

        ioloop_tasks.extend(qr_reader_tasks)
        ioloop_tasks.extend(recyklomat_tasks)
        ioloop_tasks.extend(report_sender_tasks)

        app = init_tornado_app(
            recyklomat,
            params.get('-h', Config.GUI_HOST),
            int(params.get('-p', Config.GUI_PORT)),
            log=log,
            debug=debug
        )

        add_ioloop_tasks(ioloop_tasks)

        asyncio.get_event_loop().run_forever()

    except Exception as e:
        log.exception("critical error")
        exit(-1)
    finally:
        log.info("closing recycklomat backend")
        log.handlers[0].flush()
