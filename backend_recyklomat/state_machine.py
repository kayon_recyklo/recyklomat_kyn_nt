from time import time
from typing import Dict
from copy import copy
from config import Config as RecyklomatConfig
from consts import States, Events, GarbageTypes
from excetions import StateNotSupportedError, EventNotSupported, EventParameterNotSupported, ReturnToPrevoiusState, NoStateChangeException


def garbage_dict_normalize(garbage: dict) -> dict:
    return {k.value: v for k, v in garbage.items()}


def is_noneish(x):
    return x is None or str(x).strip().lower() == str(None).lower()


def count_garbage(garbage: dict) -> int:
    return sum(garbage.values())


class RecyklomatState(object):
    def __init__(self, name: States = States.Ready,
                 collected_garbage: Dict[GarbageTypes, int] = None,
                 last_garbage: GarbageTypes = None,
                 created_at_timestamp: float = None,
                 user_id = None,
                 ecopoints: int = None,
                 user_interaction_timeout: int = None,
                 error_timeout: int = None,
                 transaction_tag: str = None):

        self.transaction_tag = transaction_tag
        self.created_at_timestamp = created_at_timestamp or time()
        self.name = name
        self.collected_garbage = collected_garbage or {
            GarbageTypes.Aluminium: 0,
            GarbageTypes.Glass: 0,
            GarbageTypes.Deodorant: 0,
            GarbageTypes.Syringe: 0,
            GarbageTypes.PET: 0,
            GarbageTypes.Battery: 0,
        }
        self.last_garbage = last_garbage
        self.user_id = user_id
        self.ecopoints = ecopoints
        self.user_interaction_timeout = user_interaction_timeout or {
            States.Ready: None,
            States.Working: RecyklomatConfig.USER_INTERACTION_TIMEOUT,
            States.Processing: RecyklomatConfig.PROCESSING_STATE_TIMEOUT,
            States.RemoveUnknownGarbage: RecyklomatConfig.REMOVE_UNKNOWN_GARBAGE_TIMEOUT,
            States.WaitingForQR: RecyklomatConfig.USER_INTERACTION_TIMEOUT,
            States.SendingReport: None,
            States.SummaryWithQR: RecyklomatConfig.SUMMARY_TIMEOUT,
            States.SummaryWithNoQr: RecyklomatConfig.SUMMARY_TIMEOUT,
            States.Maintenance: None
        }[self.name]

        self.error_timeout = error_timeout or None if self.name == States.Ready else RecyklomatConfig.ERROR_TIMEOUT

    def serialize(self):
        return {
            'created_at_timestamp': self.created_at_timestamp,
            'name': self.name.value,
            'collected_garbage': garbage_dict_normalize(self.collected_garbage),
            'last_garbage': self.last_garbage.value if self.last_garbage else None,
            'user_id': self.user_id,
            'ecopoints': self.ecopoints,
            'user_interaction_timeout': self.user_interaction_timeout,
            'error_timeout': self.error_timeout
        }

    @classmethod
    def deserialize(cls, serialized):
        name = States(serialized['name'])
        created_at_timestamp = float(serialized['created_at_timestamp'])
        collected_garbage = {GarbageTypes(k): int(v) for k, v in
                             serialized['collected_garbage'].items()}
        last_garbage = None if is_noneish(serialized['last_garbage']) else GarbageTypes(serialized['last_garbage'])
        user_id = None if is_noneish(serialized['user_id']) else serialized['user_id']
        ecopoints = None if is_noneish(serialized['ecopoints']) else serialized['ecopoints']
        user_interaction_timeout = None if is_noneish(serialized['user_interaction_timeout']) else serialized['user_interaction_timeout']
        error_timeout = None if is_noneish(serialized['error_timeout']) else serialized['error_timeout']
        return cls(
            name=name, created_at_timestamp=created_at_timestamp,
            collected_garbage=collected_garbage, last_garbage=last_garbage,
            user_id=user_id, ecopoints=ecopoints,
            user_interaction_timeout=user_interaction_timeout,
            error_timeout=error_timeout
        )

    def copy(self, name: States = None,
             collected_garbage: Dict[GarbageTypes, int] = None,
             last_garbage: GarbageTypes = None,
             user_id: str = None,
             ecopoints: int = None,
             transaction_tag:str =None
             ):
        args = {
            'name': name or self.name,
            'collected_garbage': collected_garbage or copy(self.collected_garbage),
            'last_garbage': last_garbage or None,
            'user_id': user_id or self.user_id,
            'ecopoints': ecopoints or self.ecopoints,
            'transaction_tag': transaction_tag or self.transaction_tag
        }
        return RecyklomatState(**args)

    def __str__(self):
        garbage = ', '.join(f"{k.value}: {v}" for k, v in self.collected_garbage.items())
        return f"<state:{self.name.name}, created: {self.created_at_timestamp}, garbage: {garbage}>"

    def handle(self, event: Events, event_params: tuple) -> 'RecyklomatState':
        try:
            return getattr(self, f"handle_{self.name.name}")(event, event_params)
        except AttributeError:
            raise StateNotSupportedError(f"state {self.name} is not implemented")

    def handle_Ready(self, event: Events, params: tuple):
        if event == Events.HatchOpened:
            return RecyklomatState(States.Working)
        
        if event == Events.HatchClosed:
            return self.copy(States.Processing)

        raise self.__EventNotSupported(event)

    def handle_Working(self, event: Events, params: tuple):
        if event == Events.HatchOpened:
            return self.copy()

        if event == Events.HatchClosed:
            return self.copy(States.Processing)

        if event == Events.InteractionTimeout:
            return self.copy(States.WaitingForQR)

        if event == Events.FinishCollectingGarbage:
            return self.copy(States.WaitingForQR)

        raise self.__EventNotSupported(event)

    def handle_Processing(self, event: Events, params: tuple):
        if event == Events.InteractionTimeout:
            return self.copy(States.Working)
        if event == Events.RecognitionFinished:
            try:
                last_garbage = GarbageTypes(params[0])
            except ValueError:
                raise EventParameterNotSupported(f'Parameter {str(params[0])} is not supported for state {self.name}')

            if last_garbage == GarbageTypes.Other:
                return self.copy(States.RemoveUnknownGarbage)
            elif last_garbage == GarbageTypes.Empty:
                if sum(self.collected_garbage.values()) == 0:
                    return RecyklomatState()
                return self.copy(States.Working)

            collected_garbage = copy(self.collected_garbage)
            collected_garbage[last_garbage] += 1
            return self.copy(States.Working, collected_garbage=collected_garbage, last_garbage=last_garbage)

        raise self.__EventNotSupported(event)

    def handle_RemoveUnknownGarbage(self, event: Events, params: tuple):
        if event in (Events.HatchOpened, Events.HatchClosed):
            return self.copy(States.Processing)

        if event == Events.InteractionTimeout:
            if count_garbage(self.collected_garbage) == 0:
                return RecyklomatState()
            return self.copy(States.Working)

        raise self.__EventNotSupported(event)

    def handle_WaitingForQR(self, event: Events, params: tuple):
        if event == Events.ScannedQR:
            user_id = params[0]

            return self.copy(States.SendingReport, user_id=user_id)

        if event == Events.ContinueWithNoQR:
            return self.copy(States.SendingReport)

        if event == Events.InteractionTimeout:
            return self.copy(States.SendingReport)

        raise self.__EventNotSupported(event)

    def handle_SendingReport(self, event: Events, params: tuple):
        if event == Events.ReportSent:
            ecopoints, transaction_tag= int(params[0]), params[1]
            if self.user_id is not None:
                return self.copy(States.SummaryWithQR, ecopoints=ecopoints, transaction_tag=transaction_tag)
            return self.copy(States.SummaryWithNoQr, ecopoints=ecopoints, transaction_tag=transaction_tag)

        raise self.__EventNotSupported(event)

    def handle_SummaryWithNoQr(self, event: Events, params: tuple):
        if event == Events.InteractionTimeout:
            return RecyklomatState()

        if event  == Events.HatchClosed:
            return RecyklomatState(name=States.Processing)

        if event == Events.HatchOpened:
            return RecyklomatState(name=States.Working)
        raise self.__EventNotSupported(event)

    def handle_SummaryWithQR(self, event: Events, params: tuple):
        if event == Events.InteractionTimeout:
            return RecyklomatState()

        if event  == Events.HatchClosed:
            return RecyklomatState(name=States.Processing)

        if event == Events.HatchOpened:
            return RecyklomatState(name=States.Working)

        raise self.__EventNotSupported(event)

    def handle_Maintenance(self, event: Events, params: tuple):
        self.created_at_timestamp = time()
        raise NoStateChangeException

    def __EventNotSupported(self, e: Events) -> EventNotSupported:
        return EventNotSupported(f'Event {e.name} not supported for state {self.name.name}')


if __name__ == "__main__":
    print('-' * 80)
    print(__file__)
    print('-' * 80)

    
    def garbage_dict(ALU=0, GLA=0, DEO=0, BAT=0, PET=0, SYR=0):
        return {
            GarbageTypes.Aluminium: ALU,
            GarbageTypes.Glass: GLA,
            GarbageTypes.Deodorant: DEO,
            GarbageTypes.Syringe: SYR,
            GarbageTypes.Battery: BAT,
               GarbageTypes.PET: PET
        }
    print("Testing state: Ready...")
    assert RecyklomatState().handle(Events.HatchOpened, (None,)).name == States.Working
    assert RecyklomatState(States.Working).handle(Events.HatchClosed, (None,)).name == States.Processing
    print('... OK')

    print("Testing state: Working...")
    state = RecyklomatState(States.Working)
    assert state.handle(Events.InteractionTimeout, (state,)).name == States.WaitingForQR

    assert state.handle(Events.HatchClosed, (None,)).name == States.Processing

    garbage = garbage_dict(ALU=1, DEO=2, GLA=3)
    new_state = RecyklomatState(States.Working, collected_garbage=garbage).handle(
        Events.FinishCollectingGarbage, (None,))
    assert new_state.name == States.WaitingForQR
    assert new_state.collected_garbage == garbage_dict(ALU=1, DEO=2, GLA=3)
    print('... OK')
    print("Testing state: Processing...")
    garbage = garbage_dict(ALU=1)
    new_state = RecyklomatState(States.Processing, collected_garbage=garbage).handle(
        Events.RecognitionFinished, (GarbageTypes.Deodorant.value,))
    assert new_state.name == States.Working
    assert new_state.last_garbage == GarbageTypes.Deodorant
    assert new_state.collected_garbage == garbage_dict(ALU=1, DEO=1)

    garbage = garbage_dict(ALU=1)
    new_state = RecyklomatState(States.Processing, collected_garbage=garbage).handle(
        Events.RecognitionFinished, (GarbageTypes.Other.value,))
    assert new_state.name == States.RemoveUnknownGarbage
    assert new_state.collected_garbage == garbage_dict(ALU=1)

    garbage = garbage_dict(ALU=1)
    new_state = RecyklomatState(States.Processing, collected_garbage=garbage).handle(
        Events.InteractionTimeout, (None,))
    assert new_state.name == States.Working
    assert new_state.collected_garbage == garbage_dict(ALU=1)

    state = RecyklomatState(States.Processing)
    state.collected_garbage[GarbageTypes.Aluminium]+=1
    new_state = state.handle(Events.RecognitionFinished, (GarbageTypes.Empty, ))
    assert new_state.name == States.Working


    state = RecyklomatState(States.Processing)
    new_state = state.handle(Events.RecognitionFinished, (GarbageTypes.Empty, ))
    assert new_state.name == States.Ready
    print('... OK')

    print('Testing state: RemoveUnknownGarbage')
    garbage = garbage_dict(1, 3, 5)

    new_state = RecyklomatState(States.RemoveUnknownGarbage, garbage).handle(Events.HatchOpened, (None,))
    assert new_state.name == States.Processing
    assert new_state.collected_garbage == garbage_dict(1, 3, 5)

    garbage = garbage_dict(1, 3, 5)
    new_state = RecyklomatState(States.RemoveUnknownGarbage, garbage).handle(Events.HatchClosed, (None,))
    assert new_state.name == States.Processing
    assert new_state.collected_garbage == garbage_dict(1, 3, 5)

    garbage = garbage_dict(1, 3, 5)
    new_state = RecyklomatState(States.RemoveUnknownGarbage, garbage).handle(Events.InteractionTimeout, (None,))
    assert new_state.name == States.Working
    assert new_state.collected_garbage == garbage_dict(1, 3, 5)

    garbage = garbage_dict()
    new_state = RecyklomatState(States.RemoveUnknownGarbage, garbage).handle(Events.InteractionTimeout, (None,))
    assert new_state.name == States.Ready
    assert new_state.collected_garbage == garbage_dict()
    print('... OK')

    print("Testing state: WaitingForQR")
    garbage = garbage_dict(16, 12, 4)
    state = RecyklomatState(States.WaitingForQR, collected_garbage=garbage)
    QR = '123456789012'
    new_state = state.handle(Events.ScannedQR, (QR,))
    assert new_state.name == States.SendingReport
    assert new_state.collected_garbage == garbage_dict(16, 12, 4)
    assert new_state.user_id == QR

    garbage = garbage_dict(1, 6, 9)
    state = RecyklomatState(States.WaitingForQR, collected_garbage=garbage)
    new_state = state.handle(Events.ContinueWithNoQR, (None,))
    assert new_state.name == States.SendingReport
    assert new_state.collected_garbage == garbage_dict(1, 6, 9)

    garbage = garbage_dict(1, 6, 99)
    state = RecyklomatState(States.WaitingForQR, collected_garbage=garbage)
    new_state = state.handle(Events.InteractionTimeout, (None,))
    assert new_state.name == States.SendingReport
    assert new_state.collected_garbage == garbage_dict(1, 6, 99)
    print('... OK')

    print('Testing state: SendingReport')
    garbage = garbage_dict(11, 6, 99)
    state = RecyklomatState(States.SendingReport, collected_garbage=garbage)
    ECOPOINTS = 123
    TRANSACTION_TAG = '012345678912'
    new_state = state.handle(Events.ReportSent, (ECOPOINTS, TRANSACTION_TAG))
    assert new_state.name == States.SummaryWithNoQr
    assert new_state.collected_garbage == garbage_dict(11, 6, 99)
    assert new_state.ecopoints == ECOPOINTS
    assert new_state.transaction_tag == TRANSACTION_TAG
    print('... OK')

    print('Testing state: SummaryWithNoQr')
    garbage = garbage_dict(11, 61, 99)
    state = RecyklomatState(States.SummaryWithNoQr, collected_garbage=garbage)
    new_state = state.handle(Events.InteractionTimeout, (None,))
    assert new_state.name == States.Ready
    assert new_state.collected_garbage == garbage_dict()
    assert new_state.ecopoints is None
    print('... OK')

    print('Testing state: SummaryWithQR')
    garbage = garbage_dict(12, 61, 99)
    state = RecyklomatState(States.SummaryWithQR, collected_garbage=garbage)
    new_state = state.handle(Events.InteractionTimeout, (None,))
    assert new_state.name == States.Ready
    assert new_state.collected_garbage == garbage_dict()
    assert new_state.ecopoints is None

    garbage = garbage_dict(12, 61, 99)
    state = RecyklomatState(States.SummaryWithQR, collected_garbage=garbage)
    new_state = state.handle(Events.HatchOpened, (None,))
    assert new_state.name == States.Working
    assert new_state.collected_garbage == garbage_dict()
    assert new_state.ecopoints is None

    garbage = garbage_dict(12, 61, 99)
    state = RecyklomatState(States.SummaryWithQR, collected_garbage=garbage)
    new_state = state.handle(Events.HatchClosed, (None,))
    assert new_state.name == States.Processing
    assert new_state.collected_garbage == garbage_dict()
    assert new_state.ecopoints is None
    print('... OK')

    print("Testing: state serialization...")
    name = States.SummaryWithQR
    created_at_timestamp = 1235.67890
    collected_garbage = garbage_dict(1, 5, 7)
    last_garbage = GarbageTypes.Deodorant
    user_id = '123456789012'
    ecopoints = 123
    user_interaction_timeout = 10

    expected = {
        'name': name.value,
        'created_at_timestamp': created_at_timestamp,
        'collected_garbage': garbage_dict_normalize(collected_garbage),
        'user_id': user_id,
        'ecopoints': ecopoints,
        'last_garbage': last_garbage.value,
        'user_interaction_timeout': user_interaction_timeout,
        'error_timeout': RecyklomatConfig.ERROR_TIMEOUT
    }
    state = RecyklomatState(
        name, collected_garbage, last_garbage, created_at_timestamp, user_id, ecopoints,
        user_interaction_timeout
    )

    print("... OK")
    print("Testing deserialization...")
    assert state.serialize() == expected

    state = RecyklomatState.deserialize(expected)
    assert state.name == States.SummaryWithQR
    assert abs(state.created_at_timestamp - created_at_timestamp) < 0.001
    assert state.collected_garbage == garbage_dict(1, 5, 7)
    assert state.user_id == user_id
    assert state.ecopoints == ecopoints
    assert state.last_garbage == last_garbage
    assert state.user_interaction_timeout == user_interaction_timeout
    assert state.error_timeout == RecyklomatConfig.ERROR_TIMEOUT
    print("... OK")

    print("Testing state copy...")
    TEST_TIMESTAMP = 999
    s1 = RecyklomatState(created_at_timestamp=TEST_TIMESTAMP)
    s2 = s1.copy()
    assert s1.created_at_timestamp == TEST_TIMESTAMP
    assert s2.created_at_timestamp != TEST_TIMESTAMP
    print("... OK")

