Recyklomat backend
==

Jest to aplikacja zawierająca w sobie całą logikę biznesową recyklomatu. 

Z jednej strony zbiera zdarzenia generowane przez sterownik, a zdrugiej eksponuje serwer plików statycznych oraz serwer API dla web UI.

## Konfiguracja i instalacja

### Zanim zainstalujesz aplikacje - konfiguracja

Instalacja wymaga, aby backend był już skonfigurowany.

Aby skonfigurować aplikację należy wyedytować odpowiednie pola w pliku `config.py`. Komentarze nad wartościami pomogą Ci poprawnie skonfiguraować aplikacje.

### Instalacja

Aby zainstalować backend należy przelogować się na konto użytkownika do którego ma należeć proces aplikacji oraz wszystkie jej pliki, wejść do folderu z backendem i wydać polecenie:

```bash
./backend_control.sh --install
```

Podczas instalacji system poprosi o podanie hasła administratora oraz potwierdzenie niektórych kroków. Instalacja, w zależności od szybkości łącza oraz zasobów urządzenia powinna potrwać od minuty do minut kilkunastu albo nawet kilkudziesięciu w przypadku rasbiana.

I tyle. Skrypt zainstaluje wszystkie niezbędne zależności, skonfiguruje crontaba oraz zainstaluje backend jako uslugę systemową startującą przy każdym uruchomieniu systemu.

Zarządzanie usługą backendu:

```bash
sudo systemctl start recyklomat.service
sudo systemctl stop recyklomat.service
```

## Skrypt kontrolujacy backend - przydatne do debugowania

Wszystkie operacje zarządzające backendem powinienś wykonywać za pośrednictwem skryptu `backend_control.sh`. 

Skrypt musi mieć uprawnienia do uruchomienia: `chmod +x ./backend_control.sh`

Aby sprawdzić pomoc do skryptu odpal polecenie:

```bash
Witaj w skrypcie do kontroli recyklomatowego backendu.
    Użycie: ./backend_control.sh <opcja>
Uwaga! skrypt powinien być uruchamiany z poziomu docelowego uzytkownika!
Dostepne opcje:
    --help             wyświetl ten komunikat
    --start            uruchom backend
    --kill             zabij aktualnie uruchomiony backend
    --status           sprawdz czy backend jest uruchomiony
    --install          zainstaluj wszystkie potrzebne zaleznosci (tylko ubuntu/debian)
    --test             odpal skrócone testy integracyjne aby sprawdzić czy środowisko jest dobrze zestawione
    --debug            uruchom backend bez podlaczania sie do sterownika z uruchomiona konsola debugowa dostepna pod adresem http://<url>:<port>/de
```

## Zanim uruchomisz - testy

Aby mieć pewność, że aplikacja działa na Twojej platformie, uruchom testy:

```bash
cd recyklomat_backend
./backend_control.sh --test
```

Skrypt powinien pokazywać wszędzie statusy ".. OK", w razie błędu zatrzyma się przy pierwszym problemie i wyświetli komunikat błędu.

## Testowanie frontendu oraz backendu bez skryptów i sterowników zewnętrznych

Aby uruchomić backend w trybie debugowym należy wydać polecenie `./backend_control.sh --debug`

Teraz powinienes być w stanie wyświetlić web UI w przeglądarce pod adresem `http://127.0.0.1:8888/`, odpytywać API pod adresem `http://127.0.0.1:8888/api/<endpoint>` oraz mieć dostęp do konsoli debugowej dostepnej pod adresem `http://127.0.0.1:8888/debug`.


## Endpointy plików statycznych

1. `/` (root) serwuje plik z folderu `web/`, jeśli ścieżka jest pusta, domyślnie serwuje plik `web/index.html`
2. `/static/` serwuje pliki statyczne z foldery `web/static`

## Endpointy API

1. `GET /api/status` zwraca aktualny status GUI recyklomatu. Format odpowiedzi:
```json
{
  'status': "ready/working/processing/waiting_for_qr/remove_unknown_garbage/sending_report_summary_without_qr/summary_with_qr",
  'collected_garbage': {
    'ALU': <int>,
    'GLA': <int>,
    'DEO': <int>,
    'PET': <int>,
    'SYR': <int>,
  }, 
  'last_garbage': null/"ALU"/"GLA"/"DEO"/"PET"/"SYR"/"BAT",
  'user_id': <identyfikator użytkownika> albo null,
  'ecopoints': <liczba ekopunktów> albo null,
  'created_at_timestamp': <float moment przejścia do tego stanu>,
  'user_interaction_timeout': <czas po którym backend przejdzie do następnego ekranu> albo null,
  'error_timeout': <czas po którym backend uzna iż doszło do zwiechy> albo null
}
``` 

Pola `reated_at_timestamp`, `user_interaction_timeout`, `error_timeout` są polami użyecznymi do debgowania.
Pole `user_id` będzie zawierało wartość inną niż `null` tylko gdy `"status" == "summary_with_qr"`

2. `POST /api/stop_recycling`, puste ciało - gdy stan jest "" 
3. `POST /api/no_qr`, puste ciało. Kontynuuj bez skanowania QR kodu

## Przejścia między stanami

format: <stan początkowy> --- [zdarzenie] ---> <stan wyjściowy>
```
1.0. ready --- [hatch_open] ---> working
1.1. ready --- [hatch_closed] ---> processing
2.0. working --- [hatch_close] ---> processing
2.1. working --- [POST /api/stop_recycling] ---> waiting_for_qr
2.2. working --- [USER_INTERACTION_TIMEOUT] ---> waiting_for_qr
3.0. processing --- [ERROR_TIMEOUT] ---> ready
3.1. processing --- [recognized OTH] ---> remove_unknown_garbage
3.2. processing --- [recognized ALU/GLA/SYR/DEO/PET] ---> working
3.3. processing --- [INTERACTION TIMEOUT] ---> WaitingForQr
4.0. remove_unknown_garbage --- [hatch_open] ---> Processing
4.1. remove_unknown_garbage --- [hatch_closed] ---> Processing
4.2. remove_unknown_garbage --- [ERROR_TIMEOUT] ---> ready
5.0. waiting_for_qr --- [qr_code XXXXXXXXXXXX] ---> sending_report
5.1. waiting_for_qr --- [POST /api/no_qr] ---> summary_without_qr
5.2. waiting_for_qr --- [USER_INTERACTION_TIMEOUT] ---> summary_without_qr
6.0. sending_report --- [ReportSent] ---> summary_with_qr
7.0. summary_without_qr --- [SUMMARY_TIMEOUT] ---> ready
8.0. summary_with_qr --- [SUMMARY_TIMEOUT] ---> ready
```
## Obsługiwane komunikaty od sterownika

Każdy komunikat od sterownika składa się z nazwy komunikatu bez spacji, opcjonalnego argumentu po spacji oraz obowiązkowego znaku konca linii `\n`.

1. `hatch_open\n`
2. `hatch_close\n`
3. `recognized ALU/GLA/DEO/PET/SYR/BAT/OTH\n`
4. `qr_code XXXXXXXXXXXX\n`



